package com.ccteam.network

import com.ccteam.network.calladapter.ApiResultCallAdapterFactory
import com.ccteam.network.interceptor.BusinessErrorInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import java.net.Proxy
import java.util.concurrent.TimeUnit

/**
 * @author Xiaoc
 * @since 2021/1/30
 *
 * 网络访问请求的类型
 **/
private const val BASE_URL = "https://music.ciangciang.top/"

private val okHttpClient = OkHttpClient.Builder()
    .addInterceptor(BusinessErrorInterceptor())
    .callTimeout(10,TimeUnit.SECONDS)
    .readTimeout(10,TimeUnit.SECONDS)
    .connectTimeout(10,TimeUnit.SECONDS)
    .proxy(Proxy.NO_PROXY)
    .build()

val retrofit: Retrofit = Retrofit.Builder()
    .addConverterFactory(JacksonConverterFactory.create(jacksonObjectMapper()))
    .addCallAdapterFactory(ApiResultCallAdapterFactory())
    .baseUrl(BASE_URL)
    .client(okHttpClient)
    .build()