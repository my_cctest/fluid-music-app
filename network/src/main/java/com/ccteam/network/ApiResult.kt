package com.ccteam.network

/**
 * @author Xiaoc
 * @since 2021/1/30
 *
 * 响应服务端数据返回的包装类，它主要用于与服务端进行关联
 **/
sealed class ApiResult<out T>{

    data class Success<T>(val data: T): ApiResult<T>()
    data class Failure constructor(
        val errorCode: Int,
        val errorMsg: String
    ): ApiResult<Nothing>(){
        constructor(error: Error): this(error.errorCode,error.errorMsg)
    }
    class Empty<T>: ApiResult<T>()
}