package com.ccteam.network

/**
 * @author Xiaoc
 * @since 2021/1/30
 *
 * 从服务端返回信息包装总类
 **/
data class ResultVO<T>(
    val code: Int,
    val message: String?,
    val data: T?,
    val success: Boolean
)