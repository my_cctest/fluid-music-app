package com.ccteam.network.exception

import com.ccteam.network.Error
import java.io.IOException

/**
 * @author Xiaoc
 * @since 2021/1/30
 *
 * 自定义异常类，支持从 [Error] 中进行调用
 **/
class ApiException constructor(val errorCode: Int,val errorMsg: String): IOException(errorMsg) {

    constructor(error: Error) : this(error.errorCode,error.errorMsg)
}