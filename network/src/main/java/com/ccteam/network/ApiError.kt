package com.ccteam.network

/**
 * @author Xiaoc
 * @since 2021/1/31
 *
 * 客户端本地定义的网络请求相关错误
 **/
object ApiError {
    const val nothingCode = 0x0002
    const val dataIsNullCode = 0x000F
    const val unknownCode = 0x0010
    const val dataTypeCode = 0x0011
    const val httpStatusCode = 0x0012
    const val serverErrorCode = 0x0015
    const val timeoutCode = 0x0016
    const val connectionErrorCode = 0x0017

    val dataIsNull = Error(dataIsNullCode,"返回的数据为null")
    val unknownError = Error(unknownCode,"网络请求失败，请重试")
    val connectionError = Error(connectionErrorCode,"网络请求失败，请检查网络")
    val timeoutError = Error(timeoutCode,"请求超时，请重试")
    val dataTypeError = Error(dataTypeCode,"数据返回类型不正确，不是ResultVO类型")
    val httpStatusError = Error(httpStatusCode,"与服务器连接异常")
}

/**
 * 错误数据类，存储着错误码和具体错误信息
 */
data class Error(val errorCode:Int,val errorMsg:String)