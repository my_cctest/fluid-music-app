package com.ccteam.shared.utils

import android.content.Context
import cn.hutool.core.date.DateUtil
import com.ccteam.shared.R
import com.orhanobut.logger.Logger

/**
 * @author Xiaoc
 * @since 2021/3/20
 *
 * 一些String的扩展方法
 */
/**
 * 歌手分割标识符
 */
const val ARTIST_SEPARATOR = "/"

/**
 * 空字符
 */
const val EMPTY_STRING = ""

/**
 * 专辑图片质量
 */
const val ALBUM_QUALITY = "@album"


/**
 * 朋友圈图片质量
 */
const val MOMENT_QUALITY = "@moment"

/**
 * 其他图片质量
 */
const val PHOTO_QUALITY = "@photo"

/**
 * @author Xiaoc
 * @since 2021/4/5
 *
 * 将形如 yyyy-MM-dd HH:mm:ss 类型的日期
 * 转换为 "刚刚" "几分钟前" "几天前" 等格式
 */
fun String.transToDateReciprocal(context: Context): String{
    try {
        val startTime = DateUtil.parse(this,"yyyy-MM-dd HH:mm:ss").time
        val nowTime = System.currentTimeMillis()

        val timeDiffer = nowTime - startTime
        Logger.d("日期：$startTime - $nowTime - $timeDiffer")
        if(timeDiffer <= 0){
            return context.getString(R.string.description_time_second)
        }
        val second = timeDiffer / 1000
        // 如果是60s内
        if(second < 60){
            return context.getString(R.string.description_time_second)
        } else {
            val minute = second / 60
            // 如果是60min内
            if(minute < 60L){
                return context.getString(R.string.description_time_minute,minute)
            } else {
                val hour = minute / 60
                // 如果是24h内
                if(hour < 24){
                    return context.getString(R.string.description_time_hour,hour)
                } else {
                    val day = hour / 24
                    // 如果是30day内
                    return if(day < 30){
                        context.getString(R.string.description_time_day,day)
                    } else {
                        val month = day / 30
                        // 如果是12month内
                        if(month < 12){
                            context.getString(R.string.description_time_month,month)
                        } else {
                            val year = month / 12
                            context.getString(R.string.description_time_year,year)
                        }
                    }
                }
            }
        }
    } catch (e: RuntimeException){
        return context.getString(R.string.description_time_second)
    }

}