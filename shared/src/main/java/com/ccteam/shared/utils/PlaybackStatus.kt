package com.ccteam.shared.utils

import android.support.v4.media.MediaMetadataCompat
import androidx.annotation.IntDef
import com.ccteam.fluidmusic.fluidmusic.media.extensions.id

/**
 * @author Xiaoc
 * @since 2021/5/3
 */
@IntDef(NO_PLAYING, PLAYING, PLAYING_PAUSE)
@Retention
annotation class PlaybackStatus

const val NO_PLAYING = -1
const val PLAYING = 0
const val PLAYING_PAUSE = 1

/**
 * 获得当前正在播放与当前遍历的mediaId是否相同
 * 如果相同则表示该项正在播放
 * 会返回几种值
 * [NO_PLAYING] 表示该项处于普通状态
 * [PLAYING] 表示该项处于正在播放状态
 */
@PlaybackStatus
fun MediaMetadataCompat.getPlaybackStatus(mediaId: String): Int{
    val isActive = mediaId == id
    return when {
        !isActive -> NO_PLAYING
        else ->
            PLAYING
    }
}