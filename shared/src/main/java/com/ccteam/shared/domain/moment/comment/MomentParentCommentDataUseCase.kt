package com.ccteam.shared.domain.moment.comment

import android.content.Context
import com.ccteam.model.Resource
import com.ccteam.model.moment.comment.CommentVO
import com.ccteam.network.ApiError
import com.ccteam.network.ApiResult
import com.ccteam.shared.data.moment.MomentsRepository
import com.ccteam.shared.domain.SuspendUseCase
import com.ccteam.shared.result.moment.comment.MomentCommentItem
import com.ccteam.shared.utils.EMPTY_STRING
import com.ccteam.shared.utils.PHOTO_QUALITY
import com.ccteam.shared.utils.transToDateReciprocal
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/4/1
 *
 * 朋友圈评论简易信息Domain层
 */
class MomentParentCommentDataUseCase @Inject constructor(
    @ApplicationContext private val context: Context,
    private val momentsRepository: MomentsRepository
): SuspendUseCase<String?, List<MomentCommentItem>>(
    Dispatchers.IO
) {
    override suspend fun execute(parameters: String?): Resource<List<MomentCommentItem>> {
        if(parameters.isNullOrEmpty()){
            return Resource.Error(ApiError.unknownCode,"parentCommentId为空")
        }
        return when(val result = momentsRepository.getParentMomentInfo(parameters)){
            is ApiResult.Success ->{
                Resource.Success(transformToMomentCommentItem(result.data.data))
            }
            is ApiResult.Empty ->{
                Resource.Error(ApiError.unknownCode,"未知错误")
            }
            is ApiResult.Failure ->{
                Resource.Error(result.errorCode,result.errorMsg)
            }
        }
    }

    private fun transformToMomentCommentItem(commentInfoVO: CommentVO?): List<MomentCommentItem>{
        if(commentInfoVO == null){
            return emptyList()
        }
        return listOf(MomentCommentItem(
            commentInfoVO.id,commentInfoVO.momentId,commentInfoVO.content,commentInfoVO.user.id,
            commentInfoVO.user.name,
            commentInfoVO.createTime.transToDateReciprocal(context),
            commentInfoVO.user.photoUrl + PHOTO_QUALITY,
            commentInfoVO.replyNum ?: 0,commentInfoVO.type,
            emptyList()
        ))
    }
}