package com.ccteam.shared.domain.playlistsheet

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.ccteam.shared.api.PlaylistSheetDataSource
import com.ccteam.shared.data.playlistsheet.PlaylistSheetListPagingSource
import com.ccteam.shared.domain.PagingUseCase
import com.ccteam.shared.result.playlistsheet.PlaylistSheetListItem
import com.ccteam.shared.utils.PHOTO_QUALITY
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/15
 *
 * 歌单列表Domain层（公共Domain）
 * 传入[Nothing]
 * 返回[PlaylistSheetListItem] 歌单信息
 */
class PlaylistSheetListDataUseCase @Inject constructor(
    private val playlistSheetDataSource: PlaylistSheetDataSource,
): PagingUseCase<Nothing?, PlaylistSheetListItem>(
    Dispatchers.IO
) {

    override fun execute(parameters: Nothing?): Flow<PagingData<PlaylistSheetListItem>> {
        return Pager(
            PagingConfig(pageSize = 15,initialLoadSize = 15)
        ){
            PlaylistSheetListPagingSource(playlistSheetDataSource)
        }.flow.map { pagingData ->
            // 将分页数据转换成 PlaylistSheetListItem
            pagingData.map {
                PlaylistSheetListItem(
                    it.id,it.playlistName,it.imgUrl + PHOTO_QUALITY
                )
            }
        }
    }
}
