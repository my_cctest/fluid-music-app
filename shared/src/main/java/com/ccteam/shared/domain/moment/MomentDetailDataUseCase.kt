package com.ccteam.shared.domain.moment

import android.content.Context
import com.ccteam.model.Resource
import com.ccteam.model.moment.MOMENT_SHARE_TYPE_NONE
import com.ccteam.model.moment.MomentVO
import com.ccteam.network.ApiError
import com.ccteam.shared.data.moment.MomentsRepository
import com.ccteam.shared.domain.FlowUseCase
import com.ccteam.shared.result.moment.MomentListItem
import com.ccteam.shared.utils.EMPTY_STRING
import com.ccteam.shared.utils.MOMENT_QUALITY
import com.ccteam.shared.utils.PHOTO_QUALITY
import com.ccteam.shared.utils.transToDateReciprocal
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/25
 *
 * 朋友圈详情Domain层
 * 传入[String] 朋友圈ID
 * 返回 [MomentListItem] 朋友圈详情内容
 */
class MomentDetailDataUseCase @Inject constructor(
    @ApplicationContext private val context: Context,
    private val momentsRepository: MomentsRepository
): FlowUseCase<String?, List<MomentListItem>>(
    Dispatchers.IO
) {

    override fun execute(parameters: String?): Flow<Resource<List<MomentListItem>>> {
        if(parameters.isNullOrEmpty()){
            return flowOf(Resource.Error(ApiError.unknownCode,"朋友圈ID为空"))
        }

        return momentsRepository.getMomentInfo(parameters).map {
            when(it){
                is Resource.Success ->{
                    if(it.data?.data == null){
                        Resource.Error(ApiError.unknownCode,"未知错误")
                    } else {
                        Resource.Success(transformToMomentListItem(it.data!!.data!!))
                    }
                }
                is Resource.Loading ->{
                    Resource.Loading(null)
                }
                is Resource.Error ->{
                    Resource.Error(it.errorCode,it.message ?: "未知错误")
                }
            }
        }
    }

    private fun transformToMomentListItem(momentInfoVO: MomentVO): List<MomentListItem>{
        val shareItem = if(momentInfoVO.type == MOMENT_SHARE_TYPE_NONE || momentInfoVO.momentShareVO == null || momentInfoVO.shareId == null){
            null
        } else {
            MomentListItem.MomentShareItem(momentInfoVO.shareId!!,momentInfoVO.type,momentInfoVO.momentShareVO!!.title ?: EMPTY_STRING,
                momentInfoVO.momentShareVO!!.subTitle ?: EMPTY_STRING,momentInfoVO.momentShareVO!!.imgUrl ?: EMPTY_STRING)
        }
        return listOf(
            MomentListItem(momentInfoVO.id,momentInfoVO.userMomentVO.id,momentInfoVO.userMomentVO.name,
                momentInfoVO.userMomentVO.photoUrl + PHOTO_QUALITY,
                momentInfoVO.createTime.transToDateReciprocal(context),
                momentInfoVO.content,momentInfoVO.images.map {
                    MomentListItem.MomentImageItem(it.id,it.momentId,it.url + MOMENT_QUALITY)
                },shareItem,momentInfoVO.upNum,momentInfoVO.commentNum)
        )
    }

}