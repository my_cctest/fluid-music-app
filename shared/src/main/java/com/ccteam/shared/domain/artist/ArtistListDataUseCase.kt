package com.ccteam.shared.domain.artist

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.ccteam.fluidmusic.fluidmusic.common.utils.MediaIDHelper
import com.ccteam.shared.api.ArtistDataSource
import com.ccteam.shared.data.artist.ArtistListPagingSource
import com.ccteam.shared.domain.PagingUseCase
import com.ccteam.shared.result.artist.ArtistListItem
import com.ccteam.shared.utils.PHOTO_QUALITY
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/2/15
 *
 * 歌手列表Domain层
 * 负责将仓库层 [ArtistRepository] 的内容转换为分页库类型 [PagingData<ArtistListItem>] 类型
 */
class ArtistListDataUseCase @Inject constructor(
    private val mediaIDHelper: MediaIDHelper,
    private val artistDataSource: ArtistDataSource
): PagingUseCase<Any, ArtistListItem>(Dispatchers.IO) {

    override fun execute(parameters: Any): Flow<PagingData<ArtistListItem>> {
        return Pager(
            PagingConfig(pageSize = 15,initialLoadSize = 15)
        ){
            ArtistListPagingSource(artistDataSource)
        }.flow.map { pagingData ->
            // 将分页数据转换成 ArtistListItem
            // 其中 id 为 __FM_ONLINE_ARTIST__/artistId
            pagingData.map {
                ArtistListItem(
                    mediaIDHelper.createMediaId(
                        MediaIDHelper.ONLINE_MEDIA_ARTIST, it.id),
                    it.name,it.imgUrl + PHOTO_QUALITY,null,it.id)
            }
        }
    }
}