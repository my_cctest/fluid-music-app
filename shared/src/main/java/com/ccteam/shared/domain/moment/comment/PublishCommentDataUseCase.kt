package com.ccteam.shared.domain.moment.comment

import com.ccteam.model.Resource
import com.ccteam.model.moment.comment.CommentNoIdDTO
import com.ccteam.network.ApiError
import com.ccteam.network.ApiResult
import com.ccteam.shared.core.UserCore
import com.ccteam.shared.data.moment.MomentsRepository
import com.ccteam.shared.domain.SuspendUseCase
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/31
 *
 * 发布评论Domain层
 */
class PublishCommentDataUseCase @Inject constructor(
    private val userCore: UserCore,
    private val momentsRepository: MomentsRepository
): SuspendUseCase<PublishCommentDataUseCase.PublishCommentRequest, Nothing?>(
    Dispatchers.IO
) {

    override suspend fun execute(parameters: PublishCommentRequest): Resource<Nothing?> {
        val userInfo = userCore.userInfo.value
        val isLogin = userCore.isLogin.value

        if(!isLogin || userInfo.userToken.isNullOrEmpty() || userInfo.userId.isNullOrEmpty()){
            return Resource.Error(ApiError.unknownCode,"当前未登录")
        }

        return when(val result = momentsRepository.createCommentInfo(
            userInfo.userToken, CommentNoIdDTO(parameters.commentId,
                parameters.commentContent,parameters.momentId,
                parameters.toUserId,parameters.type,userInfo.userId)
        )){
            is ApiResult.Success -> {
                Resource.Success(null)
            }
            is ApiResult.Failure -> {
                Resource.Error(result.errorCode,result.errorMsg)
            }
            else -> {
                Resource.Error(ApiError.serverErrorCode,"未知错误")
            }
        }
    }

    data class PublishCommentRequest(
        val momentId: String,
        val type: Int,
        val toUserId: String?,
        val commentId: String?,
        val commentContent: String
    )
}