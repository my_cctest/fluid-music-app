package com.ccteam.shared.domain.moment.up

import com.ccteam.model.Resource
import com.ccteam.network.ApiError
import com.ccteam.network.ApiResult
import com.ccteam.shared.core.UserCore
import com.ccteam.shared.data.moment.MomentsRepository
import com.ccteam.shared.domain.SuspendUseCase
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/4/1
 *
 * 检查朋友圈是否点赞Domain
 */
class CheckMomentUpDataUseCase @Inject constructor(
    private val momentsRepository: MomentsRepository,
    private val userCore: UserCore
): SuspendUseCase<String?, Boolean>(
    Dispatchers.IO
) {

    override suspend fun execute(parameters: String?): Resource<Boolean> {
        val userInfo = userCore.userInfo.value
        val isLogin = userCore.isLogin.value

        if(!isLogin || userInfo.userToken.isNullOrEmpty()
            || userInfo.userId.isNullOrEmpty() || parameters.isNullOrEmpty()){
            return Resource.Error(ApiError.unknownCode,"非法操作")
        }

        return when(val result = momentsRepository.checkCommentUp(userInfo.userId,parameters)){
            is ApiResult.Success -> {
                Resource.Success(result.data.data ?: false)
            }
            is ApiResult.Failure -> {
                Resource.Error(result.errorCode,result.errorMsg)
            }
            else -> {
                Resource.Error(ApiError.serverErrorCode,"未知错误")
            }
        }
    }
}