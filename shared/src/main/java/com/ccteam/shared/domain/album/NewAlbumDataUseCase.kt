package com.ccteam.shared.domain.album

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.ccteam.fluidmusic.fluidmusic.common.utils.MediaIDHelper
import com.ccteam.shared.api.OnlineMusicHomeDataSource
import com.ccteam.shared.data.album.NewAlbumPagingSource
import com.ccteam.shared.domain.PagingUseCase
import com.ccteam.shared.result.album.AlbumListItem
import com.ccteam.shared.utils.ALBUM_QUALITY
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/15
 *
 * 新歌推荐Domain层
 * 传入[Nothing]
 * 传出[AlbumListItem] 专辑列表内容
 */
class NewAlbumDataUseCase @Inject constructor(
    private val mediaIDHelper: MediaIDHelper,
    private val onlineMusicHomeDataSource: OnlineMusicHomeDataSource
): PagingUseCase<Nothing?, AlbumListItem>(
    Dispatchers.IO
) {
    override fun execute(parameters: Nothing?): Flow<PagingData<AlbumListItem>> {
        return Pager(
            PagingConfig(pageSize = 15,initialLoadSize = 15)
        ){
            NewAlbumPagingSource(onlineMusicHomeDataSource)
        }.flow.map { pagingData ->
            // 将数据转换为 AlbumListItem，它的MediaId为 __FM_ONLINE_ALBUM__/albumId
            pagingData.map {
                AlbumListItem(mediaIDHelper.createMediaId(MediaIDHelper.ONLINE_MEDIA_ALBUM,it.id),
                    it.name,it.imgUrl + ALBUM_QUALITY,it.artistName,it.artistName,it.id)
            }
        }
    }
}