package com.ccteam.shared.domain.moment

import com.ccteam.model.Resource
import com.ccteam.network.ApiError
import com.ccteam.network.ApiResult
import com.ccteam.shared.core.UserCore
import com.ccteam.shared.data.moment.MomentsRepository
import com.ccteam.shared.domain.SuspendUseCase
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/5/8
 *
 * 删除朋友圈Domain层
 * 传入[String] 朋友圈ID
 * 返回[Nothing]
 */
class DeleteMomentDataUseCase @Inject constructor(
    private val userCore: UserCore,
    private val momentsRepository: MomentsRepository
): SuspendUseCase<String,Nothing?>(
    Dispatchers.IO
) {

    override suspend fun execute(parameters: String): Resource<Nothing?> {
        val userInfo = userCore.userInfo.value
        val isLogin = userCore.isLogin.value

        if(!isLogin || userInfo.userToken.isNullOrEmpty() || userInfo.userId.isNullOrEmpty()){
            return Resource.Error(ApiError.unknownCode,"当前未登录，非法操作")
        }

        return when(val result = momentsRepository.deleteMoment(userInfo.userToken,parameters)){
            is ApiResult.Success -> {
                Resource.Success(null)
            }
            is ApiResult.Failure -> {
                Resource.Error(result.errorCode,result.errorMsg)
            }
            else -> {
                Resource.Error(ApiError.serverErrorCode,"未知错误")
            }
        }

    }
}