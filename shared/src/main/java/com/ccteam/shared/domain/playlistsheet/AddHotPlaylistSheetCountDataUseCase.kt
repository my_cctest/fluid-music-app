package com.ccteam.shared.domain.playlistsheet

import com.ccteam.model.Resource
import com.ccteam.shared.data.home.OnlineMusicHomeRepository
import com.ccteam.shared.domain.SuspendUseCase
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/5/5
 */
class AddHotPlaylistSheetCountDataUseCase @Inject constructor(
    private val onlineMusicHomeRepository: OnlineMusicHomeRepository
): SuspendUseCase<String, Nothing?>(
    Dispatchers.IO
) {
    override suspend fun execute(parameters: String): Resource<Nothing?> {
        onlineMusicHomeRepository.addHotPlaylistSheetCount(parameters)
        return Resource.Success(null)
    }
}