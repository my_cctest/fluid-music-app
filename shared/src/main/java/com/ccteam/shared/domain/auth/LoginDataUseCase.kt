package com.ccteam.shared.domain.auth

import com.ccteam.model.Resource
import com.ccteam.model.auth.AuthVO
import com.ccteam.model.user.UserLoginDTO
import com.ccteam.network.ApiError
import com.ccteam.network.ApiResult
import com.ccteam.shared.data.auth.AuthRepository
import com.ccteam.shared.domain.SuspendUseCase
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/18
 *
 * 用户登录Domain层
 * 传入[UserLoginDTO]
 * 返回[AuthVO]
 *
 * 登录传入登录相关信息，返回登录后需要保存的token、id等内容
 */
class LoginDataUseCase @Inject constructor(
    private val authRepository: AuthRepository
): SuspendUseCase<UserLoginDTO, AuthVO>(
    Dispatchers.IO
) {
    override suspend fun execute(parameters: UserLoginDTO): Resource<AuthVO> {
        val result = authRepository.loginUser(
            parameters
        )
        return when (result) {
            is ApiResult.Success -> {
                return if(result.data.data != null){
                    Resource.Success(result.data.data!!)
                } else {
                    Resource.Error(ApiError.serverErrorCode,"登录失败，请重试")
                }
            }
            is ApiResult.Failure -> {
                Resource.Error(ApiError.serverErrorCode,result.errorMsg)
            }
            else -> {
                Resource.Error(ApiError.serverErrorCode,"未知错误")
            }
        }
    }
}