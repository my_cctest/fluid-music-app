package com.ccteam.shared.domain.artist

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.ccteam.fluidmusic.fluidmusic.common.utils.MediaIDHelper
import com.ccteam.shared.api.AlbumDataSource
import com.ccteam.shared.data.artist.ArtistDetailAlbumPagingSource
import com.ccteam.shared.domain.PagingUseCase
import com.ccteam.shared.result.album.AlbumListItem
import com.ccteam.shared.utils.ALBUM_QUALITY
import com.orhanobut.logger.Logger
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/2/20
 *
 * 歌手详情专辑页Domain层
 */
class ArtistDetailAlbumUseCase @Inject constructor(
    private val mediaIDHelper: MediaIDHelper,
    private val albumDataSource: AlbumDataSource
): PagingUseCase<String?, AlbumListItem>(
    Dispatchers.IO
) {
    override fun execute(parameters: String?): Flow<PagingData<AlbumListItem>> {
        if(parameters.isNullOrEmpty()){
            Logger.d("请求参数为空：$parameters")
            return flowOf(PagingData.empty())
        }
        return Pager(
            PagingConfig(pageSize = 30)
        ){
            ArtistDetailAlbumPagingSource(parameters,albumDataSource)
        }.flow.map { pagingData ->
            // 将数据转换为 AlbumListItem，它的MediaId为 __FM_ONLINE_ALBUM__/albumId
            pagingData.map {
                AlbumListItem(
                    mediaIDHelper.createMediaId(MediaIDHelper.ONLINE_MEDIA_ALBUM,it.id),
                    it.name,it.imgUrl + ALBUM_QUALITY,it.artistName,it.artistName,it.id)
            }
        }
    }
}