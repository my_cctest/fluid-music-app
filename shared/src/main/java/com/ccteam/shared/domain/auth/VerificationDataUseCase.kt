package com.ccteam.shared.domain.auth

import com.ccteam.model.Resource
import com.ccteam.network.ApiError
import com.ccteam.network.ApiResult
import com.ccteam.shared.data.auth.AuthRepository
import com.ccteam.shared.domain.SuspendUseCase
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/29
 *
 * 获取验证码Domain层
 * 传入[Pair<String,String>] phone 手机号，type 验证码类型
 * 返回[Nothing]
 */
class VerificationDataUseCase @Inject constructor(
    private val authRepository: AuthRepository
): SuspendUseCase<Pair<String?, String>, Nothing?>(
    Dispatchers.IO
) {

    companion object{
        /**
         * 当前验证码类型为注册时使用
         */
        const val REGISTER_CODE = "REGISTER_CODE"
        /**
         * 当前验证码类型为登录时使用
         */
        const val LOGIN_CODE = "LOGIN_CODE"
        /**
         * 当前验证码类型为忘记密码时使用
         */
        const val FORGET_PASSWORD_CODE = "FORGET_PASSWORD_CODE"
        /**
         * 当前验证码类型为更改手机号时使用
         */
        const val CHANGE_PHONE_CODE = "CHANGE_PHONE_CODE"
        /**
         * 当前验证码类型为更改密码时使用
         */
        const val CHANGE_PASSWORD_CODE = "CHANGE_PASSWORD_CODE"
        /**
         * 当前验证码类型为其他用途时使用
         */
        const val OTHER_CODE = "OTHER_CODE"
    }

    override suspend fun execute(parameters: Pair<String?,String>): Resource<Nothing?> {

//        return Resource.Success(null)
        val (phone,type) = parameters
        if(phone.isNullOrEmpty()){
            return Resource.Error(ApiError.serverErrorCode,"非法操作，电话号码为空")
        }

        return when(val result = authRepository.getVerification(phone,type)){
            is ApiResult.Success -> {
                Resource.Success(null)
            }
            is ApiResult.Failure -> {
                Resource.Error(ApiError.serverErrorCode,result.errorMsg)
            }
            else -> {
                Resource.Error(ApiError.serverErrorCode,"未知错误")
            }
        }


    }
}