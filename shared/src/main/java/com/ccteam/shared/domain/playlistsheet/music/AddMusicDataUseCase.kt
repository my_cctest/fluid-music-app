package com.ccteam.shared.domain.playlistsheet.music

import com.ccteam.model.Resource
import com.ccteam.model.playlistsheet.PlaylistSheetMusicDTO
import com.ccteam.network.ApiError
import com.ccteam.network.ApiResult
import com.ccteam.shared.core.UserCore
import com.ccteam.shared.data.playlistsheet.PlaylistSheetRepository
import com.ccteam.shared.domain.SuspendUseCase
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/18
 *
 * 添加歌曲到歌曲列表的业务层
 */
class AddMusicDataUseCase @Inject constructor(
    private val userCore: UserCore,
    private val playlistSheetRepository: PlaylistSheetRepository
): SuspendUseCase<Pair<String, String>, Nothing?>(
    Dispatchers.IO
) {

    override suspend fun execute(parameters: Pair<String, String>): Resource<Nothing?> {
        val (playlistId,musicId) = parameters
        val result = playlistSheetRepository.addMusicToPlaylist(
            userCore.userInfo.value.userToken, PlaylistSheetMusicDTO(
                playlistId,musicId,userCore.userInfo.value.userId
            )
        )
        return if(result is ApiResult.Success){
            Resource.Success(null)
        } else {
            Resource.Error(ApiError.serverErrorCode,ApiError.httpStatusError.errorMsg)
        }
    }
}