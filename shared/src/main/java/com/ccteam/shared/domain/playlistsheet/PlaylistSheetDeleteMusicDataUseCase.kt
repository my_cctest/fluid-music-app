package com.ccteam.shared.domain.playlistsheet

import com.ccteam.model.Resource
import com.ccteam.network.ApiError
import com.ccteam.network.ApiResult
import com.ccteam.shared.core.UserCore
import com.ccteam.shared.data.playlistsheet.PlaylistSheetRepository
import com.ccteam.shared.domain.SuspendUseCase
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/23
 *
 * 歌单歌曲删除Domain层
 * 传入[Pair<String,String>] 歌单ID和歌曲ID
 * 返回[Nothing]
 */
class PlaylistSheetDeleteMusicDataUseCase @Inject constructor(
    private val userCore: UserCore,
    private val playlistSheetRepository: PlaylistSheetRepository
): SuspendUseCase<Pair<String, String>, Nothing?>(
    Dispatchers.IO
) {
    override suspend fun execute(parameters: Pair<String, String>): Resource<Nothing?> {
        val (playlistId,musicId) = parameters
        val isLogin = userCore.isLogin.value
        val userInfo = userCore.userInfo.value
        if(!isLogin || userInfo.userId.isNullOrEmpty()){
            return Resource.Error(ApiError.unknownCode,"用户操作非法")
        }
        return when (val result = playlistSheetRepository.deletePlaylistMusic(userInfo.userToken,playlistId,musicId)) {
            is ApiResult.Success -> {
                Resource.Success(null)
            }
            is ApiResult.Failure -> {
                Resource.Error(result.errorCode,result.errorMsg)
            }
            else -> {
                Resource.Error(ApiError.serverErrorCode,"未知错误")
            }
        }
    }
}