package com.ccteam.shared.domain.playlistsheet

import com.ccteam.model.Resource
import com.ccteam.model.playlistsheet.PLAYLIST_SHEET_TYPE_PRIVATE
import com.ccteam.model.playlistsheet.PLAYLIST_SHEET_TYPE_PUBLIC
import com.ccteam.model.playlistsheet.PLAYLIST_SHEET_TYPE_SELF_PUBLIC
import com.ccteam.model.playlistsheet.PlaylistSheetNoIdDTO
import com.ccteam.network.ApiError
import com.ccteam.network.ApiResult
import com.ccteam.shared.core.UserCore
import com.ccteam.shared.data.playlistsheet.PlaylistSheetRepository
import com.ccteam.shared.domain.SuspendUseCase
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/21
 *
 * 新增歌单Domain层
 * 传入[Pair<String,String>] 歌单名 和 歌单类型
 * 返回[Nothing]
 */
class AddPlaylistSheetDataUseCase @Inject constructor(
    private val userCore: UserCore,
    private val playlistSheetRepository: PlaylistSheetRepository
): SuspendUseCase<Pair<String, String>, Nothing?>(
    Dispatchers.IO
) {
    override suspend fun execute(parameters: Pair<String,String>): Resource<Nothing?> {
        val (playlistName,playlistTypeString) = parameters
        val playlistType = when (playlistTypeString) {
            "公开" -> {
                PLAYLIST_SHEET_TYPE_SELF_PUBLIC
            }
            "共享" -> {
                PLAYLIST_SHEET_TYPE_PUBLIC
            }
            else -> {
                PLAYLIST_SHEET_TYPE_PRIVATE
            }
        }

        val isLogin = userCore.isLogin.value
        val userInfo = userCore.userInfo.value
        if(!isLogin || userInfo.userId.isNullOrEmpty()){
            return Resource.Error(ApiError.unknownCode,"未登录，操作非法")
        }
        val result = playlistSheetRepository.createToPlaylist(
            userInfo.userToken,
            PlaylistSheetNoIdDTO(playlistName,playlistType,userInfo.userId)
        )

        return when (result) {
            is ApiResult.Success -> {
                Resource.Success(null)
            }
            is ApiResult.Failure -> {
                Resource.Error(result.errorCode,result.errorMsg)
            }
            else -> {
                Resource.Error(ApiError.serverErrorCode,"未知错误")
            }
        }
    }
}