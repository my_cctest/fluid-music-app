package com.ccteam.shared.domain.music

import android.support.v4.media.MediaMetadataCompat
import androidx.paging.*
import com.ccteam.fluidmusic.fluidmusic.common.MusicServiceConnectionFlow
import com.ccteam.fluidmusic.fluidmusic.media.extensions.*
import com.ccteam.model.music.BITRATE_NO
import com.ccteam.model.music.MusicVO
import com.ccteam.shared.api.OnlineMusicHomeDataSource
import com.ccteam.shared.data.music.NewMusicPagingSource
import com.ccteam.shared.domain.PagingUseCase
import com.ccteam.shared.result.artist.ArtistInfoData
import com.ccteam.shared.result.music.DefaultMusicItem
import com.ccteam.shared.utils.ALBUM_QUALITY
import com.ccteam.shared.utils.ARTIST_SEPARATOR
import com.ccteam.shared.utils.EMPTY_STRING
import com.ccteam.shared.utils.getPlaybackStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/28
 *
 * 新歌列表展示Domain层
 */
class NewMusicDataUseCase @Inject constructor(
    private val onlineMusicHomeDataSource: OnlineMusicHomeDataSource,
    private val musicServiceConnectionFlow: MusicServiceConnectionFlow
): PagingUseCase<String, DefaultMusicItem>(
    Dispatchers.IO
) {
    override fun execute(parameters: String): Flow<PagingData<DefaultMusicItem>> {
        return Pager(
            PagingConfig(pageSize = 15,initialLoadSize = 15)
        ){
            NewMusicPagingSource(onlineMusicHomeDataSource)
        }.flow.map { pagingData ->
            pagingData.map {
                transformToDefaultMusicItem(parameters,it)
            }
        }
    }

    private fun transformToDefaultMusicItem(
        parentId: String,
        musicVO: MusicVO
    ): DefaultMusicItem {
            /**
             * 分割成ArtistInfoData类型，其中含有歌手信息
             */
            val artistsArray = musicVO.artists.split(ARTIST_SEPARATOR)
            val artistInfo = if(artistsArray.size == musicVO.artistsId.size){
                artistsArray.mapIndexed{ index, artistName ->
                    ArtistInfoData(musicVO.artistsId[index],artistName)
                }
            } else {
                emptyList()
            }

            addMusicToBrowseTree(parentId,musicVO)
            return DefaultMusicItem(musicVO.id,musicVO.name,musicVO.artists,
                artistInfo,musicVO.albumId,musicVO.imgUrl + ALBUM_QUALITY,musicVO.maxBitrate,
                playable = !musicVO.url.isNullOrEmpty(),
                musicServiceConnectionFlow.nowPlaying.value.getPlaybackStatus(musicVO.id))
    }

    private fun addMusicToBrowseTree(parentId: String,music: MusicVO){
        if(music.maxBitrate == BITRATE_NO || music.url.isNullOrEmpty()) {
            return
        }
        musicServiceConnectionFlow.addOnlineMusicItem(
            parentId,
            MediaMetadataCompat.Builder().apply {
                id = music.id
                duration = music.duration
                title = music.name
                album = music.albumName
                albumId = music.albumId
                displayTitle = music.name
                displaySubtitle = music.albumName
                displayDescription = music.artists
                artist = music.artists
                trackNumber = music.track.toLong()
                mediaUri = music.url
                albumArtUri = music.imgUrl + ALBUM_QUALITY
                displayIconUri = music.imgUrl + ALBUM_QUALITY
            }.build()
        )
    }
}