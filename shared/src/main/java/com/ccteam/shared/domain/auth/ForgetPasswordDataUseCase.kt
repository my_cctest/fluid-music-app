package com.ccteam.shared.domain.auth

import com.ccteam.model.Resource
import com.ccteam.model.user.UserLoginDTO
import com.ccteam.network.ApiError
import com.ccteam.network.ApiResult
import com.ccteam.shared.data.auth.AuthRepository
import com.ccteam.shared.domain.SuspendUseCase
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/4/16
 *
 * 忘记密码操作的Domain层
 * 传入 [UserLoginDTO]
 * 返回 [Nothing]
 *
 * 传入登录相关数据，返回请求状态但无数据
 */
class ForgetPasswordDataUseCase @Inject constructor(
    private val authRepository: AuthRepository
): SuspendUseCase<UserLoginDTO, Nothing?>(
    Dispatchers.IO
) {
    override suspend fun execute(parameters: UserLoginDTO): Resource<Nothing?> {
        return when(val result = authRepository.forgetPassword(parameters)){
            is ApiResult.Success ->{
                Resource.Success(null)
            }
            is ApiResult.Failure ->{
                Resource.Error(result.errorCode,result.errorMsg)
            }
            is ApiResult.Empty ->{
                Resource.Error(ApiError.serverErrorCode,"未知错误")
            }
        }
    }
}