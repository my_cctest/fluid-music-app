package com.ccteam.shared.domain.file

import com.ccteam.model.Resource
import com.ccteam.network.ApiError
import com.ccteam.network.ApiResult
import com.ccteam.shared.data.file.UploadRepository
import com.ccteam.shared.domain.SuspendUseCase
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/19
 *
 * 获取Image图片上传七牛云的Token Domain层
 */
class UploadImageTokenDataUseCase @Inject constructor(
    private val uploadRepository: UploadRepository
): SuspendUseCase<Nothing?, String>(
    Dispatchers.IO
) {
    override suspend fun execute(parameters: Nothing?): Resource<String> {
        val result = uploadRepository.getImageToken()
        return if(result is ApiResult.Success){
            if(!result.data.data.isNullOrEmpty()){
                Resource.Success(result.data.data!!)
            } else {
                Resource.Error(ApiError.serverErrorCode,"获取Token为空")
            }
        } else if(result is ApiResult.Failure){
            Resource.Error(ApiError.serverErrorCode,result.errorMsg)
        } else {
            Resource.Error(ApiError.serverErrorCode,"获取Token失败")
        }
    }
}