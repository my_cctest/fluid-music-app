package com.ccteam.shared.domain.search

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.ccteam.shared.api.SearchDataSource
import com.ccteam.shared.data.search.HomeSearchArtistPagingSource
import com.ccteam.shared.domain.PagingUseCase
import com.ccteam.shared.result.artist.ArtistListItem
import com.ccteam.shared.utils.PHOTO_QUALITY
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/16
 *
 * 歌曲搜索Domain层
 * 传入[String] 歌手名
 * 返回[ArtistListItem] 歌手信息
 */
class HomeSearchArtistDataUseCase @Inject constructor(
    private val searchDataSource: SearchDataSource
): PagingUseCase<String?, ArtistListItem>(
    Dispatchers.IO
) {
    override fun execute(parameters: String?): Flow<PagingData<ArtistListItem>> {
        if(parameters.isNullOrEmpty()){
            return flowOf(PagingData.empty())
        }
        return Pager(
            PagingConfig(pageSize = 15,initialLoadSize = 15)
        ){
            HomeSearchArtistPagingSource(parameters,searchDataSource)
        }.flow.map { pagingData ->
            pagingData.map {
                ArtistListItem(it.id,it.name,it.imgUrl + PHOTO_QUALITY,null,it.id)
            }
        }
    }
}