package com.ccteam.shared.domain.playlistsheet

import com.ccteam.model.Resource
import com.ccteam.model.playlistsheet.PlaylistSheetDTO
import com.ccteam.network.ApiError
import com.ccteam.network.ApiResult
import com.ccteam.shared.core.UserCore
import com.ccteam.shared.data.playlistsheet.PlaylistSheetRepository
import com.ccteam.shared.domain.SuspendUseCase
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/19
 *
 * 编辑歌单Domain层
 * 传入[EditPlaylistRequest] 修改歌单的信息
 * 返回[Nothing]
 */
class PlaylistSheetEditDataUseCase @Inject constructor(
    private val userCore: UserCore,
    private val playlistSheetRepository: PlaylistSheetRepository
): SuspendUseCase<PlaylistSheetEditDataUseCase.EditPlaylistRequest, Nothing?>(
    Dispatchers.IO
) {
    override suspend fun execute(parameters: EditPlaylistRequest): Resource<Nothing?> {
        val userInfo = userCore.userInfo.value
        if(userInfo.userToken.isNullOrEmpty()
            || userInfo.userId.isNullOrEmpty()){
            return Resource.Error(ApiError.unknownCode,"登录状态异常")
        }
        val result = playlistSheetRepository.editPlaylist(
            userInfo.userToken,
            PlaylistSheetDTO(parameters.id,parameters.playlistImgUrl,parameters.playlistIntroduction,
                parameters.playlistName,userInfo.userId)
        )
        return when (result) {
            is ApiResult.Success -> {
                Resource.Success(null)
            }
            is ApiResult.Failure -> {
                Resource.Error(result.errorCode,result.errorMsg)
            }
            else -> {
                Resource.Error(ApiError.serverErrorCode,"未知错误")
            }
        }
    }

    data class EditPlaylistRequest(
        val id: String,
        var playlistName: String? = null,
        var playlistImgUrl: String? = null,
        var playlistIntroduction: String? = null
    )
}