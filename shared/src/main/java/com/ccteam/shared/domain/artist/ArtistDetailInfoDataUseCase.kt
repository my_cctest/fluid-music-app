package com.ccteam.shared.domain.artist

import com.ccteam.model.Resource
import com.ccteam.model.artist.ArtistVO
import com.ccteam.network.ApiError
import com.ccteam.shared.data.artist.ArtistRepository
import com.ccteam.shared.domain.FlowUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/2/20
 *
 * 歌手信息Domain层
 * 获取歌手信息
 * 传入[String] 歌手Id
 * 传出[ArtistVO] 歌手信息
 */
class ArtistDetailInfoDataUseCase @Inject constructor(
    private val artistRepository: ArtistRepository
): FlowUseCase<String, ArtistVO?>(
    Dispatchers.IO
) {
    override fun execute(parameters: String): Flow<Resource<ArtistVO?>> {
        return artistRepository.getArtistInfo(parameters).map {
            when(it){
                is Resource.Success ->
                    Resource.Success(it.data?.data)
                is Resource.Loading ->
                    Resource.Loading(it.data?.data)
                is Resource.Error ->
                    Resource.Error(ApiError.serverErrorCode,it.message ?: "未知错误",it.data?.data)
            }
        }
    }
}