package com.ccteam.shared.domain.music

import com.ccteam.model.Resource
import com.ccteam.network.ApiError
import com.ccteam.network.ApiResult
import com.ccteam.shared.data.music.MusicRepository
import com.ccteam.shared.domain.SuspendUseCase
import com.ccteam.shared.utils.EMPTY_STRING
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/4/11
 *
 * 获取在线音乐歌词Domain层
 */
class MusicLyricsDataUseCase @Inject constructor(
    private val musicRepository: MusicRepository
): SuspendUseCase<String, String>(
    Dispatchers.IO
) {

    override suspend fun execute(parameters: String): Resource<String> {
        return when (val result = musicRepository.getSuspendMusicInfoById(parameters)) {
            is ApiResult.Success -> {
                return if(result.data.data == null){
                    Resource.Error(ApiError.serverErrorCode,"未知错误")
                } else {
                    Resource.Success((result.data.data?.lrc ?: EMPTY_STRING))
                }
            }
            is ApiResult.Failure -> {
                Resource.Error(result.errorCode,result.errorMsg)
            }
            else -> {
                Resource.Error(ApiError.serverErrorCode,"未知错误")
            }
        }
    }
}