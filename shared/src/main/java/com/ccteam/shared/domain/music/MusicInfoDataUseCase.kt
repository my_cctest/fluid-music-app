package com.ccteam.shared.domain.music

import android.support.v4.media.MediaMetadataCompat
import com.ccteam.fluidmusic.fluidmusic.common.MusicServiceConnectionFlow
import com.ccteam.fluidmusic.fluidmusic.common.utils.MediaIDHelper
import com.ccteam.fluidmusic.fluidmusic.media.extensions.*
import com.ccteam.model.Resource
import com.ccteam.model.music.BITRATE_NO
import com.ccteam.model.music.MusicDetailVO
import com.ccteam.network.ApiError
import com.ccteam.network.ApiResult
import com.ccteam.shared.data.music.MusicRepository
import com.ccteam.shared.domain.SuspendUseCase
import com.ccteam.shared.utils.ALBUM_QUALITY
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/4/7
 *
 * 音乐信息Domain层
 * 传入[String] 音乐ID
 * 返回[Nothing]
 *
 * 该Domain层会将音乐信息添加入[BrowseTree]中方便访问，返回对应请求状态来判断是否请求成功
 */
class MusicInfoDataUseCase @Inject constructor(
    private val musicServiceConnectionFlow: MusicServiceConnectionFlow,
    private val musicRepository: MusicRepository
): SuspendUseCase<String, Nothing?>(
    Dispatchers.IO
) {
    override suspend fun execute(parameters: String): Resource<Nothing?> {
        return when (val result = musicRepository.getSuspendMusicInfoById(parameters)) {
            is ApiResult.Success -> {
                return if(result.data.data == null){
                    Resource.Error(ApiError.serverErrorCode,"未知错误")
                } else {
                    if(addToBrowseTree(result.data.data)){
                        Resource.Success(null)
                    } else {
                        Resource.Error(ApiError.serverErrorCode,"")
                    }
                }
            }
            is ApiResult.Failure -> {
                Resource.Error(result.errorCode,result.errorMsg)
            }
            else -> {
                Resource.Error(ApiError.serverErrorCode,"未知错误")
            }
        }
    }

    private fun addToBrowseTree(music: MusicDetailVO?): Boolean{
        music?.let {
            if(music.maxBitrate != BITRATE_NO && !music.url.isNullOrEmpty()){
                musicServiceConnectionFlow.addOnlineMusicItem(
                    MediaIDHelper.ONLINE_MEDIA_CACHE,
                    MediaMetadataCompat.Builder().apply {
                        id = music.id
                        duration = music.duration
                        title = music.name
                        album = music.albumName
                        albumId = music.albumId
                        displayTitle = music.name
                        displaySubtitle = music.albumName
                        displayDescription = music.artists
                        artist = music.artists
                        trackNumber = music.track.toLong()
                        mediaUri = music.url
                        albumArtUri = music.imgUrl + ALBUM_QUALITY
                        displayIconUri = music.imgUrl + ALBUM_QUALITY
                    }.build()
                )
                return true
            } else {
                return false
            }
        } ?: return false
    }
}