package com.ccteam.shared.domain.auth

import com.ccteam.model.Resource
import com.ccteam.model.user.UserRegisterDTO
import com.ccteam.network.ApiError
import com.ccteam.network.ApiResult
import com.ccteam.shared.data.auth.AuthRepository
import com.ccteam.shared.domain.SuspendUseCase
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/18
 *
 * 用户注册Domain层
 * 传入[UserRegisterDTO]
 * 返回[Nothing]
 *
 * 传入注册相关信息，返回对应请求状态不带数据
 */
class RegisterDataUseCase @Inject constructor(
    private val authRepository: AuthRepository
): SuspendUseCase<UserRegisterDTO, Nothing?>(
    Dispatchers.IO
) {
    override suspend fun execute(parameters: UserRegisterDTO): Resource<Nothing?> {
        val result = authRepository.register(
            parameters
        )
        return when (result) {
            is ApiResult.Success -> {
                Resource.Success(null)
            }
            is ApiResult.Failure -> {
                Resource.Error(ApiError.serverErrorCode,result.errorMsg)
            }
            else -> {
                Resource.Error(ApiError.serverErrorCode,"未知错误")
            }
        }
    }
}