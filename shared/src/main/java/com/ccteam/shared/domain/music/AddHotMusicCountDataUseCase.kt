package com.ccteam.shared.domain.music

import com.ccteam.fluidmusic.fluidmusic.common.utils.MediaIDHelper
import com.ccteam.fluidmusic.fluidmusic.media.library.BrowseTree
import com.ccteam.model.Resource
import com.ccteam.shared.data.home.OnlineMusicHomeRepository
import com.ccteam.shared.domain.SuspendUseCase
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/5/5
 */
class AddHotMusicCountDataUseCase @Inject constructor(
    private val onlineMusicHomeRepository: OnlineMusicHomeRepository
): SuspendUseCase<Pair<String?,String>,Nothing?>(
    Dispatchers.IO
) {
    override suspend fun execute(parameters: Pair<String?,String>): Resource<Nothing?> {
        val (parentId,mediaId) = parameters
        // 如果是在线歌曲的ParentId，才尝试加入到热门歌曲中，如果是本地歌曲则不做操作
        if(MediaIDHelper.isOnlineParentId(parentId)){
            onlineMusicHomeRepository.addHotMusicCount(mediaId)
        }
        return Resource.Success(null)
    }
}