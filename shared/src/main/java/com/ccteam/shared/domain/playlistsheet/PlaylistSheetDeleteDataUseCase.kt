package com.ccteam.shared.domain.playlistsheet

import com.ccteam.model.Resource
import com.ccteam.network.ApiError
import com.ccteam.network.ApiResult
import com.ccteam.shared.core.UserCore
import com.ccteam.shared.data.playlistsheet.PlaylistSheetRepository
import com.ccteam.shared.domain.SuspendUseCase
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/21
 *
 * 删除歌单Domain层
 * 传入[String]歌单ID
 * 返回[Nothing]
 */
class PlaylistSheetDeleteDataUseCase @Inject constructor(
    private val userCore: UserCore,
    private val playlistSheetRepository: PlaylistSheetRepository
): SuspendUseCase<String, Nothing?>(
    Dispatchers.IO
) {
    override suspend fun execute(parameters: String): Resource<Nothing?> {
        val isLogin = userCore.isLogin.value
        val userInfo = userCore.userInfo.value
        if(!isLogin || userInfo.userId.isNullOrEmpty()){
            return Resource.Error(ApiError.unknownCode,"用户操作非法")
        }
        return when (val result = playlistSheetRepository.deletePlaylist(userInfo.userToken,parameters)) {
            is ApiResult.Success -> {
                Resource.Success(null)
            }
            is ApiResult.Failure -> {
                Resource.Error(result.errorCode,result.errorMsg)
            }
            else -> {
                Resource.Error(ApiError.serverErrorCode,"未知错误")
            }
        }
    }
}