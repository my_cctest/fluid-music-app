package com.ccteam.shared.domain.search

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.ccteam.shared.api.SearchDataSource
import com.ccteam.shared.data.search.HomeSearchPlaylistSheetPagingSource
import com.ccteam.shared.domain.PagingUseCase
import com.ccteam.shared.result.playlistsheet.PlaylistSheetListItem
import com.ccteam.shared.utils.PHOTO_QUALITY
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/16
 *
 * 歌单搜索Domain层
 * 传入[String] 歌单名
 * 返回[PlaylistSheetListItem] 歌单信息
 */
class HomeSearchPlaylistSheetDataUseCase @Inject constructor(
    private val searchDataSource: SearchDataSource
): PagingUseCase<String?, PlaylistSheetListItem>(
    Dispatchers.IO
) {
    override fun execute(parameters: String?): Flow<PagingData<PlaylistSheetListItem>> {
        if(parameters.isNullOrEmpty()){
            return flowOf(PagingData.empty())
        }
        return Pager(
            PagingConfig(pageSize = 15,initialLoadSize = 15)
        ){
            HomeSearchPlaylistSheetPagingSource(parameters,searchDataSource)
        }.flow.map { pagingData ->
            pagingData.map {
                PlaylistSheetListItem(it.id,it.playlistName, it.imgUrl + PHOTO_QUALITY)
            }
        }
    }
}