package com.ccteam.shared.di

import com.ccteam.network.retrofit
import com.ccteam.shared.api.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * @author Xiaoc
 * @since 2021/1/30
 *
 * 提供Retrofit Api代理类的依赖注入方法
 * 基于 Hilt
 **/
@Module
@InstallIn(SingletonComponent::class)
class ApiModule {

    /**
     * 注入 [OnlineMusicHomeDataSource] 接口
     */
    @Provides
    @Singleton
    fun provideOnlineMusicHomeDataSource(): OnlineMusicHomeDataSource = retrofit.create(OnlineMusicHomeDataSource::class.java)

    /**
     * 注入 [ArtistDataSource] 接口
     */
    @Provides
    @Singleton
    fun provideArtistDataSource(): ArtistDataSource = retrofit.create(ArtistDataSource::class.java)

    /**
     * 注入 [AlbumDataSource] 接口
     */
    @Provides
    @Singleton
    fun provideAlbumDataSource(): AlbumDataSource = retrofit.create(AlbumDataSource::class.java)

    /**
     * 注入 [MusicDataSource] 接口
     */
    @Provides
    @Singleton
    fun provideMusicDataSource(): MusicDataSource = retrofit.create(MusicDataSource::class.java)

    /**
     * 注入 [PlaylistSheetDataSource] 接口
     */
    @Provides
    @Singleton
    fun providePlaylistSheetDataSource(): PlaylistSheetDataSource = retrofit.create(PlaylistSheetDataSource::class.java)

    /**
     * 注入 [SearchDataSource] 接口
     */
    @Provides
    @Singleton
    fun provideSearchDataSource(): SearchDataSource = retrofit.create(SearchDataSource::class.java)

    /**
     * 注入 [UserDataSource] 接口
     */
    @Provides
    @Singleton
    fun provideUserDataSource(): UserDataSource = retrofit.create(UserDataSource::class.java)

    /**
     * 注入 [AuthDataSource] 接口
     */
    @Provides
    @Singleton
    fun provideAuthDataSource(): AuthDataSource = retrofit.create(AuthDataSource::class.java)

    /**
     * 注入 [BaseDataSource] 接口
     */
    @Provides
    @Singleton
    fun provideBaseDataSource(): BaseDataSource = retrofit.create(BaseDataSource::class.java)

    /**
     * 注入 [UploadDataSource] 接口
     */
    @Provides
    @Singleton
    fun provideUploadDataSource(): UploadDataSource = retrofit.create(UploadDataSource::class.java)

    /**
     * 注入 [MomentsDataSource] 接口
     */
    @Provides
    @Singleton
    fun provideMomentDataSource(): MomentsDataSource = retrofit.create(MomentsDataSource::class.java)
}
