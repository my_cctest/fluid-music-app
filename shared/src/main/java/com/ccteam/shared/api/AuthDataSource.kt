package com.ccteam.shared.api

import com.ccteam.model.auth.AuthVO
import com.ccteam.model.user.UserLoginDTO
import com.ccteam.model.user.UserRegisterDTO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import retrofit2.http.*

/**
 * @author Xiaoc
 * @since 2021/3/18
 *
 * 认证相关API接口
 */
interface AuthDataSource {

    /**
     * 检查当前用户是否处于登录状态
     * @param token 用户Token
     */
    @GET("auth/user/check")
    suspend fun checkUserLogin(
        @Header("FluidToken") token: String,
    ): ApiResult<ResultVO<Nothing>>

    /**
     * 获取手机验证码
     * @param phone 用户手机号
     */
    @GET("auth/user/code")
    suspend fun getVerificationCode(
        @Query("phone") phone: String,
        @Query("codeType") codeType: String
    ): ApiResult<ResultVO<Nothing>>

    /**
     * 忘记密码
     * @param userLoginDTO 用户密码DTO
     */
    @POST("auth/user/forgetPassword")
    suspend fun forgetPassword(
        @Body userLoginDTO: UserLoginDTO
    ): ApiResult<ResultVO<Nothing>>

    /**
     * 用户登录
     * @param userDto 用户登录DTO
     */
    @POST("auth/user/login")
    suspend fun loginUser(
        @Body userDto: UserLoginDTO
    ): ApiResult<ResultVO<AuthVO>>

    /**
     * 用户退出登录
     * @param token 用户Token
     */
    @POST("auth/user/logout")
    suspend fun logoutUser(
        @Header("FluidToken") token: String
    ): ApiResult<ResultVO<Nothing>>

    /**
     * 用户注册
     * @param userRegisterDTO 用户注册信息DTO
     */
    @POST("auth/user/register")
    suspend fun registerUser(
        @Body userRegisterDTO: UserRegisterDTO
    ): ApiResult<ResultVO<Nothing>>
}