package com.ccteam.shared.api

import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.moment.MomentSimplyInfoVO
import com.ccteam.model.playlistsheet.PlaylistSheetVO
import com.ccteam.model.setting.SettingInfoDTO
import com.ccteam.model.setting.SettingInfoEditDTO
import com.ccteam.model.setting.SettingInfoVO
import com.ccteam.model.user.*
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import retrofit2.http.*

/**
 * @author Xiaoc
 * @since 2021/3/18
 *
 * 用户信息相关API接口
 */
interface UserDataSource {

    /**
     * 修改除用户密码以外的用户信息
     *
     * @param token 用户Token
     * @param userNoPasswordDTO 用户基本信息DTO
     */
    @PUT("user/user")
    suspend fun editUserInfo(
        @Header("FluidToken") token: String,
        @Body userNoPasswordDTO: UserNoPasswordDTO
    ): ApiResult<ResultVO<Nothing>>

    /**
     * 得到除用户密码以外的用户信息
     *
     * @param id 用户ID
     */
    @GET("user/user/{id}")
    suspend fun getUserInfo(
        @Path("id") id: String): ApiResult<ResultVO<UserVO>>

    /**
     * 得到除用户详细信息，包括密码
     *
     * @param token 用户Token
     */
    @GET("user/user/token")
    suspend fun getUserDetailInfo(
        @Header("FluidToken") token: String): ApiResult<ResultVO<UserDetailVO>>

    /**
     * 修改用户密码
     *
     * @param token 用户Token
     * @param userPasswordDTO 用户密码DTO
     */
    @PUT("user/user/password")
    suspend fun editUserPassword(
        @Header("FluidToken") token: String,
        @Body userPasswordDTO: UserPasswordVerifyDTO
    ): ApiResult<ResultVO<Nothing>>

    /**
     * 验证新手机号（修改手机号的最终步骤）
     *
     * @param token 用户Token
     */
    @PUT("user/user/changePhoneByCheckNewPhone")
    suspend fun verifyNewPhone(
        @Header("FluidToken") token: String,
        @Body phoneVerifyDTO: UserPhoneVerifyDTO
    ): ApiResult<ResultVO<Nothing>>

    /**
     * 验证旧手机号（修改手机号的第一步）
     *
     * @param token 用户Token
     */
    @POST("user/user/changePhoneByCheckOldPhone")
    suspend fun verifyOldPhone(
        @Header("FluidToken") token: String,
        @Body phoneVerifyDTO: UserPhoneVerifyDTO): ApiResult<ResultVO<Nothing>>


    /**
     * 得到用户歌单列表（分页）
     * @param id 用户ID
     */
    @POST("music/playlist/query/byUserId/{userId}")
    suspend fun getPlaylistListByUserId(
        @Header("FluidToken") token: String,
        @Path("userId")id: String,
        @Body pageDTO: PageDTO
    ): ApiResult<ResultVO<PageVO<PlaylistSheetVO>>>

    /**
     * 得到用户朋友圈列表（分页）
     * @param id 用户ID
     */
    @POST("moment/query/byUserId/{userId}")
    suspend fun getMomentListByUserId(
        @Header("FluidToken") token: String,
        @Path("userId")id: String,
        @Body pageDTO: PageDTO
    ): ApiResult<ResultVO<PageVO<MomentSimplyInfoVO>>>

    /**
     * 得到用户设置备份信息
     * @param token 用户Token
     * @param userId 用户Id
     */
    @GET("user/setting/byUser/{userId}")
    suspend fun getUserSettingInfo(
        @Header("FluidToken") token: String,
        @Path("userId") userId: String): ApiResult<ResultVO<SettingInfoVO>>

    /**
     * 创建用户设置备份信息
     * @param token 用户Token
     * @param settingInfo 设置信息
     */
    @POST("user/setting")
    suspend fun createUserSettingInfo(
        @Header("FluidToken") token: String,
        @Body settingInfo: SettingInfoDTO
    ): ApiResult<ResultVO<Nothing>>

    /**
     * 修改更新用户设置备份信息
     * @param token 用户Token
     * @param settingInfo 设置信息
     */
    @PUT("user/setting")
    suspend fun editUserSettingInfo(
        @Header("FluidToken") token: String,
        @Body settingInfo: SettingInfoEditDTO
    ): ApiResult<ResultVO<Nothing>>

}