package com.ccteam.shared.api

import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import retrofit2.http.GET
import retrofit2.http.Header

/**
 * @author Xiaoc
 * @since 2021/3/19
 *
 * 上传文件相关API
 */
interface UploadDataSource {

    /**
     * 得到上传图片的Token上传凭证
     */
    @GET("base/file/public/voucher/image")
    suspend fun getImageToken(): ApiResult<ResultVO<String>>
}