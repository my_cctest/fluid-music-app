package com.ccteam.shared.api

import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.music.MusicVO
import com.ccteam.model.playlistsheet.PlaylistSheetDTO
import com.ccteam.model.playlistsheet.PlaylistSheetMusicDTO
import com.ccteam.model.playlistsheet.PlaylistSheetNoIdDTO
import com.ccteam.model.playlistsheet.PlaylistSheetVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import retrofit2.http.*

/**
 * @author Xiaoc
 * @since 2021/3/4
 *
 * 歌单相关API接口
 */
interface PlaylistSheetDataSource {

    /**
     * 更新歌单信息
     * @param token 用户token
     * @param playlistSheetDTO 歌单信息内容
     */
    @PUT("music/playlist")
    suspend fun updatePlaylist(
        @Header("FluidToken") token: String,
        @Body playlistSheetDTO: PlaylistSheetDTO
    ): ApiResult<ResultVO<Nothing>>

    /**
     * 创建歌单信息
     * @param playlistSheetDTO 歌单信息
     */
    @POST("music/playlist")
    suspend fun createPlaylist(
        @Header("FluidToken") token: String,
        @Body playlistSheetDTO: PlaylistSheetNoIdDTO
    ): ApiResult<ResultVO<Nothing>>

    /**
     * 删除歌单信息
     * @param id 歌单ID
     */
    @DELETE("music/playlist/{id}")
    suspend fun deletePlaylist(
        @Header("FluidToken") token: String,
        @Path("id")id: String): ApiResult<ResultVO<Nothing>>

    /**
     * 向歌单中添加歌曲
     * @param token 用户token
     * @param playlistSheetMusicDTO 歌单音乐DTO
     */
    @POST("music/playlist/song")
    suspend fun addMusicToPlaylist(
        @Header("FluidToken") token: String,
        @Body playlistSheetMusicDTO: PlaylistSheetMusicDTO
    ): ApiResult<ResultVO<Nothing>>

    /**
     * 从歌单中删除歌曲
     * @param token 用户token
     * @param musicId 音乐ID
     * @param playlistId 歌单ID
     */
    @DELETE("music/playlist/song/{id}")
    suspend fun deleteMusicByPlaylist(
        @Header("FluidToken") token: String,
        @Path("id") musicId: String,
        @Query("playlistId") playlistId: String
    ): ApiResult<ResultVO<Nothing>>

    /**
     * 得到歌单信息
     * @param token 用户Token
     * @param id 歌单ID
     */
    @GET("music/playlist/{id}")
    suspend fun getPlaylistSheetInfo(
        @Header("FluidToken") token: String? = null,
        @Path("id")id: String): ApiResult<ResultVO<PlaylistSheetVO>>

    /**
     * 得到对应歌单的歌曲信息
     * @param token 用户Token
     * @param id 歌单ID
     */
    @GET("music/playlist/query-songs/{id}")
    suspend fun getPlaylistMusicList(
        @Header("FluidToken") token: String? = null,
        @Path("id") id: String
    ): ApiResult<ResultVO<List<MusicVO>>>

    /**
     * 得到歌单列表（分页）
     */
    @POST("music/home-page/search/playlist-name")
    suspend fun getByPlaylistList(@Body pageDTO: PageDTO): ApiResult<ResultVO<PageVO<PlaylistSheetVO>>>

}