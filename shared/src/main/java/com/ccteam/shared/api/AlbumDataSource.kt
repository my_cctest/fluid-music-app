package com.ccteam.shared.api

import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.album.AlbumSimplifyVO
import com.ccteam.model.album.AlbumVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import retrofit2.http.*

/**
 * @author Xiaoc
 * @since 2021/2/17
 *
 * 专辑业务相关的API接口定义
 */
interface AlbumDataSource {

    @GET("music/album/list")
    suspend fun getAlbumList(@Query("current") current: Int,
                             @Query("size") size: Int,
                             @Query("byPinyin") byPinyin: Boolean = true
    ): ApiResult<ResultVO<PageVO<AlbumSimplifyVO>>>

    @GET("music/album/{id}")
    suspend fun getAlbumById(@Path("id")id: String): ApiResult<ResultVO<AlbumVO>>

    @POST("music/album/query-aritst-id/{id}")
    suspend fun getAlbumByArtistId(
        @Path("id") id: String, @Body pageDTO: PageDTO
    ): ApiResult<ResultVO<PageVO<AlbumSimplifyVO>>>
}