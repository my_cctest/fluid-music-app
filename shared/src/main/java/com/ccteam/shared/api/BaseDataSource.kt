package com.ccteam.shared.api

import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import retrofit2.http.GET

/**
 * @author Xiaoc
 * @since 2021/3/18
 *
 * 基本API接口，包括文件上传服务和RSA密钥服务
 */
interface BaseDataSource {

    @GET("base/rsa/public/public-key")
    suspend fun getRsaPublicKey(): ApiResult<ResultVO<String>>
}