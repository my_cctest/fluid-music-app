package com.ccteam.shared.api

import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.moment.MomentDTO
import com.ccteam.model.moment.MomentNoIdDTO
import com.ccteam.model.moment.MomentVO
import com.ccteam.model.moment.comment.CommentDTO
import com.ccteam.model.moment.comment.CommentNoIdDTO
import com.ccteam.model.moment.comment.CommentVO
import com.ccteam.model.moment.up.MomentUpDTO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import retrofit2.http.*

/**
 * @author Xiaoc
 * @since 2021/3/28
 */
interface MomentsDataSource {

    /**
     * 修改朋友圈信息
     * @param token 用户Token
     * @param momentDTO 朋友圈信息
     */
    @PUT("moment")
    suspend fun editMomentInfo(
        @Header("FluidToken") token: String, @Body momentDTO: MomentDTO
    ): ApiResult<ResultVO<Nothing>>

    /**
     * 创建朋友圈信息
     * @param token 用户Token
     * @param momentDTO 朋友圈信息
     */
    @POST("moment")
    suspend fun createMomentInfo(
        @Header("FluidToken") token: String, @Body momentDTO: MomentNoIdDTO
    ): ApiResult<ResultVO<Nothing>>

    /**
     * 根据朋友圈ID查询朋友圈信息
     * @param id 朋友圈ID
     */
    @GET("moment/{id}")
    suspend fun getMomentInfo(
        @Path("id") id: String): ApiResult<ResultVO<MomentVO>>

    /**
     * 删除朋友圈信息
     * @param token 用户Token
     * @param id 朋友圈ID
     */
    @DELETE("moment/{id}")
    suspend fun deleteMomentInfo(@Header("FluidToken") token: String,
        @Path("id") id: String): ApiResult<ResultVO<Nothing>>

    /**
     * 朋友圈信息分页查询
     * @param pageDTO 分页DTO
     */
    @POST("moment/query")
    suspend fun getMomentInfoList(@Body pageDTO: PageDTO): ApiResult<ResultVO<PageVO<MomentVO>>>

    /**
     * 修改评论信息
     * @param token 用户Token
     * @param commentDTO 评论信息DTO
     */
    @PUT("moment/comment")
    suspend fun editCommentInfo(
        @Header("FluidToken") token: String,@Body commentDTO: CommentDTO
    ): ApiResult<ResultVO<Nothing>>

    @POST("moment/comment")
    suspend fun createCommentInfo(
        @Header("FluidToken") token: String,@Body commentDTO: CommentNoIdDTO
    ): ApiResult<ResultVO<Nothing>>

    /**
     * 查询评论信息
     *
     * @param id 评论ID
     */
    @GET("moment/comment/{id}")
    suspend fun getCommentInfo(
        @Path("id") id: String): ApiResult<ResultVO<CommentVO>>

    /**
     * 删除评论信息
     *
     * @param id 评论ID
     */
    @DELETE("moment/comment/{id}")
    suspend fun deleteCommentInfo(@Header("FluidToken") token: String,
        @Path("id") id: String): ApiResult<ResultVO<Nothing>>

    /**
     * 分页获取该条评论的回复信息
     *
     * @param id 评论ID
     */
    @POST("moment/comment/query/{id}")
    suspend fun getRelayCommentInfoList(
        @Path("id") id: String,
        @Body pageDTO: PageDTO): ApiResult<ResultVO<PageVO<CommentVO.ReplyInfoVO>>>

    /**
     * 分页获取该朋友圈的评论回复信息
     *
     * @param momentId 朋友圈ID
     */
    @POST("moment/comment/query/moment/{momentId}")
    suspend fun getCommentInfoByMomentId(
        @Path("momentId") momentId: String,@Body pageDTO: PageDTO): ApiResult<ResultVO<PageVO<CommentVO>>>


    /**
     * 检查该用户是否点赞该朋友圈
     * @param momentId 朋友圈ID
     * @param userId 用户ID
     */
    @GET("moment/up")
    suspend fun isMomentUp(@Query("momentId") momentId: String,
                           @Query("userId") userId: String): ApiResult<ResultVO<Boolean>>

    /**
     * 修改或新增点赞
     * @param momentUpDTO 点赞DTO
     */
    @POST("moment/up")
    suspend fun setMomentUp(
        @Header("FluidToken") token: String,
        @Body momentUpDTO: MomentUpDTO
    ): ApiResult<ResultVO<Nothing>>

    /**
     * 取消点赞
     * @param momentId 朋友圈ID
     * @param userId 用户ID
     */
    @DELETE("moment/up/byMomentUserId")
    suspend fun cancelMomentUp(
        @Header("FluidToken") token: String,
        @Query("momentId") momentId: String,
        @Query("userId") userId: String): ApiResult<ResultVO<Nothing>>
}