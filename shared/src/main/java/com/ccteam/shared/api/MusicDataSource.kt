package com.ccteam.shared.api

import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.music.MusicDetailVO
import com.ccteam.model.music.MusicVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

/**
 * @author Xiaoc
 * @since 2021/2/19
 *
 * 歌曲业务相关的API接口定义
 */
interface MusicDataSource {

    /**
     * 通过歌曲Id获得歌曲信息
     */
    @GET("music/song/{id}")
    suspend fun getMusicInfoById(@Path("id") id: String): ApiResult<ResultVO<MusicDetailVO>>

    /**
     * 通过专辑Id得到该专辑的音乐列表
     */
    @GET("music/song/query/album/{id}")
    suspend fun getMusicByAlbumId(@Path("id") albumId: String): ApiResult<ResultVO<List<MusicVO>>>

    /**
     * 通过歌手ID查询对应歌手信息
     * @param id 歌手ID
     */
    @POST("music/song/query/artist/{id}")
    suspend fun getMusicByArtistId(
        @Path("id") id: String, @Body pageDTO: PageDTO
    ): ApiResult<ResultVO<PageVO<MusicVO>>>

    /**
     * 通过歌单Id查找对应歌单歌曲列表
     * @param id 歌单ID
     */
    @GET("music/playlist/query-songs/{id}")
    suspend fun getMusicByPlaylistId(@Path("id") id: String): ApiResult<ResultVO<List<MusicVO>>>
}