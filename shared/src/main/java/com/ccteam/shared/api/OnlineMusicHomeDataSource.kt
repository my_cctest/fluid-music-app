package com.ccteam.shared.api

import com.ccteam.model.PageVO
import com.ccteam.model.album.AlbumSimplifyVO
import com.ccteam.model.banner.BannerVO
import com.ccteam.model.music.MusicVO
import com.ccteam.model.playlistsheet.PlaylistSheetVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * @author Xiaoc
 * @since 2021/1/30
 *
 * 在线音乐首页相关业务API接口定义
 **/
interface OnlineMusicHomeDataSource {

    /**
     * 获取轮播图
     * @param current 当前页数
     * @param size 当前页数显示数量
     * @param byPinyin 是否按拼音排序
     */
    @GET("music/home-page/carousel-image")
    suspend fun getBanner(@Query("current") current: Int,
                          @Query("size") size: Int,
                          @Query("byPinyin") byPinyin: Boolean = false
    ): ApiResult<ResultVO<PageVO<BannerVO>>>

    /**
     * 获取新专推荐
     * @param current 当前页数
     * @param size 当前页数显示数量
     * @param byPinyin 是否按拼音排序
     */
    @GET("music/home-page/recommend/album")
    suspend fun getNewAlbums(@Query("current") current: Int,
                          @Query("size") size: Int,
                          @Query("byPinyin") byPinyin: Boolean = false
    ): ApiResult<ResultVO<PageVO<AlbumSimplifyVO>>>

    /**
     * 获取新歌推荐
     * @param current 当前页数
     * @param size 当前页数显示数量
     * @param byPinyin 是否按拼音排序
     */
    @GET("music/home-page/recommend/song")
    suspend fun getNewMusic(@Query("current") current: Int,
                            @Query("size") size: Int,
                            @Query("byPinyin") byPinyin: Boolean = false
    ): ApiResult<ResultVO<PageVO<MusicVO>>>

    /**
     * 获取热门歌单
     * @param current 当前页数
     * @param size 当前页数显示数量
     * @param byPinyin 是否按拼音排序
     */
    @GET("music/playlist/hot/list")
    suspend fun getHotPlaylistSheet(@Query("start") start: Int,
                            @Query("end") end: Int
    ): ApiResult<ResultVO<List<PlaylistSheetVO>>>

    /**
     * 获取热门歌曲
     * @param current 当前页数
     * @param size 当前页数显示数量
     * @param byPinyin 是否按拼音排序
     */
    @GET("music/song/hot/list")
    suspend fun getHotSong(@Query("start") start: Int,
                    @Query("end") end: Int
    ): ApiResult<ResultVO<List<MusicVO>>>

    /**
     * 添加热门歌曲计数
     * @param id 歌曲ID
     */
    @POST("music/song/hot/{id}")
    suspend fun addHotMusicCount(@Path("id")id: String): ApiResult<ResultVO<Nothing>>


    /**
     * 添加热门歌单计数
     * @param id 歌单ID
     */
    @POST("music/playlist/hot/{id}")
    suspend fun addHotPlaylistSheetCount(@Path("id")id: String): ApiResult<ResultVO<Nothing>>
}