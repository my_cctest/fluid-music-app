package com.ccteam.shared.api

import com.ccteam.model.PageVO
import com.ccteam.model.artist.ArtistSimplifyVO
import com.ccteam.model.artist.ArtistVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * @author Xiaoc
 * @since 2021/2/15
 *
 * 歌手业务相关的API接口定义
 **/
interface ArtistDataSource {

    /**
     * 得到歌手列表
     * 支持分页
     */
    @GET("music/artist/list")
    suspend fun getArtistList(@Query("current") current: Int,
                              @Query("size") size: Int,
                              @Query("byPinyin") byPinyin: Boolean = true
    ): ApiResult<ResultVO<PageVO<ArtistSimplifyVO>>>


    /**
     * 通过歌手ID查询对应歌手信息
     * @param id 歌手ID
     */
    @GET("music/artist/{id}")
    suspend fun getArtistById(
        @Path("id") id: String
    ): ApiResult<ResultVO<ArtistVO>>

}