package com.ccteam.shared.api

import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.album.AlbumSimplifyVO
import com.ccteam.model.artist.ArtistSimplifyVO
import com.ccteam.model.music.MusicVO
import com.ccteam.model.playlistsheet.PlaylistSheetVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * @author Xiaoc
 * @since 2021/3/15
 *
 * 搜索相关API接口
 */
interface SearchDataSource {

    /**
     * 通过专辑名搜索专辑
     * @param albumName 专辑名
     */
    @POST("music/home-page/search/album-name")
    suspend fun searchByAlbumName(
        @Query("albumName") albumName: String,
        @Body pageDTO: PageDTO
    ): ApiResult<ResultVO<PageVO<AlbumSimplifyVO>>>

    /**
     * 通过专辑名搜索歌手
     * @param artistName 歌手名
     */
    @POST("music/home-page/search/artist-name")
    suspend fun searchByArtistName(
        @Query("artistName") artistName: String,
        @Body pageDTO: PageDTO): ApiResult<ResultVO<PageVO<ArtistSimplifyVO>>>

    /**
     * 通过歌单名搜索歌单
     * @param playlistName 歌单名
     */
    @POST("music/home-page/search/playlist-name")
    suspend fun searchByPlaylistName(
        @Query("playlistName") playlistName: String,
        @Body pageDTO: PageDTO
    ): ApiResult<ResultVO<PageVO<PlaylistSheetVO>>>

    /**
     * 通过音乐名搜索歌曲
     * @param musicName 歌曲名
     */
    @POST("music/home-page/search/song-name")
    suspend fun searchByMusicName(
        @Query("songName") musicName: String,
        @Body pageDTO: PageDTO): ApiResult<ResultVO<PageVO<MusicVO>>>
}