package com.ccteam.shared.data.artist

import com.ccteam.model.Resource
import com.ccteam.model.artist.ArtistVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import com.ccteam.shared.api.ArtistDataSource
import com.ccteam.shared.data.BaseOnlyNetworkBoundResource
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Xiaoc
 * @since 2021/2/15
 *
 * 歌手业务 仓库层
 **/
@Singleton
class ArtistRepository @Inject constructor(
    private val artistDataSource: ArtistDataSource
) {

    @Inject lateinit var artistListPagingSource: ArtistListPagingSource

    fun getArtistInfo(artistId: String): Flow<Resource<ResultVO<ArtistVO>>> {
        return object: BaseOnlyNetworkBoundResource<ResultVO<ArtistVO>>(){

            override suspend fun fetchFromNetwork(): ApiResult<ResultVO<ArtistVO>> {
                return artistDataSource.getArtistById(artistId)
            }

        }.asFlow()
    }

}