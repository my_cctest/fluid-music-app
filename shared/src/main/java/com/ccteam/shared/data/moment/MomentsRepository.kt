package com.ccteam.shared.data.moment

import com.ccteam.model.Resource
import com.ccteam.model.moment.MomentNoIdDTO
import com.ccteam.model.moment.MomentVO
import com.ccteam.model.moment.comment.CommentNoIdDTO
import com.ccteam.model.moment.comment.CommentVO
import com.ccteam.model.moment.up.MomentUpDTO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import com.ccteam.shared.api.MomentsDataSource
import com.ccteam.shared.data.BaseOnlyNetworkBoundResource
import com.orhanobut.logger.Logger
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Xiaoc
 * @since 2021/3/28
 */
@Singleton
class MomentsRepository @Inject constructor(
    private val momentsDataSource: MomentsDataSource
) {

    /**
     * 得到父评论的信息（replayDetails字段为空数组）
     */
    suspend fun getParentMomentInfo(id: String): ApiResult<ResultVO<CommentVO>>{
        return momentsDataSource.getCommentInfo(id)
    }

    /**
     * 创建朋友圈内容
     */
    suspend fun createMomentInfo(token: String,momentInfo: MomentNoIdDTO): ApiResult<ResultVO<Nothing>>{
        Logger.d("查看信息：$momentInfo")
        return momentsDataSource.createMomentInfo(token,momentInfo)
    }

    /**
     * 创建评论/回复内容
     */
    suspend fun createCommentInfo(token: String,commentInfo: CommentNoIdDTO): ApiResult<ResultVO<Nothing>>{
        return momentsDataSource.createCommentInfo(token,commentInfo)
    }

    /**
     * 创建评论/回复内容
     */
    fun getMomentInfo(id: String): Flow<Resource<ResultVO<MomentVO>>>{
        return object: BaseOnlyNetworkBoundResource<ResultVO<MomentVO>>(){

            override suspend fun fetchFromNetwork(): ApiResult<ResultVO<MomentVO>> {
                return momentsDataSource.getMomentInfo(id)
            }

        }.asFlow()
    }

    /**
     * 进行朋友圈点赞
     * @param momentUpDTO 朋友圈点赞DTO
     */
    suspend fun setCommentUp(token: String,momentUpDTO: MomentUpDTO): ApiResult<ResultVO<Nothing>>{
        return momentsDataSource.setMomentUp(token,momentUpDTO)
    }

    /**
     * 取消朋友圈点赞
     * @param momentId 朋友圈ID
     * @param userId 用户ID
     */
    suspend fun cancelCommentUp(token: String,momentId: String,userId: String): ApiResult<ResultVO<Nothing>>{
        return momentsDataSource.cancelMomentUp(token,momentId,userId)
    }

    /**
     * 检查该用户是否点赞该朋友圈
     *
     * @param momentId 朋友圈ID
     * @param userId 用户ID
     */
    suspend fun checkCommentUp(userId: String,momentId: String): ApiResult<ResultVO<Boolean>>{
        return momentsDataSource.isMomentUp(momentId,userId)
    }

    /**
     * 删除朋友圈内容
     * @param momentId 朋友圈ID
     * @param token 用户Token
     */
    suspend fun deleteMoment(token: String,momentId: String): ApiResult<ResultVO<Nothing>>{
        return momentsDataSource.deleteMomentInfo(token,momentId)
    }
}