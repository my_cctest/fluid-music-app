package com.ccteam.shared.data.moment

import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.moment.MomentSimplyInfoVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import com.ccteam.shared.api.UserDataSource
import com.ccteam.shared.data.BasePagingSource
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/4/7
 *
 * 查询对应用户朋友圈列表的分页数据源
 */
class MomentsUserPagingSource @Inject constructor(
    private val token: String,
    private val id: String,
    private val userDataSource: UserDataSource
): BasePagingSource<MomentSimplyInfoVO>() {

    override suspend fun loadData(pageInfo: PageDTO): ApiResult<ResultVO<PageVO<MomentSimplyInfoVO>>> {
        return userDataSource.getMomentListByUserId(token,id,pageInfo)
    }
}