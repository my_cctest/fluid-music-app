package com.ccteam.shared.data.playlistsheet

import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.playlistsheet.PlaylistSheetVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import com.ccteam.shared.api.UserDataSource
import com.ccteam.shared.data.BasePagingSource
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/4/7
 *
 * 查询对应用户歌单列表的分页数据源
 */
class PlaylistSheetUserPagingSource @Inject constructor(
    private val token: String,
    private val id: String,
    private val userdataSource: UserDataSource
): BasePagingSource<PlaylistSheetVO>() {
    override suspend fun loadData(pageInfo: PageDTO): ApiResult<ResultVO<PageVO<PlaylistSheetVO>>> {
        return userdataSource.getPlaylistListByUserId(token,id,pageInfo)
    }
}