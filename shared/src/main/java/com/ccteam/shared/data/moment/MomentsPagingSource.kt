package com.ccteam.shared.data.moment

import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.moment.MomentVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import com.ccteam.shared.api.MomentsDataSource
import com.ccteam.shared.data.BasePagingSource

/**
 * @author Xiaoc
 * @since 2021/3/28
 */
class MomentsPagingSource(
    private val momentsDataSource: MomentsDataSource
): BasePagingSource<MomentVO>() {

    override suspend fun loadData(pageInfo: PageDTO): ApiResult<ResultVO<PageVO<MomentVO>>> {
        pageInfo.byPinyin = false
        return momentsDataSource.getMomentInfoList(pageInfo)
    }
}