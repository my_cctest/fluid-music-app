package com.ccteam.shared.data.artist

import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.album.AlbumSimplifyVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import com.ccteam.shared.api.AlbumDataSource
import com.ccteam.shared.data.BasePagingSource

/**
 * @author Xiaoc
 * @since 2021/2/20
 */
class ArtistDetailAlbumPagingSource(
    private val artistId: String,
    private val albumDataSource: AlbumDataSource
): BasePagingSource<AlbumSimplifyVO>() {

    override suspend fun loadData(pageInfo: PageDTO): ApiResult<ResultVO<PageVO<AlbumSimplifyVO>>> {
        return albumDataSource.getAlbumByArtistId(artistId,pageInfo)
    }
}