package com.ccteam.shared.data.file

import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import com.ccteam.shared.api.UploadDataSource
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Xiaoc
 * @since 2021/3/19
 */
@Singleton
class UploadRepository @Inject constructor(
    private val uploadDataSource: UploadDataSource
) {

    suspend fun getImageToken(): ApiResult<ResultVO<String>>{
        return uploadDataSource.getImageToken()
    }
}