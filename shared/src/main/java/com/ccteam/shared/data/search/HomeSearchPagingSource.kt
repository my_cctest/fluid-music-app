package com.ccteam.shared.data.search

import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.album.AlbumSimplifyVO
import com.ccteam.model.artist.ArtistSimplifyVO
import com.ccteam.model.music.MusicVO
import com.ccteam.model.playlistsheet.PlaylistSheetVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import com.ccteam.shared.api.SearchDataSource
import com.ccteam.shared.data.BasePagingSource

/**
 * @author Xiaoc
 * @since 2021/3/16
 *
 * 在线内容搜索用的分页资源
 */
class HomeSearchAlbumPagingSource(
    private val albumName: String,
    private val searchDataSource: SearchDataSource
): BasePagingSource<AlbumSimplifyVO>() {
    override suspend fun loadData(pageInfo: PageDTO): ApiResult<ResultVO<PageVO<AlbumSimplifyVO>>> {
        return searchDataSource.searchByAlbumName(albumName,pageInfo)
    }
}

class HomeSearchArtistPagingSource(
    private val artistName: String,
    private val searchDataSource: SearchDataSource
): BasePagingSource<ArtistSimplifyVO>() {
    override suspend fun loadData(pageInfo: PageDTO): ApiResult<ResultVO<PageVO<ArtistSimplifyVO>>> {
        return searchDataSource.searchByArtistName(artistName,pageInfo)
    }
}

class HomeSearchMusicPagingSource(
    private val musicName: String,
    private val searchDataSource: SearchDataSource
): BasePagingSource<MusicVO>() {
    override suspend fun loadData(pageInfo: PageDTO): ApiResult<ResultVO<PageVO<MusicVO>>> {
        return searchDataSource.searchByMusicName(musicName,pageInfo)
    }
}

class HomeSearchPlaylistSheetPagingSource(
    private val playlistSheetName: String,
    private val searchDataSource: SearchDataSource
): BasePagingSource<PlaylistSheetVO>() {
    override suspend fun loadData(pageInfo: PageDTO): ApiResult<ResultVO<PageVO<PlaylistSheetVO>>> {
        return searchDataSource.searchByPlaylistName(playlistSheetName,pageInfo)
    }
}