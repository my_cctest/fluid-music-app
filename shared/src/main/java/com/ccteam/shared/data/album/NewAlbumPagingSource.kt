package com.ccteam.shared.data.album

import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.album.AlbumSimplifyVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import com.ccteam.shared.api.OnlineMusicHomeDataSource
import com.ccteam.shared.data.BasePagingSource
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/15
 */
class NewAlbumPagingSource @Inject constructor(
    private val onlineMusicHomeDataSource: OnlineMusicHomeDataSource
): BasePagingSource<AlbumSimplifyVO>() {

    override suspend fun loadData(pageInfo: PageDTO): ApiResult<ResultVO<PageVO<AlbumSimplifyVO>>> {
        return onlineMusicHomeDataSource.getNewAlbums(pageInfo.current,pageInfo.size)
    }
}