package com.ccteam.shared.data.music

import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.music.MusicVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import com.ccteam.shared.api.OnlineMusicHomeDataSource
import com.ccteam.shared.data.BasePagingSource

/**
 * @author Xiaoc
 * @since 2021/3/28
 */
class NewMusicPagingSource(
    private val onlineMusicHomeDataSource: OnlineMusicHomeDataSource
): BasePagingSource<MusicVO>() {

    override suspend fun loadData(pageInfo: PageDTO): ApiResult<ResultVO<PageVO<MusicVO>>> {
        return onlineMusicHomeDataSource.getNewMusic(pageInfo.current,pageInfo.size)
    }
}