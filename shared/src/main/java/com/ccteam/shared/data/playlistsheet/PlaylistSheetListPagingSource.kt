package com.ccteam.shared.data.playlistsheet

import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.playlistsheet.PlaylistSheetVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import com.ccteam.shared.api.PlaylistSheetDataSource
import com.ccteam.shared.data.BasePagingSource
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/15
 */
class PlaylistSheetListPagingSource @Inject constructor(
    private val playlistSheetDataSource: PlaylistSheetDataSource
): BasePagingSource<PlaylistSheetVO>() {

    override suspend fun loadData(pageInfo: PageDTO): ApiResult<ResultVO<PageVO<PlaylistSheetVO>>> {
        return playlistSheetDataSource.getByPlaylistList(pageInfo)
    }
}