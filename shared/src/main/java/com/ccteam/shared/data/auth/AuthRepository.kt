package com.ccteam.shared.data.auth

import com.ccteam.model.auth.AuthVO
import com.ccteam.model.user.UserLoginDTO
import com.ccteam.model.user.UserRegisterDTO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import com.ccteam.shared.api.AuthDataSource
import com.ccteam.shared.api.BaseDataSource
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Xiaoc
 * @since 2021/3/18
 *
 * 认证相关Repository仓库
 */
@Singleton
class AuthRepository @Inject constructor(
    private val baseDataSource: BaseDataSource,
    private val authDataSource: AuthDataSource
) {

    /**
     * 登录用户
     */
    suspend fun loginUser(userPasswordVerifyDTO: UserLoginDTO): ApiResult<ResultVO<AuthVO>>{
        return authDataSource.loginUser(userPasswordVerifyDTO)
    }

    /**
     * 登出用户
     */
    suspend fun logoutUser(token: String): ApiResult<ResultVO<Nothing>>{
        return authDataSource.logoutUser(token)
    }

    /**
     * 注册用户
     */
    suspend fun register(userRegisterDTO: UserRegisterDTO): ApiResult<ResultVO<Nothing>>{
        return authDataSource.registerUser(userRegisterDTO)
    }

    /**
     * 得到RSA加密公钥Key
     */
    suspend fun getRsaPublicKey(): ApiResult<ResultVO<String>>{
        return baseDataSource.getRsaPublicKey()
    }

    /**
     * 检查用户登录状态是否有效
     * @param token 用户Token
     */
    suspend fun checkUserLogin(token: String): ApiResult<ResultVO<Nothing>>{
        return authDataSource.checkUserLogin(token)
    }

    /**
     * 获取验证码
     * @param phone 手机号
     * @param type 验证码类型
     */
    suspend fun getVerification(phone: String,type: String): ApiResult<ResultVO<Nothing>>{
        return authDataSource.getVerificationCode(phone,type)
    }

    /**
     * 获取验证码
     * @param userLoginDTO 用户LoginDTO
     */
    suspend fun forgetPassword(userLoginDTO: UserLoginDTO): ApiResult<ResultVO<Nothing>>{
        return authDataSource.forgetPassword(userLoginDTO)
    }
}