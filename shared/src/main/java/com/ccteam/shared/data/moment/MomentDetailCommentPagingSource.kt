package com.ccteam.shared.data.moment

import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.moment.comment.CommentVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import com.ccteam.shared.api.MomentsDataSource
import com.ccteam.shared.data.BasePagingSource
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/28
 *
 * 通过朋友圈ID分页查询朋友圈对应评论（含评论对应的部分回复）
 */
class MomentDetailCommentPagingSource @Inject constructor(
    private val momentId: String,
    private val momentsDataSource: MomentsDataSource
): BasePagingSource<CommentVO>() {

    override suspend fun loadData(pageInfo: PageDTO): ApiResult<ResultVO<PageVO<CommentVO>>> {
        pageInfo.byPinyin = false
        return momentsDataSource.getCommentInfoByMomentId(momentId,pageInfo)
    }
}