package com.ccteam.shared.data.playlistsheet

import com.ccteam.model.Resource
import com.ccteam.model.music.MusicVO
import com.ccteam.model.playlistsheet.PlaylistSheetDTO
import com.ccteam.model.playlistsheet.PlaylistSheetMusicDTO
import com.ccteam.model.playlistsheet.PlaylistSheetNoIdDTO
import com.ccteam.model.playlistsheet.PlaylistSheetVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import com.ccteam.shared.api.PlaylistSheetDataSource
import com.ccteam.shared.data.BaseOnlyNetworkBoundResource
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Xiaoc
 * @since 2021/3/15
 */
@Singleton
class PlaylistSheetRepository @Inject constructor(
    private val playlistSheetDataSource: PlaylistSheetDataSource
) {

    /**
     * 通过歌单ID获取歌单信息
     */
    fun getPlaylistInfo(id: String,token: String?): Flow<Resource<ResultVO<PlaylistSheetVO>>>{
        return object: BaseOnlyNetworkBoundResource<ResultVO<PlaylistSheetVO>>(){
            override suspend fun fetchFromNetwork(): ApiResult<ResultVO<PlaylistSheetVO>> {
                return playlistSheetDataSource.getPlaylistSheetInfo(id = id,token = token)
            }
        }.asFlow()
    }

    /**
     * 通过歌单ID获取歌单歌曲列表信息
     * @param id 歌单ID
     * @param token 用户Token，可为空
     */
    fun getPlaylistMusicList(id: String,token: String? = null): Flow<Resource<ResultVO<List<MusicVO>>>>{
        return object: BaseOnlyNetworkBoundResource<ResultVO<List<MusicVO>>>(){
            override suspend fun fetchFromNetwork(): ApiResult<ResultVO<List<MusicVO>>> {
                return playlistSheetDataSource.getPlaylistMusicList(token = token,id = id)
            }
        }.asFlow()
    }

    /**
     * 添加音乐到歌单中
     * @param token 用户Token，不可为空，如果为空，操作非法
     * @param playlistSheetMusicDTO 歌单音乐内容
     */
    suspend fun addMusicToPlaylist(token: String,
                                   playlistSheetMusicDTO: PlaylistSheetMusicDTO
    ): ApiResult<ResultVO<Nothing>>{
        return playlistSheetDataSource.addMusicToPlaylist(
            token,playlistSheetMusicDTO
        )
    }

    /**
     * 修改歌单信息
     * @param token 用户Token
     * @param playlistSheetDTO 歌单信息DTO
     */
    suspend fun editPlaylist(token: String,
                             playlistSheetDTO: PlaylistSheetDTO
    ): ApiResult<ResultVO<Nothing>>{
        return playlistSheetDataSource.updatePlaylist(
            token,playlistSheetDTO
        )
    }

    /**
     * 创建歌单信息
     * @param playlistSheetDTO 歌单信息
     */
    suspend fun createToPlaylist(token: String,playlistSheetDTO: PlaylistSheetNoIdDTO): ApiResult<ResultVO<Nothing>>{
        return playlistSheetDataSource.createPlaylist(
            token,playlistSheetDTO
        )
    }

    /**
     * 删除歌单
     * @param token 用户Token
     * @param id 歌单ID
     */
    suspend fun deletePlaylist(token: String,
                             id: String): ApiResult<ResultVO<Nothing>>{
        return playlistSheetDataSource.deletePlaylist(
            token,id
        )
    }

    /**
     * 删除歌单
     * @param token 用户Token
     * @param playlistId 歌单ID
     * @param musicId 歌曲ID
     */
    suspend fun deletePlaylistMusic(token: String,
                               playlistId: String,musicId: String): ApiResult<ResultVO<Nothing>>{
        return playlistSheetDataSource.deleteMusicByPlaylist(
            token,musicId,playlistId
        )
    }

}