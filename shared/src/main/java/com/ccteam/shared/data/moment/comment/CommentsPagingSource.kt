package com.ccteam.shared.data.moment.comment

import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.moment.comment.CommentVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import com.ccteam.shared.api.MomentsDataSource
import com.ccteam.shared.data.BasePagingSource
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/29
 */
class CommentsPagingSource @Inject constructor(
    private val id: String,
    private val momentsDataSource: MomentsDataSource
): BasePagingSource<CommentVO.ReplyInfoVO>() {

    override suspend fun loadData(pageInfo: PageDTO): ApiResult<ResultVO<PageVO<CommentVO.ReplyInfoVO>>> {
        pageInfo.byPinyin = false
        return momentsDataSource.getRelayCommentInfoList(id,pageInfo)
    }
}