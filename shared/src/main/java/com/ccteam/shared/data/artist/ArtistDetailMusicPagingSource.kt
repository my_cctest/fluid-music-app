package com.ccteam.shared.data.artist

import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.music.MusicVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import com.ccteam.shared.api.MusicDataSource
import com.ccteam.shared.data.BasePagingSource

/**
 * @author Xiaoc
 * @since 2021/2/20
 */
class ArtistDetailMusicPagingSource(
    private val artistId: String,
    private val musicDataSource: MusicDataSource
): BasePagingSource<MusicVO>() {

    override suspend fun loadData(pageInfo: PageDTO): ApiResult<ResultVO<PageVO<MusicVO>>> {
        return musicDataSource.getMusicByArtistId(artistId,pageInfo)
    }
}