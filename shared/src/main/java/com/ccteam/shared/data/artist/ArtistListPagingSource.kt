package com.ccteam.shared.data.artist

import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.artist.ArtistSimplifyVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import com.ccteam.shared.api.ArtistDataSource
import com.ccteam.shared.data.BasePagingSource
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/2/15
 */
class ArtistListPagingSource @Inject constructor(
    private val artistDataSource: ArtistDataSource
): BasePagingSource<ArtistSimplifyVO>() {

    override suspend fun loadData(pageInfo: PageDTO): ApiResult<ResultVO<PageVO<ArtistSimplifyVO>>> {
        return artistDataSource.getArtistList(pageInfo.current,pageInfo.size)
    }
}