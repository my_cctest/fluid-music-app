package com.ccteam.shared.data.album

import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.album.AlbumSimplifyVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import com.ccteam.shared.api.AlbumDataSource
import com.ccteam.shared.data.BasePagingSource
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/2/17
 */
class AlbumListPagingSource @Inject constructor(
    private val albumDataSource: AlbumDataSource
): BasePagingSource<AlbumSimplifyVO>() {

    override suspend fun loadData(pageInfo: PageDTO): ApiResult<ResultVO<PageVO<AlbumSimplifyVO>>> {
        return albumDataSource.getAlbumList(pageInfo.current,pageInfo.size)
    }
}