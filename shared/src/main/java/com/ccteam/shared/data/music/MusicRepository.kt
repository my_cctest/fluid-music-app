package com.ccteam.shared.data.music

import com.ccteam.model.music.MusicDetailVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import com.ccteam.shared.api.MusicDataSource
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Xiaoc
 * @since 2021/3/17
 */
@Singleton
class MusicRepository @Inject constructor(
    private val musicDataSource: MusicDataSource
) {

    suspend fun getSuspendMusicInfoById(id: String): ApiResult<ResultVO<MusicDetailVO>> {
        return musicDataSource.getMusicInfoById(id = id)
    }
}