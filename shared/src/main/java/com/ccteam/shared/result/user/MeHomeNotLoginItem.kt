package com.ccteam.shared.result.user

import androidx.recyclerview.widget.DiffUtil
import com.ccteam.shared.result.BaseItemData
import com.ccteam.shared.result.BaseItemData.Companion.LAYOUT_ME_HOME_NOT_LOGIN

/**
 * 我的主页 未登录项
 * 该项不提供任何内容，仅作登录提醒
 */
data class MeHomeNotLoginItem(
    override var viewType: Int = LAYOUT_ME_HOME_NOT_LOGIN
): BaseItemData(){

    companion object{
        val differCallback = object: DiffUtil.ItemCallback<BaseItemData>(){

            override fun areItemsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
                return oldItem.viewType == newItem.viewType
            }

        }
    }
}