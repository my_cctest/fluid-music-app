package com.ccteam.shared.result.home


/**
 * 列表轮播图内容
 * @param imageUrl 图片地址
 * @param id 对应类型所需要的ID
 * @param type 对应轮播图类型，进行不同的操作
 */
data class OnlineMusicBanner(
    val imageUrl: String,
    val id: String,
    val type: Int
)
