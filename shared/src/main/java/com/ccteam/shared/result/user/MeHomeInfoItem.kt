package com.ccteam.shared.result.user

import android.os.Parcelable
import androidx.recyclerview.widget.DiffUtil
import com.ccteam.shared.result.BaseItemData
import kotlinx.parcelize.Parcelize

/**
 * 首页我的信息内容
 */
@Parcelize
data class MeHomeInfoItem(
    val userId: String,
    val userName: String,
    val userIntroduction: String,
    val userPhotoUrl: String,
    val playlistSheetCount: Int,
    val momentsCount: Int,
    override var viewType: Int = LAYOUT_ME_INFO_ITEM
): BaseItemData(),Parcelable {

    companion object{
        val differCallback = object: DiffUtil.ItemCallback<BaseItemData>(){

            override fun areItemsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
                if(oldItem is MeHomeInfoItem && newItem is MeHomeInfoItem){
                    return oldItem.userId == newItem.userId &&
                           oldItem.userPhotoUrl == newItem.userPhotoUrl
                }
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
                if(oldItem is MeHomeInfoItem && newItem is MeHomeInfoItem){
                    return oldItem.userId == newItem.userId &&
                            oldItem.userName == newItem.userName &&
                            oldItem.userIntroduction == newItem.userIntroduction &&
                            oldItem.userPhotoUrl == newItem.userPhotoUrl &&
                            oldItem.playlistSheetCount == newItem.playlistSheetCount &&
                            oldItem.momentsCount == newItem.momentsCount
                }
                return false
            }

        }
    }
}