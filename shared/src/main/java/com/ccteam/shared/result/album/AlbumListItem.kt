package com.ccteam.shared.result.album

import androidx.recyclerview.widget.DiffUtil
import com.ccteam.shared.result.BaseItemData

/**
 * 专辑列表页展示项
 * [com.ccteam.fluidmusic.R.layout.global_album_list_item]
 *
 * @param mediaId 访问下一个节点的mediaId（本地在线通用）
 * @param albumTitle 专辑标题
 * @param albumArtUri 专辑封面
 * @param artist 歌手名
 * @param subtitle 副标题（在歌手详情页可以是专辑歌曲数，其他地方可以是歌手名）
 * @param albumId 该专辑ID
 * @param artistId 歌手ID（仅本地可能需要）
 * @param year 专辑日期（仅本地可能需要）
 */
data class AlbumListItem(
    val mediaId: String,
    val albumTitle: String,
    val albumArtUri: String,
    val artist: String,
    val subtitle: String,
    val albumId: String,
    val artistId: String? = null,
    val year: String? = null,
    override var viewType: Int = LAYOUT_ALBUM_LIST_ITEM
): BaseItemData(){

    companion object{
        val differCallback = object: DiffUtil.ItemCallback<BaseItemData>(){

            override fun areItemsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
                if(oldItem is AlbumListItem && newItem is AlbumListItem){
                    return oldItem.mediaId == newItem.mediaId
                }
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
                if(oldItem is AlbumListItem && newItem is AlbumListItem){
                    return oldItem.mediaId == newItem.mediaId && oldItem.albumId == newItem.albumId
                }
                return false
            }

        }

        val differCallbackSingle = object: DiffUtil.ItemCallback<AlbumListItem>(){

            override fun areItemsTheSame(oldItem: AlbumListItem, newItem: AlbumListItem): Boolean {
                return oldItem.mediaId == newItem.mediaId
            }

            override fun areContentsTheSame(oldItem: AlbumListItem, newItem: AlbumListItem): Boolean {
                return oldItem.mediaId == newItem.mediaId && oldItem.albumId == newItem.albumId
            }

        }
    }
}
