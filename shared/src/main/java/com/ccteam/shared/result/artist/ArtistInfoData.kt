package com.ccteam.shared.result.artist

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

/**
 * 歌手最基本信息
 */
@Parcelize
data class ArtistInfoData(
    /**
     * 歌手ID
     */
    val id: String,

    /**
     * 歌手名
     */
    val artistName: String
): Parcelable