package com.ccteam.shared.result.album

import com.ccteam.shared.result.BaseItemData


/**
 * 专辑详情页尾部展示项
 * [com.ccteam.fluidmusic.R.layout.global_media_data_album_foot_item]
 *
 * @param trackNum 专辑曲目数
 * @param year 专辑发行年份
 * @param copyright 专辑版权所有信息
 */
data class AlbumDetailFootItem(
    val trackNum: Int,
    val year: String,
    val copyright: String,
    override var viewType: Int = LAYOUT_ALBUM_DETAIL_FOOT_ITEM
): BaseItemData()
