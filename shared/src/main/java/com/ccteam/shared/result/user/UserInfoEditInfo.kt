package com.ccteam.shared.result.user

/**
 * 个人信息修改数据
 */
data class UserInfoEditInfo(
    /**
     * 用户ID
     */
    val id: String,

    /**
     * 用户昵称
     */
    var userName: String,

    /**
     * 用户简介
     */
    var userIntroduction: String,

    /**
     * 用户生日
     */
    var userBirthday: String,

    /**
     * 用户性别
     */
    var userGender: String,

    /**
     * 用户头像地址
     */
    var userImgUrl: String
)