package com.ccteam.shared.result.playlistsheet

import androidx.recyclerview.widget.DiffUtil
import com.ccteam.shared.result.BaseItemData
import com.ccteam.shared.result.BaseItemData.Companion.LAYOUT_DEFAULT_MUSIC_ITEM

/**
 * 添加内容时显示的媒体内容项
 * 例如 歌单中添加歌曲 显示的歌曲内容，朋友圈选择歌曲专辑 等内容时
 *
 * @param mediaId 媒体ID
 * @param title 媒体标题
 * @param subtitle 媒体副标题
 * @param albumArtUri 媒体艺术图
 * @param isExist 是否已经存在于歌单（该项在歌单添加歌曲时适用）
 */
data class AddMediaItem(
    val mediaId: String,
    val title: String,
    val subtitle: String,
    val albumArtUri: String,
    val isExist: Boolean = false,
    override var viewType: Int = LAYOUT_DEFAULT_MUSIC_ITEM
): BaseItemData(){

    companion object{
        val differCallback = object: DiffUtil.ItemCallback<AddMediaItem>(){

            override fun areItemsTheSame(oldItem: AddMediaItem, newItem: AddMediaItem): Boolean {
                return oldItem.mediaId == newItem.mediaId
            }

            override fun areContentsTheSame(oldItem: AddMediaItem, newItem: AddMediaItem): Boolean {
                return oldItem.mediaId == newItem.mediaId
                        && oldItem.isExist == newItem.isExist && oldItem.subtitle == newItem.subtitle
            }

        }
    }

}