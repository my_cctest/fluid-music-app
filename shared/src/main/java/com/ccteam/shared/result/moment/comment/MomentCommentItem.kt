package com.ccteam.shared.result.moment.comment

import androidx.recyclerview.widget.DiffUtil
import com.ccteam.shared.result.BaseItemData
import com.ccteam.shared.result.BaseItemData.Companion.LAYOUT_MOMENTS_COMMENT

data class MomentCommentItem(
    val id: String,
    val momentId: String,
    val commentContent: String,
    val userId: String,
    val userName: String,
    val publishTime: String,
    val userPhotoUrl: String,
    val replayCount: Int,
    val type: Int,
    val replayList: List<MomentReplayItem>,
    override var viewType: Int = LAYOUT_MOMENTS_COMMENT
): BaseItemData(){

    companion object{
        val differCallback = object: DiffUtil.ItemCallback<MomentCommentItem>(){

            override fun areItemsTheSame(oldItem: MomentCommentItem, newItem: MomentCommentItem): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: MomentCommentItem, newItem: MomentCommentItem): Boolean {
                return oldItem.id == newItem.id && oldItem.commentContent == newItem.commentContent
            }

        }
    }
}