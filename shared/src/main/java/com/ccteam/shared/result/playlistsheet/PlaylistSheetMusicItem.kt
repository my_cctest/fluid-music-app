package com.ccteam.shared.result.playlistsheet

import androidx.recyclerview.widget.DiffUtil
import com.ccteam.shared.result.BaseItemData
import com.ccteam.shared.result.BaseItemData.Companion.LAYOUT_DEFAULT_MUSIC_ITEM
import com.ccteam.shared.result.artist.ArtistInfoData
import com.ccteam.shared.utils.PlaybackStatus

/**
 * @author Xiaoc
 * @since 2021/5/3
 */
/**
 * 歌单音乐列表的展示歌曲的Item
 * 该Item包含封面、标题、副标题、比特等等内容，最重要包含一个是否为当前用户编辑的歌曲
 *
 * @param id 歌单歌曲ID关联唯一值
 * @param mediaId 歌曲ID
 * @param title 标题
 * @param subtitle 副标题
 * @param albumArtUri 歌曲封面地址
 * @param bitrate 比特
 * @param playable 是否可播放
 * @param playbackRes 当前播放状态
 * @param isSelfUserMusic 是否是用户自己操作的歌曲，如果为true则可以对其进行一些权限操作
 */
data class PlaylistSheetMusicItem(
    val id: String,
    val mediaId: String,
    val title: String,
    val subtitle: String,
    val artistInfo: List<ArtistInfoData>,
    val albumId: String,
    val albumArtUri: String,
    val bitrate: String?,
    val playable: Boolean,
    @PlaybackStatus val playbackRes: Int,
    val isSelfUserMusic: Boolean,
    override var viewType: Int = LAYOUT_DEFAULT_MUSIC_ITEM
): BaseItemData(){

    companion object{
        val differCallback = object: DiffUtil.ItemCallback<BaseItemData>(){

            override fun areItemsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
                if(oldItem is PlaylistSheetMusicItem && newItem is PlaylistSheetMusicItem){
                    return oldItem.mediaId == newItem.mediaId && oldItem.id == newItem.id
                }
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
                if(oldItem is PlaylistSheetMusicItem && newItem is PlaylistSheetMusicItem){
                    return oldItem.mediaId == newItem.mediaId && oldItem.id == newItem.id &&
                            oldItem.playbackRes == newItem.playbackRes
                }
                return false
            }

        }

        val differCallbackSingle = object: DiffUtil.ItemCallback<PlaylistSheetMusicItem>(){

            override fun areItemsTheSame(oldItem: PlaylistSheetMusicItem, newItem: PlaylistSheetMusicItem): Boolean {
                return oldItem.mediaId == newItem.mediaId && oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: PlaylistSheetMusicItem, newItem: PlaylistSheetMusicItem): Boolean {
                return oldItem.mediaId == newItem.mediaId && oldItem.id == newItem.id &&
                        oldItem.playbackRes == newItem.playbackRes
            }

        }
    }
}
