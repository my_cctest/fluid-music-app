package com.ccteam.shared.result.home

import androidx.recyclerview.widget.DiffUtil
import com.ccteam.shared.result.BaseItemData
import com.ccteam.shared.result.BaseItemData.Companion.LAYOUT_ONLINE_MUSIC_HOME_SMALL_ITEM

/**
 * 首页小标题版内容
 * @param title 小标题
 * @param innerSmallItems 对应小标题中的内容
 */
data class OnlineMusicHomeSmallItem(
    val title: String,
    val type: Int,
    var innerSmallItems: List<OnlineMusicHomeInnerSmallItem>,
    val showMore: Boolean = true,
    override var viewType: Int = LAYOUT_ONLINE_MUSIC_HOME_SMALL_ITEM,
): BaseItemData(){

    companion object{
        const val SMALL_ITEM_ALBUM = 1
        const val SMALL_ITEM_MUSIC = 2
        const val SMALL_ITEM_PLAYLIST = 3

        val differCallback = object: DiffUtil.ItemCallback<BaseItemData>(){

            override fun areItemsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
                if(oldItem is OnlineMusicHomeSmallItem && newItem is OnlineMusicHomeSmallItem){
                    return oldItem == newItem
                } else if(oldItem is OnlineMusicHomeTypeBrowseItem && newItem is OnlineMusicHomeTypeBrowseItem){
                    return oldItem == newItem
                }
                return true
            }

            override fun areContentsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
                if(oldItem is OnlineMusicHomeSmallItem && newItem is OnlineMusicHomeSmallItem){
                    return oldItem.innerSmallItems == newItem.innerSmallItems
                } else if(oldItem is OnlineMusicHomeTypeBrowseItem && newItem is OnlineMusicHomeTypeBrowseItem){
                    return oldItem.banners == newItem.banners
                }
                return true
            }

        }

    }
}

data class OnlineMusicHomeInnerSmallItem(
    val itemId: String,
    val imageUrl: String,
    val title: String,
    val subtitle: String,
    val playable: Boolean,
    val playbackRes: Int,
    val parentId: String? = null
){
    companion object{
        val differCallback = object: DiffUtil.ItemCallback<OnlineMusicHomeInnerSmallItem>(){

            override fun areItemsTheSame(oldItem: OnlineMusicHomeInnerSmallItem, newItem: OnlineMusicHomeInnerSmallItem): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: OnlineMusicHomeInnerSmallItem, newItem: OnlineMusicHomeInnerSmallItem): Boolean {
                return oldItem.itemId == newItem.itemId && oldItem.imageUrl == newItem.imageUrl
                        && oldItem.playbackRes == newItem.playbackRes

            }

        }

    }
}