package com.ccteam.shared.result.album

import android.net.Uri
import androidx.recyclerview.widget.DiffUtil
import com.ccteam.shared.result.BaseItemData
import com.ccteam.shared.result.artist.ArtistInfoData

/**
 * 专辑详情页专辑歌曲展示项
 * [com.ccteam.fluidmusic.R.layout.global_media_data_album_music_item]
 *
 * @param mediaId 歌曲ID
 * @param artist 歌手
 * @param title 歌曲标题
 * @param artistInfo 歌手信息列表
 * @param albumId 专辑ID
 * @param numTrack 歌曲所在曲目顺序
 * @param bitrate 比特率
 * @param playable 是否可播放
 * @param playbackRes 当前播放状态
 * @param mediaUri 本地用（用于taglib查找标签信息）
 */
data class AlbumDetailMusicItem(
    val mediaId: String,
    val artist: String,
    val title: String,
    val artistInfo: List<ArtistInfoData>,
    val albumId: String,
    val numTrack: Int,
    val bitrate: String?,
    val playable: Boolean,
    val playbackRes: Int,
    val mediaUri: Uri = Uri.EMPTY,
    override var viewType: Int = LAYOUT_ALBUM_DETAIL_MUSIC_ITEM
): BaseItemData(){

    companion object{
        val differCallback = object: DiffUtil.ItemCallback<BaseItemData>(){

            override fun areItemsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
                if(oldItem is AlbumDetailMusicItem && newItem is AlbumDetailMusicItem){
                    return oldItem.mediaId == newItem.mediaId
                } else if(oldItem is AlbumDetailHeaderItem && newItem is AlbumDetailHeaderItem){
                    return oldItem.albumId == newItem.albumId
                } else if(oldItem is AlbumDetailFootItem && newItem is AlbumDetailFootItem){
                    return oldItem.trackNum == newItem.trackNum
                }
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
                if(oldItem is AlbumDetailMusicItem && newItem is AlbumDetailMusicItem){
                    return oldItem.mediaId == newItem.mediaId && oldItem.playbackRes == newItem.playbackRes
                } else if(oldItem is AlbumDetailHeaderItem && newItem is AlbumDetailHeaderItem){
                    return oldItem.albumId == newItem.albumId
                } else if(oldItem is AlbumDetailFootItem && newItem is AlbumDetailFootItem){
                    return oldItem.trackNum == newItem.trackNum
                }
                return false
            }

        }
    }
}

