package com.ccteam.shared.result.artist

import android.os.Parcelable
import androidx.recyclerview.widget.DiffUtil
import com.ccteam.shared.result.BaseItemData
import kotlinx.parcelize.Parcelize

/**
 * 歌手列表项内容
 * [com.ccteam.fluidmusic.R.layout.global_artist_list_item]
 *
 * @param mediaId 访问下一个节点的mediaId（本地在线通用）
 * @param artistName 歌手名
 * @param subtitle 副标题
 * @param artistId 歌手ID
 */
@Parcelize
data class ArtistListItem(
    val mediaId: String,
    val artistName: String,
    val artistPhoto: String,
    val subtitle: ArtistSubtitle?,
    val artistId: String,
    override var viewType: Int = LAYOUT_ARTIST_LIST_ITEM
): BaseItemData(), Parcelable {

    companion object{
        val differCallback = object: DiffUtil.ItemCallback<BaseItemData>(){

            override fun areItemsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
                if(oldItem is ArtistListItem && newItem is ArtistListItem){
                    return oldItem.mediaId == newItem.mediaId
                }
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
                if(oldItem is ArtistListItem && newItem is ArtistListItem){
                    return oldItem.mediaId == newItem.mediaId
                            && oldItem.artistId == newItem.artistId && oldItem.subtitle == newItem.subtitle
                }
                return false
            }

        }

        val differCallbackSingle = object: DiffUtil.ItemCallback<ArtistListItem>(){

            override fun areItemsTheSame(oldItem: ArtistListItem, newItem: ArtistListItem): Boolean {
                return oldItem.mediaId == newItem.mediaId
            }

            override fun areContentsTheSame(oldItem: ArtistListItem, newItem: ArtistListItem): Boolean {
                return oldItem.mediaId == newItem.mediaId
                        && oldItem.artistId == newItem.artistId && oldItem.subtitle == newItem.subtitle
            }

        }
    }

    @Parcelize
    data class ArtistSubtitle(
        val trackNum: Int?,
        val albumNum: Int?
    ): Parcelable
}