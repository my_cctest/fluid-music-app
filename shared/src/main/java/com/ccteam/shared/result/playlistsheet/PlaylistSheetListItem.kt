package com.ccteam.shared.result.playlistsheet

import androidx.recyclerview.widget.DiffUtil
import com.ccteam.shared.result.BaseItemData

data class PlaylistSheetListItem(
    val id: String,
    val title: String,
    val imgUrl: String,
    override var viewType: Int = LAYOUT_PLAYLIST_SHEET_LIST_ITEM,
): BaseItemData(){

    companion object{
        val differCallback = object: DiffUtil.ItemCallback<PlaylistSheetListItem>(){

            override fun areItemsTheSame(oldItem: PlaylistSheetListItem, newItem: PlaylistSheetListItem): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: PlaylistSheetListItem, newItem: PlaylistSheetListItem): Boolean {
                return oldItem.id == newItem.id && oldItem.imgUrl == newItem.imgUrl && oldItem.title == newItem.title
            }

        }
    }
}