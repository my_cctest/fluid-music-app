package com.ccteam.shared.result.music

import android.net.Uri
import androidx.recyclerview.widget.DiffUtil
import com.ccteam.shared.result.BaseItemData
import com.ccteam.shared.result.BaseItemData.Companion.LAYOUT_DEFAULT_MUSIC_ITEM
import com.ccteam.shared.result.artist.ArtistInfoData
import com.ccteam.shared.utils.PlaybackStatus

/**
 * 默认的展示歌曲的Item
 * 该Item包含封面、标题、副标题、比特等等内容
 *
 * @param mediaId 歌曲ID
 * @param title 标题
 * @param subtitle 副标题
 * @param albumArtUri 歌曲封面地址
 * @param bitrate 比特
 * @param playable 是否可播放
 * @param playbackRes 当前播放状态
 * @param mediaUri 文件播放的Uri（本地使用，用于Taglib获取信息）
 */
data class DefaultMusicItem(
    val mediaId: String,
    val title: String,
    val subtitle: String,
    val artistInfo: List<ArtistInfoData>,
    val albumId: String,
    val albumArtUri: String,
    val bitrate: String?,
    val playable: Boolean,
    @PlaybackStatus val playbackRes: Int,
    val mediaUri: Uri = Uri.EMPTY,
    override var viewType: Int = LAYOUT_DEFAULT_MUSIC_ITEM
): BaseItemData(){

    companion object{
        val differCallback = object: DiffUtil.ItemCallback<BaseItemData>(){

            override fun areItemsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
                if(oldItem is DefaultMusicItem && newItem is DefaultMusicItem){
                    return oldItem.mediaId == newItem.mediaId
                }
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
                if(oldItem is DefaultMusicItem && newItem is DefaultMusicItem){
                    return oldItem.mediaId == newItem.mediaId && oldItem.title == newItem.title &&
                            oldItem.subtitle == newItem.subtitle &&
                            oldItem.playbackRes == newItem.playbackRes
                }
                return false
            }

        }

        val differCallbackSingle = object: DiffUtil.ItemCallback<DefaultMusicItem>(){

            override fun areItemsTheSame(oldItem: DefaultMusicItem, newItem: DefaultMusicItem): Boolean {
                return oldItem.mediaId == newItem.mediaId
            }

            override fun areContentsTheSame(oldItem: DefaultMusicItem, newItem: DefaultMusicItem): Boolean {
                return oldItem.mediaId == newItem.mediaId &&
                        oldItem.playbackRes == newItem.playbackRes
            }

        }
    }
}
