package com.ccteam.shared.result.moment.comment

import androidx.recyclerview.widget.DiffUtil
import com.ccteam.shared.result.BaseItemData
import com.ccteam.shared.result.BaseItemData.Companion.LAYOUT_MOMENTS_COMMENT_RELAY

data class MomentReplayItem(
    val id: String,
    val momentId: String,
    val commentId: String,
    val commentContent: String,
    val userId: String,
    val userName: String,
    val publishTime: String,
    val userPhotoUrl: String,
    val toUserId: String,
    val toUserName: String,
    val replayCount: Int,
    val type: Int,
    override var viewType: Int = LAYOUT_MOMENTS_COMMENT_RELAY
): BaseItemData(){

    companion object{
        val differCallback = object: DiffUtil.ItemCallback<MomentReplayItem>(){

            override fun areItemsTheSame(oldItem: MomentReplayItem, newItem: MomentReplayItem): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: MomentReplayItem, newItem: MomentReplayItem): Boolean {
                return oldItem.id == newItem.id && oldItem.commentContent == newItem.commentContent
            }

        }
    }
}