package com.ccteam.shared.result.home

import com.ccteam.shared.result.BaseItemData
import com.ccteam.shared.result.BaseItemData.Companion.LAYOUT_ONLINE_MUSIC_HOME_TYPE_BROWSE

/**
 * @author Xiaoc
 * @since 2021/5/3
 */
/**
 * 包含Banner轮播图的按类型分类内容
 */
data class OnlineMusicHomeTypeBrowseItem(
    val banners: List<OnlineMusicBanner>,
    override var viewType: Int = LAYOUT_ONLINE_MUSIC_HOME_TYPE_BROWSE,
): BaseItemData()
