package com.ccteam.shared.result

/**
 * @author Xiaoc
 * @since 2021/5/3
 */
abstract class BaseItemData{
    abstract var viewType: Int


    companion object{
        /**
         * 空布局
         */
        const val LAYOUT_EMPTY = -1
        /**
         * 歌手详情页的歌手信息
         */
        const val LAYOUT_ARTIST_INFO = 2

        /**
         * 全局Toolbar大标题
         */
        const val LAYOUT_TOOLBAR_TITLE = 3

        /**
         * 在线音乐首页类型项
         */
        const val LAYOUT_ONLINE_MUSIC_HOME_TYPE_BROWSE = 4

        /**
         * 在线音乐首页小内容项
         */
        const val LAYOUT_ONLINE_MUSIC_HOME_SMALL_ITEM = 5

        /**
         * 专辑列表子项
         */
        const val LAYOUT_ALBUM_LIST_ITEM = 6

        /**
         * 专辑详情页头部
         */
        const val LAYOUT_ALBUM_DETAIL_HEADER = 7

        /**
         * 专辑详情歌曲列表
         */
        const val LAYOUT_ALBUM_DETAIL_MUSIC_ITEM = 8

        /**
         * 我的界面首页基本信息项
         */
        const val LAYOUT_ME_INFO_ITEM = 9

        /**
         * 歌单列表子项
         */
        const val LAYOUT_PLAYLIST_SHEET_LIST_ITEM = 10

        /**
         * 朋友圈列表子项
         */
        const val LAYOUT_MOMENTS_ITEM = 11

        /**
         * 朋友圈列表子项
         */
        const val LAYOUT_ARTIST_LIST_ITEM = 12

        /**
         * 专辑列表子项（本地音乐专辑布局）
         */
        const val LAYOUT_ALBUM_LIST_ITEM_SMALL = 13

        /**
         * 专辑详情尾部项
         */
        const val LAYOUT_ALBUM_DETAIL_FOOT_ITEM = 14

        /**
         * 默认的歌曲项
         */
        const val LAYOUT_DEFAULT_MUSIC_ITEM = 15

        /**
         * 默认的歌曲项
         */
        const val LAYOUT_LYRICS_BODY_ITEM = 20

        /**
         * 默认的歌曲项
         */
        const val LAYOUT_LYRICS_STRING_ITEM = 21


        /**
         * 歌单列表子项
         */
        const val LAYOUT_PLAYLIST_SHEET_DETAIL_HEADER_ITEM = 16

        /**
         * 我的主页未登录项
         */
        const val LAYOUT_ME_HOME_NOT_LOGIN = 17


        /**
         * 朋友圈主评论项
         */
        const val LAYOUT_MOMENTS_COMMENT = 18


        /**
         * 朋友圈评论回复项
         */
        const val LAYOUT_MOMENTS_COMMENT_RELAY = 19
    }
}