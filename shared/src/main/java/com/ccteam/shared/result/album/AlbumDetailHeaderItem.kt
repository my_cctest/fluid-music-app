package com.ccteam.shared.result.album

import android.os.Parcelable
import com.ccteam.shared.result.BaseItemData
import kotlinx.parcelize.Parcelize

/**
 * 专辑详情页头部展示项
 * [com.ccteam.fluidmusic.R.layout.global_media_data_album_head_item]
 *
 * @param albumId 专辑ID
 * @param albumTitle 专辑标题
 * @param albumArtUri 专辑封面
 * @param artist 歌手名
 * @param subtitle 副标题（比如年份、流派等）
 * @param description 专辑描述（本地音乐无此内容）
 * @param artistId 歌手ID
 */
@Parcelize
data class AlbumDetailHeaderItem(
    val albumId: String,
    val albumTitle: String,
    val albumArtUri: String,
    val artist: String,
    val subtitle: String?,
    val description: String?,
    val artistId: String,
    override var viewType: Int = LAYOUT_ALBUM_DETAIL_HEADER
): BaseItemData(), Parcelable
