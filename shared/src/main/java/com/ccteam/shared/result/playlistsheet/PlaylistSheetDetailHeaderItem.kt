package com.ccteam.shared.result.playlistsheet

import android.os.Parcelable
import androidx.recyclerview.widget.DiffUtil
import com.ccteam.shared.result.BaseItemData
import kotlinx.parcelize.Parcelize

/**
 * 歌单详情页头部内容
 * @param id 歌单ID
 * @param playlistSheetTitle 歌单标题
 * @param playlistSheetImgUrl 歌单封面
 * @param userId 用户ID
 * @param userPhotoUrl 用户头像
 * @param userName 用户昵称
 * @param introduction 歌单简介
 * @param type 歌单类型
 * @param enableAddMusic 是否开启添加歌曲功能
 * @param enableEditPlaylistSheet 是否开启编辑歌单功能
 * @param enableDeletePlaylistSheet 是否开启删除歌单功能
 */
@Parcelize
data class PlaylistSheetDetailHeaderItem(
    val id: String,
    val playlistSheetTitle: String,
    val playlistSheetImgUrl: String,
    val userId: String,
    val userPhotoUrl: String,
    val userName: String,
    val introduction: String,
    val type: Int,
    val enableAddMusic: Boolean = false,
    val enableEditPlaylistSheet: Boolean = false,
    val enableDeletePlaylistSheet: Boolean = false,
    override var viewType: Int = LAYOUT_PLAYLIST_SHEET_DETAIL_HEADER_ITEM,
): BaseItemData(), Parcelable {

    companion object{
        val differCallback = object: DiffUtil.ItemCallback<PlaylistSheetDetailHeaderItem>(){

            override fun areItemsTheSame(oldItem: PlaylistSheetDetailHeaderItem, newItem: PlaylistSheetDetailHeaderItem): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: PlaylistSheetDetailHeaderItem, newItem: PlaylistSheetDetailHeaderItem): Boolean {
                return oldItem.id == newItem.id && oldItem.playlistSheetTitle == newItem.playlistSheetTitle
            }
        }
    }
}