package com.ccteam.shared.result.moment

import android.os.Parcelable
import androidx.recyclerview.widget.DiffUtil
import com.ccteam.shared.result.BaseItemData
import kotlinx.parcelize.Parcelize

/**
 * 朋友圈信息项
 * @param id 朋友圈ID
 * @param userId 用户ID
 * @param userName 用户名
 * @param userPhotoUrl 用户头像
 * @param publishTime 发布日期
 * @param momentsContent 朋友圈内容
 * @param momentsImgList 朋友圈图片列表
 * @param shareContent 分享的内容对象，可为空，如果为空不显示分享内容
 * @param upNum 点赞数
 * @param commentNum 评论数
 */
data class MomentListItem(
    val id: String,
    val userId: String,
    val userName: String,
    val userPhotoUrl: String,
    val publishTime: String,
    val momentsContent: String,
    val momentsImgList: List<MomentImageItem>,
    val shareContent: MomentShareItem?,
    val upNum: Int,
    val commentNum: Int,
    override var viewType: Int = LAYOUT_MOMENTS_ITEM
): BaseItemData(){

    companion object{
        val differCallback = object: DiffUtil.ItemCallback<MomentListItem>(){

            override fun areItemsTheSame(oldItem: MomentListItem, newItem: MomentListItem): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: MomentListItem, newItem: MomentListItem): Boolean {
                return oldItem.id == newItem.id && oldItem.upNum == newItem.upNum
                        && oldItem.commentNum == newItem.commentNum && oldItem.momentsImgList.size == newItem.momentsImgList.size
            }

        }
    }

    /**
     * 朋友圈分享内容的对象内容（例如分享歌曲、歌单、专辑）
     * @param id 内容ID
     * @param type 内容类型
     * @param title 内容标题
     * @param subTitle 内容副标题
     * @param imgUrl 内容封面
     */
    @Parcelize
    data class MomentShareItem(
        val id: String,
        val type: Int,
        val title: String,
        val subTitle: String,
        val imgUrl: String
    ): Parcelable

    @Parcelize
    data class MomentImageItem(
        val id: String,
        val momentId: String,
        val imageUrl: String
    ): Parcelable
}