package com.ccteam.shared.result.title

import androidx.recyclerview.widget.DiffUtil
import com.ccteam.shared.result.BaseItemData

/**
 * @author Xiaoc
 * @since 2021/5/3
 */
/**
 * 全局通用的Toolbar标题项
 */
data class ToolbarTitleItem(
    val title: String,
    override var viewType: Int = LAYOUT_TOOLBAR_TITLE,
): BaseItemData(){

    companion object{
        val differCallback = object: DiffUtil.ItemCallback<ToolbarTitleItem>(){

            override fun areItemsTheSame(oldItem: ToolbarTitleItem, newItem: ToolbarTitleItem): Boolean {
                return oldItem.title == newItem.title
            }

            override fun areContentsTheSame(oldItem: ToolbarTitleItem, newItem: ToolbarTitleItem): Boolean {
                return oldItem.title == newItem.title
            }

        }
    }

}
