cmake_minimum_required(VERSION 3.7.1 FATAL_ERROR)

# Enable C++11 features.
set(CMAKE_CXX_STANDARD 11)

project(libtagJNI C CXX)

# 设置taglib本地路径变量
set(taglib_location "${CMAKE_CURRENT_SOURCE_DIR}/taglib")

# 将tag加入so库
add_library(
        tag
        STATIC
        IMPORTED)

# 制定tag库的so文件
set_target_properties(
        tag
        PROPERTIES
        IMPORTED_LOCATION
        ${taglib_location}/build/taglib/${ANDROID_ABI}/libtag.so)

include_directories(${taglib_location})
# 加入Androidlog包
find_library(android_log_lib log)

# 将jni_cpp加入到tagJNI库中
add_library(tagJNI
        SHARED
        taglib_jni.cpp)

# so库链接tagJNI、android、tag
target_link_libraries(
        tagJNI
        PRIVATE android
        PRIVATE tag
        PRIVATE ${android_log_lib})