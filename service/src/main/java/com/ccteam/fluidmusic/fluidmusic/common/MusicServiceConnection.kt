package com.ccteam.fluidmusic.fluidmusic.common

import android.content.ComponentName
import android.content.Context
import android.os.Bundle
import android.support.v4.media.MediaBrowserCompat
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.support.v4.media.session.PlaybackStateCompat.REPEAT_MODE_NONE
import android.support.v4.media.session.PlaybackStateCompat.SHUFFLE_MODE_NONE
import androidx.lifecycle.MutableLiveData
import com.ccteam.fluidmusic.fluidmusic.media.FluidMusicService
import com.ccteam.fluidmusic.fluidmusic.media.extensions.*
import com.ccteam.fluidmusic.fluidmusic.media.library.BrowseTree
import com.orhanobut.logger.Logger
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

/**
 *
 * @author Xiaoc
 * @since 2020/12/28
 *
 * 该类为单例类，用来管理与 [androidx.media.MediaBrowserServiceCompat] 间的连接，而它的具体服务类
 * 就是 [com.ccteam.fluidmusic.fluidmusic.media.FluidMusicService]
 *
 * 该类存储着基本的可能跨Activity的信息，如 当前播放的歌曲信息、当前播放状态、与Service的连接状态等等
 * 随时监听着Media的变化
 * 他们都使用 [MutableLiveData] 进行存储，方便观察其变化
 *
 * 同时，该类还存在一些基本方法去订阅或者使用一些对媒体的操作
 */
@Deprecated("被废弃，请使用Flow版本的MusicServiceConnectionFlow")
class MusicServiceConnection constructor(
    @ApplicationContext context: Context,
    private val browseTree: BrowseTree) {

//    private val serviceComponent = ComponentName(context,FluidMusicService::class.java)
//
//    val localRootId : String get() = mediaBrowser.root
//
//    /**
//     * 是否与 [FluidMusicService] 连接成功，连接成功为true
//     */
//    val isConnected = MutableLiveData<Boolean>().apply {
//        postValue(false)
//    }
//
//    /**
//     * 当前播放内容信息
//     */
//    val nowPlaying = MutableLiveData<MediaMetadataCompat>().apply {
//        postValue(METADATA_NOTHING_PLAYING)
//    }
//
//    /**
//     * 当前播放状态信息
//     */
//    val nowPlaybackState = MutableLiveData<PlaybackStateCompat>().apply {
//        postValue(EMPTY_PLAYBACK_STATE)
//    }
//
//    val nowPlaylistQueue = MutableLiveData<List<MediaMetadataCompat>>().apply {
//        postValue(mutableListOf())
//    }
//
//    val nowRepeatMode = MutableLiveData<@PlaybackStateCompat.RepeatMode Int>().apply {
//        postValue(REPEAT_MODE_NONE)
//    }
//
//    val nowShuffleMode = MutableLiveData<@PlaybackStateCompat.ShuffleMode Int>().apply {
//        postValue(SHUFFLE_MODE_NONE)
//    }
//
//
//    private val mediaBrowserConnectionCallback = MediaBrowserConnectionCallback(context)
//    private val mediaBrowser = MediaBrowserCompat(
//        context, serviceComponent,mediaBrowserConnectionCallback,null
//    ).apply {
//        connect()
//    }
//
//    private val mediaControllerCallback = MediaControllerCallback()
//
//    private val playlistCallback = PlaylistCallback()
//
//    lateinit var mediaController: MediaControllerCompat
//
//    fun subscribe(parentId: String, callback: MediaBrowserCompat.SubscriptionCallback,bundle: Bundle) {
//        mediaBrowser.subscribe(parentId, bundle, callback)
//    }
//
//    fun subscribe(parentId: String, callback: MediaBrowserCompat.SubscriptionCallback) {
//        mediaBrowser.subscribe(parentId, callback)
//    }
//
//    fun unsubscribe(parentId: String, callback: MediaBrowserCompat.SubscriptionCallback) {
//        mediaBrowser.unsubscribe(parentId, callback)
//    }
//
//    fun addOnlineMusicItems(parentId: String,items: List<MediaMetadataCompat>){
//        browseTree.addOnlineMediaItems(parentId,items)
//    }
//
//    private inner class MediaBrowserConnectionCallback(private val context: Context):
//        MediaBrowserCompat.ConnectionCallback(){
//
//        override fun onConnected() {
//            mediaController = MediaControllerCompat(context,mediaBrowser.sessionToken).apply {
//                registerCallback(mediaControllerCallback)
//            }
//            PlaylistManager.setPlaylistCallback(playlistCallback)
//
//            isConnected.postValue(true)
//
//            // 连接成功后准备播放列表，会调用 onPrepare 进行准备
//            mediaController.transportControls.prepare()
//        }
//
//        override fun onConnectionSuspended() {
//            isConnected.postValue(false)
//            PlaylistManager.clearPlaylistCallback()
//        }
//
//        override fun onConnectionFailed() {
//            isConnected.postValue(false)
//            PlaylistManager.clearPlaylistCallback()
//        }
//    }
//
//    private inner class MediaControllerCallback: MediaControllerCompat.Callback(){
//
//        override fun onPlaybackStateChanged(state: PlaybackStateCompat?) {
//            Logger.d("播放状态改变 ${state?.state}")
//            nowPlaybackState.postValue(state)
//        }
//
//        override fun onMetadataChanged(metadata: MediaMetadataCompat?) {
//            Logger.d("播放内容改变 当前播放的歌曲播放列表所在索引ID：${metadata?.title}")
//            nowPlaying.postValue(
//                if(metadata?.id == null){
//                    METADATA_NOTHING_PLAYING
//                } else {
//                    metadata
//                }
//            )
//        }
//
//        override fun onShuffleModeChanged(shuffleMode: Int) {
//            Logger.d("随机播放模式改变：${shuffleMode}")
//            nowShuffleMode.postValue(shuffleMode)
//        }
//
//        override fun onRepeatModeChanged(repeatMode: Int) {
//            Logger.d("重复播放模式改变：${repeatMode}")
//            nowRepeatMode.postValue(repeatMode)
//        }
//
//        override fun onSessionDestroyed() {
//            mediaBrowserConnectionCallback.onConnectionSuspended()
//        }
//    }
//
//    private inner class PlaylistCallback: PlaylistManager.PlaylistCallback{
//
//        override fun playlistQueueChanged(playlistQueue: List<MediaMetadataCompat>) {
//            Logger.d("播放列表发生变化")
//            nowPlaylistQueue.postValue(playlistQueue)
//        }
//
//    }
}

//val METADATA_NOTHING_PLAYING: MediaMetadataCompat = MediaMetadataCompat.Builder().apply {
//    id = ""
//    title = "纵想音乐的乐趣"
//    artist = "Fluid Music"
//    album = "ccteam"
//    duration = 1
//    queueId = -1
//}.build()
//
///**
// * 闲置时的播放状态
// */
//val EMPTY_PLAYBACK_STATE: PlaybackStateCompat = PlaybackStateCompat.Builder()
//    .setState(PlaybackStateCompat.STATE_NONE,0,0f).build()