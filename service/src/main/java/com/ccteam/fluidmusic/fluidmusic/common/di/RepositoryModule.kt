package com.ccteam.fluidmusic.fluidmusic.common.di

import android.content.Context
import com.ccteam.fluidmusic.fluidmusic.common.repositories.impl.ProtoDataStoreRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * @author Xiaoc
 * @since 2021/1/23
 *
 * 仓库Hilt注入的Module
 * 用于仓库层的注入方法告知
 */
@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    /**
     * 提供ProDataStore仓库单例类
     */
    @Provides
    @Singleton
    fun provideProtoDataStoreRepository(
        @ApplicationContext context: Context): ProtoDataStoreRepository{
        return ProtoDataStoreRepository(context)
    }
}