package com.ccteam.fluidmusic.fluidmusic.room.dao

import androidx.room.*
import com.ccteam.fluidmusic.fluidmusic.room.entity.MusicEntity

/**
 * @author Xiaoc
 * @since 2020.1.22
 *
 * 播放列表播放内容的数据库操作
 * 使用suspend修饰后，该执行将会自动被Room切换到内置的IO执行器中执行
 * 不会阻塞UI
 */
@Dao
interface MusicDao {

    @Transaction @Query("SELECT * FROM playlist_music ORDER BY queueId ASC")
    suspend fun getAllMusic(): List<MusicEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMusicList(musicList: List<MusicEntity>)

    @Delete
    suspend fun deleteMusicList(musicList: List<MusicEntity>)

    @Transaction
    suspend fun deleteAllAndInsertInTransaction(musicList: List<MusicEntity>){
        // 当中的方法将运行在一个事务中
        deleteMusicList(getAllMusic())
        insertMusicList(musicList)
    }
}