package com.ccteam.fluidmusic.fluidmusic.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.ccteam.fluidmusic.fluidmusic.room.dao.MusicDao
import com.ccteam.fluidmusic.fluidmusic.room.entity.MusicEntity

@Database(entities = [MusicEntity::class], version = 1,exportSchema = false)
abstract class AppDatabase: RoomDatabase() {

    companion object{
        private const val DATABASE_NAME = "fluid-music.db"

        @Volatile
        private var INSTANCE: AppDatabase?= null

        fun getDatabase(applicationContext: Context): AppDatabase{
            val tempInstance = INSTANCE
            if(tempInstance != null){
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(
                    applicationContext,
                    AppDatabase::class.java,
                    DATABASE_NAME
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }

    abstract fun musicDao(): MusicDao
}