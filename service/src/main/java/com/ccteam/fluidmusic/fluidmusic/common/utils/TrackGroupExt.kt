package com.ccteam.fluidmusic.fluidmusic.common.utils

import com.google.android.exoplayer2.Format
import com.google.android.exoplayer2.source.TrackGroup
import com.google.android.exoplayer2.source.TrackGroupArray
import kotlin.reflect.KClass

/**
 * @author Xiaoc
 * @since 2021/4/21
 */
inline fun <R> TrackGroupArray.mapIndexed(transform: (Int,TrackGroup) -> List<R>): List<R>{
    val list = mutableListOf<R>()

    for(index in 0 until length){
        list.addAll(transform(index,get(index)))
    }
    return list
}

inline fun <R> TrackGroup.mapIndexed(transform: (Int,Format) -> R): List<R>{
    val list = mutableListOf<R>()

    for(index in 0 until length){
        list.add(transform(index,getFormat(index)))
    }
    return list
}