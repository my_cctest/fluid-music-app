package com.ccteam.fluidmusic.fluidmusic.common.model

/**
 * 当前播放内容的存储模型
 * @param currentQueueId 当前播放的Id
 * @param currentPosition 当前播放的位置
 * @param currentRepeatMode 当前设置的播放模式
 * @param currentShuffleMode 是否开启了随机播放
 */
data class CurrentPlayingModel(
    val currentQueueId: Long,
    val currentPosition: Long,
    val currentRepeatMode: Int,
    val currentShuffleMode: Boolean
)