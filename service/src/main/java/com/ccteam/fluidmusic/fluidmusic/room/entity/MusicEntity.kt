package com.ccteam.fluidmusic.fluidmusic.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "playlist_music")
data class MusicEntity(
    @PrimaryKey val queueId: Long,
    @ColumnInfo(name = "media_id") val mediaId: String,
    @ColumnInfo val title: String,
    @ColumnInfo val album: String,
    @ColumnInfo(name = "album_id") val albumId: String,
    @ColumnInfo val duration: Long,
    @ColumnInfo(name = "track_num") val trackNum: Long,
    @ColumnInfo val year: Long,
    @ColumnInfo val artist: String,
    @ColumnInfo(name = "artist_id") val artistId: String,
    @ColumnInfo(name = "media_uri") val mediaUri: String,
    @ColumnInfo(name = "album_art_uri") val albumArtUri: String
)