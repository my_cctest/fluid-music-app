package com.ccteam.fluidmusic.fluidmusic.common.utils

import android.content.ContentResolver
import android.net.Uri
import android.provider.MediaStore

/**
 * @author Xiaoc
 * @since 2021/4/9
 *
 * Uri相关简单的扩展方法
 */

/**
 * 检测当前Uri是否为本地音乐媒体资源
 * 形如 content://media/XXX/audio/XXX/XXX
 */
fun Uri?.isLocalMedia(): Boolean{
    return if(this == null) {
        false
    } else {
        ContentResolver.SCHEME_CONTENT == scheme
                && MediaStore.AUTHORITY == authority
                && pathSegments.contains("audio")
    }
}

/**
 * 检测当前Uri是否为本地音乐媒体资源且是否版本号高于Android Q（分区存储）
 * 形如 content://media/XXX/audio/XXX/XXX 且 版本号 高于 Android Q
 */
fun Uri?.isLocalMediaSdk29(): Boolean{
    return isLocalMedia() && android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q
}