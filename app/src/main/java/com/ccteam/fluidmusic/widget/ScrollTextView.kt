package com.ccteam.fluidmusic.widget

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView

/**
 * @author Xiaoc
 * @since 2021/1/2
 *
 * 滚动TextView
 * 该TextView始终返回焦点true，防止其他控件触发焦点后滚动还原的问题
 *
 **/
class ScrollTextView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0): AppCompatTextView(context,attrs,defStyleAttr) {

    override fun isFocused(): Boolean = true
}