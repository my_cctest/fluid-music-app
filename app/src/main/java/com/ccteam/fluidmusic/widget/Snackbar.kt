package com.ccteam.fluidmusic.widget

import android.view.View
import android.widget.TextView
import androidx.annotation.StringRes
import com.ccteam.fluidmusic.R
import com.ccteam.fluidmusic.utils.getAttrColor
import com.google.android.material.snackbar.Snackbar

/**
 * @author Xiaoc
 * @since 2021/4/30
 *
 * 自定义的Snackbar工具集，因为默认Snackbar的自定义程度太低
 */
/**
 * 通过View调用Snackbar，默认展示在屏幕下方，带有部分底部margin
 * 改变为浅色背景和深色文字
 */
fun View.snackbar(@StringRes resId: Int,
                  type: Int = SNACKBAR_PLAY_BAR,
                  duration: Int = Snackbar.LENGTH_SHORT): Snackbar{
    return snackbar(this.resources.getString(resId),type,duration)
}

fun View.snackbar(text: String,
                  type: Int = SNACKBAR_PLAY_BAR,
                  duration: Int = Snackbar.LENGTH_SHORT): Snackbar{
    val snackbar = Snackbar.make(this,text,duration)
    val transitionY = when(type){
        SNACKBAR_ALL_BOTTOM_BAR ->{
            this.resources.getDimension(R.dimen.bottomBarAllHeight) + 25
        }
        SNACKBAR_PLAY_BAR ->{
            this.resources.getDimension(R.dimen.bottomPlayerBarHeight) + 25
        }
        else ->{
            this.resources.getDimension(R.dimen.space_global_horizontal_small) + 25
        }
    }
    val snackbarView = snackbar.view
    snackbarView.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
        .setTextColor(snackbarView.getAttrColor(R.attr.colorOnSurface))
    snackbarView.translationY = -transitionY
    snackbarView.elevation = 16.5f
    snackbarView.setBackgroundResource(R.drawable.shape_snackbar)
    return snackbar
}

/**
 * 展示在底部音乐栏全部View的上方
 */
const val SNACKBAR_ALL_BOTTOM_BAR = 1

/**
 * 展示在底部音乐栏仅播放控件View的上方
 */
const val SNACKBAR_PLAY_BAR = 2

/**
 * 展示在底部的上方
 */
const val SNACKBAR_BOTTOM = 3