package com.ccteam.fluidmusic.widget

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.lang.ref.WeakReference
import kotlin.math.max

/**
 * @author Xiaoc
 * @since 2021/1/21
 *
 * Toolbar滑动监听适配器
 * 该适配器用于在进行RecyclerView滑动时，动态操作Toolbar内容等操作
 */
class AnimToolbarScrollListener(
    view: View,
    /**
     * 这里的ratio即比例，代表从第一个布局的XX%多少开始就进行动画滑动到Toolbar
     */
    private val ratio: Float = 1F
) : RecyclerView.OnScrollListener() {

    /**
     * 弱引用，可能会出现这个View准备被回收但是却被该适配器持有导致不能回收的情况
     * 所以弱引用该View
     */
    private val view = WeakReference(view)

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        val layoutManager = recyclerView.layoutManager as LinearLayoutManager
        // 如果是第一项
        if (isFirstToolbarView(layoutManager)) {
            val toolbarTitleView = layoutManager.findViewByPosition(0)
            val startTranslationHeight = (toolbarTitleView?.height ?: 0) * ratio
            // 得到从第一项开始下滑的距离
            val scrollY = -(toolbarTitleView?.top ?: 0)
            view.get()?.let {
                if (scrollY <= 0) {
                    it.visibility = View.INVISIBLE
                } else {
                    it.visibility = View.VISIBLE
                    if(isLastView(layoutManager)){
                        // 首先如果已经滑到最底部了，我们直接将View的位置放在正确地方
                        // 防止RecyclerView长度不足导致只显示出一点点View的内容
                        it.animate().translationY(0f).setDuration(120).start()
                    } else {
                        // 如果在第一项向上滑动，则根据滑动距离来进行相对位置值的设置
                        it.translationY = max(startTranslationHeight - scrollY, 0f)
                    }
                }
            }
        } else {
            view.get()?.visibility = View.VISIBLE
            view.get()?.translationY = 0f
        }
    }

    private fun isFirstToolbarView(layoutManager: LinearLayoutManager): Boolean {
        return layoutManager.findFirstVisibleItemPosition() == 0
    }

    private fun isLastView(layoutManager: LinearLayoutManager): Boolean {
        return layoutManager.findLastCompletelyVisibleItemPosition() == layoutManager.itemCount - 1
    }
}