package com.ccteam.fluidmusic.widget.comment

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.ccteam.fluidmusic.R
import com.ccteam.fluidmusic.databinding.MomentDetailInnerCommentItemBinding
import com.ccteam.shared.result.moment.comment.MomentReplayItem
import com.google.android.material.card.MaterialCardView

/**
 * @author Xiaoc
 * @since 2021/3/29
 *
 * 回复的评论视图View
 * 封装了基本的回复操作，它与 [ReplyLayout] 搭配操作
 */
class ReplyView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0): MaterialCardView(context,attrs,defStyleAttr) {

    private val binding: MomentDetailInnerCommentItemBinding = MomentDetailInnerCommentItemBinding.inflate(LayoutInflater.from(context),this,true)

    init {
        background = null
    }

    fun updateReplayItem(replayItem: MomentReplayItem){
        updateReplayView(replayItem)
    }

    fun setReplayItemClickedListener(listener: (View) -> Unit){
        setOnClickListener {
            listener(it)
        }
    }

    fun setReplayMoreClickedListener(listener: (View) -> Unit){
        binding.btnMoreComment.setOnClickListener {
            listener(it)
        }
    }

    fun setMoreVisible(isVisible: Boolean){
        binding.btnMoreComment.isVisible = isVisible
    }

    private fun updateReplayView(replayItem: MomentReplayItem){
        binding.tvMomentInnerCommentAccountName.text = replayItem.userName
        binding.tvMomentInnerCommentContent.text = replayItem.commentContent
        binding.btnMoreComment.text = context.getString(R.string.description_comment_see_more,replayItem.replayCount)
        binding.tvInnerCommentDatetime.text = replayItem.publishTime
        Glide.with(binding.ivMomentInnerCommentHeadImg)
            .load(replayItem.userPhotoUrl)
            .placeholder(R.drawable.img_default_artist_small)
            .into(binding.ivMomentInnerCommentHeadImg)
    }
}