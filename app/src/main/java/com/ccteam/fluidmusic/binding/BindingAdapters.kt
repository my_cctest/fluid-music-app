package com.ccteam.fluidmusic.binding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

/**
 *
 * @author Xiaoc
 * @since 2021/1/7
 */
@BindingAdapter("glideImg")
fun glideImg(imageView: ImageView, url: String) {
    Glide.with(imageView)
        .load(url)
        .into(imageView)
}