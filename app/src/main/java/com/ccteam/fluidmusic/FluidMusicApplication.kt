package com.ccteam.fluidmusic

import androidx.hilt.work.HiltWorkerFactory
import androidx.multidex.MultiDexApplication
import androidx.work.Configuration
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

/**
 * @author Xiaoc
 *
 * 应用启动时进行初始化的Application
 * 可在此处初始化应用必要的一些内容
 */
@HiltAndroidApp
class FluidMusicApplication: MultiDexApplication(),Configuration.Provider {

    @Inject lateinit var workerFactory: HiltWorkerFactory

    override fun onCreate() {
        super.onCreate()
        // 初始化日志器
        Logger.addLogAdapter(AndroidLogAdapter())
    }

    override fun getWorkManagerConfiguration(): Configuration {
        // 配置注入WorkFactory
        return Configuration.Builder()
            .setWorkerFactory(workerFactory)
            .build()
    }
}