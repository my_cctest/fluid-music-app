package com.ccteam.fluidmusic.view.adapters.databinding;


import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.DiffUtil;

/**
 * @author Xiaoc
 * @date 2020.10.22
 *
 * 一个工作于{@link DataBindViewHolder}的适配器
 * 该适配器用于同种类型的Item的适配，需要一个LayoutId
 * @param <T> 基于ViewDataBinding的类，也就是生成的DataBinding类型
 */
public abstract class DataBindAdapter<T extends ViewDataBinding,E> extends
        BaseDataBindAdapter<T,E> {
    @LayoutRes
    private final int mLayoutId;

    protected DataBindAdapter(@NonNull DiffUtil.ItemCallback<E> diffCallback, @LayoutRes int layoutId) {
        super(diffCallback);
        mLayoutId = layoutId;
    }

    @Override
    public int getItemLayoutId(int position) {
        return mLayoutId;
    }

    @Override
    public int getItemCount() {
        return getCurrentList().size();
    }
}
