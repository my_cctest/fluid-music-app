package com.ccteam.fluidmusic.view.adapters.paging

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.ccteam.shared.result.BaseItemData

/**
 * @author Xiaoc
 * @since 2021/2/16
 *
 * 多类型布局的分页适配器支持，而且它是基于ViewBinding机制
 * 支持多类型布局的对应内容，这里的数据类全部必须基于 [BaseItemData]
 * 因为它自带了一个 viewType 值，需要告诉RecyclerView当前的布局类型值的内容
 *
 **/
abstract class MultiTypePagingAdapter(
    diffItemCallback: DiffUtil.ItemCallback<BaseItemData>
): BasePagingAdapter<BaseItemData, RecyclerView.ViewHolder>(diffItemCallback) {

    final override fun createPagingViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder? {
        return createMultiPagingViewHolder(parent,viewType)
    }

    /**
     * 创建多类型布局的布局容器，需要子类实现
     * 您可能需要根据对应的viewType去返回对应的布局容器（可能涉及到强转）
     */
    abstract fun createMultiPagingViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder?

    final override fun bindPagingItem(
        holder: RecyclerView.ViewHolder,
        position: Int,
        item: BaseItemData?,
        payloads: MutableList<Any>
    ) {
        bindMultiPagingItem(holder,position,item,payloads,getItemViewType(position))
    }

    /**
     * 多类型布局的视图与数据之间的绑定，为抽象方法，需要自己实现
     * 您可能需要涉及到根据viewType判断然后进行对应强转后进行操作
     *
     * @param holder 基于ViewBinding的视图容器
     * @param position 当前绑定的是什么位置的数据和视图
     * @param item 当前绑定的数据，数据可为空，需要进行空处理
     * @param payloads 局部加载的数据
     * @param viewType 布局类型值
     */
    abstract fun bindMultiPagingItem(holder: RecyclerView.ViewHolder,
                                     position: Int,
                                     item: BaseItemData?,
                                     payloads: MutableList<Any>,
                                     viewType: Int
    )

    override fun getItemViewType(position: Int): Int {
        return getItem(position)?.viewType ?: 0
    }
}