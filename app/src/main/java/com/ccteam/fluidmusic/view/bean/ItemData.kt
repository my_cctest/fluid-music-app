package com.ccteam.fluidmusic.view.bean

import androidx.recyclerview.widget.DiffUtil
import com.ccteam.shared.result.BaseItemData

///**
// *
// * @author Xiaoc
// * @since 2021/1/17
// */
//abstract class BaseItemData{
//    abstract var viewType: Int
//
//    companion object{
//        /**
//         * 空布局
//         */
//        const val LAYOUT_EMPTY = -1
//        /**
//         * 歌手详情页的歌手信息
//         */
//        const val LAYOUT_ARTIST_INFO = 2
//
//        /**
//         * 全局Toolbar大标题
//         */
//        const val LAYOUT_TOOLBAR_TITLE = 3
//
//        /**
//         * 在线音乐首页类型项
//         */
//        const val LAYOUT_ONLINE_MUSIC_HOME_TYPE_BROWSE = 4
//
//        /**
//         * 在线音乐首页小内容项
//         */
//        const val LAYOUT_ONLINE_MUSIC_HOME_SMALL_ITEM = 5
//
//        /**
//         * 专辑列表子项
//         */
//        const val LAYOUT_ALBUM_LIST_ITEM = 6
//
//        /**
//         * 专辑详情页头部
//         */
//        const val LAYOUT_ALBUM_DETAIL_HEADER = 7
//
//        /**
//         * 专辑详情歌曲列表
//         */
//        const val LAYOUT_ALBUM_DETAIL_MUSIC_ITEM = 8
//
//        /**
//         * 我的界面首页基本信息项
//         */
//        const val LAYOUT_ME_INFO_ITEM = 9
//
//        /**
//         * 歌单列表子项
//         */
//        const val LAYOUT_PLAYLIST_SHEET_LIST_ITEM = 10
//
//        /**
//         * 朋友圈列表子项
//         */
//        const val LAYOUT_MOMENTS_ITEM = 11
//
//        /**
//         * 朋友圈列表子项
//         */
//        const val LAYOUT_ARTIST_LIST_ITEM = 12
//
//        /**
//         * 专辑列表子项（本地音乐专辑布局）
//         */
//        const val LAYOUT_ALBUM_LIST_ITEM_SMALL = 13
//
//        /**
//         * 专辑详情尾部项
//         */
//        const val LAYOUT_ALBUM_DETAIL_FOOT_ITEM = 14
//
//        /**
//         * 默认的歌曲项
//         */
//        const val LAYOUT_DEFAULT_MUSIC_ITEM = 15
//
//        /**
//         * 默认的歌曲项
//         */
//        const val LAYOUT_LYRICS_BODY_ITEM = 20
//
//        /**
//         * 默认的歌曲项
//         */
//        const val LAYOUT_LYRICS_STRING_ITEM = 21
//
//
//        /**
//         * 歌单列表子项
//         */
//        const val LAYOUT_PLAYLIST_SHEET_DETAIL_HEADER_ITEM = 16
//
//        /**
//         * 我的主页未登录项
//         */
//        const val LAYOUT_ME_HOME_NOT_LOGIN = 17
//
//
//        /**
//         * 朋友圈主评论项
//         */
//        const val LAYOUT_MOMENTS_COMMENT = 18
//
//
//        /**
//         * 朋友圈评论回复项
//         */
//        const val LAYOUT_MOMENTS_COMMENT_RELAY = 19
//    }
//}
//
//@IntDef(NO_PLAYING, PLAYING, PLAYING_PAUSE)
//@Retention
//annotation class PlaybackStatus
//
//const val NO_PLAYING = -1
//const val PLAYING = 0
//const val PLAYING_PAUSE = 1
//
//
///**
// * 全局通用的Toolbar标题项
// */
//data class ToolbarTitleItem(
//    val title: String,
//    override var viewType: Int = LAYOUT_TOOLBAR_TITLE,
//): BaseItemData(){
//
//    companion object{
//        val differCallback = object: DiffUtil.ItemCallback<ToolbarTitleItem>(){
//
//            override fun areItemsTheSame(oldItem: ToolbarTitleItem, newItem: ToolbarTitleItem): Boolean {
//                return oldItem.title == newItem.title
//            }
//
//            override fun areContentsTheSame(oldItem: ToolbarTitleItem, newItem: ToolbarTitleItem): Boolean {
//                return oldItem.title == newItem.title
//            }
//
//        }
//    }
//
//}
//
///**
// * 包含Banner轮播图的按类型分类内容
// */
//data class OnlineMusicHomeTypeBrowseItem(
//    val banners: List<OnlineMusicBanner>,
//    override var viewType: Int = LAYOUT_ONLINE_MUSIC_HOME_TYPE_BROWSE,
//): BaseItemData()
//
///**
// * 列表轮播图内容
// * @param imageUrl 图片地址
// * @param id 对应类型所需要的ID
// * @param type 对应轮播图类型，进行不同的操作
// */
//data class OnlineMusicBanner(
//    val imageUrl: String,
//    val id: String,
//    val type: Int
//)
//
///**
// * 首页小标题版内容
// * @param title 小标题
// * @param innerSmallItems 对应小标题中的内容
// */
//data class OnlineMusicHomeSmallItem(
//    val title: String,
//    val type: Int,
//    var innerSmallItems: List<OnlineMusicHomeInnerSmallItem>,
//    override var viewType: Int = LAYOUT_ONLINE_MUSIC_HOME_SMALL_ITEM,
//): BaseItemData(){
//
//    companion object{
//        const val SMALL_ITEM_ALBUM = 1
//        const val SMALL_ITEM_MUSIC = 2
//
//        val differCallback = object: DiffUtil.ItemCallback<BaseItemData>(){
//
//            override fun areItemsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
//                if(oldItem is OnlineMusicHomeSmallItem && newItem is OnlineMusicHomeSmallItem){
//                    return oldItem.innerSmallItems == newItem.innerSmallItems
//                } else if(oldItem is OnlineMusicHomeTypeBrowseItem && newItem is OnlineMusicHomeTypeBrowseItem){
//                    return oldItem.banners == newItem.banners
//                }
//                return true
//            }
//
//            override fun areContentsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
//                if(oldItem is OnlineMusicHomeSmallItem && newItem is OnlineMusicHomeSmallItem){
//                    return oldItem.innerSmallItems == newItem.innerSmallItems
//                } else if(oldItem is OnlineMusicHomeTypeBrowseItem && newItem is OnlineMusicHomeTypeBrowseItem){
//                    return oldItem.banners == newItem.banners
//                }
//                return true
//            }
//
//        }
//
//    }
//}
//
//data class OnlineMusicHomeInnerSmallItem(
//    val itemId: String,
//    val imageUrl: String,
//    val title: String,
//    val subtitle: String,
//    val playable: Boolean,
//    val playbackRes: Int
//){
//    companion object{
//        val differCallback = object: DiffUtil.ItemCallback<OnlineMusicHomeInnerSmallItem>(){
//
//            override fun areItemsTheSame(oldItem: OnlineMusicHomeInnerSmallItem, newItem: OnlineMusicHomeInnerSmallItem): Boolean {
//                return oldItem == newItem
//            }
//
//            override fun areContentsTheSame(oldItem: OnlineMusicHomeInnerSmallItem, newItem: OnlineMusicHomeInnerSmallItem): Boolean {
//                return oldItem.itemId == newItem.itemId && oldItem.imageUrl == newItem.imageUrl
//                        && oldItem.playbackRes == newItem.playbackRes
//
//            }
//
//        }
//
//    }
//}
//
///**
// * 默认的展示歌曲的Item
// * 该Item包含封面、标题、副标题、比特等等内容
// *
// * @param mediaId 歌曲ID
// * @param title 标题
// * @param subtitle 副标题
// * @param albumArtUri 歌曲封面地址
// * @param bitrate 比特
// * @param playable 是否可播放
// * @param playbackRes 当前播放状态
// * @param mediaUri 文件播放的Uri（本地使用，用于Taglib获取信息）
// */
//data class DefaultMusicItem(
//    val mediaId: String,
//    val title: String,
//    val subtitle: String,
//    val artistInfo: List<ArtistInfoData>,
//    val albumId: String,
//    val albumArtUri: String,
//    val bitrate: String?,
//    val playable: Boolean,
//    @PlaybackStatus val playbackRes: Int,
//    val mediaUri: Uri = Uri.EMPTY,
//    override var viewType: Int = LAYOUT_DEFAULT_MUSIC_ITEM
//): BaseItemData(){
//
//    companion object{
//        val differCallback = object: DiffUtil.ItemCallback<BaseItemData>(){
//
//            override fun areItemsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
//                if(oldItem is DefaultMusicItem && newItem is DefaultMusicItem){
//                    return oldItem.mediaId == newItem.mediaId
//                }
//                return oldItem == newItem
//            }
//
//            override fun areContentsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
//                if(oldItem is DefaultMusicItem && newItem is DefaultMusicItem){
//                    return oldItem.mediaId == newItem.mediaId && oldItem.title == newItem.title &&
//                            oldItem.subtitle == newItem.subtitle &&
//                            oldItem.playbackRes == newItem.playbackRes
//                }
//                return false
//            }
//
//        }
//
//        val differCallbackSingle = object: DiffUtil.ItemCallback<DefaultMusicItem>(){
//
//            override fun areItemsTheSame(oldItem: DefaultMusicItem, newItem: DefaultMusicItem): Boolean {
//                return oldItem.mediaId == newItem.mediaId
//            }
//
//            override fun areContentsTheSame(oldItem: DefaultMusicItem, newItem: DefaultMusicItem): Boolean {
//                return oldItem.mediaId == newItem.mediaId &&
//                            oldItem.playbackRes == newItem.playbackRes
//            }
//
//        }
//    }
//}
//
///**
// * 歌单音乐列表的展示歌曲的Item
// * 该Item包含封面、标题、副标题、比特等等内容，最重要包含一个是否为当前用户编辑的歌曲
// *
// * @param id 歌单歌曲ID关联唯一值
// * @param mediaId 歌曲ID
// * @param title 标题
// * @param subtitle 副标题
// * @param albumArtUri 歌曲封面地址
// * @param bitrate 比特
// * @param playable 是否可播放
// * @param playbackRes 当前播放状态
// * @param isSelfUserMusic 是否是用户自己操作的歌曲，如果为true则可以对其进行一些权限操作
// */
//data class PlaylistSheetMusicItem(
//    val id: String,
//    val mediaId: String,
//    val title: String,
//    val subtitle: String,
//    val artistInfo: List<ArtistInfoData>,
//    val albumId: String,
//    val albumArtUri: String,
//    val bitrate: String?,
//    val playable: Boolean,
//    @PlaybackStatus val playbackRes: Int,
//    val isSelfUserMusic: Boolean,
//    override var viewType: Int = LAYOUT_DEFAULT_MUSIC_ITEM
//): BaseItemData(){
//
//    companion object{
//        val differCallback = object: DiffUtil.ItemCallback<BaseItemData>(){
//
//            override fun areItemsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
//                if(oldItem is PlaylistSheetMusicItem && newItem is PlaylistSheetMusicItem){
//                    return oldItem.mediaId == newItem.mediaId && oldItem.id == newItem.id
//                }
//                return oldItem == newItem
//            }
//
//            override fun areContentsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
//                if(oldItem is PlaylistSheetMusicItem && newItem is PlaylistSheetMusicItem){
//                    return oldItem.mediaId == newItem.mediaId && oldItem.id == newItem.id &&
//                            oldItem.playbackRes == newItem.playbackRes
//                }
//                return false
//            }
//
//        }
//
//        val differCallbackSingle = object: DiffUtil.ItemCallback<PlaylistSheetMusicItem>(){
//
//            override fun areItemsTheSame(oldItem: PlaylistSheetMusicItem, newItem: PlaylistSheetMusicItem): Boolean {
//                return oldItem.mediaId == newItem.mediaId && oldItem.id == newItem.id
//            }
//
//            override fun areContentsTheSame(oldItem: PlaylistSheetMusicItem, newItem: PlaylistSheetMusicItem): Boolean {
//                return oldItem.mediaId == newItem.mediaId && oldItem.id == newItem.id &&
//                        oldItem.playbackRes == newItem.playbackRes
//            }
//
//        }
//    }
//}
//
///**
// * 专辑列表页展示项
// * [com.ccteam.fluidmusic.R.layout.global_album_list_item]
// *
// * @param mediaId 访问下一个节点的mediaId（本地在线通用）
// * @param albumTitle 专辑标题
// * @param albumArtUri 专辑封面
// * @param artist 歌手名
// * @param subtitle 副标题（在歌手详情页可以是专辑歌曲数，其他地方可以是歌手名）
// * @param albumId 该专辑ID
// * @param artistId 歌手ID（仅本地可能需要）
// * @param year 专辑日期（仅本地可能需要）
// */
//data class AlbumListItem(
//    val mediaId: String,
//    val albumTitle: String,
//    val albumArtUri: String,
//    val artist: String,
//    val subtitle: String,
//    val albumId: String,
//    val artistId: String? = null,
//    val year: String? = null,
//    override var viewType: Int = LAYOUT_ALBUM_LIST_ITEM
//): BaseItemData(){
//
//    companion object{
//        val differCallback = object: DiffUtil.ItemCallback<BaseItemData>(){
//
//            override fun areItemsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
//                if(oldItem is AlbumListItem && newItem is AlbumListItem){
//                    return oldItem.mediaId == newItem.mediaId
//                }
//                return oldItem == newItem
//            }
//
//            override fun areContentsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
//                if(oldItem is AlbumListItem && newItem is AlbumListItem){
//                    return oldItem.mediaId == newItem.mediaId && oldItem.albumId == newItem.albumId
//                }
//                return false
//            }
//
//        }
//
//        val differCallbackSingle = object: DiffUtil.ItemCallback<AlbumListItem>(){
//
//            override fun areItemsTheSame(oldItem: AlbumListItem, newItem: AlbumListItem): Boolean {
//                return oldItem.mediaId == newItem.mediaId
//            }
//
//            override fun areContentsTheSame(oldItem: AlbumListItem, newItem: AlbumListItem): Boolean {
//                return oldItem.mediaId == newItem.mediaId && oldItem.albumId == newItem.albumId
//            }
//
//        }
//    }
//}
//
///**
// * 专辑详情页头部展示项
// * [com.ccteam.fluidmusic.R.layout.global_media_data_album_head_item]
// *
// * @param albumId 专辑ID
// * @param albumTitle 专辑标题
// * @param albumArtUri 专辑封面
// * @param artist 歌手名
// * @param subtitle 副标题（比如年份、流派等）
// * @param description 专辑描述（本地音乐无此内容）
// * @param artistId 歌手ID
// */
//@Parcelize
//data class AlbumDetailHeaderItem(
//    val albumId: String,
//    val albumTitle: String,
//    val albumArtUri: String,
//    val artist: String,
//    val subtitle: String?,
//    val description: String?,
//    val artistId: String,
//    override var viewType: Int = LAYOUT_ALBUM_DETAIL_HEADER
//): BaseItemData(),Parcelable
//
///**
// * 专辑详情页尾部展示项
// * [com.ccteam.fluidmusic.R.layout.global_media_data_album_foot_item]
// *
// * @param trackNum 专辑曲目数
// * @param year 专辑发行年份
// * @param copyright 专辑版权所有信息
// */
//data class AlbumDetailFootItem(
//    val trackNum: Int,
//    val year: String,
//    val copyright: String,
//    override var viewType: Int = LAYOUT_ALBUM_DETAIL_FOOT_ITEM
//): BaseItemData()
//
///**
// * 专辑详情页专辑歌曲展示项
// * [com.ccteam.fluidmusic.R.layout.global_media_data_album_music_item]
// *
// * @param mediaId 歌曲ID
// * @param artist 歌手
// * @param title 歌曲标题
// * @param artistInfo 歌手信息列表
// * @param albumId 专辑ID
// * @param numTrack 歌曲所在曲目顺序
// * @param bitrate 比特率
// * @param playable 是否可播放
// * @param playbackRes 当前播放状态
// * @param mediaUri 本地用（用于taglib查找标签信息）
// */
//data class AlbumDetailMusicItem(
//    val mediaId: String,
//    val artist: String,
//    val title: String,
//    val artistInfo: List<ArtistInfoData>,
//    val albumId: String,
//    val numTrack: Int,
//    val bitrate: String?,
//    val playable: Boolean,
//    val playbackRes: Int,
//    val mediaUri: Uri = Uri.EMPTY,
//    override var viewType: Int = LAYOUT_ALBUM_DETAIL_MUSIC_ITEM
//): BaseItemData(){
//
//    companion object{
//        val differCallback = object: DiffUtil.ItemCallback<BaseItemData>(){
//
//            override fun areItemsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
//                if(oldItem is AlbumDetailMusicItem && newItem is AlbumDetailMusicItem){
//                    return oldItem.mediaId == newItem.mediaId
//                } else if(oldItem is AlbumDetailHeaderItem && newItem is AlbumDetailHeaderItem){
//                    return oldItem.albumId == newItem.albumId
//                } else if(oldItem is AlbumDetailFootItem && newItem is AlbumDetailFootItem){
//                    return oldItem.trackNum == newItem.trackNum
//                }
//                return oldItem == newItem
//            }
//
//            override fun areContentsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
//                if(oldItem is AlbumDetailMusicItem && newItem is AlbumDetailMusicItem){
//                    return oldItem.mediaId == newItem.mediaId && oldItem.playbackRes == newItem.playbackRes
//                } else if(oldItem is AlbumDetailHeaderItem && newItem is AlbumDetailHeaderItem){
//                    return oldItem.albumId == newItem.albumId
//                } else if(oldItem is AlbumDetailFootItem && newItem is AlbumDetailFootItem){
//                    return oldItem.trackNum == newItem.trackNum
//                }
//                return false
//            }
//
//        }
//    }
//}
//
///**
// * 首页我的信息内容
// */
//@Parcelize
//data class MeHomeInfoItem(
//    val userId: String,
//    val userName: String,
//    val userIntroduction: String,
//    val userPhotoUrl: String,
//    val playlistSheetCount: Int,
//    val momentsCount: Int,
//    override var viewType: Int = LAYOUT_ME_INFO_ITEM
//): BaseItemData(),Parcelable{
//
//    companion object{
//        val differCallback = object: DiffUtil.ItemCallback<BaseItemData>(){
//
//            override fun areItemsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
//                if(oldItem is MeHomeInfoItem && newItem is MeHomeInfoItem){
//                    return oldItem.userId == newItem.userId &&
//                           oldItem.userPhotoUrl == newItem.userPhotoUrl
//                }
//                return oldItem == newItem
//            }
//
//            override fun areContentsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
//                if(oldItem is MeHomeInfoItem && newItem is MeHomeInfoItem){
//                    return oldItem.userId == newItem.userId &&
//                            oldItem.userName == newItem.userName &&
//                            oldItem.userIntroduction == newItem.userIntroduction &&
//                            oldItem.userPhotoUrl == newItem.userPhotoUrl &&
//                            oldItem.playlistSheetCount == newItem.playlistSheetCount &&
//                            oldItem.momentsCount == newItem.momentsCount
//                }
//                return false
//            }
//
//        }
//    }
//}
//
//data class PlaylistSheetListItem(
//    val id: String,
//    val title: String,
//    val imgUrl: String,
//    override var viewType: Int = LAYOUT_PLAYLIST_SHEET_LIST_ITEM,
//): BaseItemData(){
//
//    companion object{
//        val differCallback = object: DiffUtil.ItemCallback<PlaylistSheetListItem>(){
//
//            override fun areItemsTheSame(oldItem: PlaylistSheetListItem, newItem: PlaylistSheetListItem): Boolean {
//                return oldItem.id == newItem.id
//            }
//
//            override fun areContentsTheSame(oldItem: PlaylistSheetListItem, newItem: PlaylistSheetListItem): Boolean {
//                return oldItem.id == newItem.id && oldItem.imgUrl == newItem.imgUrl && oldItem.title == newItem.title
//            }
//
//        }
//    }
//}
//
///**
// * 歌单详情页头部内容
// * @param id 歌单ID
// * @param playlistSheetTitle 歌单标题
// * @param playlistSheetImgUrl 歌单封面
// * @param userId 用户ID
// * @param userPhotoUrl 用户头像
// * @param userName 用户昵称
// * @param introduction 歌单简介
// * @param type 歌单类型
// * @param enableAddMusic 是否开启添加歌曲功能
// * @param enableEditPlaylistSheet 是否开启编辑歌单功能
// * @param enableDeletePlaylistSheet 是否开启删除歌单功能
// */
//@Parcelize
//data class PlaylistSheetDetailHeaderItem(
//    val id: String,
//    val playlistSheetTitle: String,
//    val playlistSheetImgUrl: String,
//    val userId: String,
//    val userPhotoUrl: String,
//    val userName: String,
//    val introduction: String,
//    val type: Int,
//    val enableAddMusic: Boolean = false,
//    val enableEditPlaylistSheet: Boolean = false,
//    val enableDeletePlaylistSheet: Boolean = false,
//    override var viewType: Int = LAYOUT_PLAYLIST_SHEET_DETAIL_HEADER_ITEM,
//): BaseItemData(),Parcelable{
//
//    companion object{
//        val differCallback = object: DiffUtil.ItemCallback<PlaylistSheetDetailHeaderItem>(){
//
//            override fun areItemsTheSame(oldItem: PlaylistSheetDetailHeaderItem, newItem: PlaylistSheetDetailHeaderItem): Boolean {
//                return oldItem.id == newItem.id
//            }
//
//            override fun areContentsTheSame(oldItem: PlaylistSheetDetailHeaderItem, newItem: PlaylistSheetDetailHeaderItem): Boolean {
//                return oldItem.id == newItem.id && oldItem.playlistSheetTitle == newItem.playlistSheetTitle
//            }
//        }
//    }
//}
//
///**
// * 朋友圈信息项
// * @param id 朋友圈ID
// * @param userId 用户ID
// * @param userName 用户名
// * @param userPhotoUrl 用户头像
// * @param publishTime 发布日期
// * @param momentsContent 朋友圈内容
// * @param momentsImgList 朋友圈图片列表
// * @param shareContent 分享的内容对象，可为空，如果为空不显示分享内容
// * @param upNum 点赞数
// * @param commentNum 评论数
// */
//data class MomentListItem(
//    val id: String,
//    val userId: String,
//    val userName: String,
//    val userPhotoUrl: String,
//    val publishTime: String,
//    val momentsContent: String,
//    val momentsImgList: List<MomentImageItem>,
//    val shareContent: MomentShareItem?,
//    val upNum: Int,
//    val commentNum: Int,
//    override var viewType: Int = LAYOUT_MOMENTS_ITEM
//): BaseItemData(){
//
//    companion object{
//        val differCallback = object: DiffUtil.ItemCallback<MomentListItem>(){
//
//            override fun areItemsTheSame(oldItem: MomentListItem, newItem: MomentListItem): Boolean {
//                return oldItem.id == newItem.id
//            }
//
//            override fun areContentsTheSame(oldItem: MomentListItem, newItem: MomentListItem): Boolean {
//                return oldItem.id == newItem.id && oldItem.upNum == newItem.upNum
//                        && oldItem.commentNum == newItem.commentNum && oldItem.momentsImgList.size == newItem.momentsImgList.size
//            }
//
//        }
//    }
//
//    /**
//     * 朋友圈分享内容的对象内容（例如分享歌曲、歌单、专辑）
//     * @param id 内容ID
//     * @param type 内容类型
//     * @param title 内容标题
//     * @param subTitle 内容副标题
//     * @param imgUrl 内容封面
//     */
//    @Parcelize
//    data class MomentShareItem(
//        val id: String,
//        val type: Int,
//        val title: String,
//        val subTitle: String,
//        val imgUrl: String
//    ): Parcelable
//
//    @Parcelize
//    data class MomentImageItem(
//        val id: String,
//        val momentId: String,
//        val imageUrl: String
//    ): Parcelable
//}
//
//data class MomentCommentItem(
//    val id: String,
//    val momentId: String,
//    val commentContent: String,
//    val userId: String,
//    val userName: String,
//    val publishTime: String,
//    val userPhotoUrl: String,
//    val replayCount: Int,
//    val type: Int,
//    val replayList: List<MomentReplayItem>,
//    override var viewType: Int = LAYOUT_MOMENTS_COMMENT
//): BaseItemData(){
//
//    companion object{
//        val differCallback = object: DiffUtil.ItemCallback<MomentCommentItem>(){
//
//            override fun areItemsTheSame(oldItem: MomentCommentItem, newItem: MomentCommentItem): Boolean {
//                return oldItem.id == newItem.id
//            }
//
//            override fun areContentsTheSame(oldItem: MomentCommentItem, newItem: MomentCommentItem): Boolean {
//                return oldItem.id == newItem.id && oldItem.commentContent == newItem.commentContent
//            }
//
//        }
//    }
//}
//
//data class MomentReplayItem(
//    val id: String,
//    val momentId: String,
//    val commentId: String,
//    val commentContent: String,
//    val userId: String,
//    val userName: String,
//    val publishTime: String,
//    val userPhotoUrl: String,
//    val toUserId: String,
//    val toUserName: String,
//    val replayCount: Int,
//    val type: Int,
//    override var viewType: Int = LAYOUT_MOMENTS_COMMENT_RELAY
//): BaseItemData(){
//
//    companion object{
//        val differCallback = object: DiffUtil.ItemCallback<MomentReplayItem>(){
//
//            override fun areItemsTheSame(oldItem: MomentReplayItem, newItem: MomentReplayItem): Boolean {
//                return oldItem.id == newItem.id
//            }
//
//            override fun areContentsTheSame(oldItem: MomentReplayItem, newItem: MomentReplayItem): Boolean {
//                return oldItem.id == newItem.id && oldItem.commentContent == newItem.commentContent
//            }
//
//        }
//    }
//}
//
///**
// * 歌手列表项内容
// * [com.ccteam.fluidmusic.R.layout.global_artist_list_item]
// *
// * @param mediaId 访问下一个节点的mediaId（本地在线通用）
// * @param artistName 歌手名
// * @param subtitle 副标题
// * @param artistId 歌手ID
// */
//@Parcelize
//data class ArtistListItem(
//    val mediaId: String,
//    val artistName: String,
//    val artistPhoto: String,
//    val subtitle: ArtistSubtitle?,
//    val artistId: String,
//    override var viewType: Int = LAYOUT_ARTIST_LIST_ITEM
//): BaseItemData(),Parcelable{
//
//    companion object{
//        val differCallback = object: DiffUtil.ItemCallback<BaseItemData>(){
//
//            override fun areItemsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
//                if(oldItem is ArtistListItem && newItem is ArtistListItem){
//                    return oldItem.mediaId == newItem.mediaId
//                }
//                return oldItem == newItem
//            }
//
//            override fun areContentsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
//                if(oldItem is ArtistListItem && newItem is ArtistListItem){
//                    return oldItem.mediaId == newItem.mediaId
//                            && oldItem.artistId == newItem.artistId && oldItem.subtitle == newItem.subtitle
//                }
//                return false
//            }
//
//        }
//
//        val differCallbackSingle = object: DiffUtil.ItemCallback<ArtistListItem>(){
//
//            override fun areItemsTheSame(oldItem: ArtistListItem, newItem: ArtistListItem): Boolean {
//                return oldItem.mediaId == newItem.mediaId
//            }
//
//            override fun areContentsTheSame(oldItem: ArtistListItem, newItem: ArtistListItem): Boolean {
//                return oldItem.mediaId == newItem.mediaId
//                        && oldItem.artistId == newItem.artistId && oldItem.subtitle == newItem.subtitle
//            }
//
//        }
//    }
//
//    @Parcelize
//    data class ArtistSubtitle(
//        val trackNum: Int?,
//        val albumNum: Int?
//    ):Parcelable
//}
//
//data class EmptyItem(
//    override var viewType: Int
//):BaseItemData()

/**
 * 歌词本体内容项
 * @param startTime 当前歌词开始时间
 * @param isDots 当前内容是否为等待条
 * @param lyricText 歌词内容
 */
data class LyricsBodyItem(
    val startTime: Long,
    val isDots: Boolean,
    val lyricText: String,
    override var viewType: Int = LAYOUT_LYRICS_BODY_ITEM
): BaseItemData(){
    companion object{

        const val HIGH_LIGHT_CHANGE = 5

        val differCallback = object: DiffUtil.ItemCallback<BaseItemData>(){

            override fun areItemsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
                if(oldItem is LyricsBodyItem && newItem is LyricsBodyItem){
                    return oldItem.lyricText == newItem.lyricText
                } else if(oldItem is LyricsStringItem && newItem is LyricsStringItem){
                    return oldItem.lyricText == newItem.lyricText
                }
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
                if(oldItem is LyricsBodyItem && newItem is LyricsBodyItem){
                    return oldItem.lyricText == newItem.lyricText
                } else if(oldItem is LyricsStringItem && newItem is LyricsStringItem){
                    return oldItem.lyricText == newItem.lyricText
                }
                return false
            }

        }
    }
}

/**
 * 歌词本体内容项（仅展示文字，无滚动功能的）
 * @param lyricText 歌词内容
 */
data class LyricsStringItem(
    val lyricText: String,
    override var viewType: Int = LAYOUT_LYRICS_STRING_ITEM
): BaseItemData(){
}

///**
// * 添加内容时显示的媒体内容项
// * 例如 歌单中添加歌曲 显示的歌曲内容，朋友圈选择歌曲专辑 等内容时
// *
// * @param mediaId 媒体ID
// * @param title 媒体标题
// * @param subtitle 媒体副标题
// * @param albumArtUri 媒体艺术图
// * @param isExist 是否已经存在于歌单（该项在歌单添加歌曲时适用）
// */
//data class AddMediaItem(
//    val mediaId: String,
//    val title: String,
//    val subtitle: String,
//    val albumArtUri: String,
//    val isExist: Boolean = false,
//    override var viewType: Int = LAYOUT_DEFAULT_MUSIC_ITEM
//): BaseItemData(){
//
//    companion object{
//        val differCallback = object: DiffUtil.ItemCallback<AddMediaItem>(){
//
//            override fun areItemsTheSame(oldItem: AddMediaItem, newItem: AddMediaItem): Boolean {
//                return oldItem.mediaId == newItem.mediaId
//            }
//
//            override fun areContentsTheSame(oldItem: AddMediaItem, newItem: AddMediaItem): Boolean {
//                return oldItem.mediaId == newItem.mediaId
//                        && oldItem.isExist == newItem.isExist && oldItem.subtitle == newItem.subtitle
//            }
//
//        }
//    }
//
//}
//
///**
// * 我的主页 未登录项
// * 该项不提供任何内容，仅作登录提醒
// */
//data class MeHomeNotLoginItem(
//    override var viewType: Int = LAYOUT_ME_HOME_NOT_LOGIN
//): BaseItemData(){
//
//    companion object{
//        val differCallback = object: DiffUtil.ItemCallback<BaseItemData>(){
//
//            override fun areItemsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
//                return oldItem == newItem
//            }
//
//            override fun areContentsTheSame(oldItem: BaseItemData, newItem: BaseItemData): Boolean {
//                return oldItem.viewType == newItem.viewType
//            }
//
//        }
//    }
//}


