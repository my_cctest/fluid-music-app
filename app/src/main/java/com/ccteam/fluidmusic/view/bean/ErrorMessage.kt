package com.ccteam.fluidmusic.view.bean

import com.ccteam.network.ApiError

/**
 * @author Xiaoc
 * @since 2021/2/18
 *
 * UI错误信息展示
 * @param errorCode 错误代码
 * @param description 错误描述，可为空
 */
data class ErrorMessage(
    val description: String? = "未知错误",
    val errorCode: Int = ApiError.unknownCode
)

/**
 * 加载信息
 * 由于错误信息和加载信息需要同时操作UI
 * 所以我们将其设定为加载信息，方便进行判断
 */
sealed class LoadMessage{

    /**
     * 当前为加载成功状态，此状态与 NotLoading 有些许不同
     * 用于区分那些需要知道成功状态的操作，如上传图片
     */
    object Success : LoadMessage()

    /**
     * 当前状态为空，表示没有数据展示，但是已经加载完成
     */
    object Empty: LoadMessage()

    /**
     * 当前为未加载状态
     */
    object NotLoading : LoadMessage()

    /**
     * 当前为正在加载状态
     */
    object Loading : LoadMessage()

    /**
     * 当前为错误状态
     * @param errorMessage 错误数据信息
     */
    class Error(
        val errorMessage: ErrorMessage
    ) : LoadMessage()
}