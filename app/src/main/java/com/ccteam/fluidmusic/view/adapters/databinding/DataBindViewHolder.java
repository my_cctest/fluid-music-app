package com.ccteam.fluidmusic.view.adapters.databinding;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @author Xiaoc
 * @date 2020.10.22
 *
 * 基于ViewDataBinding的ViewHolder
 * 将DataBinding存储与holder中，以便访问
 *
 * @param <T> 基于ViewDataBinding的类
 */
public class DataBindViewHolder<T extends ViewDataBinding> extends RecyclerView.ViewHolder {
    public final T binding;

    public DataBindViewHolder(T binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public static <T extends ViewDataBinding> DataBindViewHolder<T> create(ViewGroup parent, @LayoutRes int layoutId){
        T binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                layoutId,parent,false);
        return new DataBindViewHolder<>(binding);
    }
}
