package com.ccteam.fluidmusic.view.adapters.viewbinding

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.ccteam.fluidmusic.databinding.GlobalToolbarTitleItemBinding
import com.ccteam.fluidmusic.view.adapters.BaseViewBindingAdapter
import com.ccteam.fluidmusic.view.adapters.toolbar.ToolbarTitleViewHolder
import com.ccteam.shared.result.BaseItemData

/**
 * @author Xiaoc
 * @since 2021/1/26
 *
 * 多类型布局的RecyclerView支持，基于ViewBinding机制
 * 支持多类型布局的对应内容，这里的数据类全部必须基于 [BaseItemData]
 * 因为它自带了一个 viewType 值，需要告诉RecyclerView当前的布局类型值的内容
 *
 * 我们建议使用 [ConcatAdapter] 将ToolbarTitle的数据进行分离展示
 * 您可以省略该布局的处理
 **/
abstract class MultiTypeViewBindingAdapter(
    diffItemCallback: DiffUtil.ItemCallback<BaseItemData>
) : BaseViewBindingAdapter<RecyclerView.ViewHolder, BaseItemData>(diffItemCallback) {

    final override fun createViewBinding(parent: ViewGroup, viewType: Int):
            RecyclerView.ViewHolder?{
        // 如果是Toolbar标题大布局，直接处理
        return if(viewType == BaseItemData.LAYOUT_TOOLBAR_TITLE){
            ToolbarTitleViewHolder(
                GlobalToolbarTitleItemBinding.inflate(
                LayoutInflater.from(parent.context),parent,false))
        } else {
            createMultiViewBinding(parent,viewType)
        }
    }

    /**
     * 创建多类型布局的布局容器，需要子类实现
     * 您可能需要根据对应的viewType去返回对应的布局容器（可能涉及到强转）
     */
    abstract fun createMultiViewBinding(parent: ViewGroup, viewType: Int):
            RecyclerView.ViewHolder?

    final override fun bindItem(
        holder: RecyclerView.ViewHolder,
        position: Int,
        item: BaseItemData,
        payloads: MutableList<Any>
    ) {
        bindMultiItem(holder,position,item,payloads,getItemViewType(position))
    }

    /**
     * 多类型布局的视图与数据之间的绑定，为抽象方法，需要自己实现
     * 您可能需要涉及到根据viewType判断然后进行对应强转后进行操作
     *
     * @param holder 基于ViewBinding的视图容器
     * @param position 当前绑定的是什么位置的数据和视图
     * @param item 当前绑定的数据
     * @param payloads 局部加载的数据
     * @param viewType 布局类型值
     */
    abstract fun bindMultiItem(
        holder: RecyclerView.ViewHolder,
        position: Int,
        item: BaseItemData,
        payloads: MutableList<Any>,
        viewType: Int
    )

    /**
     * 得到当前布局类型值
     * 如果没有数据，则返回空布局的值
     * 其他情况返回 [BaseItemData] 中的viewType的值
     */
    override fun getItemViewType(position: Int): Int {
        return getItem(position).viewType
    }


}