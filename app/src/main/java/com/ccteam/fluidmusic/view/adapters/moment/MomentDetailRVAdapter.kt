package com.ccteam.fluidmusic.view.adapters.moment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.ccteam.fluidmusic.R
import com.ccteam.fluidmusic.databinding.MomentDetailContentItemBinding
import com.ccteam.fluidmusic.view.adapters.ViewBindViewHolder
import com.ccteam.fluidmusic.view.adapters.image.MomentsImageWrapperAdapter
import com.ccteam.fluidmusic.view.adapters.viewbinding.ViewBindingAdapter
import com.ccteam.fluidmusic.view.components.CornerImageWrapperView
import com.ccteam.model.moment.MOMENT_SHARE_TYPE_ALBUM
import com.ccteam.shared.result.moment.MomentListItem

/**
 * @author Xiaoc
 * @since 2021/3/31
 */
class MomentDetailRVAdapter(
    private val imageCallback: CornerImageWrapperView.ImageEventListener<MomentListItem.MomentImageItem>,
    private val momentDetailCallback: MomentDetailItemCallback
): ViewBindingAdapter<MomentDetailContentItemBinding, MomentDetailRVAdapter.MomentDetailViewHolder, MomentListItem>(
    MomentListItem.differCallback){

    inner class MomentDetailViewHolder(
        binding: MomentDetailContentItemBinding
    ): ViewBindViewHolder<MomentDetailContentItemBinding>(binding){
        var item: MomentListItem? = null

        init {
            binding.imagesWrapper.setAdapter(MomentsImageWrapperAdapter(imageCallback))

            binding.cardViewShareContainer.setOnClickListener {
                item?.shareContent?.let { data ->
                    momentDetailCallback.shareContentClicked(it,data)
                }
            }
        }
    }

    override fun createViewBinding(parent: ViewGroup, viewType: Int): MomentDetailViewHolder {
        return MomentDetailViewHolder(
            MomentDetailContentItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        )
    }

    override fun bindItem(
        holder: MomentDetailViewHolder,
        position: Int,
        item: MomentListItem,
        payloads: MutableList<Any>
    ) {
        handleMomentsItem(holder,item)
    }

    private fun handleMomentsItem(holder: MomentDetailViewHolder,item: MomentListItem){
        holder.item = item
        holder.binding.tvMomentDetailAccountName.text = item.userName
        holder.binding.tvMomentsDatetime.text = item.publishTime
        holder.binding.cardViewShareContainer.isVisible = item.shareContent != null
        holder.binding.tvMomentDetailContent.text = item.momentsContent
        holder.binding.tvMomentDetailCommentDescription.text =
            holder.binding.tvMomentDetailCommentDescription.context.getString(R.string.description_moment_detail_comment,item.commentNum.toString())
        Glide.with(holder.binding.ivMomentDetailHeadImg)
            .load(item.userPhotoUrl)
            .placeholder(R.drawable.img_default_artist_small)
            .into(holder.binding.ivMomentDetailHeadImg)

        item.shareContent?.let {
            holder.binding.cardViewShareContainer.transitionName = it.id
            holder.binding.tvShareTitle.text = it.title
            holder.binding.tvShareSubtitle.text = it.subTitle
            Glide.with(holder.binding.ivShareImg)
                .load(it.imgUrl)
                .placeholder(R.drawable.img_default_album)
                .into(holder.binding.ivShareImg)
            holder.binding.ivAlbumHeader.isVisible = it.type == MOMENT_SHARE_TYPE_ALBUM
        }

        val adapter = holder.binding.imagesWrapper.getAdapter() as? MomentsImageWrapperAdapter
        adapter?.subImageList(item.momentsImgList)
    }
}

interface MomentDetailItemCallback{
    fun shareContentClicked(view: View,item: MomentListItem.MomentShareItem)
}