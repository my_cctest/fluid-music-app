package com.ccteam.fluidmusic.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.ccteam.fluidmusic.R
import com.ccteam.fluidmusic.databinding.GlobalPlaylistSheetHeaderItemBinding
import com.ccteam.fluidmusic.view.adapters.viewbinding.ViewBindingAdapter
import com.ccteam.shared.result.playlistsheet.PlaylistSheetDetailHeaderItem

/**
 * @author Xiaoc
 * @since 2021/3/16
 *
 * 歌单详情页头部的RecyclerView适配器， 形成 ConcatAdapter 组合
 */
class PlaylistSheetDetailHeaderAdapter(
    private val callback: PlaylistSheetDetailHeaderCallback
): ViewBindingAdapter<GlobalPlaylistSheetHeaderItemBinding,
        PlaylistSheetDetailHeaderAdapter.PlaylistSheetHeaderViewHolder, PlaylistSheetDetailHeaderItem>(PlaylistSheetDetailHeaderItem.differCallback) {

    inner class PlaylistSheetHeaderViewHolder(
        binding: GlobalPlaylistSheetHeaderItemBinding
    ): ViewBindViewHolder<GlobalPlaylistSheetHeaderItemBinding>(binding){

        var item: PlaylistSheetDetailHeaderItem? = null

        init {
            binding.btnAddMusicToPlaylistSheet.setOnClickListener {
                item?.let { data ->
                    callback.addMusicToPlaylistClicked(data)
                }
            }
            binding.btnEditPlaylistSheet.setOnClickListener {
                item?.let { data ->
                    callback.editPlaylistClicked(data)
                }
            }
            binding.btnDeletePlaylistSheet.setOnClickListener {
                item?.let { data ->
                    callback.deletePlaylistClicked(data)
                }
            }
            binding.btnPlay.setOnClickListener {
                callback.playClicked()
            }
            binding.btnShufflePlay.setOnClickListener {
                callback.shufflePlayClicked()
            }
            binding.tvPlaylistSheetIntroduction.setOnClickListener {
                item?.let { data ->
                    callback.descriptionClicked(data)
                }
            }
        }
    }

    override fun createViewBinding(
        parent: ViewGroup,
        viewType: Int
    ): PlaylistSheetHeaderViewHolder {
        return PlaylistSheetHeaderViewHolder(GlobalPlaylistSheetHeaderItemBinding.inflate(
            LayoutInflater.from(parent.context),parent,false))
    }

    override fun bindItem(
        holder: PlaylistSheetHeaderViewHolder,
        position: Int,
        item: PlaylistSheetDetailHeaderItem,
        payloads: MutableList<Any>
    ) {
        handlePlaylistSheetHeader(holder,item)
    }

    private fun handlePlaylistSheetHeader(holder: PlaylistSheetHeaderViewHolder,item: PlaylistSheetDetailHeaderItem){
        holder.item = item
        holder.binding.tvUserName.text = item.userName
        holder.binding.tvPlaylistSheetIntroduction.text = item.introduction
        holder.binding.tvPlaylistSheetTitle.text = item.playlistSheetTitle
        Glide.with(holder.binding.ivPlaylistSheet)
            .load(item.playlistSheetImgUrl)
            .placeholder(R.drawable.img_default_album)
            .into(holder.binding.ivPlaylistSheet)
        Glide.with(holder.binding.ivUserPhoto)
            .load(item.userPhotoUrl)
            .placeholder(R.drawable.img_default_album)
            .into(holder.binding.ivUserPhoto)

        holder.binding.groupOperationPlaylist.isVisible = item.enableAddMusic
                || item.enableDeletePlaylistSheet || item.enableEditPlaylistSheet

        holder.binding.btnAddMusicToPlaylistSheet.isEnabled = item.enableAddMusic
        holder.binding.btnEditPlaylistSheet.isEnabled = item.enableEditPlaylistSheet
        holder.binding.btnDeletePlaylistSheet.isEnabled = item.enableDeletePlaylistSheet

        // 判断如果专辑描述文字超过了预设的行数，则开启点击事件可进入详情内容
        with(holder.binding.tvPlaylistSheetIntroduction){
            viewTreeObserver.addOnGlobalLayoutListener {
                isEnabled = layout?.let {
                    layout.getEllipsisCount(
                        lineCount - 1
                    ) > 0
                } ?: false
            }
        }
    }
}

interface PlaylistSheetDetailHeaderCallback{
    /**
     * 当点击添加音乐到播放列表时回调
     */
    fun addMusicToPlaylistClicked(item: PlaylistSheetDetailHeaderItem)

    /**
     * 当点击编辑播放列表时回调
     */
    fun editPlaylistClicked(item: PlaylistSheetDetailHeaderItem)

    /**
     * 当点击删除播放列表时回调
     */
    fun deletePlaylistClicked(item: PlaylistSheetDetailHeaderItem)

    /**
     * 当点击播放时回调
     */
    fun playClicked()

    /**
     * 当点击随机播放时回调
     */
    fun shufflePlayClicked()

    /**
     * 当点击更多介绍时调用
     */
    fun descriptionClicked(item: PlaylistSheetDetailHeaderItem)
}