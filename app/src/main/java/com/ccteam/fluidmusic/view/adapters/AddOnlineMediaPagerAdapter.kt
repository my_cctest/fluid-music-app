package com.ccteam.fluidmusic.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.ccteam.fluidmusic.databinding.GlobalAddMediaItemBinding
import com.ccteam.fluidmusic.view.adapters.paging.PagingAdapter
import com.ccteam.shared.result.playlistsheet.AddMediaItem

/**
 * @author Xiaoc
 * @since 2021/3/17
 */
class AddOnlineMediaPagerAdapter(
    private val callback: AddMediaCallback
): PagingAdapter<AddMediaItem,
        AddOnlineMediaPagerAdapter.AddMediaItemViewHolder,GlobalAddMediaItemBinding>(AddMediaItem.differCallback) {

    inner class AddMediaItemViewHolder(
        binding: GlobalAddMediaItemBinding
    ): ViewBindViewHolder<GlobalAddMediaItemBinding>(binding){
        var item: AddMediaItem? = null

        init {
            binding.root.setOnClickListener {
                item?.let {
                    callback.itemClick(it)
                }
            }
        }
    }

    override fun createPagingViewHolder(parent: ViewGroup, viewType: Int): AddMediaItemViewHolder {
        return AddMediaItemViewHolder(
            GlobalAddMediaItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        )
    }

    override fun bindPagingItem(
        holder: AddMediaItemViewHolder,
        position: Int,
        item: AddMediaItem?,
        payloads: MutableList<Any>
    ) {
        handleAddMediaItem(holder,item)
    }

    private fun handleAddMediaItem(holder: AddMediaItemViewHolder,item: AddMediaItem?){
        item?.let {
            holder.item = it
            holder.binding.tvMusicTitle.text = item.title
            holder.binding.tvMusicSubtitle.text = item.subtitle
            holder.binding.tvAlreadyAdd.isVisible = item.isExist
            Glide.with(holder.binding.ivAddMedia)
                .load(item.albumArtUri)
                .into(holder.binding.ivAddMedia)
        }
    }
}

interface AddMediaCallback{
    fun itemClick(item: AddMediaItem)
}