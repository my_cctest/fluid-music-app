package com.ccteam.fluidmusic.view.adapters.comment

import android.view.View
import com.ccteam.shared.result.moment.comment.MomentReplayItem

/**
 * @author Xiaoc
 * @since 2021/3/30
 */
interface ReplayItemCallback {
    fun replayItemClicked(view: View,item: MomentReplayItem)
}