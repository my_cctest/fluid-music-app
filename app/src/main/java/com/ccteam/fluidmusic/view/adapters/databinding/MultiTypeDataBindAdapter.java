package com.ccteam.fluidmusic.view.adapters.databinding;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.ccteam.fluidmusic.view.adapters.databinding.BaseDataBindAdapter;
import com.ccteam.fluidmusic.view.adapters.databinding.DataBindViewHolder;

import java.util.List;

/**
 *
 * @author Xiaoc
 * @date 2020.10.22
 *
 * 多类型的DataBindingAdapter适配器
 * 基于ListAdapter
 */
@SuppressWarnings({"rawtypes", "AlibabaAbstractClassShouldStartWithAbstractNaming"})
abstract public class MultiTypeDataBindAdapter extends BaseDataBindAdapter {

    public MultiTypeDataBindAdapter(@NonNull DiffUtil.ItemCallback diffCallback) {
        super(diffCallback);
    }

    /**
     * 绑定数据和监听事件
     * @param holder ViewHolder视图数据保持层
     * @param position 位置
     * @param payloads payload参数
     */
    @Override
    protected abstract void bindItem(DataBindViewHolder holder, int position, List payloads);
}
