package com.ccteam.fluidmusic.view.adapters.viewbinding

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.viewbinding.ViewBinding
import com.ccteam.fluidmusic.view.adapters.BaseViewBindingAdapter
import com.ccteam.fluidmusic.view.adapters.ViewBindViewHolder

/**
 * @author Xiaoc
 * @since 2021/1/26
 *
 * 基于ViewBinding机制的单个Item内容的适配器
 * 你需要实现两个方法：
 * [createViewBinding] 它需要返回一个基于 [ViewBindViewHolder] 的ViewHolder
 * 你不需要考虑空布局的viewType
 *
 * @param V 这是基于ViewBinding的视图类
 * @param VH 这是基于[ViewBindViewHolder]的ViewHolder类
 * @param E 这是代表该Item的数据类型
 **/
abstract class ViewBindingAdapter<V: ViewBinding,VH: ViewBindViewHolder<V>,E>(
    diffItemCallback: DiffUtil.ItemCallback<E>
): BaseViewBindingAdapter<VH, E>(diffItemCallback) {

    /**
     * 创建视图ViewBinding视图容器，为抽象方法，需要自己实现
     */
    abstract override fun createViewBinding(parent: ViewGroup, viewType: Int): VH

    /**
     * 视图与数据绑定，为抽象方法，需要自己实现
     * @param holder 基于ViewBinding的视图容器
     * @param position 当前绑定的是什么位置的数据和视图
     * @param item 当前绑定的数据
     * @param payloads 局部加载的数据
     */
    abstract override fun bindItem(holder: VH, position: Int, item: E, payloads: MutableList<Any>)
}