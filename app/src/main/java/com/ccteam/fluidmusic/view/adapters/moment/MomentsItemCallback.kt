package com.ccteam.fluidmusic.view.adapters.moment

import android.view.View
import com.ccteam.shared.result.moment.MomentListItem

/**
 * @author Xiaoc
 * @since 2021/3/31
 */
interface MomentsItemCallback {
    fun itemClicked(view: View,item: MomentListItem)
    fun shareContentClicked(view: View,item: MomentListItem.MomentShareItem)
}