package com.ccteam.fluidmusic.view.adapters.comment

import android.view.View
import com.ccteam.shared.result.moment.comment.MomentCommentItem

/**
 * @author Xiaoc
 * @since 2021/3/30
 */
interface CommentItemCallback{
    fun commentItemClicked(view: View, item: MomentCommentItem)
}