package com.ccteam.fluidmusic.view.adapters.toolbar

import com.ccteam.fluidmusic.databinding.GlobalToolbarTitleItemBinding
import com.ccteam.fluidmusic.view.adapters.ViewBindViewHolder

/**
 * @author Xiaoc
 * @since 2021/2/16
 *
 * 适用于Toolbar标题的容器视图
 */
class ToolbarTitleViewHolder(
    binding: GlobalToolbarTitleItemBinding
): ViewBindViewHolder<GlobalToolbarTitleItemBinding>(binding)