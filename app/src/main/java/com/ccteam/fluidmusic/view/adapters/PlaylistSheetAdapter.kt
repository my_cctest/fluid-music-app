package com.ccteam.fluidmusic.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.ccteam.fluidmusic.R
import com.ccteam.fluidmusic.databinding.GlobalPlaylistSheetListItemBinding
import com.ccteam.fluidmusic.databinding.GlobalPlaylistSheetSmallItemBinding
import com.ccteam.fluidmusic.view.adapters.paging.PagingAdapter
import com.ccteam.shared.result.playlistsheet.PlaylistSheetListItem

/**
 * @author Xiaoc
 * @since 2021/3/16
 *
 * 歌单列表的分页RecyclerView适配器
 */
class PlaylistSheetPagingAdapter(
    private val callback: PlaylistSheetItemCallback
): PagingAdapter<PlaylistSheetListItem,
        PlaylistSheetPagingAdapter.PlaylistSheetListItemViewHolder, GlobalPlaylistSheetListItemBinding>(
    PlaylistSheetListItem.differCallback){

    val spanSizeLookup = object: GridLayoutManager.SpanSizeLookup(){
        override fun getSpanSize(position: Int): Int {
            return if(position == 0){
                3
            } else {
                1
            }
        }

    }

    inner class PlaylistSheetListItemViewHolder(
        binding: GlobalPlaylistSheetListItemBinding
    ): ViewBindViewHolder<GlobalPlaylistSheetListItemBinding>(binding){

        var item: PlaylistSheetListItem? = null

        init {
            binding.root.setOnClickListener {
                item?.let { item ->
                    callback.playlistItemClicked(binding.ivPlaylistSheetListItem,item)
                }
            }
        }
    }

    override fun createPagingViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PlaylistSheetListItemViewHolder {
        return PlaylistSheetListItemViewHolder(
            GlobalPlaylistSheetListItemBinding.inflate(
            LayoutInflater.from(parent.context),parent,false))
    }

    override fun bindPagingItem(
        holder: PlaylistSheetListItemViewHolder,
        position: Int,
        item: PlaylistSheetListItem?,
        payloads: MutableList<Any>
    ) {
        handlePlaylistItem(holder,item)
    }

    private fun handlePlaylistItem(holder: PlaylistSheetListItemViewHolder,item: PlaylistSheetListItem?){
        item?.let {
            holder.item = it
            holder.binding.ivPlaylistSheetListItem.transitionName = it.id
            holder.binding.tvPlaylistSheetListTitle.text = it.title
            Glide.with(holder.binding.ivPlaylistSheetListItem)
                .load(it.imgUrl)
                .placeholder(R.drawable.img_default_album)
                .into(holder.binding.ivPlaylistSheetListItem)
        }
    }
}

class PlaylistSheetSmallPagingAdapter(
    private val callback: PlaylistSheetItemCallback
): PagingAdapter<PlaylistSheetListItem,
        PlaylistSheetSmallPagingAdapter.PlaylistSheetSmallItemViewHolder, GlobalPlaylistSheetSmallItemBinding>(
    PlaylistSheetListItem.differCallback){

    inner class PlaylistSheetSmallItemViewHolder(
        binding: GlobalPlaylistSheetSmallItemBinding
    ): ViewBindViewHolder<GlobalPlaylistSheetSmallItemBinding>(binding){

        var item: PlaylistSheetListItem? = null

        init {
            binding.root.setOnClickListener {
                item?.let { item ->
                    callback.playlistItemClicked(binding.ivThumbnailPlaylist,item)
                }
            }
        }
    }

    override fun createPagingViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PlaylistSheetSmallItemViewHolder {
        return PlaylistSheetSmallItemViewHolder(
            GlobalPlaylistSheetSmallItemBinding.inflate(
                LayoutInflater.from(parent.context),parent,false))
    }

    override fun bindPagingItem(
        holder: PlaylistSheetSmallItemViewHolder,
        position: Int,
        item: PlaylistSheetListItem?,
        payloads: MutableList<Any>
    ) {
        handlePlaylistItem(holder,item)
    }

    private fun handlePlaylistItem(holder: PlaylistSheetSmallItemViewHolder,item: PlaylistSheetListItem?){
        item?.let {
            holder.item = it
            holder.binding.tvPlaylistName.text = it.title
            Glide.with(holder.binding.ivThumbnailPlaylist)
                .load(it.imgUrl)
                .placeholder(R.drawable.img_default_album)
                .into(holder.binding.ivThumbnailPlaylist)
        }
    }
}

interface PlaylistSheetItemCallback{

    fun playlistItemClicked(view: View, item: PlaylistSheetListItem)
}