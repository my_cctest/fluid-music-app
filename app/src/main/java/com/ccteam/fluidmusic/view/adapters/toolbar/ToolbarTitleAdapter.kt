package com.ccteam.fluidmusic.view.adapters.toolbar

import android.view.LayoutInflater
import android.view.ViewGroup
import com.ccteam.fluidmusic.databinding.GlobalToolbarTitleItemBinding
import com.ccteam.fluidmusic.view.adapters.viewbinding.ViewBindingAdapter
import com.ccteam.shared.result.title.ToolbarTitleItem

/**
 * @author Xiaoc
 * @since 2021/2/16
 *
 * 适用于Toolbar标题的Adapter
 * 将该布局单独提取出来是为了方便使用 [androidx.recyclerview.widget.ConcatAdapter] 进行拼接
 * 虽然 [com.ccteam.fluidmusic.view.adapters.MultiTypeViewBindingAdapter] 自带处理标题内容的操作
 * 但我们还是建议使用 ConcatAdapter 将逻辑单独提取出来
 */
class ToolbarTitleAdapter:
    ViewBindingAdapter<GlobalToolbarTitleItemBinding, ToolbarTitleViewHolder, ToolbarTitleItem>(ToolbarTitleItem.differCallback){

    override fun createViewBinding(parent: ViewGroup, viewType: Int): ToolbarTitleViewHolder {
        return ToolbarTitleViewHolder(GlobalToolbarTitleItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun bindItem(
        holder: ToolbarTitleViewHolder,
        position: Int,
        item: ToolbarTitleItem,
        payloads: MutableList<Any>
    ) {
        // 设置Toolbar标题
        holder.binding.tvToolbarTitleItem.text = item.title
    }

}