package com.ccteam.fluidmusic.view.adapters

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.ccteam.fluidmusic.R
import com.ccteam.fluidmusic.databinding.DefaultMusicItemBinding
import com.ccteam.fluidmusic.utils.getColor
import com.ccteam.fluidmusic.view.adapters.viewbinding.ViewBindingAdapter
import com.ccteam.fluidmusic.view.bean.METADATA_CHANGED
import com.ccteam.model.music.BITRATE_NO
import com.ccteam.model.music.BITRATE_SQ
import com.ccteam.shared.result.playlistsheet.PlaylistSheetMusicItem
import com.ccteam.shared.utils.NO_PLAYING
import com.google.android.material.color.MaterialColors

/**
 * @author Xiaoc
 * @since 2021/3/23
 */
class PlaylistSheetDetailMusicAdapter(
    private val callback: PlaylistSheetMusicItemClickCallback
): ViewBindingAdapter<DefaultMusicItemBinding,
        PlaylistSheetDetailMusicAdapter.PlaylistSheetMusicItemViewHolder, PlaylistSheetMusicItem>(PlaylistSheetMusicItem.differCallbackSingle) {

    override fun createViewBinding(
        parent: ViewGroup,
        viewType: Int
    ): PlaylistSheetMusicItemViewHolder {
        return PlaylistSheetMusicItemViewHolder(DefaultMusicItemBinding.inflate(
            LayoutInflater.from(parent.context),parent,false))
    }

    override fun bindItem(
        holder: PlaylistSheetMusicItemViewHolder,
        position: Int,
        item: PlaylistSheetMusicItem,
        payloads: MutableList<Any>
    ) {
        var fullRefresh = payloads.isEmpty()

        if(payloads.isNotEmpty()){
            payloads.forEach { payload ->
                when(payload){
                    METADATA_CHANGED -> {
                        // 当前播放内容更改时进行的局部UI更新
                        updateActiveItem(holder,item)
                    }
                    else ->
                        fullRefresh = true
                }
            }
        }

        // 完整更新列表内容
        if(fullRefresh) {
            handlePlaylistSheetMusicItem(holder,item)
        }
    }

    private fun handlePlaylistSheetMusicItem(holder: PlaylistSheetMusicItemViewHolder,item: PlaylistSheetMusicItem){
        holder.item = item
        holder.binding.tvMusicTitle.text = item.title
        holder.binding.tvMusicSubtitle.text = item.subtitle
        holder.binding.tvBitrate.isVisible = item.bitrate == BITRATE_SQ
        // 设置播放项状态（播放 or 未播放）
        updateActiveItem(holder,item)
        Glide.with(holder.binding.ivThumbnailAlbumArt)
            .load(item.albumArtUri)
            .transition(DrawableTransitionOptions.withCrossFade())
            .placeholder(R.drawable.img_default_album)
            .thumbnail(0.3f)
            .into(holder.binding.ivThumbnailAlbumArt)
    }

    private fun updateActiveItem(holder: PlaylistSheetMusicItemViewHolder, item: PlaylistSheetMusicItem){
        when(item.playbackRes){
            // 如果没有播放状态的项，则是默认状态
            NO_PLAYING ->{
                holder.binding.root.isEnabled = item.playable
                holder.binding.btnMore.isEnabled = item.playable
                if(item.playable){
                    holder.binding.tvMusicTitle.setTextColor(MaterialColors.getColor(holder.binding.tvMusicTitle, R.attr.colorOnSurface))
                    holder.binding.tvMusicSubtitle.setTextColor(holder.binding.tvMusicSubtitle.getColor(
                        R.color.defaultTextColor))
                    holder.binding.btnMore.iconTint = ColorStateList.valueOf(MaterialColors.getColor(holder.binding.tvMusicTitle, R.attr.colorOnSurface))
                } else {
                    holder.binding.tvMusicTitle.setTextColor(holder.binding.tvMusicTitle.getColor(R.color.disableTextColor))
                    holder.binding.tvMusicSubtitle.setTextColor(holder.binding.tvMusicSubtitle.getColor(
                        R.color.disableTextColor))
                    holder.binding.btnMore.iconTint = ColorStateList.valueOf(holder.binding.tvMusicTitle.getColor(R.color.disableTextColor))
                }
                holder.binding.ivMusicWave.cancelAnimation()
                holder.binding.ivMusicWave.visibility = View.GONE
            }
            else ->{
                val color =
                    MaterialColors.getColor(holder.binding.tvMusicTitle, R.attr.colorPrimary)
                holder.binding.tvMusicTitle.setTextColor(color)
                holder.binding.tvMusicSubtitle.setTextColor(color)
                holder.binding.ivMusicWave.playAnimation()
                holder.binding.ivMusicWave.visibility = View.VISIBLE
            }

        }
    }

    inner class PlaylistSheetMusicItemViewHolder(
        binding: DefaultMusicItemBinding
    ): ViewBindViewHolder<DefaultMusicItemBinding>(binding){
        var item: PlaylistSheetMusicItem? = null

        init {
            binding.root.setOnClickListener {
                item?.let { data ->
                    if(data.playable){
                        callback.musicItemClicked(data)
                    }
                }
            }
            binding.root.setOnLongClickListener {
                item?.let { data ->
                    if(data.playable && data.bitrate != BITRATE_NO){
                        callback.moreClicked(data)
                        return@setOnLongClickListener true
                    }
                    return@setOnLongClickListener false
                }
                return@setOnLongClickListener false
            }
            binding.btnMore.setOnClickListener {
                item?.let { data ->
                    if(data.playable && data.bitrate != BITRATE_NO){
                        callback.moreClicked(data)
                    }
                }
            }
        }
    }
}

interface PlaylistSheetMusicItemClickCallback{
    fun musicItemClicked(item: PlaylistSheetMusicItem)
    fun moreClicked(item: PlaylistSheetMusicItem)
}