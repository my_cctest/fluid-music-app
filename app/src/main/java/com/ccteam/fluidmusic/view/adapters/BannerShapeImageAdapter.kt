package com.ccteam.fluidmusic.view.adapters

import android.util.TypedValue
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ccteam.fluidmusic.R
import com.ccteam.shared.result.home.OnlineMusicBanner
import com.google.android.material.imageview.ShapeableImageView
import com.google.android.material.shape.ShapeAppearanceModel
import com.youth.banner.adapter.BannerAdapter

/**
 *
 * @author Xiaoc
 * @since 2021/1/18
 *
 * Banner轮播图ViewPager2适配器，展示圆角图片的轮播图
 */
class BannerShapeImageAdapter(
    private val callback: BannerCallback,
    list: List<OnlineMusicBanner>
): BannerAdapter<OnlineMusicBanner, BannerShapeImageAdapter.BannerShapeImageViewHolder>(list) {

    companion object{
        val layoutParams =  ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        val shapeAppearanceModel = ShapeAppearanceModel().withCornerSize(30f)
    }

    inner class BannerShapeImageViewHolder(
        imageView: ShapeableImageView
    ): RecyclerView.ViewHolder(imageView){
        var item: OnlineMusicBanner? = null
        val shapeImageView = imageView

        init {
            shapeImageView.layoutParams = layoutParams
            shapeImageView.scaleType = ImageView.ScaleType.CENTER_CROP
            // 设置圆角
            shapeImageView.shapeAppearanceModel = shapeAppearanceModel
            // 设置边距
            val paddingHorizontal = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,14.0f,imageView.resources.displayMetrics).toInt()
            shapeImageView.setPadding(paddingHorizontal,0,paddingHorizontal,0)

            shapeImageView.setOnClickListener {
                item?.let {
                    callback.bannerItemClicked(it)
                }
            }
        }
    }

    override fun onCreateHolder(parent: ViewGroup, viewType: Int): BannerShapeImageViewHolder {
        val shapeImageView = ShapeableImageView(parent.context)
        return BannerShapeImageViewHolder(shapeImageView)
    }

    override fun onBindView(
        holder: BannerShapeImageViewHolder,
        data: OnlineMusicBanner?,
        position: Int,
        size: Int
    ) {
        holder.item = data
        Glide.with(holder.itemView)
            .load(data?.imageUrl)
            .placeholder(R.drawable.img_default_album)
            .into(holder.shapeImageView)
    }
}

interface BannerCallback{
    fun bannerItemClicked(item: OnlineMusicBanner)
}