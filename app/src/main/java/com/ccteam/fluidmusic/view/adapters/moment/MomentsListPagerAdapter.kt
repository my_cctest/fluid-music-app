package com.ccteam.fluidmusic.view.adapters.moment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.ccteam.fluidmusic.R
import com.ccteam.fluidmusic.databinding.MomentsListItemBinding
import com.ccteam.fluidmusic.view.adapters.ViewBindViewHolder
import com.ccteam.fluidmusic.view.adapters.image.MomentsImageWrapperAdapter
import com.ccteam.fluidmusic.view.adapters.paging.PagingAdapter
import com.ccteam.fluidmusic.view.components.CornerImageWrapperView
import com.ccteam.model.moment.MOMENT_SHARE_TYPE_ALBUM
import com.ccteam.shared.result.moment.MomentListItem

/**
 * @author Xiaoc
 * @since 2021/3/31
 */
class MomentsListPagerAdapter(
    private val imageCallback: CornerImageWrapperView.ImageEventListener<MomentListItem.MomentImageItem>,
    private val momentsCallback: MomentsItemCallback
): PagingAdapter<MomentListItem, MomentsListPagerAdapter.MomentListItemViewHolder, MomentsListItemBinding>(
    MomentListItem.differCallback) {

    inner class MomentListItemViewHolder(
        binding: MomentsListItemBinding
    ) : ViewBindViewHolder<MomentsListItemBinding>(binding) {
        var item: MomentListItem? = null


        init {
            binding.imagesWrapper.setAdapter(MomentsImageWrapperAdapter(imageCallback))

            binding.root.setOnClickListener {
                item?.let { data ->
                    momentsCallback.itemClicked(it,data)
                }
            }

            binding.cardViewShareContainer.setOnClickListener {
                item?.shareContent?.let { data ->
                    momentsCallback.shareContentClicked(it,data)
                }
            }
        }
    }

    override fun createPagingViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MomentListItemViewHolder {
        return MomentListItemViewHolder(
            MomentsListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun bindPagingItem(
        holder: MomentListItemViewHolder,
        position: Int,
        item: MomentListItem?,
        payloads: MutableList<Any>
    ) {
        handleMomentsItem(holder, item)
    }

    private fun handleMomentsItem(holder: MomentListItemViewHolder, item: MomentListItem?) {
        item?.let {
            holder.item = item
            holder.binding.root.transitionName = item.id
            holder.binding.tvMomentsAccountName.text = item.userName
            holder.binding.tvMomentsDatetime.text = item.publishTime
            holder.binding.cardViewShareContainer.isVisible = item.shareContent != null
            holder.binding.tvMomentsContent.text = item.momentsContent
            Glide.with(holder.binding.ivMomentsItemHeadImg)
                .load(item.userPhotoUrl)
                .placeholder(R.drawable.img_default_artist_small)
                .into(holder.binding.ivMomentsItemHeadImg)
            holder.binding.btnMomentsThumbUp.text = item.upNum.toString()
            holder.binding.btnMomentsComment.text = item.commentNum.toString()

            item.shareContent?.let {
                holder.binding.cardViewShareContainer.transitionName = item.id + it.id
                holder.binding.tvShareTitle.text = it.title
                holder.binding.tvShareSubtitle.text = it.subTitle
                Glide.with(holder.binding.ivShareImg)
                    .load(it.imgUrl)
                    .into(holder.binding.ivShareImg)
                holder.binding.ivAlbumHeader.isVisible = it.type == MOMENT_SHARE_TYPE_ALBUM
            }

            val adapter =
                holder.binding.imagesWrapper.getAdapter() as? MomentsImageWrapperAdapter
            adapter?.subImageList(item.momentsImgList)
        }

    }
}