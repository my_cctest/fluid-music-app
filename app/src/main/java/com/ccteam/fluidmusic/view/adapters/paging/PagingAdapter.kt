package com.ccteam.fluidmusic.view.adapters.paging

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.viewbinding.ViewBinding
import com.ccteam.fluidmusic.view.adapters.ViewBindViewHolder

/**
 * @author Xiaoc
 * @since 2021/2/16
 *
 * 基于[BasePagingAdapter]的单种类型Item内容的分页适配器
 * 你需要实现两个方法：
 * [createPagingViewHolder] 它需要返回一个基于 [ViewBindViewHolder] 的ViewHolder
 * 你不需要考虑空布局的viewType
 * 还有 [bindPagingItem]，该方法会传递当前需要绑定的数据，进行对应显示
 *
 * @param V 这是基于ViewBinding的视图类
 * @param VH 这是基于[ViewBindViewHolder]的ViewHolder类
 * @param E 这是代表该Item的数据类型
**/
abstract class PagingAdapter<E: Any,VH: ViewBindViewHolder<V>,V: ViewBinding>(
    diffItemCallback: DiffUtil.ItemCallback<E>
): BasePagingAdapter<E, VH>(diffItemCallback) {

    /**
     * 创建单种的视图容器对象
     */
    abstract override fun createPagingViewHolder(parent: ViewGroup, viewType: Int): VH

    /**
     * 绑定对应视图的数据
     * @param item 该内容可为空，需要进行空处理
     */
    abstract override fun bindPagingItem(
        holder: VH,
        position: Int,
        item: E?,
        payloads: MutableList<Any>
    )
}