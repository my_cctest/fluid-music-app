package com.ccteam.fluidmusic.view.adapters.image

import com.ccteam.fluidmusic.view.components.CornerImageWrapperView
import com.ccteam.shared.result.moment.MomentListItem

/**
 * @author Xiaoc
 * @since 2021/3/28
 */

class MomentsImageWrapperAdapter(
    callback: CornerImageWrapperView.ImageEventListener<MomentListItem.MomentImageItem>
): CornerImageWrapperView.ImageWrapperAdapter<MomentListItem.MomentImageItem>(callback){

    override fun getTransformImageUrl(imageInfo: MomentListItem.MomentImageItem): String {
        return imageInfo.imageUrl
    }

}