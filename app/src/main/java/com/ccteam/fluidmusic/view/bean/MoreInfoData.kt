package com.ccteam.fluidmusic.view.bean

import android.net.Uri
import android.os.Parcelable
import com.ccteam.shared.result.artist.ArtistInfoData
import kotlinx.parcelize.Parcelize

/**
 * @author Xiaoc
 * @since 2021/3/5
 *
 * 需要传递给更多弹窗功能的数据
 * 因为更多弹窗中要展示一些数据，需要予以传递
 *
 * @param parentId 当前点击更多功能界面的parentId
 * @param mediaId 当前点击更多的媒体ID
 * @param mediaTitle 媒体标题
 * @param mediaSubtitle 媒体副标题
 * @param albumId 专辑ID
 * @param artists 歌手信息
 * @param mediaArtUri 媒体封面地址（如果为空，则不显示）
 * @param isOffline 是否为离线歌曲
 * @param inPlaylistShouldDelete 是否是删除
 * @param id 当该项有特殊值时使用（例如歌单歌曲有一个它们自己独立的歌单歌曲关联ID）
 * @param mediaUri 媒体的Uri（本地使用，用于标签信息的显示）
 * @param canShare 当前歌曲是否能够分享
 */
@Parcelize
data class MoreInfoData(
    val parentId: String,
    val mediaId: String,
    val mediaTitle: String,
    val mediaSubtitle: String,
    val isOffline: Boolean,
    val albumId: String? = null,
    val artists: List<ArtistInfoData> = emptyList(),
    val mediaArtUri: String? = null,
    val inPlaylistShouldDelete: Boolean = false,
    val id: String? = null,
    val mediaUri: Uri = Uri.EMPTY,
    val canShare: Boolean = false
): Parcelable

/**
 * @author Xiaoc
 * @since 2021/3/5
 *
 * 需要传递给更多弹窗功能的数据
 * 因为更多弹窗中要展示一些数据，需要予以传递
 * 该更多内容展示的搜索界面更多内容，需要提供相关音乐信息让它可以加入到播放列表
 *
 * @param id 媒体ID
 * @param mediaTitle 媒体标题
 * @param mediaSubtitle 媒体副标题
 * @param albumId 专辑ID
 * @param artists 歌手信息
 * @param imgUrl 媒体封面地址（如果为空，则不显示）
 * @param artistInfo 歌手信息简介
 * @param id 当该项有特殊值时使用（例如歌单歌曲有一个它们自己独立的歌单歌曲关联ID）
 * @param albumName 专辑名称
 * @param bitrateDTO 码率信息
 */
@Parcelize
data class SearchMoreInfoData(
    val id: String,
    val mediaTitle: String,
    val mediaSubtitle: String,
    val duration: Long,
    val track: Int,
    val imgUrl: String?,
    val artistInfo: String?,
    val artists: List<ArtistInfoData> = emptyList(),
    val albumId: String,
    val albumName: String,
    val mediaUrl: String?
): Parcelable

