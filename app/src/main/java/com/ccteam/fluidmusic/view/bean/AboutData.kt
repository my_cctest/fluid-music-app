package com.ccteam.fluidmusic.view.bean

import androidx.recyclerview.widget.DiffUtil

/**
 * @author Xiaoc
 * @since 2021/4/6
 *
 * 关于界面 副标题项
 */
data class AboutSubTitleItem(
    val title: String
){

    companion object{
        val differCallback = object: DiffUtil.ItemCallback<Any>(){

            override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean {
                return oldItem == newItem
            }

        }
    }
}

/**
 * 关于界面 开发者项
 */
data class AboutDeveloperItem(
    val developerPhotoUrl: String,
    val developerName: String
)

/**
 * 关于界面 开源库项
 */
data class AboutOpenSourceItem(
    val title: String,
    val openSourceUrl: String,
    val description: String
)