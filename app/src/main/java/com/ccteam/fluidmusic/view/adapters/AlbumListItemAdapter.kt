package com.ccteam.fluidmusic.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.ccteam.fluidmusic.R
import com.ccteam.fluidmusic.databinding.GlobalAlbumListItemBinding
import com.ccteam.fluidmusic.databinding.LocalMediaDataAlbumItemBinding
import com.ccteam.fluidmusic.view.adapters.AlbumListItemAdapter.AlbumListItemCallback
import com.ccteam.fluidmusic.view.adapters.paging.PagingAdapter
import com.ccteam.fluidmusic.view.adapters.viewbinding.MultiTypeViewBindingAdapter
import com.ccteam.shared.result.BaseItemData
import com.ccteam.shared.result.album.AlbumListItem

/**
 * @author Xiaoc
 * @since 2021/2/4
 *
 * 专辑列表项RecyclerView适配器
 * 其中包含了 Normal样式 和 Small样式 的专辑列表布局
 * 并设置了 [AlbumListItemCallback] 点击事件
 **/
class AlbumListItemAdapter(
    private val callback: AlbumListItemCallback
): MultiTypeViewBindingAdapter(AlbumListItem.differCallback) {

    interface AlbumListItemCallback {
        fun onClickItem(view: View, albumItem: AlbumListItem)
    }

    override fun createMultiViewBinding(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder? {
        return when(viewType){
            // 通用样式的专辑列表项
            BaseItemData.LAYOUT_ALBUM_LIST_ITEM ->{
                AlbumListItemNormalViewHolder(
                    GlobalAlbumListItemBinding.inflate(
                        LayoutInflater.from(parent.context),parent,false)
                    ,callback)
            }
            // 本地样式的专辑列表项
            BaseItemData.LAYOUT_ALBUM_LIST_ITEM_SMALL ->{
                AlbumListItemSmallViewHolder(
                    LocalMediaDataAlbumItemBinding.inflate(
                        LayoutInflater.from(parent.context),parent,false)
                    ,callback)
            }
            else ->
                null
        }
    }

    override fun bindMultiItem(
        holder: RecyclerView.ViewHolder,
        position: Int,
        item: BaseItemData,
        payloads: MutableList<Any>,
        viewType: Int
    ) {
        when(viewType){
            // 通用样式的专辑列表项
            BaseItemData.LAYOUT_ALBUM_LIST_ITEM ->{
                handleAlbumListItemNormal(holder as AlbumListItemNormalViewHolder,item as AlbumListItem)
            }
            // 本地样式的专辑列表项
            BaseItemData.LAYOUT_ALBUM_LIST_ITEM_SMALL ->{
                handleAlbumListItemSmall(holder as AlbumListItemSmallViewHolder,item as AlbumListItem)
            }
        }
    }


    private fun handleAlbumListItemNormal(holder: AlbumListItemNormalViewHolder,item: AlbumListItem){
        holder.item = item
        // 设置过渡动画
        holder.binding.ivAlbumItem.transitionName = item.albumId
        holder.binding.tvAlbumTitle.text = item.albumTitle
        holder.binding.tvAlbumArtist.text = item.subtitle
        Glide.with(holder.binding.ivAlbumItem)
            .load(item.albumArtUri)
            .transition(DrawableTransitionOptions.withCrossFade())
            .placeholder(R.drawable.img_default_album_browse)
            .thumbnail(0.3f)
            .into(holder.binding.ivAlbumItem)
    }

    private fun handleAlbumListItemSmall(holder: AlbumListItemSmallViewHolder,item: AlbumListItem){
        holder.item = item
        // 设置过渡动画
        holder.binding.ivThumbnailAlbumArt.transitionName = holder.binding.root.context.getString(R.string.local_music_browse_transition_name,item.albumId)

        holder.binding.tvMusicTitle.text = item.albumTitle
        holder.binding.tvMusicSubtitle.text = item.subtitle
        Glide.with(holder.binding.ivThumbnailAlbumArt)
            .load(item.albumArtUri)
            .transition(DrawableTransitionOptions.withCrossFade())
            .placeholder(R.drawable.img_default_album_browse)
            .thumbnail(0.3f)
            .into(holder.binding.ivThumbnailAlbumArt)
    }

    inner class AlbumListItemNormalViewHolder(
        binding: GlobalAlbumListItemBinding,
        callback: AlbumListItemCallback
    ): ViewBindViewHolder<GlobalAlbumListItemBinding>(binding){
        var item: AlbumListItem? = null

        init {
            binding.root.setOnClickListener {
                item?.let {
                    callback.onClickItem(binding.ivAlbumItem,it)
                }
            }
        }
    }

    inner class AlbumListItemSmallViewHolder(
        binding: LocalMediaDataAlbumItemBinding,
        callback: AlbumListItemCallback
    ): ViewBindViewHolder<LocalMediaDataAlbumItemBinding>(binding){
        var item: AlbumListItem? = null

        init {
            binding.root.setOnClickListener {
                item?.let {
                    callback.onClickItem(binding.ivThumbnailAlbumArt,it)
                }
            }
        }
    }
}

/**
 * 专辑列表项RecyclerView适配器（分页版）
 * 适配的是 [PagingAdapter] 分页适配器，内容与普通RecyclerView适配器相同
 */
class AlbumListItemPagingAdapter(
    private val callback: AlbumListItemAdapter.AlbumListItemCallback
): PagingAdapter<AlbumListItem, AlbumListItemPagingAdapter.AlbumListItemNormalViewHolder, GlobalAlbumListItemBinding>(AlbumListItem.differCallbackSingle){

    override fun createPagingViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AlbumListItemNormalViewHolder {
        return AlbumListItemNormalViewHolder(
            GlobalAlbumListItemBinding.inflate(LayoutInflater.from(parent.context),parent,false),callback)
    }

    override fun bindPagingItem(
        holder: AlbumListItemNormalViewHolder,
        position: Int,
        item: AlbumListItem?,
        payloads: MutableList<Any>
    ) {
        handleAlbumListItemNormal(holder,item)
    }

    private fun handleAlbumListItemNormal(holder: AlbumListItemNormalViewHolder, item: AlbumListItem?){
        // 设置专辑内容
        item?.let {
            holder.item = item
            holder.binding.ivAlbumItem.transitionName = item.albumId
            holder.binding.tvAlbumTitle.text = item.albumTitle
            holder.binding.tvAlbumArtist.text = item.subtitle
            Glide.with(holder.binding.ivAlbumItem)
                .load(item.albumArtUri)
                .transition(DrawableTransitionOptions.withCrossFade())
                .placeholder(R.drawable.img_default_album_browse)
                .thumbnail(0.3f)
                .into(holder.binding.ivAlbumItem)
        }
    }

    inner class AlbumListItemNormalViewHolder(
        binding: GlobalAlbumListItemBinding,
        callback: AlbumListItemAdapter.AlbumListItemCallback
    ): ViewBindViewHolder<GlobalAlbumListItemBinding>(binding){
        var item: AlbumListItem? = null

        init {
            binding.root.setOnClickListener {
                item?.let {
                    callback.onClickItem(binding.ivAlbumItem,it)
                }
            }
        }
    }
}

class ArtistAlbumListAdapter(
    private val callback: AlbumListItemAdapter.AlbumListItemCallback
): PagingAdapter<AlbumListItem, ArtistAlbumListAdapter.AlbumListItemSmallViewHolder, LocalMediaDataAlbumItemBinding>(AlbumListItem.differCallbackSingle) {

    inner class AlbumListItemSmallViewHolder(
        binding: LocalMediaDataAlbumItemBinding,
        callback: AlbumListItemAdapter.AlbumListItemCallback
    ): ViewBindViewHolder<LocalMediaDataAlbumItemBinding>(binding){
        var item: AlbumListItem? = null

        init {
            binding.root.setOnClickListener {
                item?.let {
                    callback.onClickItem(binding.ivThumbnailAlbumArt,it)
                }
            }
        }
    }

    override fun createPagingViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AlbumListItemSmallViewHolder {
        return AlbumListItemSmallViewHolder(
            LocalMediaDataAlbumItemBinding.inflate(
                LayoutInflater.from(parent.context),parent,false)
            ,callback)
    }

    override fun bindPagingItem(
        holder: AlbumListItemSmallViewHolder,
        position: Int,
        item: AlbumListItem?,
        payloads: MutableList<Any>
    ) {
        item?.let {
            holder.item = it
            // 设置过渡动画
            holder.binding.ivThumbnailAlbumArt.transitionName = item.albumId

            holder.binding.tvMusicTitle.text = it.albumTitle
            holder.binding.tvMusicSubtitle.text = it.subtitle
            Glide.with(holder.binding.ivThumbnailAlbumArt)
                .load(it.albumArtUri)
                .transition(DrawableTransitionOptions.withCrossFade())
                .placeholder(R.drawable.img_default_album_browse)
                .thumbnail(0.3f)
                .into(holder.binding.ivThumbnailAlbumArt)
        }

    }
}