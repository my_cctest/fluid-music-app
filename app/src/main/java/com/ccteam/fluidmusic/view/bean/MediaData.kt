package com.ccteam.fluidmusic.view.bean

import android.net.Uri
import androidx.recyclerview.widget.DiffUtil

/**
 * 正在播放的音乐的UI基本信息
 * 此数据类不包含专辑封面的Uri
 * 专辑封面Uri在 [com.ccteam.fluidmusic.viewmodels.MainActivityViewModel]中单独处理
 * 因存在多首歌曲相同专辑封面的情况，故不会每次都去刷新封面
 * @see com.ccteam.fluidmusic.viewmodels.NowPlayingViewModel.updateMediaMetadata
 */
data class NowPlayingMediaData(
    val mediaId: String,
    val title: String?,
    val artist: String?,
    val album: String?,
    val duration: Long,
    val isOffline: Boolean,
    val mediaUri: Uri
)

data class PlaylistMediaData(
    val mediaId: String,
    val isPlaying: Boolean,
    val queueId: Long,
    val title: String?,
    val artist: String?,
    val albumArtUri: Uri?
){

    companion object{

        val diffCallback = object : DiffUtil.ItemCallback<PlaylistMediaData>(){

            override fun areItemsTheSame(oldItem: PlaylistMediaData, newItem: PlaylistMediaData): Boolean =
                oldItem.mediaId == newItem.mediaId

            override fun areContentsTheSame(oldItem: PlaylistMediaData, newItem: PlaylistMediaData): Boolean =
                oldItem.mediaId == newItem.mediaId
                        && oldItem.isPlaying == newItem.isPlaying && oldItem.queueId == newItem.queueId

            override fun getChangePayload(
                oldItem: PlaylistMediaData,
                newItem: PlaylistMediaData
            ): Any? {
                if(oldItem.isPlaying != newItem.isPlaying){
                    return null
                }
                return super.getChangePayload(oldItem, newItem)
            }
        }
    }

}

/**
 * 当前播放音乐改变时，给 [androidx.recyclerview.widget.RecyclerView] 的 Payload 赋值
 * 代表当前音乐发生变化
 */
const val METADATA_CHANGED = 1

/**
 * 当前播放音乐的状态改变时，给 [androidx.recyclerview.widget.RecyclerView] 的 Payload 赋值
 * 代表当前音乐播放状态发生变化
 */
const val PLAYBACK_CHANGED = 2