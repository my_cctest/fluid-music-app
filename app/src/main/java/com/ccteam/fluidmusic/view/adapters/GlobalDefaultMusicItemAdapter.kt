package com.ccteam.fluidmusic.view.adapters

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.ccteam.fluidmusic.R
import com.ccteam.fluidmusic.databinding.DefaultMusicItemBinding
import com.ccteam.fluidmusic.databinding.SimpleMusicItemBinding
import com.ccteam.fluidmusic.utils.getColor
import com.ccteam.fluidmusic.view.adapters.viewbinding.MultiTypeViewBindingAdapter
import com.ccteam.fluidmusic.view.bean.*
import com.ccteam.model.music.BITRATE_NO
import com.ccteam.model.music.BITRATE_SQ
import com.ccteam.shared.result.BaseItemData
import com.ccteam.shared.result.BaseItemData.Companion.LAYOUT_DEFAULT_MUSIC_ITEM
import com.ccteam.shared.result.music.DefaultMusicItem
import com.ccteam.shared.utils.NO_PLAYING
import com.google.android.material.color.MaterialColors


/**
 * @author Xiaoc
 * @since 2021/2/9
 **/
class GlobalDefaultMusicItemAdapter(
    private val callback: DefaultMusicItemClickCallback
): MultiTypeViewBindingAdapter(DefaultMusicItem.differCallback)  {

    override fun createMultiViewBinding(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder? {
        return when(viewType){
            LAYOUT_DEFAULT_MUSIC_ITEM ->{
                return DefaultMusicItemViewHolder(
                    callback,
                    DefaultMusicItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
                )
            }
            else -> null
        }
    }

    override fun bindMultiItem(
        holder: RecyclerView.ViewHolder,
        position: Int,
        item: BaseItemData,
        payloads: MutableList<Any>,
        viewType: Int
    ) {
        var fullRefresh = payloads.isEmpty()

        when(viewType){
            LAYOUT_DEFAULT_MUSIC_ITEM ->{
                if(payloads.isNotEmpty()){
                    payloads.forEach { payload ->
                        when(payload){
                            METADATA_CHANGED -> {
                                // 当前播放内容更改时进行的局部UI更新
                                updateActiveItem(holder as DefaultMusicItemViewHolder,item as DefaultMusicItem)
                            }
                            else ->
                                fullRefresh = true
                        }
                    }
                }

                // 完整更新列表内容
                if(fullRefresh) {
                    updateDefaultMusicItem(holder as DefaultMusicItemViewHolder,item as DefaultMusicItem)

                }
            }
        }

    }

    private fun updateDefaultMusicItem(holder: DefaultMusicItemViewHolder,item: DefaultMusicItem){
        holder.item = item
        holder.binding.tvMusicTitle.text = item.title
        holder.binding.tvMusicSubtitle.text = item.subtitle
        holder.binding.tvBitrate.isVisible = item.bitrate == BITRATE_SQ
        // 设置播放项状态（播放 or 未播放）
        updateActiveItem(holder,item)
        // 如果当前内容为可播放，但是却没有音乐，说明是特殊情况，不显示更多按钮
        holder.binding.btnMore.isVisible = !(item.playable && item.bitrate == BITRATE_NO)

        Glide.with(holder.binding.ivThumbnailAlbumArt)
            .load(item.albumArtUri)
            .transition(DrawableTransitionOptions.withCrossFade())
            .placeholder(R.drawable.img_default_album)
            .thumbnail(0.3f)
            .into(holder.binding.ivThumbnailAlbumArt)
    }

    private fun updateActiveItem(holder: DefaultMusicItemViewHolder,item: DefaultMusicItem){
        when(item.playbackRes){
            // 如果没有播放状态的项，则是默认状态
            NO_PLAYING ->{
                holder.binding.root.isEnabled = item.playable
                holder.binding.btnMore.isEnabled = item.playable
                if(item.playable){
                    holder.binding.tvMusicTitle.setTextColor(MaterialColors.getColor(holder.binding.tvMusicTitle, R.attr.colorOnSurface))
                    holder.binding.tvMusicSubtitle.setTextColor(holder.binding.tvMusicSubtitle.getColor(R.color.defaultTextColor))
                    holder.binding.btnMore.iconTint = ColorStateList.valueOf(MaterialColors.getColor(holder.binding.tvMusicTitle, R.attr.colorOnSurface))
                } else {
                    holder.binding.tvMusicTitle.setTextColor(holder.binding.tvMusicTitle.getColor(R.color.disableTextColor))
                    holder.binding.tvMusicSubtitle.setTextColor(holder.binding.tvMusicSubtitle.getColor(R.color.disableTextColor))
                    holder.binding.btnMore.iconTint = ColorStateList.valueOf(holder.binding.tvMusicTitle.getColor(R.color.disableTextColor))
                }
                holder.binding.ivMusicWave.cancelAnimation()
                holder.binding.ivMusicWave.visibility = View.GONE
            }
            else ->{
                val color =
                    MaterialColors.getColor(holder.binding.tvMusicTitle, R.attr.colorPrimary)
                holder.binding.tvMusicTitle.setTextColor(color)
                holder.binding.tvMusicSubtitle.setTextColor(color)
                holder.binding.ivMusicWave.playAnimation()
                holder.binding.ivMusicWave.visibility = View.VISIBLE
            }

        }
    }

    class DefaultMusicItemViewHolder(
        callback: DefaultMusicItemClickCallback,
        binding: DefaultMusicItemBinding
    ): ViewBindViewHolder<DefaultMusicItemBinding>(binding){
        var item: DefaultMusicItem? = null

        init {
            binding.root.setOnClickListener {
                item?.let { data ->
                    if(data.playable){
                        callback.musicItemClicked(data)
                    }
                }
            }
            binding.root.setOnLongClickListener {
                item?.let { data ->
                    if(data.playable && data.bitrate != BITRATE_NO){
                        callback.moreClicked(data)
                        return@setOnLongClickListener true
                    }
                    return@setOnLongClickListener false
                }
                return@setOnLongClickListener false
            }
            binding.btnMore.setOnClickListener {
                item?.let { data ->
                    if(data.playable && data.bitrate != BITRATE_NO){
                        callback.moreClicked(data)
                    }
                }
            }
        }
    }

    class SimpleMusicItemViewHolder(
        callback: DefaultMusicItemClickCallback,
        binding: SimpleMusicItemBinding
    ): ViewBindViewHolder<SimpleMusicItemBinding>(binding){
        var item: DefaultMusicItem? = null

        init {
            binding.root.setOnClickListener {
                item?.let { data ->
                    callback.musicItemClicked(data)
                }
            }
            binding.root.setOnLongClickListener {
                item?.let { data ->
                    if(data.playable && data.bitrate != BITRATE_NO){
                        callback.moreClicked(data)
                        return@setOnLongClickListener true
                    }
                    return@setOnLongClickListener true
                }
                return@setOnLongClickListener false
            }
            binding.btnMore.setOnClickListener {
                item?.let { data ->
                    if(data.playable && data.bitrate != BITRATE_NO){
                        callback.moreClicked(data)
                    }
                }
            }
        }
    }

    interface DefaultMusicItemClickCallback{
        fun musicItemClicked(item: DefaultMusicItem)
        fun moreClicked(item: DefaultMusicItem)
    }
}