package com.ccteam.fluidmusic.view.adapters.paging

import androidx.core.view.isVisible
import androidx.paging.LoadState
import com.ccteam.fluidmusic.databinding.GlobalPagingLoadStateItemBinding
import com.ccteam.fluidmusic.view.adapters.ViewBindViewHolder
import com.orhanobut.logger.Logger

/**
 * @author Xiaoc
 * @since 2021/2/16
 *
 * 分页加载状态的视图容器
 * 它同样基于 [ViewBindViewHolder]
 */
class LoadStateViewHolder(
    retry: () -> Unit,
    binding: GlobalPagingLoadStateItemBinding
): ViewBindViewHolder<GlobalPagingLoadStateItemBinding>(binding) {

    init {
        binding.btnStateRetry.setOnClickListener {
            retry()
        }
    }

    /**
     * 提供一个方法
     * 该方法接收 [LoadState] 来判断当前处于何种状态
     * 然后进行何种显示
     *
     * @param loadState 加载状态
     */
    fun bindState(loadState: LoadState){
        if(loadState is LoadState.Error){
            binding.tvStateError.text = loadState.error.localizedMessage
        }
        binding.progressLoading.isVisible = loadState is LoadState.Loading
        binding.tvStateEmpty.isVisible = loadState.endOfPaginationReached && loadState is LoadState.NotLoading
        binding.tvStateError.isVisible = loadState is LoadState.Error
        binding.btnStateRetry.isVisible = loadState is LoadState.Error
    }
}