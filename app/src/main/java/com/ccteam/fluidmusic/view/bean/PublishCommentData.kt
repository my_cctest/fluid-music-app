package com.ccteam.fluidmusic.view.bean

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

/**
 * @author Xiaoc
 * @since 2021/3/30
 *
 * 发布评论时需要传递的评论数据
 * @param momentId 朋友圈ID
 * @param type 评论类型
 * @param toUserId 评论给别人的用户ID（回复类型使用）
 * @param toUserName 评论给别人的用户名（用户编辑框显示）
 * @param commentId 具体回复给哪条评论（回复类型使用）
 */
@Parcelize
data class PublishCommentData(
    val momentId: String,
    val type: Int,
    val toUserId: String?,
    val toUserName: String,
    val commentId: String?
): Parcelable