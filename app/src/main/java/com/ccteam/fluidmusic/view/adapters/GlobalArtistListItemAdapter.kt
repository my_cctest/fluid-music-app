package com.ccteam.fluidmusic.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ccteam.fluidmusic.R
import com.ccteam.fluidmusic.databinding.GlobalArtistListItemBinding
import com.ccteam.fluidmusic.view.adapters.paging.PagingAdapter
import com.ccteam.fluidmusic.view.adapters.viewbinding.MultiTypeViewBindingAdapter
import com.ccteam.shared.result.BaseItemData
import com.ccteam.shared.result.artist.ArtistListItem

/**
 * @author Xiaoc
 * @since 2021/2/9
 *
 * 全局歌手信息列表RecyclerView适配器
 * 展示的是歌手列表的内容
 **/
class GlobalArtistListItemAdapter(
    private val callback: ArtistListItemClickCallback
): MultiTypeViewBindingAdapter(ArtistListItem.differCallback) {

    override fun createMultiViewBinding(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder? {
        return when(viewType){
            BaseItemData.LAYOUT_ARTIST_LIST_ITEM ->{
                ArtistListItemViewHolder(callback,GlobalArtistListItemBinding.inflate(
                    LayoutInflater.from(parent.context),parent,false))
            }
            else ->{
                null
            }
        }
    }

    override fun bindMultiItem(
        holder: RecyclerView.ViewHolder,
        position: Int,
        item: BaseItemData,
        payloads: MutableList<Any>,
        viewType: Int
    ) {
        when(viewType){
            BaseItemData.LAYOUT_ARTIST_LIST_ITEM ->{
                handleArtistListItem(holder as ArtistListItemViewHolder,item as ArtistListItem)
            }
        }
    }

    private fun handleArtistListItem(holder: ArtistListItemViewHolder,item: ArtistListItem){
        holder.item = item
        // 设置共享动画值为唯一值歌手ID
        holder.binding.ivArtistHeadImg.transitionName = item.artistId

        holder.binding.tvArtistName.text = item.artistName
        // 如果subtitle为空则隐藏控件不显示
        holder.binding.group.isVisible = item.subtitle != null
        item.subtitle?.let {
            holder.binding.tvArtistTrackNum.text = it.trackNum.toString()
            holder.binding.tvArtistAlbumNum.text = it.albumNum.toString()
        }
        Glide.with(holder.binding.ivArtistHeadImg)
            .load(item.artistPhoto)
            .placeholder(R.drawable.img_default_artist_small)
            .into(holder.binding.ivArtistHeadImg)
    }

    inner class ArtistListItemViewHolder(
        callback: ArtistListItemClickCallback,
        binding: GlobalArtistListItemBinding
    ): ViewBindViewHolder<GlobalArtistListItemBinding>(binding){

        var item: ArtistListItem? = null

        init {
            // 设置歌手项点击事件
            binding.root.setOnClickListener {
                item?.let { data ->
                    callback.artistItemClicked(binding.ivArtistHeadImg,data)
                }
            }
        }
    }
}

/**
 * 全局歌手信息列表RecyclerView适配器（分页版）
 * 展示的是歌手列表的内容
 **/
class ArtistListPagingAdapter(
    private val callback: ArtistListItemClickCallback
): PagingAdapter<ArtistListItem,
        ArtistListPagingAdapter.ArtistListItemViewHolder,
        GlobalArtistListItemBinding>(ArtistListItem.differCallbackSingle){

    private fun handleArtistListItem(holder: ArtistListItemViewHolder,item: ArtistListItem?){
        item?.let {
            holder.item = item
            // 设置共享动画值为唯一值歌手ID
            holder.binding.ivArtistHeadImg.transitionName = item.artistId
            holder.binding.tvArtistName.text = item.artistName
            holder.binding.group.isVisible = item.subtitle != null
            item.subtitle?.let {
                holder.binding.tvArtistTrackNum.text = it.trackNum.toString()
                holder.binding.tvArtistAlbumNum.text = it.albumNum.toString()
            }
            Glide.with(holder.binding.ivArtistHeadImg)
                .load(item.artistPhoto)
                .placeholder(R.drawable.img_default_artist_small)
                .into(holder.binding.ivArtistHeadImg)
        }
    }

    override fun createPagingViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ArtistListItemViewHolder {
        return ArtistListItemViewHolder(callback,GlobalArtistListItemBinding.inflate(
            LayoutInflater.from(parent.context),parent,false))
    }

    override fun bindPagingItem(
        holder: ArtistListItemViewHolder,
        position: Int,
        item: ArtistListItem?,
        payloads: MutableList<Any>
    ) {
        handleArtistListItem(holder,item)
    }

    inner class ArtistListItemViewHolder(
        callback: ArtistListItemClickCallback,
        binding: GlobalArtistListItemBinding
    ): ViewBindViewHolder<GlobalArtistListItemBinding>(binding){
        var item: ArtistListItem? = null

        init {
            // 设置歌手项点击事件
            binding.root.setOnClickListener {
                item?.let {
                    callback.artistItemClicked(binding.ivArtistHeadImg,it)
                }
            }
        }
    }
}

interface ArtistListItemClickCallback{
    fun artistItemClicked(view: View,item: ArtistListItem)
}