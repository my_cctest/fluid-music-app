package com.ccteam.fluidmusic.di

import androidx.work.Constraints
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequest
import androidx.work.PeriodicWorkRequest
import com.ccteam.fluidmusic.utils.preference.SettingWorker
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * @author Xiaoc
 * @since 2021/4/16
 */
@Module
@InstallIn(SingletonComponent::class)
class WorkManagerModule {

    /**
     * 注入 设置 同步WorkManager（定期任务）
     */
    @Provides
    fun provideSettingWorkManagerRequest(): PeriodicWorkRequest = PeriodicWorkRequest
        .Builder(SettingWorker::class.java,5, TimeUnit.DAYS)
        .setInitialDelay(0L, TimeUnit.MILLISECONDS)
        .setConstraints(
            Constraints.Builder()
            .setRequiresStorageNotLow(true)
            .setRequiredNetworkType(NetworkType.UNMETERED)
            .build())
        .build()

    /**
     * 注入 设置 同步WorkManager（一次性工作）
     */
    @Provides
    fun provideOnceSettingWorkManagerRequest(): OneTimeWorkRequest = OneTimeWorkRequest
        .Builder(SettingWorker::class.java)
        .setConstraints(
            Constraints.Builder()
                .setRequiresStorageNotLow(true)
                .setRequiredNetworkType(NetworkType.UNMETERED)
                .build())
        .build()
}