package com.ccteam.fluidmusic.core

import com.ccteam.taglib.core.MediaTag
import com.ccteam.taglib.core.TaglibLibrary
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Xiaoc
 * @since 2021/4/3
 *
 * Taglib标签库解析库
 * 调用 [TaglibLibrary] 库进行对应内容的获取
 */
@Singleton
class TaglibParser @Inject constructor(){

    private val taglibLibrary = TaglibLibrary()

    /**
     * 得到Base（基本）媒体标签，返回 [MediaTag] 类
     * 其中存储着通用基本的媒体标签
     *
     * @param fd 媒体描述符，通过该媒体描述符进行native层读取数据
     * @return 媒体通过标签信息，如果native层没有获取到，则返回内容为默认的 MediaTag 空值
     *
     * 该方法在 IO 线程中执行
     */
    suspend fun getMediaTag(fd: Int): MediaTag{
        return withContext(Dispatchers.IO){
            taglibLibrary.getMediaTag(fd)
        }
    }

    /**
     * 设置Base（基本）媒体标签，返回 是否修改成功
     *
     * @param mediaTag 媒体信息，具体修改值并不是该类所有内容，由native层决定
     * @param fd 媒体描述符，通过该媒体描述符进行native层读取数据
     * @return 是否修改成功
     *
     * 该方法在 IO 线程中执行
     */
    suspend fun setMediaTag(mediaTag: MediaTag,fd: Int): Boolean{
        return withContext(Dispatchers.IO){
            taglibLibrary.setMediaTag(mediaTag,fd)
        }
    }

    /**
     * 得到歌词内嵌歌词
     *
     * @param fd 媒体描述符，通过该媒体描述符进行native层读取数据
     * @return 歌词字符串
     *
     * 该方法在 IO 线程中执行
     */
    suspend fun getLyricsByTaglib(fd: Int): String{
        return withContext(Dispatchers.IO){
            taglibLibrary.getLyricsByTaglib(fd)
        }
    }
}