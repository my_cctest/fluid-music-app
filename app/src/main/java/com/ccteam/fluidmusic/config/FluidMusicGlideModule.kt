package com.ccteam.fluidmusic.config

import android.content.Context
import android.net.Uri
import android.os.Build
import com.bumptech.glide.Glide
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule
import java.io.InputStream
import java.nio.ByteBuffer

/**
 *
 * @author Xiaoc
 * @since 2021/1/7
 *
 * 告诉Glide我们需要自定义的 [com.bumptech.glide.load.model.ModelLoader]
 * 此处当 Android 版本大于Q后，加载 [MediaStoreAlbumArtModelLoader]
 *
 */
@GlideModule
class FluidMusicGlideModule: AppGlideModule() {

    override fun registerComponents(context: Context, glide: Glide, registry: Registry) {
        super.registerComponents(context, glide, registry)
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
            registry.prepend(
                Uri::class.java,
                InputStream::class.java,
                MediaStoreAlbumArtModelLoader.BitmapToInputStreamFactory(context.contentResolver))
        }
    }
}