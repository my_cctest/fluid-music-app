package com.ccteam.fluidmusic.utils

import android.content.Context
import android.view.View
import androidx.annotation.AttrRes
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import com.ccteam.fluidmusic.R
import com.google.android.material.color.MaterialColors
import com.google.android.material.transition.MaterialContainerTransform
import com.google.android.material.transition.MaterialElevationScale

/**
 * @author Xiaoc
 * @since 2021/4/11
 *
 * 基本动画内容存储
 * 防止每次进入都创建相同动画
 */
object TransitionUtils {

    /**
     * 退出缩放动画
     */
    val exitScale = MaterialElevationScale(/* growing= */ false).apply {
        duration = TRANSITION_MEDIUM
    }

    /**
     * 进入缩放动画
     */
    val enterScale = MaterialElevationScale(/* growing= */ true).apply {
        duration = TRANSITION_MEDIUM
    }
}

const val TRANSITION_SLOW = 275L
const val TRANSITION_MEDIUM = 225L
const val TRANSITION_FAST = 150L

/**
 * 生成容器转变动画对象
 * 该对象具有以下属性
 * 1.绘制的ViewId，默认为主MainFragmentHost容器的Id
 * 2.时长（默认[TRANSITION_SLOW]）
 * 3.开始的容器绘制颜色
 * 4.结束的容器绘制颜色
 * 加上3、4是因为防止进行动画时背景绘制透明，导致很难看的问题
 */
fun Fragment.generateContainerTransform(view: View,
                                        @IdRes drawingId: Int = R.id.main_nav_host_container,
                                        duration: Long = TRANSITION_SLOW,
                                        @AttrRes containerColor: Int = R.attr.colorSurface): MaterialContainerTransform =
    generateContainerTransform(view.context,drawingId,duration,containerColor)

fun Fragment.generateContainerTransform(context: Context,
                                        @IdRes drawingId: Int = R.id.main_nav_host_container,
                                        duration: Long = TRANSITION_SLOW,
                                        @AttrRes containerColor: Int = R.attr.colorSurface): MaterialContainerTransform {
    val colorSurface = context.getAttrColor(containerColor)
    return MaterialContainerTransform().apply {
        drawingViewId = drawingId
        setDuration(duration)
        isElevationShadowEnabled = false
        startContainerColor = colorSurface
        endContainerColor = colorSurface
    }
}