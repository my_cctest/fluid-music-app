package com.ccteam.fluidmusic.utils

import android.content.Context
import com.ccteam.fluidmusic.R
import com.ccteam.fluidmusic.view.bean.AboutDeveloperItem
import com.ccteam.fluidmusic.view.bean.AboutOpenSourceItem
import com.ccteam.fluidmusic.view.bean.AboutSubTitleItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * @author Xiaoc
 * @since 2021/1/30
 **/
object DataTestUtils {

//    fun generateLocalMusicHomeItems(): List<BaseItemData>{
//        val list = mutableListOf<BaseItemData>()
//        list.add(ToolbarTitleItem("在线音乐"))
//        list.add(OnlineMusicHomeTypeBrowseItem(
//            mutableListOf(OnlineMusicBanner("","",0), OnlineMusicBanner("","",0))))
//        list.add(
//            OnlineMusicHomeSmallItem(
//                "新专上架", 0,mutableListOf(
//            OnlineMusicHomeInnerSmallItem("1","","folklore","TaylorSwift"),
//            OnlineMusicHomeInnerSmallItem("2","","evermore","TaylorSwift")
//                    , OnlineMusicHomeInnerSmallItem("3","","摩天动物园","邓紫棋")
//                ))
//        )
//        list.add(
//            OnlineMusicHomeSmallItem(
//                "新歌上架", 1,mutableListOf(
//            OnlineMusicHomeInnerSmallItem("1","","the 1","folklore"),
//            OnlineMusicHomeInnerSmallItem("2","","cardigan","folklore"),
//                    OnlineMusicHomeInnerSmallItem("3","","爱情买卖","慕容晓晓")
//                ))
//        )
//        list.add(
//            OnlineMusicHomeSmallItem(
//                "歌单推荐", 2,mutableListOf(
//            OnlineMusicHomeInnerSmallItem("1","","夏日必听神曲","Xiaoc"),
//            OnlineMusicHomeInnerSmallItem("2","","冬日必听神曲","Ghost"),
//                    OnlineMusicHomeInnerSmallItem("4","","那些感谢人的歌词","Xiaoc"),
//            OnlineMusicHomeInnerSmallItem("3","","喜欢你","123")
//                ))
//        )
//        return list
//    }
//
//    fun generateAlbumListItem(): List<BaseItemData>{
//        return mutableListOf(
//            ToolbarTitleItem("专辑"),
//            AlbumListItem("","Folklore","","TaylorSwift","TaylorSwift","0",null,null),
//            AlbumListItem("","Folklore","","TaylorSwift","TaylorSwift","0",null,null),
//            AlbumListItem("","Folklore","","TaylorSwift","TaylorSwift","0",null,null),
//            AlbumListItem("","Folklore","","TaylorSwift","TaylorSwift","0",null,null),
//            AlbumListItem("","Folklore","","TaylorSwift","TaylorSwift","0",null,null),
//            AlbumListItem("","Folklore","","TaylorSwift","TaylorSwift","0",null,null),)
//    }
//
//    fun generateArtistListItem(): List<BaseItemData>{
//        return mutableListOf(
//            ToolbarTitleItem("歌手"),
//            ArtistListItem("邓紫棋","","",null,""),
//            ArtistListItem("","","",null,""),
//            ArtistListItem("","","",null,""),
//            ArtistListItem("","","",null,""),
//            ArtistListItem("","","",null,""))
//    }

//    fun generateAlbumDetailItem(): List<BaseItemData>{
//        return mutableListOf(
//            AlbumDetailHeaderItem("","", "","","","",""),
//            AlbumDetailMusicItem("","","","","",2,"",true,0),
//            AlbumDetailMusicItem("","","","","",2,"",true,0),
//            AlbumDetailMusicItem("","","","","",2,"",true,0),
//            AlbumDetailMusicItem("","","","","",2,"",true,0),
//            AlbumDetailMusicItem("","","","","",2,"",true,0),
//            AlbumDetailMusicItem("","","","","",2,"",true,0))
//    }
//
//    fun generateDefaultMusicItem(): List<BaseItemData>{
//        return mutableListOf(
//            DefaultMusicItem("","folklore","","","","","",false,NO_PLAYING),
//            DefaultMusicItem("","mad woman","","","","","",false,PLAYING),
//            DefaultMusicItem("","xiaomi","","","","","",false,NO_PLAYING),
//            DefaultMusicItem("","Xiaoc","","","","","",false,NO_PLAYING),
//            DefaultMusicItem("","Ghost","","","","","",false,NO_PLAYING),
//            DefaultMusicItem("","HU","","","","","",false,NO_PLAYING),
//            DefaultMusicItem("","HU","","","","","",false,NO_PLAYING),
//            DefaultMusicItem("","HU","","","","","",false,NO_PLAYING),
//            DefaultMusicItem("","HU","","","","","",false,NO_PLAYING),
//            DefaultMusicItem("","HU","","","","","",false,NO_PLAYING),
//            DefaultMusicItem("","HU","","","","","",false,NO_PLAYING),
//            DefaultMusicItem("","HU","","","","","",false,NO_PLAYING),
//            DefaultMusicItem("","HU","","","","","",false,NO_PLAYING)
//        )
//    }

//    fun generateLyrics(): List<BaseItemData>{
//        return LyricsUtils.parseLyrics("[00:00.13]Taylor Swift - cardigan\n" +
//                "[00:00.73]Written by：Taylor Swift、Aaron Dessner\n" +
//                "[00:00.94]© 2020 TASRM Publishing、administered by：Songs Of Universal、Inc. (BMI), Ingrid Stella Music、administered by SonyATV Tunes LLC (ASCAP)\n" +
//                "[00:01.69]All Rights Reserved. Used by Permission.\n" +
//                "[00:01.90]Produced by：Aaron Dessner\n" +
//                "[00:02.05]Recorded by：Jonathan Low and Aaron Dessner at Long Pond (Hudson Valley、NY)\n" +
//                "[00:02.50]Vocals recorded by：Laura Sisk at the Kitty Committee Studio (Los Angeles、CA)\n" +
//                "[00:02.96]Additional recording by：Aaron Dessner at Gaite Lyrique (Paris、FR) and Bella Blasko on tour with The National(Stuttgart、DE)\n" +
//                "[00:03.67]Mixed by：Jonathan Low at Long Pond (Hudson Valley、NY)\n" +
//                "[00:04.02]Mastered by：Randy Merrill at Sterling Sound (New York、NY)\n" +
//                "[00:04.38]Drum Programming、Percussion、Piano、Bass、Electric Guitars、Mellotron and Synthesizers by：Aaron Dessner (Paris、FR and Hudson Valley、NY)\n" +
//                "[00:05.08]Orchestration by：Bryce Dessner (Biarritz、FR)\n" +
//                "[00:05.28]Modular Synth by：Benjamin Lanz (Paris、FR) recorded by Bella Blasko\n" +
//                "[00:05.69]Trombone by：Dave Nelson (Bone Hollow Recording; Accord、NY) recorded by Dave Nelson\n" +
//                "[00:06.15]Drum Programming by：James McAlister (Los Angeles、CA) recorded by James McAlister\n" +
//                "[00:06.55]Viola and Violin by：Yuki Numata Resnick (Buffalo、NY) recorded by Kyle Resnick\n" +
//                "[00:07.06]Cello by：Clarice Jensen (Brooklyn、NY) recorded by Clarice Jensen\n" +
//                "[00:07.56]Vintage tee brand new phone\n" +
//                "[00:10.65]High heels on cobblestones\n" +
//                "[00:14.49]When you are young they assume you know nothing\n" +
//                "[00:21.73]Sequin smile black lipstick\n" +
//                "[00:25.32]Sensual politics\n" +
//                "[00:29.06]When you are young they assume you know nothing\n" +
//                "[00:36.34]But I knew you\n" +
//                "[00:38.26]Dancing in your Levi's\n" +
//                "[00:40.60]Drunk under a streetlight\n" +
//                "[00:43.84]I knew you\n" +
//                "[00:45.51]Hand under my sweatshirt\n" +
//                "[00:47.78]Baby kiss it better\n" +
//                "[00:51.12]And when I felt like I was an old cardigan under someone's bed\n" +
//                "[00:58.67]You put me on and said I was your favorite\n" +
//                "[01:05.90]A friend to all is a friend to none\n" +
//                "[01:09.70]Chase two girls lose the one\n" +
//                "[01:13.40]When you are young they assume you know nothing\n" +
//                "[01:20.64]But I knew you\n" +
//                "[01:22.61]Playing hide-and-seek and\n" +
//                "[01:24.72]Giving me your weekends\n" +
//                "[01:28.17]I knew you\n" +
//                "[01:29.73]Your heartbeat on the High Line\n" +
//                "[01:32.02]Once in twenty lifetimes\n" +
//                "[01:35.42]And when I felt like I was an old cardigan under someone's bed\n" +
//                "[01:42.96]You put me on and said I was your favorite\n" +
//                "[02:03.15]To kiss in cars and downtown bars\n" +
//                "[02:06.74]Was all we needed\n" +
//                "[02:10.48]You drew stars around my scars\n" +
//                "[02:14.12]But now I'm bleeding\n" +
//                "[02:19.69]Cause I knew you\n" +
//                "[02:21.66]Stepping on the last train\n" +
//                "[02:23.53]Marked me like a bloodstain\n" +
//                "[02:27.28]I knew you\n" +
//                "[02:29.05]Tried to change the ending\n" +
//                "[02:30.92]Peter losing Wendy\n" +
//                "[02:34.66]I knew you\n" +
//                "[02:36.48]Leaving like a father\n" +
//                "[02:38.30]Running like water\n" +
//                "[02:41.94]When you are young they assume you know nothing\n" +
//                "[02:47.41]But I knew you'd linger like a tattoo kiss\n" +
//                "[02:51.01]I knew you'd haunt all of my what-ifs\n" +
//                "[02:54.70]The smell of smoke would hang around this long\n" +
//                "[02:58.39]Cause I knew everything when I was young\n" +
//                "[03:02.08]I knew I'd curse you for the longest time\n" +
//                "[03:06.03]Chasing shadows in the grocery line\n" +
//                "[03:09.63]I knew you'd miss me once the thrill expired\n" +
//                "[03:13.17]And you'd be standing in my front porch light\n" +
//                "[03:16.60]And I knew you'd come back to me\n" +
//                "[03:20.86]You'd come back to me\n" +
//                "[03:24.35]And you'd come back to me\n" +
//                "[03:28.00]And you'd come back\n" +
//                "[03:33.77]And when I felt like I was an old cardigan\n" +
//                "[03:37.87]Under someone's bed\n" +
//                "[03:41.10]You put me on and said I was your favorite")
//    }

//    fun generatePlaylistSheet(): PagingData<PlaylistSheetListItem>{
//        return PagingData.from(listOf(
//            PlaylistSheetListItem("123","这是你应该赶紧听的歌曲",
//                "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg"),
//            PlaylistSheetListItem("124","这是你应该赶紧听的歌曲2",
//                "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg"),
//            PlaylistSheetListItem("125","这是你应该赶紧听的歌曲3",
//                "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg"),
//            PlaylistSheetListItem("126","这是你应该赶紧听的歌曲4",
//                "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg")
//        ))
//    }
//
//    fun generatePlaylistHeader(): List<PlaylistSheetDetailHeaderItem>{
//        return listOf(
//            PlaylistSheetDetailHeaderItem("123","这是你应该赶紧听的歌曲",
//                "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg",
//                "123","https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg","Xiaoc","介绍lalalalaalalallaa",0),
//        )
//    }
//
//    fun generateMomentsList(): List<MomentListItem>{
//        return listOf(
//            MomentListItem("123","1",
//                "Xiaoc",
//                "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg",
//                "12分钟前","今天我分享TaylorSwift", emptyList(),null,4,20),
//            MomentListItem("122","4",
//                "Ghost",
//                "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg",
//                "15分钟前","今天我分享GEM邓紫棋，特别喜欢她，最近又出新歌曲了，爱你到永远", listOf(
//                    MomentListItem.MomentImageItem("1","1","https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg"),
//                    MomentListItem.MomentImageItem("1","1","https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg"),
//                    MomentListItem.MomentImageItem("1","1","https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg"),
//                ),
//                MomentListItem.MomentShareItem(
//                    "123", MOMENT_SHARE_TYPE_PLAYLIST,"句号","GEM邓紫棋",
//                    "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg"
//                ),7,43),
//
//            MomentListItem("125","12",
//                "小飞侠",
//                "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg",
//                "15分钟前","今天我分享言语行政，有意境！", listOf(
//                    MomentListItem.MomentImageItem("1","1","https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg"),
//                    MomentListItem.MomentImageItem("1","1","https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg")
//                ),
//                MomentListItem.MomentShareItem(
//                    "12231", MOMENT_SHARE_TYPE_ALBUM,"言语行政","Xiaoc",
//                    "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg"
//                ),1,23),
//
//            MomentListItem("12121","122",
//                "小XIO",
//                "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg",
//                "15分钟前","朋友圈测试！朋友圈测试朋友圈测试朋友圈测试", listOf(
//                    MomentListItem.MomentImageItem("1","1","https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg")
//                ),
//                MomentListItem.MomentShareItem(
//                    "12231",MOMENT_SHARE_TYPE_ALBUM,"言语行政","Xiaoc",
//                    "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg"
//                ),1,23),

//            MomentListItem("1212121","1222",
//                "Taylor Swift",
//                "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg",
//                "15分钟前","Hello，I'm Taylor. Freeless is coming soon!", mutableListOf(
//                    "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg"
//                ),
//                MomentListItem.MomentShareItem(
//                    "12231",1,"言语行政","Xiaoc",
//                    "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg"
//                ),1,23),
//            MomentListItem("1212122","1222",
//                "Taylor Swift",
//                "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg",
//                "15分钟前","Hello，I'm Taylor. Freeless is coming soon!", mutableListOf(
//                    "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg"
//                ),
//                MomentListItem.MomentShareItem(
//                    "12231",1,"言语行政","Xiaoc",
//                    "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg"
//                ),1,23),
//            MomentListItem("1212123","1222",
//                "邓紫棋",
//                "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg",
//                "15分钟前","大家好，我的摩天动物园来啦！", mutableListOf(
//                    "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg"
//                ),
//                null,4,10),
//            MomentListItem("1212124","1222",
//                "Taylor Swift",
//                "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg",
//                "15分钟前","Hello，I'm Taylor. Freeless is coming soon!", mutableListOf(
//                    "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg",
//                    "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg",
//                    "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg",
//                    "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg",
//                    "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg",
//                    "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg"
//                ),
//                MomentListItem.MomentShareItem(
//                    "12231",1,"言语行政","Xiaoc",
//                    "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg"
//                ),1,23),
//            )
//    }

//    fun generatePlaylistMusic(): List<DefaultMusicItem>{
//        return listOf(
//            DefaultMusicItem("123","这是你应该赶紧听的歌曲",
//                "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg",
//                "123","https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg","Xiaoc","介绍",true,0),
//        )
//    }

//    fun generateCommentItem(): List<MomentCommentItem>{
//        return listOf(
//            MomentCommentItem("12","12","主内容","","X","12-12","",4, 0,generateReplayItem4())
//        )
//    }
//
//    fun generateReplayItem2(): List<MomentReplayItem>{
//        return listOf(
//            MomentReplayItem("12","12","1","QQ输入法很好用","","X","12-12","","1","XXX",0,0),
//            MomentReplayItem("12","12","1","QQ输入法很好用2","","X","12-12","","1","XXX",0,0)
//        )
//    }
//
//    fun generateReplayItem3(): List<MomentReplayItem>{
//        return listOf(
//            MomentReplayItem("12","12","1","搜狗输入法很好用","","X","12-12","","1","XXX",0,0),
//            MomentReplayItem("12","12","1","搜狗输入法很好用2","","X","12-12","","1","XXX",0,0),
//            MomentReplayItem("12","12","1","搜狗输入法很好用3","","X","12-12","","1","XXX",0,0)
//        )
//    }
//
//    fun generateReplayItem4(): List<MomentReplayItem>{
//        return listOf(
//            MomentReplayItem("12","12","1","屁屁输入法很好用","","X","12-12","","1","XXX",0,0),
//            MomentReplayItem("12","12","1","屁屁输入法很好用2","","X","12-12","","1","XXX",0,0),
//            MomentReplayItem("12","12","1","屁屁输入法很好用3","","X","12-12","","1","XXX",0,0),
//            MomentReplayItem("12","12","1","屁屁输入法很好用4","","X","12-12","","1","XXX",0,0)
//        )
//    }
//
//    fun generateReplayItem(): List<MomentReplayItem>{
//        return listOf(
//            MomentReplayItem("12","12","1","虾皮输入法很好用","","X","12-12","","1","XXX",0,0)
//        )
//    }
//
//    fun generateReplayItemALL(): List<MomentReplayItem>{
//        return listOf(
//            MomentReplayItem("12","12","1","QQ输入法很好用","","X","12-12","","1","XXX",0,0),
//            MomentReplayItem("12","12","1","QQ输入法很好用2","","X","12-12","","1","XXX",0,0),
//                    MomentReplayItem("12","12","1","QQ输入法很好用","","X","12-12","","1","XXX",0,0),
//        MomentReplayItem("12","1","12","QQ输入法很好用2","","X","12-12","","1","XXX",0,0),
//                MomentReplayItem("12","12","1","QQ输入法很好用","","X","12-12","","1","XXX",0,0),
//        MomentReplayItem("12","1","12","QQ输入法很好用2","","X","12-12","","1","XXX",0,0),
//                MomentReplayItem("12","12","1","QQ输入法很好用","","X","12-12","","1","XXX",0,0),
//        MomentReplayItem("12","12","1","QQ输入法很好用2","","X","12-12","","1","XXX",0,0),
//            MomentReplayItem("12","12","1","QQ输入法很好用2","","X","12-12","","1","XXX",0,0),
//            MomentReplayItem("12","12","1","QQ输入法很好用2","","X","12-12","","1","XXX",0,0),
//            MomentReplayItem("12","12","1","QQ输入法很好用2","","X","12-12","","1","XXX",0,0)
//        )
//    }

    suspend fun generateAboutList(context: Context): List<Any>{
        return withContext(Dispatchers.Default){
            listOf(
                AboutSubTitleItem(context.getString(R.string.description_developer_title)),
                AboutDeveloperItem("https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg","Xiaoc"),
                AboutDeveloperItem("https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/bd/dd/54/bddd543b-4e0f-007b-1398-5fef90b963c3/20UM1IM14847.rgb.jpg/300x300bb-60.jpg","Rainbow"),
                AboutSubTitleItem(context.getString(R.string.description_open_source_title)),
                AboutOpenSourceItem(context.getString(R.string.description_open_source_kotlin_title),
                    context.getString(R.string.description_open_source_kotlin_url),
                    context.getString(R.string.description_open_source_kotlin_description)),
                AboutOpenSourceItem(context.getString(R.string.description_open_source_androidx_title),
                    context.getString(R.string.description_open_source_androidx_url),
                    context.getString(R.string.description_open_source_androidx_description)),
                AboutOpenSourceItem(context.getString(R.string.description_open_source_exoplayer_title),
                    context.getString(R.string.description_open_source_exoplayer_url),
                    context.getString(R.string.description_open_source_exoplayer_description)),
                AboutOpenSourceItem(context.getString(R.string.description_open_source_qiniu_title),
                    context.getString(R.string.description_open_source_qiniu_url),
                    context.getString(R.string.description_open_source_qiniu_description)),
                AboutOpenSourceItem(context.getString(R.string.description_open_source_glide_title),
                    context.getString(R.string.description_open_source_glide_url),
                    context.getString(R.string.description_open_source_glide_description)),
                AboutOpenSourceItem(context.getString(R.string.description_open_source_taglib_title),
                    context.getString(R.string.description_open_source_taglib_url),
                    context.getString(R.string.description_open_source_taglib_description))
            )
        }

    }
}