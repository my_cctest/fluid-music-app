package com.ccteam.fluidmusic.utils

import androidx.documentfile.provider.DocumentFile

/**
 * @author Xiaoc
 * @since 2021/3/19
 */

/**
 * 解析文件的格式信息
 * 例如 XXX.jpg，解析后返回 jpg 格式
 * 如果文件名为空，则返回空字符串
 */
fun DocumentFile.parseMimeType(): String{
    name?.let { fileName ->
        return fileName.substring(fileName.lastIndexOf(".") + 1)
    }
    return ""
}