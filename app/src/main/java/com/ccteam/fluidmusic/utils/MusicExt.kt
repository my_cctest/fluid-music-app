package com.ccteam.fluidmusic.utils

import androidx.core.view.isVisible
import com.google.android.material.progressindicator.BaseProgressIndicator

/**
 * @author Xiaoc
 * @since 2021/3/20
 */
/**
 * 显示或隐藏进度条
 */
fun BaseProgressIndicator<*>.showHide(visibility: Boolean){
    if(visibility){
        show()
    } else {
        hide()
    }
}