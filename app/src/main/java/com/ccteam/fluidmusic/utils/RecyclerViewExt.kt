package com.ccteam.fluidmusic.utils

import androidx.recyclerview.widget.RecyclerView

/**
 * @author Xiaoc
 * @since 2021/1/27
 *
 * RecyclerView内联工具集
 **/

/**
 * 遍历所有可见的ViewHolder
 * @param action 对ViewHolder要做的操作
 */
inline fun <reified T: RecyclerView.ViewHolder> RecyclerView.forEachVisibleHolder(
    action: (T?) -> Unit
){
    for(i in 0 until childCount){
        action(getChildViewHolder(getChildAt(i)) as? T)
    }
}