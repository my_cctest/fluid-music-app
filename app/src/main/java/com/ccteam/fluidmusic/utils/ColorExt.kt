package com.ccteam.fluidmusic.utils

import android.content.Context
import android.graphics.Color
import android.view.View
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.core.content.res.use
import com.google.android.material.color.MaterialColors

/**
 * @author Xiaoc
 * @since 2021/3/19
 *
 * 简洁获取颜色值的操作
 */
fun View.getColor(@ColorRes id: Int): Int =resources.getColor(id,context.theme)

/**
 * 获取Attr属性的颜色值
 */
@ColorInt
fun View.getAttrColor(@AttrRes id: Int): Int = MaterialColors.getColor(this,id)

/**
 * 获取Attr属性的颜色值
 */
@ColorInt
fun Context.getAttrColor(
    @AttrRes themeAttrId: Int
): Int {
    return obtainStyledAttributes(
        intArrayOf(themeAttrId)
    ).use {
        it.getColor(0, Color.MAGENTA)
    }
}