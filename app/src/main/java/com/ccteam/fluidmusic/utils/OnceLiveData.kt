package com.ccteam.fluidmusic.utils

import androidx.lifecycle.LiveData

/**
 * @author Xiaoc
 * @since 2021/4/17
 *
 * 一次性LiveData，该LiveData仅支持赋值一次，一旦有值了则不能再被设置新的值
 */
class OnceLiveData<T>: LiveData<T> {

    constructor(): super()

    constructor(value: T):super(value)

    public override fun setValue(value: T) {
        if(getValue() == null){
            super.setValue(value)
        }
    }

    public override fun postValue(value: T) {
        if(getValue() == null){
            super.postValue(value)
        }
    }
}