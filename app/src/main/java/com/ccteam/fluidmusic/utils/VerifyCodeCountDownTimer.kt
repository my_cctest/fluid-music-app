package com.ccteam.fluidmusic.utils

import android.os.CountDownTimer
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

/**
 * @author Xiaoc
 * @since 2021/4/16
 *
 * 自定义的验证码倒计时计时器
 * 基于 [CountDownTimer] 倒计时工具
 *
 * 内部持有一个基于Flow的倒计时状态，用于外部进行进行访问
 *
 * 为避免空指针问题，请在需要结束时调用 cancel() 方法避免空指针
 */
class VerifyCodeCountDownTimer: CountDownTimer(60L * 1000,1000){

    private val _countDown = MutableStateFlow<Status>(Status.Ready)
    val countDown get() = _countDown.asStateFlow()

    override fun onTick(millisUntilFinished: Long) {
        _countDown.value = Status.Timing((millisUntilFinished / 1000).toInt())
    }

    override fun onFinish() {
        _countDown.value = Status.Finish
    }

    /**
     * 倒计时状态密封类
     */
    sealed class Status{

        /**
         * 倒计时处于准备状态
         */
        object Ready : Status()

        /**
         * 倒计时正在执行
         */
        data class Timing(val currentCountDownNum: Int) : Status()

        /**
         * 倒计时已完成
         */
        object Finish : Status()
    }


}