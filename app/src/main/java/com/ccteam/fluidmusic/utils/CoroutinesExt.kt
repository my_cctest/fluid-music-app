package com.ccteam.fluidmusic.utils

import kotlinx.coroutines.Job

/**
 * @author Xiaoc
 * @since 2021/2/7
 **/

/**
 * 如果活跃则取消当前协程工作
 */
fun Job?.cancelIfActive(){
    if(this?.isActive == true){
        cancel()
    }
}