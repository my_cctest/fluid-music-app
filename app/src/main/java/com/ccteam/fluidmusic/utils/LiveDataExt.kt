package com.ccteam.fluidmusic.utils

import androidx.lifecycle.MutableLiveData

/**
 * @author Xiaoc
 * @since 2021/2/19
 *
 * LiveData的扩展方法
 */

/**
 * 给LiveData设置新值，但是如果和原来值相同，则不会进行设置
 */
fun <T> MutableLiveData<T>.setValueIfNew(newValue: T?){
    if(this.value != newValue && newValue != null) value = newValue
}