package com.ccteam.fluidmusic.ui.playlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import com.ccteam.fluidmusic.R
import com.ccteam.fluidmusic.databinding.DialogAddPlaylistSheetBottomSheetBinding
import com.ccteam.fluidmusic.utils.EventObserver
import com.ccteam.fluidmusic.view.bean.LoadMessage
import com.ccteam.fluidmusic.widget.SNACKBAR_ALL_BOTTOM_BAR
import com.ccteam.fluidmusic.widget.SNACKBAR_BOTTOM
import com.ccteam.fluidmusic.widget.snackbar
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint

/**
 * @author Xiaoc
 * @since 2021/3/21
 *
 * 添加歌单 底部弹出DialogFragment
 */
@AndroidEntryPoint
class AddPlaylistBottomSheetFragment: BottomSheetDialogFragment()  {

    private var _binding: DialogAddPlaylistSheetBottomSheetBinding? = null
    private val binding get() = _binding!!

    private val addPlaylistViewModel by viewModels<AddPlaylistViewModel>()

    companion object{
        const val ADD_PLAYLIST_RESULT = "add_playlist_result"
    }

    private var isPlaylistNameSuccess = false
    private var isPlaylistTypeSuccess = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // 设置弹出主题，防止软键盘弹出导致底部显示不全
        setStyle(DialogFragment.STYLE_NORMAL, R.style.Widget_BottomSheetDialog_BottomSheetEdit)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DialogAddPlaylistSheetBottomSheetBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val adapter = ArrayAdapter(requireContext(), R.layout.textfiled_select_list_item, addPlaylistViewModel.playlistSheetTypeItems)
        (binding.editPlaylistSheetType.editText as? AutoCompleteTextView)?.setAdapter(adapter)

        addPlaylistViewModel.loadMessage.observe(viewLifecycleOwner, EventObserver {
            updateStatus(it is LoadMessage.Loading)
            if(it is LoadMessage.Success){
                view.snackbar(R.string.description_playlist_sheet_add_success, SNACKBAR_ALL_BOTTOM_BAR).show()
                setFragmentResult(ADD_PLAYLIST_RESULT, bundleOf(ADD_PLAYLIST_RESULT to true))
                dismiss()
            } else if(it is LoadMessage.Error){
                view.snackbar(it.errorMessage.description.toString(), SNACKBAR_BOTTOM).show()
            }
        })

        binding.btnAddPlaylist.setProgressButtonOnClickListener {
            addPlaylistViewModel.addPlaylist(
                binding.editPlaylistSheetName.editText?.text.toString(),
                binding.editPlaylistSheetType.editText?.text.toString()
            )
        }

        binding.editPlaylistSheetName.addOnEditTextAttachedListener { inputLayout ->
            inputLayout.editText?.addTextChangedListener {
                if(it.toString().length in 1..20){
                    isPlaylistNameSuccess = true
                    inputLayout.error = null
                } else {
                    isPlaylistNameSuccess = false
                    inputLayout.error = getString(R.string.description_playlist_sheet_add_name_length)
                }
                notifyFinishButton()
            }
        }

        binding.editPlaylistSheetType.addOnEditTextAttachedListener { inputLayout ->
            inputLayout.editText?.addTextChangedListener {
                isPlaylistTypeSuccess = it.toString().isNotEmpty()
                notifyFinishButton()
            }
        }

        binding.containerCancel.setOnClickListener {
            dismiss()
        }

    }

    private fun notifyFinishButton() = with(binding){
        btnAddPlaylist.isEnabled = isPlaylistNameSuccess
                && isPlaylistTypeSuccess
    }

    private fun updateStatus(isLoading: Boolean){
        binding.btnAddPlaylist.setLoading(isLoading)
    }

    override fun onDestroyView() {
        super.onDestroyView()

        _binding = null
    }
}