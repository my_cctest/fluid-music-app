package com.ccteam.fluidmusic.ui.moment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.doOnPreDraw
import androidx.fragment.app.*
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.ConcatAdapter
import com.ccteam.fluidmusic.NavAlbumDirections
import com.ccteam.fluidmusic.NavMomentDetailDirections
import com.ccteam.fluidmusic.NavPlaylistSheetDirections
import com.ccteam.fluidmusic.R
import com.ccteam.fluidmusic.databinding.FragmentMomentsBinding
import com.ccteam.fluidmusic.fluidmusic.common.utils.MediaIDHelper
import com.ccteam.fluidmusic.ui.MainActivityViewModel
import com.ccteam.fluidmusic.ui.moment.PublishMomentFragment.Companion.SHOULD_REFRESH_MOMENT
import com.ccteam.fluidmusic.ui.moment.imageview.ImageBrowseActivityDirections
import com.ccteam.fluidmusic.ui.moment.imageview.ImageBrowseData
import com.ccteam.fluidmusic.ui.moment.imageview.ImageViewData
import com.ccteam.fluidmusic.ui.music.MusicInfoViewModel
import com.ccteam.fluidmusic.utils.EventObserver
import com.ccteam.fluidmusic.utils.TransitionUtils
import com.ccteam.fluidmusic.view.adapters.moment.MomentsItemCallback
import com.ccteam.fluidmusic.view.adapters.moment.MomentsListPagerAdapter
import com.ccteam.fluidmusic.view.adapters.paging.CustomLoadStateAdapter
import com.ccteam.fluidmusic.view.adapters.toolbar.ToolbarTitleAdapter
import com.ccteam.fluidmusic.view.base.LightBounceEdgeEffectFactory
import com.ccteam.fluidmusic.view.bean.*
import com.ccteam.fluidmusic.view.components.CornerImageWrapperView
import com.ccteam.fluidmusic.widget.AnimToolbarScrollListener
import com.ccteam.fluidmusic.widget.SNACKBAR_ALL_BOTTOM_BAR
import com.ccteam.fluidmusic.widget.snackbar
import com.ccteam.model.moment.MOMENT_SHARE_TYPE_ALBUM
import com.ccteam.model.moment.MOMENT_SHARE_TYPE_MUSIC
import com.ccteam.model.moment.MOMENT_SHARE_TYPE_PLAYLIST
import com.ccteam.shared.result.moment.MomentListItem
import com.ccteam.shared.result.title.ToolbarTitleItem
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class MomentsFragment : Fragment() {

    private var _binding: FragmentMomentsBinding? = null
    private val binding get() = _binding!!

    @Inject lateinit var lightBounceEdgeEffectFactory: LightBounceEdgeEffectFactory

    private val momentsListViewModel by viewModels<MomentListViewModel>()
    private val momentsHomeViewModel by viewModels<MomentsHomeViewModel>()
    private val musicInfoViewModel by viewModels<MusicInfoViewModel>()
    private val mainActivityViewModel by activityViewModels<MainActivityViewModel>()

    private lateinit var dataAdapter: MomentsListPagerAdapter

    private val startForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            val intent = result.data
            if(intent != null){
                if(intent.getBooleanExtra(SHOULD_REFRESH_MOMENT,false)){
                    momentsListViewModel.refreshMomentList()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        exitTransition = TransitionUtils.exitScale
        reenterTransition = TransitionUtils.enterScale
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMomentsBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        postponeEnterTransition()
        view.doOnPreDraw {
            startPostponedEnterTransition()
        }

        val titleAdapter = ToolbarTitleAdapter()
        titleAdapter.submitList(listOf(ToolbarTitleItem(getString(R.string.moments_name))))
        dataAdapter = MomentsListPagerAdapter(imageCallback,momentsCallback)

        lightBounceEdgeEffectFactory.applyBounceView(binding.rvMomentsHome,this)
        binding.rvMomentsHome.edgeEffectFactory = lightBounceEdgeEffectFactory

        binding.rvMomentsHome.addOnScrollListener(AnimToolbarScrollListener(binding.toolbarTitle))
        binding.rvMomentsHome.adapter = ConcatAdapter(titleAdapter,dataAdapter.withLoadStateFooter(
            CustomLoadStateAdapter(dataAdapter,true){
                dataAdapter.retry()
            }
        ))

        binding.refreshView.setOnRefreshListener {
            dataAdapter.refresh()
        }

        viewLifecycleOwner.lifecycleScope.launch {
            momentsListViewModel.momentsList.collectLatest {
                dataAdapter.submitData(it)
            }
        }

        momentsListViewModel.shouldRefreshMomentList.observe(viewLifecycleOwner, EventObserver{
            dataAdapter.refresh()
        })

        /**
         * 监听是否处于登录状态，如果不处于登录状态不显示发布广场按钮
         */
        momentsHomeViewModel.isLogin.observe(viewLifecycleOwner, { isLogin ->
            binding.toolbarMomentsHome.menu.findItem(R.id.toolbar_moments_add).isVisible = isLogin
        })

        musicInfoViewModel.loadMessage.observe(viewLifecycleOwner,EventObserver {
            when(it){
                is LoadMessage.Loading ->{
                    view.snackbar(R.string.description_moment_music_loading, SNACKBAR_ALL_BOTTOM_BAR, Snackbar.LENGTH_LONG)
                        .show()
                }
                is LoadMessage.Error ->{
                    view.snackbar(R.string.description_moment_music_error, SNACKBAR_ALL_BOTTOM_BAR).show()
                }
                is LoadMessage.Success ->{
                    mainActivityViewModel.addQueueItem(musicInfoViewModel.currentMediaId,
                        MediaIDHelper.ONLINE_MEDIA_CACHE,MediaIDHelper.PLAYLIST_ADD_TYPE_NOW_PLAY)
                }
            }
        })

        viewLifecycleOwner.lifecycleScope.launch {
            dataAdapter.loadStateFlow.collectLatest { state ->
                binding.refreshView.isRefreshing = state.refresh is LoadState.Loading
                binding.statusView.changeToLoadingStatus(state.refresh is LoadState.Loading)
                binding.statusView.changeToErrorStatus(state.refresh,
                    state.append.endOfPaginationReached && dataAdapter.itemCount < 1)
            }
        }

        binding.toolbarMomentsHome.setOnMenuItemClickListener { menuItem ->
            return@setOnMenuItemClickListener when(menuItem.itemId){
                R.id.toolbar_moments_add ->{
                    startForResult.launch(Intent(requireContext(), AddMomentActivity::class.java))
                    true
                }
                else ->
                    false
            }
        }

        binding.statusView.setRetryClickListener {
            dataAdapter.refresh()
        }
    }

    override fun onResume() {
        super.onResume()

        setFragmentResultListener(REFRESH_MOMENT_KEY){ _, bundle ->
            if(bundle.getBoolean(REFRESH_MOMENT_KEY)){
                dataAdapter.refresh()
            }
        }
    }

    private val momentsCallback = object: MomentsItemCallback{
        override fun itemClicked(view: View,item: MomentListItem) {
            val transitionName = getString(R.string.moment_detail_transition_name)
            val extra = FragmentNavigatorExtras(view to transitionName)

            findNavController().navigate(NavMomentDetailDirections
                .actionGlobalMomentDetailFragment(item.id),extra)
        }

        override fun shareContentClicked(view: View,item: MomentListItem.MomentShareItem) {
            when(item.type){
                MOMENT_SHARE_TYPE_ALBUM ->{
                    val transitionName = getString(R.string.album_detail_transition_name)
                    val extra = FragmentNavigatorExtras(view to transitionName)

                    findNavController().navigate(
                        NavAlbumDirections.actionGlobalAlbumDetailFragment(item.id),extra
                    )
                }
                MOMENT_SHARE_TYPE_PLAYLIST ->{
                    val transitionName = getString(R.string.playlist_detail_transition_name)
                    val extra = FragmentNavigatorExtras(view to transitionName)

                    findNavController().navigate(
                        NavPlaylistSheetDirections.actionGlobalPlaylistSheetDetailFragment(item.id),extra
                    )
                }
                MOMENT_SHARE_TYPE_MUSIC ->{
                    musicInfoViewModel.prepareMusic(item.id)
                }
            }
        }

    }

    private val imageCallback = object: CornerImageWrapperView.ImageEventListener<MomentListItem.MomentImageItem>{

        override fun imageClick(
            view: View,
            position: Int,
            imageList: List<MomentListItem.MomentImageItem>
        ) {
            findNavController().navigate(
                ImageBrowseActivityDirections.actionGlobalImageBrowseActivity(
                ImageBrowseData(position,imageList.map {
                    ImageViewData(it.id,it.imageUrl)
                })
            ))
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()

        binding.rvMomentsHome.clearOnScrollListeners()
        _binding = null
    }
}