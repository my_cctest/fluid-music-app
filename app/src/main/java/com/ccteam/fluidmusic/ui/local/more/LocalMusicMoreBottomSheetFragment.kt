package com.ccteam.fluidmusic.ui.local.more

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.ccteam.fluidmusic.BottomNavLocalMusicDirections
import com.ccteam.fluidmusic.R
import com.ccteam.fluidmusic.databinding.DialogLocalMusicMoreBinding
import com.ccteam.fluidmusic.fluidmusic.common.utils.MediaIDHelper
import com.ccteam.fluidmusic.view.bean.MoreInfoData
import com.ccteam.fluidmusic.widget.SNACKBAR_ALL_BOTTOM_BAR
import com.ccteam.fluidmusic.widget.snackbar
import com.ccteam.fluidmusic.ui.MainActivityViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint

/**
 * @author Xiaoc
 * @since 2021/3/4
 */
@AndroidEntryPoint
class LocalMusicMoreBottomSheetFragment: BottomSheetDialogFragment() {

    private var _binding: DialogLocalMusicMoreBinding? = null
    private val binding get() = _binding!!

    private val moreViewModel by viewModels<MusicMoreDialogViewModel>()
    private val mainActivityViewModel by activityViewModels<MainActivityViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DialogLocalMusicMoreBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        moreViewModel.mediaInfo.observe(viewLifecycleOwner, {
            if(it != null){
                updateMediaInfo(it)
            }
        })

        binding.containerAddToPlaylist.setOnClickListener {
            moreViewModel.mediaInfo.value?.let {
                mainActivityViewModel.addQueueItem(it.mediaId,it.parentId,MediaIDHelper.PLAYLIST_ADD_TYPE_LAST_PLAY)
                requireActivity().findViewById<TextView>(R.id.FluidMusicBottomBar).snackbar(
                    getString(R.string.description_add_to_playlist_info,it.mediaTitle), SNACKBAR_ALL_BOTTOM_BAR
                ).show()
            }

            dismiss()
        }

        binding.containerAddNextToPlaylist.setOnClickListener {
            moreViewModel.mediaInfo.value?.let {
                mainActivityViewModel.addQueueItem(it.mediaId,it.parentId,MediaIDHelper.PLAYLIST_ADD_TYPE_NEXT_PLAY)
                requireActivity().findViewById<TextView>(R.id.FluidMusicBottomBar).snackbar(
                    getString(R.string.description_add_next_to_playlist_info,it.mediaTitle), SNACKBAR_ALL_BOTTOM_BAR
                ).show()
            }

            dismiss()
        }

        binding.containerTag.setOnClickListener {
            moreViewModel.mediaInfo.value?.let {
                findNavController().navigate(BottomNavLocalMusicDirections.actionGlobalLocalMusicTagFragment(
                    it.mediaUri
                ))
            }
        }

        binding.containerCancel.setOnClickListener {
            dismiss()
        }
    }

    private fun updateMediaInfo(mediaInfo: MoreInfoData) = with(binding){
        tvMusicName.text = mediaInfo.mediaTitle
        tvMusicSubtitle.text = mediaInfo.mediaSubtitle
        ivMusicImg.isVisible = !mediaInfo.mediaArtUri.isNullOrEmpty()
        Glide.with(ivMusicImg)
            .load(mediaInfo.mediaArtUri)
            .placeholder(R.drawable.img_default_album)
            .into(ivMusicImg)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}