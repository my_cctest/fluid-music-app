package com.ccteam.fluidmusic.ui.moment.imageview

import android.os.Parcelable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.ccteam.fluidmusic.utils.setValueIfNew
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.parcelize.Parcelize
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/4/7
 */
@HiltViewModel
class ImageBrowseViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle
): ViewModel() {

    private val _images = MutableLiveData<ImageBrowseData>()
    val images: LiveData<ImageBrowseData> get() = _images

    init {
        _images.setValueIfNew(savedStateHandle["imageBrowse"])
    }
}

/**
 * 图片浏览包装Data
 * 包含当前应浏览的index和图片列表数据
 */
@Parcelize
data class ImageBrowseData(
    val currentIndex: Int,
    val images: List<ImageViewData>
): Parcelable

/**
 * 图片信息
 * @param id 图片ID
 * @param imageUrl 图片地址
 */
@Parcelize
data class ImageViewData(
    val id: String,
    val imageUrl: String
): Parcelable