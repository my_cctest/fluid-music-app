package com.ccteam.fluidmusic.ui.moment.imageview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.ccteam.fluidmusic.databinding.ActivityImageBrowseBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ImageBrowseActivity : AppCompatActivity() {

    private lateinit var binding: ActivityImageBrowseBinding

    private val imageBrowseViewModel by viewModels<ImageBrowseViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityImageBrowseBinding.inflate(layoutInflater)
        setContentView(binding.root)

        imageBrowseViewModel.images.observe(this, {
            binding.viewpagerImage.adapter = ImageViewAdapter(it)
            binding.viewpagerImage.setCurrentItem(it.currentIndex,false)
        })
    }

    inner class ImageViewAdapter(private val browseData: ImageBrowseData): FragmentStateAdapter(this){

        override fun getItemCount(): Int = browseData.images.size

        override fun createFragment(position: Int): Fragment {
            return ImageViewFragment.newInstance(browseData.images[position])
        }

    }
}