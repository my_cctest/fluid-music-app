package com.ccteam.fluidmusic.ui.moment.comment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.ccteam.fluidmusic.R
import com.ccteam.fluidmusic.databinding.DialogPublishCommentBinding
import com.ccteam.fluidmusic.utils.EventObserver
import com.ccteam.fluidmusic.view.bean.LoadMessage
import com.ccteam.fluidmusic.view.bean.PublishCommentData
import com.ccteam.fluidmusic.widget.SNACKBAR_BOTTOM
import com.ccteam.fluidmusic.widget.snackbar
import com.ccteam.model.moment.comment.COMMENT_TYPE_COMMENT
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint

/**
 * @author Xiaoc
 * @since 2021/3/25
 */
@AndroidEntryPoint
class PublishCommentFragment: BottomSheetDialogFragment(){

    private var _binding: DialogPublishCommentBinding? = null
    private val binding get() = _binding!!

    private val publishCommentViewModel by viewModels<PublishCommentViewModel>()

    private var isContentSuccess = false

    companion object{
        const val REFRESH_COMMENT_RESULT = "refresh_comment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // 设置弹出主题，防止软键盘弹出导致底部显示不全
        setStyle(DialogFragment.STYLE_NORMAL, R.style.Widget_BottomSheetDialog_BottomSheetEdit)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DialogPublishCommentBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        binding.editCommentContent.addOnEditTextAttachedListener { inputLayout ->
            inputLayout.editText?.addTextChangedListener {
                isContentSuccess = it.toString().length in 1..500
                notifyPublishButton()
            }
        }

        publishCommentViewModel.loadMessage.observe(viewLifecycleOwner, EventObserver{
            updateStateUI(it is LoadMessage.Loading)
            if(it is LoadMessage.Error){
                view.snackbar(it.errorMessage.description.toString(), SNACKBAR_BOTTOM).show()
            } else if(it is LoadMessage.Success){
                view.snackbar(R.string.description_success_publish, SNACKBAR_BOTTOM).show()
                setFragmentResult(REFRESH_COMMENT_RESULT, bundleOf(REFRESH_COMMENT_RESULT to true))
                findNavController().navigateUp()
            }
        })

        publishCommentViewModel.publishCommentInfo.observe(viewLifecycleOwner, {
            if(it != null){
                updatePublishCommentUI(it)
            }
        })

        binding.containerCancel.setOnClickListener {
            dismiss()
        }

        binding.btnPublishComment.setProgressButtonOnClickListener {
            publishCommentViewModel.publishComment(binding.editCommentContent.editText?.text.toString())
        }
    }

    private fun updateStateUI(isLoading: Boolean) = with(binding){
        btnPublishComment.setLoading(isLoading)
    }

    private fun updatePublishCommentUI(commentData: PublishCommentData) = with(binding){
        if(commentData.type == COMMENT_TYPE_COMMENT){
            addCommentType.text = getString(R.string.description_publish_comment_info)
            editCommentContent.hint = getString(R.string.hint_comment_info,commentData.toUserName)
        } else {
            addCommentType.text = getString(R.string.description_publish_replay_info)
            editCommentContent.hint = getString(R.string.hint_publish_replay_info,commentData.toUserName)
        }
    }

    private fun notifyPublishButton(){
        binding.btnPublishComment.isEnabled = isContentSuccess
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.editCommentContent.clearOnEditTextAttachedListeners()

        _binding = null
    }
}