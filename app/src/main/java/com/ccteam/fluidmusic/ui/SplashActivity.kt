package com.ccteam.fluidmusic.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.navigation.findNavController
import com.ccteam.fluidmusic.NavSplashDirections
import com.ccteam.fluidmusic.R
import com.ccteam.fluidmusic.utils.EventObserver
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashActivity : AppCompatActivity() {

    private val splashViewModel by viewModels<SplashViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        splashViewModel.isConnected.observe(this, {
            if(it){
                findNavController(R.id.splash_host_container).navigate(NavSplashDirections.actionGlobalMainActivity())
                // 结束当前Activity
                finish()
            }
        })

        /**
         * 检测是否Token失效，如果为 true 则Token没有失效，false则为失效Token
         */
        splashViewModel.isLogin.observe(this, { _ ->
            // 如果登录工作判断完毕，则可以直接跳转下一个Activity
            splashViewModel.forwardToNextActivity()
        })

        splashViewModel.shouldForwardToNextActivity.observe(this, {
            if(it){
                findNavController(R.id.splash_host_container).navigate(NavSplashDirections.actionGlobalMainActivity())
                // 结束当前Activity
                finish()
            }
        })

        splashViewModel.errorMessage.observe(this, EventObserver{
            Toast.makeText(this.applicationContext,it,Toast.LENGTH_SHORT).show()
            // 如果出现错误，则可以直接跳转下一个Activity
            splashViewModel.forwardToNextActivity()
        })
    }


}