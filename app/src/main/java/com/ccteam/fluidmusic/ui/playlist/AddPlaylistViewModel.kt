package com.ccteam.fluidmusic.ui.playlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ccteam.fluidmusic.utils.Event
import com.ccteam.fluidmusic.view.bean.ErrorMessage
import com.ccteam.fluidmusic.view.bean.LoadMessage
import com.ccteam.model.Resource
import com.ccteam.shared.domain.playlistsheet.AddPlaylistSheetDataUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/21
 *
 * 添加歌单ViewModel
 */
@HiltViewModel
class AddPlaylistViewModel @Inject constructor(
    private val addPlaylistSheetDataUseCase: AddPlaylistSheetDataUseCase
): ViewModel() {

    val playlistSheetTypeItems = arrayOf("共享","公开","私人")

    private val _loadMessage = MutableLiveData<Event<LoadMessage>>()
    val loadMessage: LiveData<Event<LoadMessage>> get() = _loadMessage

    fun addPlaylist(playlistName: String,playlistType: String){
        _loadMessage.value = Event(LoadMessage.Loading)
        viewModelScope.launch {
            val result = addPlaylistSheetDataUseCase(Pair(playlistName,playlistType))
            if(result is Resource.Success){
                _loadMessage.value = Event(LoadMessage.Success)
            } else {
                _loadMessage.value = Event(LoadMessage.Error(ErrorMessage(result.message,result.errorCode)))
            }
        }
    }
}