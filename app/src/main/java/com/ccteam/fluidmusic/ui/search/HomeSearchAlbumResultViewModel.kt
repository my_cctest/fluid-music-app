package com.ccteam.fluidmusic.ui.search

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.ccteam.fluidmusic.ui.playlist.select.AddOnlineMediaFragment.Companion.TYPE_SEARCH
import com.ccteam.shared.domain.search.HomeSearchAlbumDataUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/16
 *
 * 首页搜索专辑结果ViewModel
 */
@HiltViewModel
class HomeSearchAlbumResultViewModel @Inject constructor(
    homeSearchAlbumDataUseCase: HomeSearchAlbumDataUseCase,
    savedStateHandle: SavedStateHandle
): ViewModel() {

    val type: Int = savedStateHandle["type"] ?: TYPE_SEARCH

    val albumList = homeSearchAlbumDataUseCase(savedStateHandle[SEARCH_KEY])
        .cachedIn(viewModelScope)

}