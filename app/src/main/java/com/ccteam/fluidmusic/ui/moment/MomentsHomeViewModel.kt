package com.ccteam.fluidmusic.ui.moment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ccteam.fluidmusic.utils.cancelIfActive
import com.ccteam.shared.core.UserCore
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/30
 */
@HiltViewModel
class MomentsHomeViewModel @Inject constructor(
    private val userCore: UserCore
): ViewModel() {

    private val _isLogin = MutableLiveData(false)
    val isLogin: LiveData<Boolean> get() = _isLogin

    private var loginJob: Job? = null
    init {
        loginJob = viewModelScope.launch {
            userCore.isLogin.collectLatest {
                _isLogin.value = it
            }
        }
    }

    override fun onCleared() {
        super.onCleared()

        loginJob.cancelIfActive()
    }
}