package com.ccteam.fluidmusic.ui.search

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.ccteam.shared.domain.search.HomeSearchArtistDataUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/16
 *
 * 首页搜索歌手结果ViewModel
 */
@HiltViewModel
class HomeSearchArtistResultViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    homeSearchArtistDataUseCase: HomeSearchArtistDataUseCase
): ViewModel() {

    val artistList = homeSearchArtistDataUseCase(savedStateHandle[SEARCH_KEY])
        .cachedIn(viewModelScope)
}