package com.ccteam.fluidmusic.ui.music

import androidx.lifecycle.*
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.map
import com.ccteam.fluidmusic.fluidmusic.common.MusicServiceConnectionFlow
import com.ccteam.fluidmusic.fluidmusic.common.utils.MediaIDHelper
import com.ccteam.fluidmusic.utils.cancelIfActive
import com.ccteam.shared.domain.music.NewMusicDataUseCase
import com.ccteam.shared.result.music.DefaultMusicItem
import com.ccteam.shared.utils.getPlaybackStatus
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/28
 *
 * 新音乐ViewModel
 */
@HiltViewModel
class NewMusicViewModel @Inject constructor(
    private val musicServiceConnectionFlow: MusicServiceConnectionFlow,
    newMusicDataUseCase: NewMusicDataUseCase
): ViewModel() {

    val parentId: String = MediaIDHelper.ONLINE_MEDIA_NEW_MUSIC

    private val _newMusicListLiveData = newMusicDataUseCase(
        parentId
    ).cachedIn(viewModelScope).asLiveData().let {
        it as MutableLiveData<PagingData<DefaultMusicItem>>
    }

    val newMusicListLiveData: LiveData<PagingData<DefaultMusicItem>> get() = _newMusicListLiveData

    private var metadataChangedJob: Job? = null

    init {

        metadataChangedJob = viewModelScope.launch {
            musicServiceConnectionFlow.nowPlaying.collectLatest {
                refreshNowPlaying()
            }
        }
    }

    fun refreshNowPlaying(){
        val newMusicList = _newMusicListLiveData.value ?: return

        newMusicList.map { item ->
            item.copy(playbackRes = musicServiceConnectionFlow.nowPlaying.value.getPlaybackStatus(item.mediaId))
        }.let {
            _newMusicListLiveData.value = (it)
        }
    }

    override fun onCleared() {
        super.onCleared()

        metadataChangedJob.cancelIfActive()
    }


}