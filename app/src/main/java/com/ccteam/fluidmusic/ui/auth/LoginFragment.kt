package com.ccteam.fluidmusic.ui.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.ccteam.fluidmusic.R
import com.ccteam.fluidmusic.databinding.FragmentLoginBinding
import com.ccteam.fluidmusic.utils.EventObserver
import com.ccteam.fluidmusic.utils.TransitionUtils
import com.ccteam.fluidmusic.utils.hideKeyboard
import com.ccteam.fluidmusic.view.bean.LoadMessage
import com.ccteam.fluidmusic.widget.SNACKBAR_ALL_BOTTOM_BAR
import com.ccteam.fluidmusic.widget.SNACKBAR_BOTTOM
import com.ccteam.fluidmusic.widget.snackbar
import com.orhanobut.logger.Logger
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginFragment : Fragment() {

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private val loginViewModel by viewModels<LoginViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        exitTransition = TransitionUtils.exitScale
        reenterTransition = TransitionUtils.enterScale
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        binding.toolbarLogin.setNavigationOnClickListener {
            findNavController().navigateUp()
        }

        binding.btnRegister.setOnClickListener {
            findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToRegisterFragment())
        }

        binding.btnLoginForgetPassword.setOnClickListener {
            findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToForgetPasswordFragment())
        }

        loginViewModel.isLogin.observe(viewLifecycleOwner, {
            if(it){
                findNavController().navigateUp()
            }
        })

        loginViewModel.loadMessage.observe(viewLifecycleOwner, EventObserver{
            updateStatus(it is LoadMessage.Loading)
            if(it is LoadMessage.Error){
                view.snackbar(it.errorMessage.description.toString(), SNACKBAR_BOTTOM).show()
            } else if(it is LoadMessage.NotLoading){
                view.snackbar(R.string.description_success_login, SNACKBAR_ALL_BOTTOM_BAR).show()
//                findNavController().navigateUp()
            }

        })

        binding.btnLoginVerify.setOnClickListener {
            findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToLoginVerificationFragment())
        }

        binding.btnLogin.setProgressButtonOnClickListener {
            hideKeyboard()
            loginViewModel.login(
                binding.editAccount.editText?.text.toString(),
                binding.editPassword.editText?.text.toString()
            )
        }
    }

    private fun updateStatus(isLoading: Boolean) = with(binding){
        editAccount.isEnabled = !isLoading
        editPassword.isEnabled = !isLoading
        btnLogin.setLoading(isLoading)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}