package com.ccteam.fluidmusic.ui.moment.imageview

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.ccteam.fluidmusic.utils.setValueIfNew
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/4/7
 *
 * 图片预览ViewModel
 */
@HiltViewModel
class ImageViewViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle
): ViewModel() {

    private val _image = MutableLiveData<ImageViewData>()
    val image: LiveData<ImageViewData> get() = _image

    init {
        _image.setValueIfNew(savedStateHandle["image"])
    }
}