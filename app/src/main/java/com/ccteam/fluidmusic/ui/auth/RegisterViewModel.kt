package com.ccteam.fluidmusic.ui.auth

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ccteam.fluidmusic.utils.Event
import com.ccteam.fluidmusic.view.bean.ErrorMessage
import com.ccteam.fluidmusic.view.bean.LoadMessage
import com.ccteam.model.Resource
import com.ccteam.model.user.UserRegisterDTO
import com.ccteam.shared.domain.auth.RSADataUseCase
import com.ccteam.shared.domain.auth.RegisterDataUseCase
import com.ccteam.shared.domain.auth.VerificationDataUseCase
import com.orhanobut.logger.Logger
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/17
 */
@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val registerDataUseCase: RegisterDataUseCase,
    private val rsaDataUseCase: RSADataUseCase,
    private val verificationDataUseCase: VerificationDataUseCase
): ViewModel() {

    private val _loadMessage = MutableLiveData<Event<LoadMessage>>()
    val loadMessage: LiveData<Event<LoadMessage>> get() = _loadMessage

    /**
     * 是否开启获取验证码按钮
     * 与 Timer 倒计时共同作用验证码开关
     */
    private val _enableVerifyCodeButton = MutableLiveData(false)
    val enableVerifyCodeButton: LiveData<Boolean> get() = _enableVerifyCodeButton

    /**
     * 是否开启倒计时计数
     */
    private val _enableCountDown = MutableLiveData(Event(false))
    val enableCountDown: LiveData<Event<Boolean>> get() = _enableCountDown

    fun setEnableVerify(enable: Boolean){
        _enableVerifyCodeButton.value = enable
    }

    fun getVerification(phone: String){
        if(_enableVerifyCodeButton.value == false){
            _loadMessage.value = Event(LoadMessage.Error(
                ErrorMessage(description = "无法进行此操作")
            ))
            return
        }

        viewModelScope.launch {
            val result = verificationDataUseCase(Pair(phone,VerificationDataUseCase.REGISTER_CODE))

            if(result is Resource.Success){
                _enableCountDown.value = Event(true)
            } else {
                _loadMessage.value = Event(LoadMessage.Error(
                    ErrorMessage(result.message,result.errorCode)))

                _enableVerifyCodeButton.value = true
            }
        }

    }

    fun register(
        phone: String,
        name: String,
        password: String,
        code: String
    ){
        _loadMessage.value = Event(LoadMessage.Loading)
        viewModelScope.launch {
            val rsaResult = rsaDataUseCase(password)
            if(rsaResult is Resource.Success){
                val result = registerDataUseCase(UserRegisterDTO(code,name,rsaResult.data!!,phone))

                if(result is Resource.Success){
                    Logger.d("注册成功:$result")
                    _loadMessage.value = Event(LoadMessage.NotLoading)
                } else {
                    _loadMessage.value = Event(LoadMessage.Error(
                        ErrorMessage(result.message,result.errorCode)
                    ))
                }
            } else {
                _loadMessage.value = Event(LoadMessage.Error(
                    ErrorMessage(rsaResult.message,rsaResult.errorCode)
                ))
                Logger.d("密钥获取失败:$rsaResult")
            }
        }
    }
}