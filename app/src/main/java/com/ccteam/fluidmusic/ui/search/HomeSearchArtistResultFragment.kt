package com.ccteam.fluidmusic.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import com.ccteam.fluidmusic.NavArtistDirections
import com.ccteam.fluidmusic.databinding.FragmentSearchResultArtistBinding
import com.ccteam.fluidmusic.view.adapters.ArtistListItemClickCallback
import com.ccteam.fluidmusic.view.adapters.ArtistListPagingAdapter
import com.ccteam.fluidmusic.view.adapters.paging.CustomLoadStateAdapter
import com.ccteam.shared.result.artist.ArtistListItem
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

/**
 * @author Xiaoc
 * @since 2021/3/16
 */
@AndroidEntryPoint
class HomeSearchArtistResultFragment: Fragment() {

    private var _binding: FragmentSearchResultArtistBinding? = null
    private val binding: FragmentSearchResultArtistBinding get() = _binding!!

    private val homeSearchArtistResultViewModel by viewModels<HomeSearchArtistResultViewModel>()

    companion object {
        @JvmStatic
        fun newInstance(searchKey: String): HomeSearchArtistResultFragment {
            return HomeSearchArtistResultFragment().apply {
                arguments = Bundle().apply {
                    putString(SEARCH_KEY, searchKey)
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSearchResultArtistBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val dataAdapter = ArtistListPagingAdapter(callback)
        binding.rvArtistList.adapter = dataAdapter.withLoadStateFooter(
            CustomLoadStateAdapter(dataAdapter,true){
                dataAdapter.retry()
            }
        )

        viewLifecycleOwner.lifecycleScope.launch {
            homeSearchArtistResultViewModel.artistList.collectLatest {
                dataAdapter.submitData(it)
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            dataAdapter.loadStateFlow.collectLatest { state ->
                binding.statusView.changeToLoadingStatus(state.refresh is LoadState.Loading)
                binding.statusView.changeToErrorStatus(state.refresh,
                    state.append.endOfPaginationReached && dataAdapter.itemCount < 1)
            }
        }

        binding.statusView.setRetryClickListener {
            dataAdapter.refresh()
        }
    }

    private val callback = object: ArtistListItemClickCallback{
        override fun artistItemClicked(view: View,item: ArtistListItem) {
            findNavController().navigate(NavArtistDirections.actionGlobalArtistDetailFragment(
                item.artistId
            ))
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}