package com.ccteam.fluidmusic.ui.album

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.ccteam.shared.domain.album.AlbumListDataUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/2/17
 *
 * 专辑列表显示ViewModel
 */
@HiltViewModel
class AlbumListViewModel @Inject constructor(
    albumListDataUseCase: AlbumListDataUseCase
): ViewModel() {

    val albumListFlow = albumListDataUseCase(Any())
        .cachedIn(viewModelScope)
}