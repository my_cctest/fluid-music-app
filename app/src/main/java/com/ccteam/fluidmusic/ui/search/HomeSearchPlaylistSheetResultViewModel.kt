package com.ccteam.fluidmusic.ui.search

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.ccteam.fluidmusic.ui.playlist.select.AddOnlineMediaFragment
import com.ccteam.shared.domain.search.HomeSearchPlaylistSheetDataUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/16
 *
 * 首页搜索歌单结果ViewModel
 */
@HiltViewModel
class HomeSearchPlaylistSheetResultViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    homeSearchPlaylistSheetDataUseCase: HomeSearchPlaylistSheetDataUseCase
): ViewModel() {

    val type: Int = savedStateHandle["type"] ?: AddOnlineMediaFragment.TYPE_SEARCH

    val playlistSheetList = homeSearchPlaylistSheetDataUseCase(savedStateHandle[SEARCH_KEY])
        .cachedIn(viewModelScope)
}