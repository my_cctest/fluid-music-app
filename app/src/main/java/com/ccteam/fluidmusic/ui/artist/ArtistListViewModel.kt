package com.ccteam.fluidmusic.ui.artist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.ccteam.shared.domain.artist.ArtistListDataUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/2/17
 *
 * 歌手列表显示ViewModel
 */
@HiltViewModel
class ArtistListViewModel @Inject constructor(
    artistListDataUseCase: ArtistListDataUseCase
): ViewModel() {

    val artistListFlow = artistListDataUseCase(Any())
        .cachedIn(viewModelScope)

}