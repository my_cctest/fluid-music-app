package com.ccteam.fluidmusic.ui.moment

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.ccteam.fluidmusic.databinding.ActivityAddMomentBinding
import com.ccteam.shared.result.moment.MomentListItem
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AddMomentActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAddMomentBinding

    private val publishMomentViewModel by viewModels<PublishMomentViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddMomentBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // 如果是从intent中获得了分享内容，则可以直接设置分享的内容
        intent?.getParcelableExtra<MomentListItem.MomentShareItem>("shareContent")?.let {
            publishMomentViewModel.setShareItem(
                it
            )
        }


    }
}