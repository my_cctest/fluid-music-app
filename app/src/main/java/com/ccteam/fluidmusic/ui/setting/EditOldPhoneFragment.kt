package com.ccteam.fluidmusic.ui.setting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.ccteam.fluidmusic.R
import com.ccteam.fluidmusic.databinding.FragmentEditOldPhoneBinding
import com.ccteam.fluidmusic.utils.EventObserver
import com.ccteam.fluidmusic.utils.VerifyCodeCountDownTimer
import com.ccteam.fluidmusic.view.bean.LoadMessage
import com.ccteam.fluidmusic.widget.SNACKBAR_BOTTOM
import com.ccteam.fluidmusic.widget.snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

/**
 * @author Xiaoc
 * @since 2021/4/12
 */
@AndroidEntryPoint
class EditOldPhoneFragment: Fragment() {

    private var _binding: FragmentEditOldPhoneBinding? = null
    private val binding get() = _binding!!

    private val editOldPhoneViewModel by viewModels<EditOldPhoneViewModel>()

    private val verifyCodeCountDownTimer = VerifyCodeCountDownTimer()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentEditOldPhoneBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewLifecycleOwner.lifecycleScope.launch {
            verifyCodeCountDownTimer.countDown.collectLatest {
                if(it is VerifyCodeCountDownTimer.Status.Timing){
                    updateTiming(it.currentCountDownNum)
                } else {
                    updateNotTiming()
                }
            }
        }

        editOldPhoneViewModel.verifyCodeSuccess.observe(viewLifecycleOwner, {
            binding.btnNext.isEnabled = it
        })

        editOldPhoneViewModel.loadMessage.observe(viewLifecycleOwner, {
            binding.containerEditPhone.isVisible = it is LoadMessage.NotLoading
            binding.statusView.changeToLoadingStatus(it is LoadMessage.Loading)
            binding.statusView.changeToErrorStatus(it)
        })

        editOldPhoneViewModel.enableCountDown.observe(viewLifecycleOwner,EventObserver{
            if(it){
                verifyCodeCountDownTimer.start()
            }
        })

        editOldPhoneViewModel.verifyLoadMessage.observe(viewLifecycleOwner, EventObserver{
            binding.btnNext.setLoading(it is LoadMessage.Loading)
            if(it is LoadMessage.Success){
                findNavController().navigate(EditOldPhoneFragmentDirections.actionEditPhoneFragmentToEditNewPhoneFragment())
            } else if(it is LoadMessage.Error){
                view.snackbar(it.errorMessage.description.toString(), SNACKBAR_BOTTOM).show()
            }
        })

        binding.editVerifyCode.addOnEditTextAttachedListener {  inputLayout ->
            inputLayout.editText?.doOnTextChanged { text, _, _, _ ->
                editOldPhoneViewModel.setVerifyCodeSuccess(text.toString().isNotEmpty())
            }
        }

        binding.btnVerifyOldPhone.setProgressButtonOnClickListener{
            editOldPhoneViewModel.getVerifyCode()
        }

        binding.btnNext.setProgressButtonOnClickListener{
            editOldPhoneViewModel.verifyOldPhone(
                binding.editVerifyCode.editText?.text.toString()
            )
        }
    }

    private fun updateTiming(countDownNum: Int){
        binding.btnVerifyOldPhone.isEnabled = false
        binding.btnVerifyOldPhone.text = countDownNum.toString()
    }

    private fun updateNotTiming(){
        binding.btnVerifyOldPhone.isEnabled = true
        binding.btnVerifyOldPhone.text = getString(R.string.description_get_verification)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        verifyCodeCountDownTimer.cancel()

        _binding = null
    }
}