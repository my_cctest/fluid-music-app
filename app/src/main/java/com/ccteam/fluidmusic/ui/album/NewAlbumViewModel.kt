package com.ccteam.fluidmusic.ui.album

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.ccteam.shared.domain.album.NewAlbumDataUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/15
 *
 * 新专辑列表ViewModel
 */
@HiltViewModel
class NewAlbumViewModel @Inject constructor(
    newAlbumDataUseCase: NewAlbumDataUseCase
) : ViewModel() {

    val newAlbums = newAlbumDataUseCase(null)
        .cachedIn(viewModelScope)

}