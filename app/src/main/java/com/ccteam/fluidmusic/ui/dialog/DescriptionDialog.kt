package com.ccteam.fluidmusic.view.fragments.common

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.ccteam.fluidmusic.databinding.DialogDescriptionBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.parcelize.Parcelize

/**
 * @author Xiaoc
 * @since 2021/4/26
 */
@AndroidEntryPoint
class DescriptionDialog: BottomSheetDialogFragment(){

    private var _binding: DialogDescriptionBinding? = null
    private val binding get() = _binding!!

    private val args by navArgs<DescriptionDialogArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DialogDescriptionBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.tvDescriptionTitle.text = args.descriptionData.descriptionTitle
        binding.tvDescription.text = args.descriptionData.description
    }

    override fun onDestroyView() {
        super.onDestroyView()

        _binding = null
    }
}

@Parcelize
data class DescriptionData(
    val descriptionTitle: String,
    val description: String
): Parcelable