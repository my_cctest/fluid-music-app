package com.ccteam.fluidmusic.ui.dialog

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.ccteam.fluidmusic.view.bean.MoreInfoData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/15
 *
 * 音乐的更多选项ViewModel层
 * 负责接收并通知UI进行对应内容的显示更新
 */
@HiltViewModel
class MusicMoreDialogViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle
): ViewModel() {

    private val _mediaInfo = MutableLiveData<MoreInfoData>(savedStateHandle["mediaInfo"])
    val mediaInfo: LiveData<MoreInfoData> get() = _mediaInfo
}