package com.ccteam.fluidmusic.ui.local.album

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.ccteam.fluidmusic.databinding.FragmentLocalMusicAlbumDetailBinding
import com.ccteam.fluidmusic.ui.MainActivityViewModel
import com.ccteam.fluidmusic.ui.local.more.LocalMusicMoreBottomSheetFragmentDirections
import com.ccteam.fluidmusic.utils.TransitionUtils
import com.ccteam.fluidmusic.utils.generateContainerTransform
import com.ccteam.fluidmusic.view.adapters.GlobalAlbumDetailItemAdapter
import com.ccteam.fluidmusic.view.base.LightBounceEdgeEffectFactory
import com.ccteam.fluidmusic.view.bean.MoreInfoData
import com.ccteam.fluidmusic.widget.AnimToolbarScrollListener
import com.ccteam.shared.result.album.AlbumDetailHeaderItem
import com.ccteam.shared.result.album.AlbumDetailMusicItem
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/1/7
 *
 * 本地歌曲详情页面显示
 * 与 [LocalMusicAlbumDetailViewModel] 联动
 * 显示本地歌曲的详细列表信息，例如从专辑点击后进入显示该专辑的详细信息
 */
@AndroidEntryPoint
class LocalMusicAlbumDetailFragment : Fragment() {

    private lateinit var parentId: String

    @Inject
    lateinit var lightBounceEdgeEffectFactory: LightBounceEdgeEffectFactory

    private val args: LocalMusicAlbumDetailFragmentArgs by navArgs()

    private var _binding: FragmentLocalMusicAlbumDetailBinding? = null
    private val binding get() = _binding!!

    private val localMusicDetailViewModel by viewModels<LocalMusicAlbumDetailViewModel>()
    private val mainActivityViewModel by activityViewModels<MainActivityViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        exitTransition = TransitionUtils.exitScale
        reenterTransition = TransitionUtils.enterScale
        // 设置共享动画
        sharedElementEnterTransition = generateContainerTransform(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLocalMusicAlbumDetailBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        binding.toolbarAlbumList.setNavigationOnClickListener {
            findNavController().navigateUp()
        }

        parentId = args.fluidmusicParentId
        localMusicDetailViewModel.setAlbumHeaderFoot(args.AlbumInfo)
        // 设置Toolbar标题为专辑标题
        binding.toolbarTitle.text = args.AlbumInfo.albumTitle

        lightBounceEdgeEffectFactory.applyBounceView(binding.rvLocalMusicList,this)
        binding.rvLocalMusicList.edgeEffectFactory = lightBounceEdgeEffectFactory

        binding.rvLocalMusicList.addOnScrollListener(
            AnimToolbarScrollListener(binding.toolbarTitle,0.75f)
        )

        val musicListAdapter = GlobalAlbumDetailItemAdapter(albumMusicItemClickedCallback)

        binding.rvLocalMusicList.adapter = musicListAdapter
        localMusicDetailViewModel.albumDetailData.observe(viewLifecycleOwner, {
            musicListAdapter.submitList(it)
        })
    }

    /**
     * Item的点击回调（全局专辑Adapter）[GlobalAlbumDetailItemAdapter]
     */
    private val albumMusicItemClickedCallback = object: GlobalAlbumDetailItemAdapter.ItemClickedCallback{

        /**
         * 当 播放 按钮点击后
         */
        override fun playButtonClicked(view: View) {
            localMusicDetailViewModel.albumDetailData.value?.let {
                if(it.size > 2){
                    mainActivityViewModel.playMediaId((it[1] as AlbumDetailMusicItem).mediaId,parentId,true)
                }
            }
        }

        /**
         * 当 随机播放 按钮点击后
         * 会随机播放该专辑中的一首歌
         */
        override fun shufflePlayButtonClicked(view: View) {
            localMusicDetailViewModel.albumDetailData.value?.let {
                if(it.size > 2){
                    mainActivityViewModel.playMediaId(
                        (it[(1 until it.size - 1).random()] as AlbumDetailMusicItem).mediaId,parentId,true)
                }
            }
        }

        override fun moreAlbumDescriptionClicked(view: View, description: AlbumDetailHeaderItem) {}

        /**
         * 歌曲项点击后
         * 播放指定歌曲
         */
        override fun musicItemClicked(view: View, musicItem: AlbumDetailMusicItem) {
            mainActivityViewModel.playMediaId(musicItem.mediaId,parentId,true)
        }

        override fun musicItemMoreClicked(view: View, musicItem: AlbumDetailMusicItem) {
            findNavController().navigate(
                LocalMusicMoreBottomSheetFragmentDirections.actionLocalMoreBottomSheet(
                    MoreInfoData(
                        parentId,
                        musicItem.mediaId,
                        musicItem.title,
                        musicItem.artist,
                        true,
                        null,
                        emptyList(),
                        localMusicDetailViewModel.headerItem?.albumArtUri,
                        mediaUri = musicItem.mediaUri
                    )
                )
            )
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()

        binding.rvLocalMusicList.clearOnScrollListeners()
        _binding = null
    }

}