package com.ccteam.fluidmusic.ui.local

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.ccteam.fluidmusic.ui.PARENT_ID_KEY
import com.ccteam.fluidmusic.R
import com.ccteam.fluidmusic.databinding.FragmentLocalArtistPagerBinding
import com.ccteam.fluidmusic.view.adapters.ArtistListItemClickCallback
import com.ccteam.fluidmusic.view.adapters.GlobalArtistListItemAdapter
import com.ccteam.fluidmusic.view.base.LightBounceEdgeEffectFactory
import com.ccteam.fluidmusic.view.bean.LoadMessage
import com.ccteam.model.Resource
import com.ccteam.shared.result.artist.ArtistListItem
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class LocalArtistPagerFragment : Fragment() {
    private var _binding: FragmentLocalArtistPagerBinding? = null
    private val binding get() = _binding!!

    private lateinit var parentId: String

    private var isFirstLoad: Boolean = true

    private val localMusicViewModel by viewModels<LocalMusicViewModel>(ownerProducer = { parentFragment ?: this })

    private val artistListItemCallback = object: ArtistListItemClickCallback {
        override fun artistItemClicked(view: View,item: ArtistListItem) {
            val musicListDetailTransitionName = getString(R.string.local_music_list_detail_transition_name)
            val extra = FragmentNavigatorExtras(view to musicListDetailTransitionName)

            findNavController().navigate(LocalMusicFragmentDirections
                .actionLocalMusicFragmentToLocalArtistDetailFragment(item.mediaId,item),extra)
        }
    }

    private val adapter = GlobalArtistListItemAdapter(artistListItemCallback)

    @Inject lateinit var lightBounceEdgeEffectFactory: LightBounceEdgeEffectFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLocalArtistPagerBinding.inflate(inflater,container,false)
        return binding.root
    }

    companion object {
        @JvmStatic
        fun newInstance(parentId: String) =
            LocalArtistPagerFragment().apply {
                arguments = Bundle().apply {
                    putString(PARENT_ID_KEY, parentId)
                }
            }
    }

    override fun onResume() {
        super.onResume()
        if(isFirstLoad){
            observeArtistItems()
            isFirstLoad = false
        }
    }

    private fun observeArtistItems(){
        localMusicViewModel.artistItems.observe(viewLifecycleOwner, {
            if(it is Resource.Success){
                adapter.submitList(it.data)
            }
        })

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        parentId = arguments?.getString(PARENT_ID_KEY) ?: return

        lightBounceEdgeEffectFactory.applyBounceView(binding.rvLocalArtistPagerList,this)
        binding.rvLocalArtistPagerList.edgeEffectFactory = lightBounceEdgeEffectFactory

        binding.rvLocalArtistPagerList.adapter = adapter

        localMusicViewModel.rescanStatusByTab.observe(viewLifecycleOwner, {
            // 如果当前parentId处于未扫描状态，则开始订阅，否则不订阅，防止重复订阅
            if(it[parentId] == LocalMusicViewModel.RESCAN_NONE){
                // 订阅前显示进度条
                binding.statusView.changeToLoadingStatus(true)
                localMusicViewModel.subscribeByChildrenParentId(parentId)
            }
        })

        localMusicViewModel.artistLoadMessage.observe(viewLifecycleOwner, {
            binding.statusView.changeToLoadingStatus(it is LoadMessage.Loading)
            binding.statusView.changeToErrorStatus(it)
        })

    }


    override fun onDestroyView() {
        super.onDestroyView()
        isFirstLoad = true
        _binding = null
    }
}