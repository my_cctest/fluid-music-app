package com.ccteam.fluidmusic.ui.setting

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.preference.*
import com.ccteam.fluidmusic.R
import com.ccteam.fluidmusic.fluidmusic.core.SettingCore
import com.ccteam.fluidmusic.utils.EventObserver
import com.ccteam.fluidmusic.utils.preference.DataStorePreference
import com.ccteam.fluidmusic.view.base.LightBounceEdgeEffectFactory
import com.ccteam.fluidmusic.view.bean.LoadMessage
import com.ccteam.fluidmusic.widget.SNACKBAR_BOTTOM
import com.ccteam.fluidmusic.widget.snackbar
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 *
 * @author Xiaoc
 * @since 2021/1/20
 *
 * FluidMusic设置界面
 */
@AndroidEntryPoint
class FluidMusicSettingsFragment: PreferenceFragmentCompat(){

    @Inject lateinit var dataStorePreference: DataStorePreference
    @Inject lateinit var lightBounceEdgeEffectFactory: LightBounceEdgeEffectFactory

    private val settingViewModel by viewModels<SettingViewModel>()

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        preferenceManager.preferenceDataStore = dataStorePreference
        setPreferencesFromResource(R.xml.preference_fluidmusic_setting, rootKey)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // 设置回弹效果
        lightBounceEdgeEffectFactory.applyBounceView(view,this)
        listView.edgeEffectFactory = lightBounceEdgeEffectFactory

        findPreference<Preference>(SettingCore.KEY_ACCOUNT_LOGOUT)?.setOnPreferenceClickListener {
            // 退出登录提示
            MaterialAlertDialogBuilder(it.context)
                .setTitle(R.string.description_account_logout)
                .setMessage(getString(R.string.description_logout))
                .setNegativeButton(R.string.description_cancel) { _,_ -> }
                .setPositiveButton(R.string.description_logout) { _,_ ->
                    settingViewModel.logout()
                }.show()
            return@setOnPreferenceClickListener true
        }

        /**
         * 根据是否开启定时任务来显示对应是否开启定时滑动块
         */
        val timerSeekBarPreference = findPreference<SeekBarPreference>(SettingCore.KEY_PLAY_TIMER_SEEKBAR)
        val timerEnablePreference = findPreference<SwitchPreferenceCompat>(SettingCore.KEY_PLAY_TIMER_SWITCH)
        timerSeekBarPreference?.isEnabled = timerEnablePreference?.isChecked == true

        timerEnablePreference?.setOnPreferenceChangeListener { _, newValue ->
            timerSeekBarPreference?.isEnabled = newValue == true
            return@setOnPreferenceChangeListener true
        }

        /**
         * 监听是否显示账户相关按钮
         */
        settingViewModel.shouldShowAccount.observe(viewLifecycleOwner, {
            val accountPreferenceCategory = findPreference<PreferenceCategory>(SettingCore.KEY_ACCOUNT)
            accountPreferenceCategory?.isVisible = it
        })

        settingViewModel.loadMessage.observe(viewLifecycleOwner, EventObserver{
            if(it is LoadMessage.Success){
                view.snackbar(R.string.description_account_logout_success, SNACKBAR_BOTTOM).show()
                findNavController().navigateUp()
            } else if(it is LoadMessage.Error){
                Snackbar.make(view,it.errorMessage.description.toString(), Snackbar.LENGTH_SHORT).show()
            }

        })

    }

}