package com.ccteam.fluidmusic.ui.home

import android.app.Application
import androidx.lifecycle.*
import com.ccteam.fluidmusic.R
import com.ccteam.fluidmusic.utils.cancelIfActive
import com.ccteam.fluidmusic.view.bean.LoadMessage
import com.ccteam.model.Resource
import com.ccteam.shared.domain.home.recommend.HomeRecommendDataUseCase
import com.ccteam.shared.domain.home.recommend.Recommend
import com.ccteam.shared.result.BaseItemData
import com.ccteam.shared.result.home.OnlineMusicHomeSmallItem
import com.ccteam.shared.result.home.OnlineMusicHomeSmallItem.Companion.SMALL_ITEM_ALBUM
import com.ccteam.shared.result.home.OnlineMusicHomeSmallItem.Companion.SMALL_ITEM_MUSIC
import com.ccteam.shared.result.home.OnlineMusicHomeSmallItem.Companion.SMALL_ITEM_PLAYLIST
import com.ccteam.shared.result.home.OnlineMusicHomeTypeBrowseItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 *
 * @author Xiaoc
 * @since 2021/1/17
 *
 * 在线音乐首页的ViewModel
 */
@HiltViewModel
class OnlineMusicHomeViewModel @Inject constructor(
    application: Application,
    private val homeRecommendDataUseCase: HomeRecommendDataUseCase
): AndroidViewModel(application) {

    private val _bannerItems = MutableLiveData<List<BaseItemData>>()
    val bannerItems: LiveData<List<BaseItemData>> = _bannerItems


    private val _newAlbumRecommends = MutableLiveData<List<BaseItemData>>()
    val newAlbumRecommends: LiveData<List<BaseItemData>> get() = _newAlbumRecommends

    private val _newMusicRecommends = MutableLiveData<List<BaseItemData>>()
    val newMusicRecommends: LiveData<List<BaseItemData>> get() = _newMusicRecommends

    private val _hotPlaylistSheetRecommends = MutableLiveData<List<BaseItemData>>()
    val hotPlaylistSheetRecommends: LiveData<List<BaseItemData>> get() = _hotPlaylistSheetRecommends

    private val _hotMusicRecommends = MutableLiveData<List<BaseItemData>>()
    val hotMusicRecommends: LiveData<List<BaseItemData>> get() = _hotMusicRecommends

    private val _loadMessage = MutableLiveData<LoadMessage>()
    val loadMessage: LiveData<LoadMessage> get() = _loadMessage

    private var loadOnlineHomeDataJob: Job? = null

    init {
        // 初始加载首页内容
        refreshOnlineHomeData()
    }

    /**
     * 刷新首页数据
     */
    fun refreshOnlineHomeData(){
        notifyOnlineHomeDataChanges()
    }

    /**
     * 进行首页内容的获取，使用Flow流
     */
    private fun notifyOnlineHomeDataChanges(){
        loadOnlineHomeDataJob.cancelIfActive()
        _loadMessage.value = LoadMessage.Loading
        val application = getApplication<Application>()
        loadOnlineHomeDataJob = viewModelScope.launch {
            homeRecommendDataUseCase(null).map {
                if(it is Resource.Success){
                    it.data ?: emptyList()
                } else {
                    emptyList()
                }
            }.collect {
                if(it.isNullOrEmpty()){
                    return@collect
                }
                when(it[0]){
                    is Recommend.RecommendBanner ->{
                        _bannerItems.value = listOf(OnlineMusicHomeTypeBrowseItem(it.filterIsInstance<Recommend.RecommendBanner>().map { banner ->
                            banner.item
                        }))
                    }
                    is Recommend.RecommendNewMusic ->{
                        _newMusicRecommends.value = listOf(
                            OnlineMusicHomeSmallItem(application.getString(R.string.online_music_new_music),
                                SMALL_ITEM_MUSIC, it.filterIsInstance<Recommend.RecommendNewMusic>().map { music ->
                                music.item
                            }))
                    }
                    is Recommend.RecommendNewAlbum ->{
                        _newAlbumRecommends.value = listOf(
                            OnlineMusicHomeSmallItem(application.getString(R.string.online_music_new_album),
                                SMALL_ITEM_ALBUM, it.filterIsInstance<Recommend.RecommendNewAlbum>().map { album ->
                                    album.item
                                })
                        )
                    }
                    is Recommend.RecommendHotPlaylistSheet ->{
                        _hotPlaylistSheetRecommends.value = listOf(
                            OnlineMusicHomeSmallItem(application.getString(R.string.online_music_hot_playlistSheet),
                                SMALL_ITEM_PLAYLIST, it.filterIsInstance<Recommend.RecommendHotPlaylistSheet>().map { playlist ->
                                    playlist.item
                                },showMore = false)
                        )
                    }
                    is Recommend.RecommendHotMusic ->{
                        _hotMusicRecommends.value = listOf(
                            OnlineMusicHomeSmallItem(application.getString(R.string.online_music_hot_music),
                                SMALL_ITEM_MUSIC, it.filterIsInstance<Recommend.RecommendHotMusic>().map { playlist ->
                                    playlist.item
                                },showMore = false)
                        )
                    }
                }

                _loadMessage.value = LoadMessage.Success
            }
        }
    }

    override fun onCleared() {
        super.onCleared()

        loadOnlineHomeDataJob.cancelIfActive()
    }
}