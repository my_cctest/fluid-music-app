package com.ccteam.fluidmusic.ui.auth

import androidx.lifecycle.*
import com.ccteam.fluidmusic.utils.Event
import com.ccteam.fluidmusic.view.bean.ErrorMessage
import com.ccteam.fluidmusic.view.bean.LoadMessage
import com.ccteam.model.Resource
import com.ccteam.model.user.UserLoginDTO
import com.ccteam.shared.domain.auth.ForgetPasswordDataUseCase
import com.ccteam.shared.domain.auth.RSADataUseCase
import com.ccteam.shared.domain.auth.VerificationDataUseCase
import com.orhanobut.logger.Logger
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/4/16
 *
 * 忘记密码ViewModel
 * 负责存储忘记密码页面数据及对应显示逻辑
 */
@HiltViewModel
class ForgetPasswordViewModel @Inject constructor(
    private val rsaDataUseCase: RSADataUseCase,
    private val forgetPasswordDataUseCase: ForgetPasswordDataUseCase,
    private val verificationDataUseCase: VerificationDataUseCase
): ViewModel() {

    private val _loadMessage = MutableLiveData<Event<LoadMessage>>()
    val loadMessage: LiveData<Event<LoadMessage>> get() = _loadMessage

    private val _enableForgetPasswordButton = MediatorLiveData<Boolean>()
    val enableForgetPasswordButton: LiveData<Boolean> get() = _enableForgetPasswordButton

    /**
     * 是否开启获取验证码按钮
     * 与 Timer 倒计时共同作用验证码开关
     */
    private val _enableVerifyCodeButton = MutableLiveData(false)
    val enableVerifyCodeButton: LiveData<Boolean> get() = _enableVerifyCodeButton

    /**
     * 是否开启倒计时计数
     */
    private val _enableCountDown = MutableLiveData(Event(false))
    val enableCountDown: LiveData<Event<Boolean>> get() = _enableCountDown

    private val phoneSuccess = MutableLiveData(false)
    private val passwordSuccess = MutableLiveData(false)
    private val confirmPasswordSuccess = MutableLiveData(false)
    private val verifyCodeSuccess = MutableLiveData(false)

    init {
        _enableForgetPasswordButton.addSource(phoneSuccess){
            _enableVerifyCodeButton.value = it
            _enableForgetPasswordButton.value = it
                    && passwordSuccess.value == true
                    && confirmPasswordSuccess.value == true
                    && verifyCodeSuccess.value == true
        }

        _enableForgetPasswordButton.addSource(passwordSuccess){
            _enableForgetPasswordButton.value = it
                    && phoneSuccess.value == true
                    && confirmPasswordSuccess.value == true
                    && verifyCodeSuccess.value == true
        }

        _enableForgetPasswordButton.addSource(confirmPasswordSuccess){
            _enableForgetPasswordButton.value = it
                    && phoneSuccess.value == true
                    && passwordSuccess.value == true
                    && verifyCodeSuccess.value == true
        }

        _enableForgetPasswordButton.addSource(verifyCodeSuccess){
            _enableForgetPasswordButton.value = it
                    && phoneSuccess.value == true
                    && passwordSuccess.value == true
                    && confirmPasswordSuccess.value == true
        }
    }

    fun getVerification(phone: String){
        if(_enableVerifyCodeButton.value == false){
            _loadMessage.value = Event(
                LoadMessage.Error(
                ErrorMessage(description = "无法进行此操作")
            ))
            return
        }
        viewModelScope.launch {
            val result = verificationDataUseCase(Pair(phone,VerificationDataUseCase.FORGET_PASSWORD_CODE))

            if(result is Resource.Success){
                _enableCountDown.value = Event(true)
            } else {
                _loadMessage.value = Event(LoadMessage.Error(
                    ErrorMessage(result.message,result.errorCode)))

                _enableVerifyCodeButton.value = true
            }
        }
    }

    fun forgetPassword(phone: String,password: String,code: String){
        _loadMessage.value = Event(LoadMessage.Loading)

        viewModelScope.launch {
            val rsaResult = rsaDataUseCase(password)
            if(rsaResult is Resource.Success){
                val result = forgetPasswordDataUseCase(UserLoginDTO(code,phone,rsaResult.data))

                if(result is Resource.Success){
                    _loadMessage.value = Event(LoadMessage.Success)
                } else {
                    _loadMessage.value = Event(LoadMessage.Error(
                        ErrorMessage(result.message,result.errorCode)
                    ))
                }
            } else {
                _loadMessage.value = Event(LoadMessage.Error(
                    ErrorMessage(rsaResult.message,rsaResult.errorCode)
                ))
                Logger.d("密钥获取失败:$rsaResult")
            }
        }
    }

    fun setPhoneSuccess(success: Boolean){
        phoneSuccess.value = success
    }

    fun setPasswordSuccess(success: Boolean){
        passwordSuccess.value = success
    }

    fun setConfirmPasswordSuccess(success: Boolean){
        confirmPasswordSuccess.value = success
    }

    fun setVerifyCodeSuccess(success: Boolean){
        verifyCodeSuccess.value = success
    }
}