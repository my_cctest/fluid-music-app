package com.ccteam.fluidmusic.ui.base

import androidx.lifecycle.ViewModel
import com.luck.picture.lib.PictureSelector
import com.luck.picture.lib.entity.LocalMedia

/**
 * @author Xiaoc
 * @since 2021/1/25
 *
 * 这是图片选择Fragment的基本ViewModel
 * 因为图片选择Fragment可能复用，所以该基本ViewModel存储着选择的内容，方便其他Fragment复用
 * 前提是该ViewModel在多个Fragment中是公用的（生命周期是Activity等）
 **/
class BasePhotoPickerViewModel: ViewModel() {

    /**
     * 已选择的图片数据
     * 使用 [PictureSelector] 框架，使用它自带的 [LocalMedia] 存储对应图片内容
     */
    val selectImages: MutableList<LocalMedia> = mutableListOf()

    /**
     * 最大能选择的数量，默认为9，会根据对应内容添加删除进行调整
     */
    var maxSelectCount = 9

    /**
     * 移除某个位置的图片数据
     */
    fun removeItem(position: Int){
        selectImages.removeAt(position)
        maxSelectCount += 1
    }

    /**
     * 添加一系列图片数据
     */
    fun addItem(images: MutableList<LocalMedia>){
        selectImages.addAll(images)
        maxSelectCount -= images.size
    }



}