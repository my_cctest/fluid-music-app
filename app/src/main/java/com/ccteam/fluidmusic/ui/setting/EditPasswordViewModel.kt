package com.ccteam.fluidmusic.ui.setting

import androidx.lifecycle.*
import com.ccteam.fluidmusic.utils.Event
import com.ccteam.fluidmusic.view.bean.ErrorMessage
import com.ccteam.fluidmusic.view.bean.LoadMessage
import com.ccteam.model.Resource
import com.ccteam.shared.core.UserCore
import com.ccteam.shared.domain.auth.RSADataUseCase
import com.ccteam.shared.domain.auth.VerificationDataUseCase
import com.ccteam.shared.domain.user.EditPasswordDataUseCase
import com.ccteam.shared.domain.user.EditPasswordRequest
import com.ccteam.shared.domain.user.UserPhoneDataUseCase
import com.ccteam.shared.utils.EMPTY_STRING
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/4/12
 *
 * 修改密码ViewModel
 */
@HiltViewModel
class EditPasswordViewModel @Inject constructor(
    private val userCore: UserCore,
    private val rsaDataUseCase: RSADataUseCase,
    private val userPhoneDataUseCase: UserPhoneDataUseCase,
    private val editPasswordDataUseCase: EditPasswordDataUseCase,
    private val verificationDataUseCase: VerificationDataUseCase
): ViewModel() {

    private val _enableEditPassword = MediatorLiveData<Boolean>()
    val enableEditPassword: LiveData<Boolean> get() = _enableEditPassword

    /**
     * 是否开启倒计时计数
     */
    private val _enableCountDown = MutableLiveData<Event<Boolean>>()
    val enableCountDown: LiveData<Event<Boolean>> get() = _enableCountDown

    private val _editLoadMessage = MutableLiveData<Event<LoadMessage>>()
    val editLoadMessage: LiveData<Event<LoadMessage>> get() = _editLoadMessage

    private val _loadMessage = MutableLiveData<LoadMessage>(LoadMessage.Loading)
    val loadMessage: LiveData<LoadMessage> get() = _loadMessage

    private val verifySuccess = MutableLiveData(false)
    private val passwordSuccess = MutableLiveData(false)
    private val passwordConfirmSuccess = MutableLiveData(false)

    private var userPhone: String = EMPTY_STRING

    init {
        _enableEditPassword.addSource(verifySuccess){
            _enableEditPassword.value = it && passwordSuccess.value == true
                    && passwordConfirmSuccess.value == true
        }
        _enableEditPassword.addSource(passwordSuccess){
            _enableEditPassword.value = it && verifySuccess.value == true
                    && passwordConfirmSuccess.value == true
        }
        _enableEditPassword.addSource(passwordConfirmSuccess){
            _enableEditPassword.value = it && passwordSuccess.value == true
                    && verifySuccess.value == true
        }

        // 获取用户手机信息
        getUserPhone()
    }

    /**
     * 获取手机验证码
     */
    fun getVerifyCode(){
        viewModelScope.launch {
            val result = verificationDataUseCase(userPhone to VerificationDataUseCase.CHANGE_PASSWORD_CODE)

            _enableCountDown.value = Event(result is Resource.Success)
            if(result is Resource.Error){
                _editLoadMessage.value = Event(LoadMessage.Error(ErrorMessage(result.message,result.errorCode)))
            }
        }
    }

    fun editPassword(password: String,code: String){
        _editLoadMessage.value = Event(LoadMessage.Loading)

        viewModelScope.launch {
            val rsaResult = rsaDataUseCase(password)
            if(rsaResult is Resource.Success){
                val result = editPasswordDataUseCase(
                    EditPasswordRequest(
                    userPhone,rsaResult.data!!,code)
                )
                if(result is Resource.Success){
                    // 修改密码成功
                    _editLoadMessage.value = Event(LoadMessage.Success)
                    // 修改密码成功，退出登录状态
                    userCore.logout()
                } else {
                    _editLoadMessage.value = Event(LoadMessage.Error(ErrorMessage(result.message,result.errorCode)))
                }
            } else {
                _editLoadMessage.value = Event(LoadMessage.Error(ErrorMessage(rsaResult.message,rsaResult.errorCode)))
            }
        }
    }

    fun setVerifySuccess(success: Boolean){
        verifySuccess.value = success
    }

    fun setPasswordSuccess(success: Boolean){
        passwordSuccess.value = success
    }

    fun setPasswordConfirmSuccess(success: Boolean){
        passwordConfirmSuccess.value = success
    }

    private fun getUserPhone(){
        viewModelScope.launch {
            val result = userPhoneDataUseCase(null)
            if(result is Resource.Success){
                userPhone = result.data!!
                _loadMessage.value = LoadMessage.NotLoading
            } else {
                _loadMessage.value = LoadMessage.Error(ErrorMessage(result.message,result.errorCode))
            }
        }
    }
}