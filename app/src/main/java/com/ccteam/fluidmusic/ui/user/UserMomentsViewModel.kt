package com.ccteam.fluidmusic.ui.user

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.ccteam.shared.domain.moment.MomentInfoUserDataUseCase
import com.ccteam.shared.result.moment.MomentListItem
import com.ccteam.shared.result.user.MeHomeInfoItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/4/7
 *
 * 用户朋友圈列表ViewModel
 */
@HiltViewModel
class UserMomentsViewModel @Inject constructor(
    private val momentInfoUserDataUseCase: MomentInfoUserDataUseCase,
    savedStateHandle: SavedStateHandle
): ViewModel() {

    /**
     * 如果传来了userId，代表查询用户的歌单，需要调用另外的Domain层
     */
    val momentsList: Flow<PagingData<MomentListItem>> = momentInfoUserDataUseCase(savedStateHandle.get<MeHomeInfoItem>("userInfo"))
            .cachedIn(viewModelScope)
}