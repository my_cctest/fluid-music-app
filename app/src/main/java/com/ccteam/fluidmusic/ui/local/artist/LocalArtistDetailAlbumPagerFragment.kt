package com.ccteam.fluidmusic.ui.local.artist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.ccteam.fluidmusic.ui.PARENT_ID_KEY
import com.ccteam.fluidmusic.R
import com.ccteam.fluidmusic.databinding.FragmentLocalArtistDetailAlbumPagerBinding
import com.ccteam.fluidmusic.view.adapters.AlbumListItemAdapter
import com.ccteam.fluidmusic.view.base.LightBounceEdgeEffectFactory
import com.ccteam.model.Resource
import com.ccteam.shared.result.album.AlbumDetailHeaderItem
import com.ccteam.shared.result.album.AlbumListItem
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class LocalArtistDetailAlbumPagerFragment : Fragment() {

    private lateinit var parentId: String

    private var _binding: FragmentLocalArtistDetailAlbumPagerBinding? = null
    private val binding get() = _binding!!

    private val artistAlbumViewModel: LocalArtistDetailAlbumViewModel by viewModels(ownerProducer = { parentFragment ?: this })

    private val localMusicAlbumClickCallback = object : AlbumListItemAdapter.AlbumListItemCallback {

        override fun onClickItem(view: View, albumItem: AlbumListItem) {
            // 设置过渡动画
            val musicListDetailTransitionName = getString(R.string.local_music_list_detail_transition_name)
            val extra = FragmentNavigatorExtras(view to musicListDetailTransitionName)

            findNavController().navigate(
                LocalArtistDetailFragmentDirections.actionLocalArtistDetailFragmentToLocalMusicAlbumListFragment(
                    albumItem.mediaId,
                    AlbumDetailHeaderItem(
                        albumItem.albumId, albumItem.albumTitle,
                        albumItem.albumArtUri, albumItem.artist, albumItem.year,
                        null, albumItem.artistId ?: "-1"
                    )
                ),extra)
        }

    }

    private val adapter = AlbumListItemAdapter(localMusicAlbumClickCallback)

    @Inject lateinit var lightBounceEdgeEffectFactory: LightBounceEdgeEffectFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLocalArtistDetailAlbumPagerBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        parentId = arguments?.getString(PARENT_ID_KEY) ?: return

        lightBounceEdgeEffectFactory.applyBounceView(binding.rvArtistDetailAlbum,this)
        binding.rvArtistDetailAlbum.edgeEffectFactory = lightBounceEdgeEffectFactory

        binding.rvArtistDetailAlbum.adapter = adapter

        artistAlbumViewModel.albumItems.observe(viewLifecycleOwner, {
            when(it){
                is Resource.Success ->{
                    adapter.submitList(it.data)
                }
                else -> { }
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        @JvmStatic
        fun newInstance(parentId: String) =
            LocalArtistDetailAlbumPagerFragment().apply {
                arguments = Bundle().apply {
                    putString(PARENT_ID_KEY, parentId)
                }
            }
    }
}