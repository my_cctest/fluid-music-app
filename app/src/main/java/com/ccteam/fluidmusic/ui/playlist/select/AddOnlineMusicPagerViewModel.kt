package com.ccteam.fluidmusic.ui.playlist.select

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.ccteam.fluidmusic.ui.search.SEARCH_KEY
import com.ccteam.shared.domain.playlistsheet.music.PlaylistSheetAddedMusicDataUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/17
 */
@HiltViewModel
class AddOnlineMusicPagerViewModel @Inject constructor(
    addOnlineMusicDataUseCase: PlaylistSheetAddedMusicDataUseCase,
    savedStateHandle: SavedStateHandle
): ViewModel() {

    val type: Int = savedStateHandle["type"] ?: AddOnlineMediaFragment.TYPE_ADD_MEDIA_MOMENTS

    val musicList = addOnlineMusicDataUseCase(savedStateHandle[SEARCH_KEY])
        .cachedIn(viewModelScope)
}