package com.ccteam.fluidmusic.ui.moment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.ccteam.fluidmusic.utils.Event
import com.ccteam.shared.domain.moment.MomentsHomeDataUseCase
import com.ccteam.shared.result.moment.MomentListItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/24
 *
 * 朋友圈列表显示ViewModel
 */
@HiltViewModel
class MomentListViewModel @Inject constructor(
    momentsHomeDataUseCase: MomentsHomeDataUseCase
): ViewModel() {

    private val _shouldRefreshMomentList = MutableLiveData<Event<Boolean>>()
    val shouldRefreshMomentList: LiveData<Event<Boolean>> get() = _shouldRefreshMomentList

    /**
     * 如果传来了userId，代表查询用户的歌单，需要调用另外的Domain层
     */
    val momentsList: Flow<PagingData<MomentListItem>> = momentsHomeDataUseCase(null)
            .cachedIn(viewModelScope)

    fun refreshMomentList(){
        _shouldRefreshMomentList.value = Event(true)
    }

    /**
     * 获取音乐信息
     * 这里主要是朋友圈点击歌曲时需要使用该方法
     * 进行获取音乐信息后进行播放
     */
//    suspend fun getMusicInfo(musicId: String): SearchMoreInfoData?{
//        return when(val result = musicInfoDataUseCase(musicId)){
//            is Resource.Success ->{
//                result.data
//            }
//            else -> {
//                null
//            }
//        }
//    }
}