package com.ccteam.fluidmusic.ui.setting

import androidx.lifecycle.*
import com.ccteam.fluidmusic.utils.Event
import com.ccteam.fluidmusic.utils.setValueIfNew
import com.ccteam.fluidmusic.view.bean.ErrorMessage
import com.ccteam.fluidmusic.view.bean.LoadMessage
import com.ccteam.model.Resource
import com.ccteam.shared.core.UserCore
import com.ccteam.shared.domain.auth.VerificationDataUseCase
import com.ccteam.shared.domain.auth.VerificationDataUseCase.Companion.CHANGE_PHONE_CODE
import com.ccteam.shared.domain.user.VerifyPhoneDataUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/4/16
 *
 * 修改手机号ViewModel
 */
@HiltViewModel
class EditNewPhoneViewModel @Inject constructor(
    private val userCore: UserCore,
    private val verifyPhoneDataUseCase: VerifyPhoneDataUseCase,
    private val verificationDataUseCase: VerificationDataUseCase
): ViewModel() {

    private val _enableEditNewPhoneButton = MediatorLiveData<Boolean>()
    val enableEditNewPhoneButton: LiveData<Boolean> get() = _enableEditNewPhoneButton

    /**
     * 是否开启获取验证码按钮
     * 与 Timer 倒计时共同作用验证码开关
     */
    private val _enableVerifyCodeButton = MutableLiveData(false)
    val enableVerifyCodeButton: LiveData<Boolean> get() = _enableVerifyCodeButton

    /**
     * 是否开启倒计时计数
     */
    private val _enableCountDown = MutableLiveData(Event(false))
    val enableCountDown: LiveData<Event<Boolean>> get() = _enableCountDown

    private val phoneSuccess = MutableLiveData(false)
    private val verifyCodeSuccess = MutableLiveData(false)

    private val _snackLoadMessage = MutableLiveData<Event<LoadMessage>>()
    val snackLoadMessage: LiveData<Event<LoadMessage>> get() = _snackLoadMessage

    init {
        _enableEditNewPhoneButton.addSource(phoneSuccess){
            _enableVerifyCodeButton.setValueIfNew(it)
            _enableEditNewPhoneButton.value = it && verifyCodeSuccess.value == true
        }
        _enableEditNewPhoneButton.addSource(verifyCodeSuccess){
            _enableEditNewPhoneButton.value = it && phoneSuccess.value == true
        }
    }

    fun getVerification(phone: String){
        if(_enableVerifyCodeButton.value == false){
            _snackLoadMessage.value = Event(
                LoadMessage.Error(
                ErrorMessage(description = "无法进行此操作")
            ))
            return
        }
        viewModelScope.launch {
            val result = verificationDataUseCase(phone to CHANGE_PHONE_CODE)
            if(result is Resource.Success){
                _enableCountDown.value = Event(true)
            } else {
                _snackLoadMessage.value = Event(LoadMessage.Error(ErrorMessage(result.message,result.errorCode)))
            }
        }

    }

    fun editNewPhone(phone: String,code: String){
        viewModelScope.launch {
            val result = verifyPhoneDataUseCase(VerifyPhoneDataUseCase.VerifyPhoneRequest(true,phone,code))
            if(result is Resource.Success){
                // 修改新手机号成功后，退出登录
                userCore.logout()
                _snackLoadMessage.value = Event(LoadMessage.Success)
            } else {
                _snackLoadMessage.value = Event(LoadMessage.Error(ErrorMessage(result.message,result.errorCode)))
            }
        }
    }

    fun setPhoneSuccess(success: Boolean){
        phoneSuccess.value = success
    }

    fun setVerifyCodeSuccess(success: Boolean){
        verifyCodeSuccess.value = success
    }
}