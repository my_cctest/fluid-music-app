package com.ccteam.fluidmusic.ui.setting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.ccteam.fluidmusic.BR
import com.ccteam.fluidmusic.R
import com.ccteam.fluidmusic.databinding.FragmentAboutBinding
import com.ccteam.fluidmusic.utils.DataTestUtils
import com.ccteam.fluidmusic.view.adapters.databinding.DataBindViewHolder
import com.ccteam.fluidmusic.view.adapters.databinding.MultiTypeDataBindAdapter
import com.ccteam.fluidmusic.view.base.LightBounceEdgeEffectFactory
import com.ccteam.fluidmusic.view.bean.AboutDeveloperItem
import com.ccteam.fluidmusic.view.bean.AboutOpenSourceItem
import com.ccteam.fluidmusic.view.bean.AboutSubTitleItem
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/4/6
 */
@AndroidEntryPoint
class AboutFragment: Fragment() {

    private var _binding: FragmentAboutBinding? = null
    private val binding get() = _binding!!

    @Inject lateinit var lightBounceEdgeEffectFactory: LightBounceEdgeEffectFactory

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAboutBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        lightBounceEdgeEffectFactory.applyBounceView(binding.rvAbout,this)
        binding.rvAbout.edgeEffectFactory = lightBounceEdgeEffectFactory

        val adapter = AboutRVAdapter()
        binding.rvAbout.adapter = adapter

        viewLifecycleOwner.lifecycleScope.launch {
            val aboutList = DataTestUtils.generateAboutList(requireContext())
            adapter.submitList(aboutList)
        }
    }

    class AboutRVAdapter: MultiTypeDataBindAdapter(AboutSubTitleItem.differCallback) {

        override fun bindItem(
            holder: DataBindViewHolder<*>?,
            position: Int,
            payloads: MutableList<Any?>?
        ) {
            holder?.binding?.setVariable(BR.data,getItem(position))
        }

        override fun getItemLayoutId(position: Int): Int {
            return when (getItem(position)) {
                is AboutSubTitleItem ->{
                    R.layout.about_subtitle_item
                }
                is AboutOpenSourceItem ->{
                    R.layout.about_open_source_item
                }
                is AboutDeveloperItem ->{
                    R.layout.about_developer_item
                }
                else ->{
                    throw IllegalArgumentException("错误的布局")
                }
            }
        }

    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}