package com.ccteam.fluidmusic.ui.local.more

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.ccteam.fluidmusic.view.bean.MoreInfoData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/5
 *
 * 本地音乐更多功能显示的ViewModel
 */
@HiltViewModel
class MusicMoreDialogViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle
): ViewModel() {

    private val _mediaInfo = MutableLiveData<MoreInfoData>(savedStateHandle["mediaInfo"])
    val mediaInfo: LiveData<MoreInfoData> get() = _mediaInfo

}