package com.ccteam.fluidmusic.ui.artist

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.ccteam.shared.domain.artist.ArtistDetailAlbumUseCase
import com.ccteam.shared.result.album.AlbumListItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/2/20
 *
 * 歌手详情专辑页内容ViewModel
 */
@HiltViewModel
class ArtistDetailAlbumViewModel @Inject constructor(
    artistDetailAlbumUseCase: ArtistDetailAlbumUseCase,
    savedStateHandle: SavedStateHandle
): ViewModel() {

    val artistAlbumFlow: Flow<PagingData<AlbumListItem>> =
        artistDetailAlbumUseCase(savedStateHandle["artistId"])
            .cachedIn(viewModelScope)
}