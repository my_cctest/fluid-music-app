package com.ccteam.fluidmusic.ui.local.artist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.ccteam.fluidmusic.ui.PARENT_ID_KEY
import com.ccteam.fluidmusic.databinding.FragmentLocalArtistDetailMusicPagerBinding
import com.ccteam.fluidmusic.view.adapters.GlobalDefaultMusicItemAdapter
import com.ccteam.fluidmusic.view.base.LightBounceEdgeEffectFactory
import com.ccteam.fluidmusic.view.bean.MoreInfoData
import com.ccteam.fluidmusic.ui.MainActivityViewModel
import com.ccteam.fluidmusic.ui.local.more.LocalMusicMoreBottomSheetFragmentDirections
import com.ccteam.model.Resource
import com.ccteam.shared.result.music.DefaultMusicItem
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class LocalArtistDetailMusicPagerFragment : Fragment() {

    private lateinit var parentId: String

    private var _binding: FragmentLocalArtistDetailMusicPagerBinding? = null
    private val binding get() = _binding!!

    private val artistMusicViewModel by viewModels<LocalArtistDetailMusicViewModel>(ownerProducer = { parentFragment ?: this })
    private val mainActivityViewModel by activityViewModels<MainActivityViewModel>()

    private val callback = object: GlobalDefaultMusicItemAdapter.DefaultMusicItemClickCallback{

        override fun musicItemClicked(item: DefaultMusicItem) {
            mainActivityViewModel.playMediaId(item.mediaId,parentId,true)
        }

        override fun moreClicked(item: DefaultMusicItem) {
            findNavController().navigate(
                LocalMusicMoreBottomSheetFragmentDirections.actionLocalMoreBottomSheet(
                    MoreInfoData(
                        parentId, item.mediaId, item.title, item.subtitle, true, null,
                        emptyList(), item.albumArtUri, mediaUri = item.mediaUri
                    )
                )
            )
        }

    }

    private val adapter = GlobalDefaultMusicItemAdapter(callback)

    @Inject lateinit var lightBounceEdgeEffectFactory: LightBounceEdgeEffectFactory

    private var isFirstLoad = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLocalArtistDetailMusicPagerBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        parentId = arguments?.getString(PARENT_ID_KEY) ?: return
        lightBounceEdgeEffectFactory.applyBounceView(binding.rvArtistDetailMusic,this)
        binding.rvArtistDetailMusic.edgeEffectFactory = lightBounceEdgeEffectFactory

        binding.rvArtistDetailMusic.adapter = adapter
    }

    override fun onResume() {
        super.onResume()

        if(isFirstLoad){
            observeArtistMusic()
            isFirstLoad = false
        }
    }

    private fun observeArtistMusic(){
        artistMusicViewModel.musicItems.observe(viewLifecycleOwner, {
            when(it){
                is Resource.Success ->{
                    adapter.submitList(it.data)
                }
                is Resource.Loading ->{

                }
                else -> {

                }
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        isFirstLoad = true
        _binding = null
    }

    companion object {
        @JvmStatic
        fun newInstance(parentId: String) =
            LocalArtistDetailMusicPagerFragment().apply {
                arguments = Bundle().apply {
                    putString(PARENT_ID_KEY, parentId)
                }
            }
    }
}