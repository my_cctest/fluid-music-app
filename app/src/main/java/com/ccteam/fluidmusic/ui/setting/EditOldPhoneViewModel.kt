package com.ccteam.fluidmusic.ui.setting

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ccteam.fluidmusic.utils.Event
import com.ccteam.fluidmusic.view.bean.ErrorMessage
import com.ccteam.fluidmusic.view.bean.LoadMessage
import com.ccteam.model.Resource
import com.ccteam.shared.domain.auth.VerificationDataUseCase
import com.ccteam.shared.domain.user.UserPhoneDataUseCase
import com.ccteam.shared.domain.user.VerifyPhoneDataUseCase
import com.ccteam.shared.utils.EMPTY_STRING
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/4/12
 *
 * 验证旧手机号ViewModel
 */
@HiltViewModel
class EditOldPhoneViewModel @Inject constructor(
    private val verifyPhoneDataUseCase: VerifyPhoneDataUseCase,
    private val userPhoneDataUseCase: UserPhoneDataUseCase,
    private val verificationDataUseCase: VerificationDataUseCase
): ViewModel() {

    private val _verifyCodeSuccess = MutableLiveData(false)
    val verifyCodeSuccess: LiveData<Boolean> get() = _verifyCodeSuccess

    /**
     * 是否开启倒计时计数
     */
    private val _enableCountDown = MutableLiveData(Event(false))
    val enableCountDown: LiveData<Event<Boolean>> get() = _enableCountDown

    /**
     * 验证验证码时的状态信息
     * 如果成功则调至下一页
     */
    private val _verifyLoadMessage = MutableLiveData<Event<LoadMessage>>()
    val verifyLoadMessage: LiveData<Event<LoadMessage>> get() = _verifyLoadMessage

    /**
     * 获取手机号的状态信息
     * 如果出错显示错误View
     */
    private val _loadMessage = MutableLiveData<LoadMessage>(LoadMessage.Loading)
    val loadMessage: LiveData<LoadMessage> get() = _loadMessage

    private var userPhone: String = EMPTY_STRING

    init {
        getUserPhone()
    }

    /**
     * 获取手机验证码
     */
    fun getVerifyCode(){
        viewModelScope.launch {
            val result = verificationDataUseCase(userPhone to VerificationDataUseCase.CHANGE_PHONE_CODE)

            if(result is Resource.Success){
                _enableCountDown.value = Event(true)
            } else {
                _verifyLoadMessage.value = Event(LoadMessage.Error(ErrorMessage(result.message,result.errorCode)))
            }
        }
    }

    fun verifyOldPhone(code: String){
        _verifyLoadMessage.value = Event(LoadMessage.Loading)

        viewModelScope.launch {
            val result = verifyPhoneDataUseCase(VerifyPhoneDataUseCase.VerifyPhoneRequest(false,userPhone,code))
            if(result is Resource.Success){
                // 验证旧手机验证码成功
                _verifyLoadMessage.value = Event(LoadMessage.Success)
            } else {
                _verifyLoadMessage.value = Event(LoadMessage.Error(ErrorMessage(result.message,result.errorCode)))
            }
        }
    }

    fun setVerifyCodeSuccess(success: Boolean){
        _verifyCodeSuccess.value = success
    }

    private fun getUserPhone(){
        viewModelScope.launch {
            val result = userPhoneDataUseCase(null)
            if(result is Resource.Success){
                userPhone = result.data!!
                _loadMessage.value = LoadMessage.NotLoading
            } else {
                _loadMessage.value = LoadMessage.Error(ErrorMessage(result.message,result.errorCode))
            }
        }
    }
}