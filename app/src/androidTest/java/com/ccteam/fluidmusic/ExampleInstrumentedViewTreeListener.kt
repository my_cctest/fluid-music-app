package com.ccteam.fluidmusic

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.ccteam.fluidmusic.utils.transToDateReciprocal
import com.orhanobut.logger.Logger

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedViewTreeListener {
    @Test
    fun useAppContext() {
        val time = "2021-03-01 20:07:12"
        val transTime = time.transToDateReciprocal(InstrumentationRegistry.getInstrumentation().targetContext)
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.ccteam.fluidmusic", transTime)
    }
}