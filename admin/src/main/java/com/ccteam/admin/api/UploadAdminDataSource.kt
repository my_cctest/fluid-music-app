package com.ccteam.admin.api

import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import retrofit2.http.GET

/**
 * @author Xiaoc
 * @since 2021/3/10
 *
 * 上传文件相关API
 */
interface UploadAdminDataSource {

    /**
     * 得到上传图片的Token上传凭证
     */
    @GET("base/file/public/voucher/image")
    suspend fun getImageToken(): ApiResult<ResultVO<String>>

    /**
     * 得到上传音频的Token上传凭证
     */
    @GET("base/file/public/voucher/music")
    suspend fun getMediaToken(): ApiResult<ResultVO<String>>
}