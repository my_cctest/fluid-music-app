package com.ccteam.admin.api

import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.banner.BannerDTO
import com.ccteam.model.banner.BannerNoIdDTO
import com.ccteam.model.banner.BannerVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import retrofit2.http.*

/**
 * @author Xiaoc
 * @since 2021/3/26
 *
 * Banner轮播图相关API
 */
interface BannerAdminDataSource {

    /**
     * 得到轮播图列表（分页）
     * @param token 管理员Token
     * @param pageDTO 分页DTO
     */
    @POST("admin/carousel-image/query")
    suspend fun getBannerList(
        @Header("FluidToken") token: String,
        @Body pageDTO: PageDTO
    ): ApiResult<ResultVO<PageVO<BannerVO>>>

    /**
     * 得到轮播图信息
     * @param token 管理员Token
     * @param id 轮播图ID
     */
    @GET("admin/carousel-image/{id}")
    suspend fun getBannerInfo(
        @Header("FluidToken") token: String,
        @Path("id")id: String): ApiResult<ResultVO<BannerVO>>

    /**
     * 修改轮播图信息
     * @param token 管理员Token
     * @param bannerDTO 轮播图信息DTO
     */
    @PUT("admin/carousel-image")
    suspend fun editBannerInfo(
        @Header("FluidToken") token: String,
        @Body bannerDTO: BannerDTO
    ): ApiResult<ResultVO<Nothing>>

    /**
     * 创建轮播图信息
     * @param token 管理员Token
     * @param bannerDTO 轮播图信息DTO
     */
    @POST("admin/carousel-image")
    suspend fun createBannerInfo(
        @Header("FluidToken") token: String,
        @Body bannerDTO: BannerNoIdDTO
    ): ApiResult<ResultVO<Nothing>>

    /**
     * 删除轮播图信息
     * @param token 管理员Token
     * @param id 轮播图ID
     */
    @DELETE("admin/carousel-image/{id}")
    suspend fun deleteBannerInfo(
        @Header("FluidToken") token: String,
        @Path("id")id: String): ApiResult<ResultVO<Nothing>>
}