package com.ccteam.admin.api

import com.ccteam.model.music.BitrateDTO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

/**
 * @author Xiaoc
 * @since 2021/3/3
 *
 * 音质音源接口API
 */
interface BitrateAdminDataSource {

    /**
     * 创建音源信息
     * 该接口接受一个List列表批量操作
     * @param token 管理员Token
     * @param bitrateList 音源信息List
     */
    @POST("admin/bitrate/list")
    suspend fun createBitrateList(
        @Header("FluidToken") token: String,
        @Body bitrateList: List<BitrateDTO>): ApiResult<ResultVO<Nothing>>
}