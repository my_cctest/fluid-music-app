package com.ccteam.admin.api

import com.ccteam.model.auth.AuthVO
import com.ccteam.model.user.AdminUserDTO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

/**
 * @author Xiaoc
 * @since 2021/4/6
 *
 * 基本管理内容接口
 */
interface BaseAdminDataSource {

    /**
     * 获得RSA公钥
     */
    @GET("base/rsa/public/public-key")
    suspend fun getRsaPublicKey(): ApiResult<ResultVO<String>>

    /**
     * 管理员登录
     * @param loginDTO 登录DTO
     */
    @POST("auth/admin/login")
    suspend fun loginAdmin(@Body loginDTO: AdminUserDTO): ApiResult<ResultVO<AuthVO>>

    /**
     * 管理员退出登录
     * @param token 管理员Token
     */
    @POST("auth/admin/logout")
    suspend fun logoutAdmin(@Header("FluidToken") token: String): ApiResult<ResultVO<Nothing>>
}