package com.ccteam.admin.api

import com.ccteam.model.music.MusicDTO
import com.ccteam.model.music.MusicDetailVO
import com.ccteam.model.music.MusicInfoNoIdDTO
import com.ccteam.model.music.MusicVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import retrofit2.http.*

/**
 * @author Xiaoc
 * @since 2021/3/3
 *
 * 歌曲管理相关接口
 */
interface MusicAdminDataSource {

    /**
     * 通过专辑ID获得歌曲信息
     * @param token 管理员Token
     * @param id 专辑ID
     */
    @GET("admin/song/query-album-id/{id}")
    suspend fun getMusicInfoByAlbumId(
        @Header("FluidToken") token: String,
        @Path("id") id: String): ApiResult<ResultVO<List<MusicDetailVO>>>

    /**
     * 编辑歌曲信息
     * @param token 管理员Token
     * @param musicDTO 歌曲信息DTO
     */
    @PUT("admin/song")
    suspend fun editMusicInfo(
        @Header("FluidToken") token: String,
        @Body musicDTO: MusicDTO
    ): ApiResult<ResultVO<Nothing>>

    /**
     * 新增歌曲信息
     * @param token 管理员Token
     * @param musicNoIdDTO 歌曲信息DTO
     */
    @POST("admin/song")
    suspend fun addMusicInfo(
        @Header("FluidToken") token: String,
        @Body musicNoIdDTO: MusicInfoNoIdDTO
    ): ApiResult<ResultVO<Nothing>>

    /**
     * 通过ID获取歌曲信息
     * @param token 管理员Token
     * @param id 歌曲ID
     */
    @GET("admin/song/{id}")
    suspend fun getMusicInfoById(
        @Header("FluidToken") token: String,
        @Path("id") id: String): ApiResult<ResultVO<MusicDetailVO>>

    /**
     * 删除歌曲信息
     * @param token 管理员Token
     * @param id 歌曲ID
     */
    @DELETE("admin/song/{id}")
    suspend fun deleteMusic(
        @Header("FluidToken") token: String,
        @Path("id") id: String): ApiResult<ResultVO<Nothing>>

}