package com.ccteam.admin.api

import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.album.AlbumDTO
import com.ccteam.model.album.AlbumInfoNoIdDTO
import com.ccteam.model.album.AlbumVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import retrofit2.http.*

/**
 * @author Xiaoc
 * @since 2021/3/1
 *
 * 专辑管理相关接口
 */
interface AlbumAdminDataSource {

    /**
     * 通过专辑ID查询专辑信息
     * @param token 管理员Token
     * @param id 专辑ID
     */
    @GET("admin/album/{id}")
    suspend fun getAlbumInfoById(@Header("FluidToken") token: String,
                                 @Path("id") id: String): ApiResult<ResultVO<AlbumVO>>

    /**
     * 分页查询专辑列表信息
     * @param token 管理员Token
     * @param pageDto 分页DTO
     */
    @POST("admin/album/query")
    suspend fun queryAlbumInfo(
        @Header("FluidToken") token: String,
        @Body pageDto: PageDTO
    ): ApiResult<ResultVO<PageVO<AlbumVO>>>

    /**
     * 修改专辑信息
     * @param token 管理员Token
     * @param albumInfoDto 专辑信息DTO
     */
    @PUT("admin/album")
    suspend fun editAlbumInfo(
        @Header("FluidToken") token: String,
        @Body albumInfoDto: AlbumDTO): ApiResult<ResultVO<Nothing>>

    /**
     * 添加专辑信息
     * @param token 管理员Token
     * @param albumInfoDto 专辑信息DTO
     */
    @POST("admin/album")
    suspend fun addAlbumInfo(
        @Header("FluidToken") token: String,
        @Body albumInfoDto: AlbumInfoNoIdDTO): ApiResult<ResultVO<Nothing>>

    /**
     * 删除专辑信息
     * @param token 管理员Token
     * @param id 专辑ID
     */
    @DELETE("admin/album/{id}")
    suspend fun deleteAlbumInfo(
        @Header("FluidToken") token: String,
        @Path("id") id: String): ApiResult<ResultVO<Nothing>>

}