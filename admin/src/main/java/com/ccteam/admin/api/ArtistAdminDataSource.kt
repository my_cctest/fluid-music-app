package com.ccteam.admin.api

import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.artist.ArtistInfoDTO
import com.ccteam.model.artist.ArtistInfoNoIdDTO
import com.ccteam.model.artist.ArtistVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import retrofit2.http.*

/**
 * @author Xiaoc
 * @since 2021/2/5
 *
 * 歌手管理相关接口
 **/
interface ArtistAdminDataSource {

    /**
     * 修改歌手信息
     * @param token 管理员Token
     * @param artistInfoNoIdDto 歌手信息数据
     */
    @PUT("admin/artist")
    suspend fun editArtistInfo(
        @Header("FluidToken") token: String,
        @Body artistInfoNoIdDto: ArtistInfoDTO): ApiResult<ResultVO<Nothing>>

    /**
     * 添加歌手信息
     * @param token 管理员Token
     * @param artistInfoNoIdDto 歌手信息数据
     */
    @POST("admin/artist")
    suspend fun addArtistInfo(
        @Header("FluidToken") token: String,
        @Body artistInfoNoIdDto: ArtistInfoNoIdDTO): ApiResult<ResultVO<Nothing>>

    /**
     * 根据ID查询对应歌手
     * @param token 管理员Token
     * @param artistId 歌手Id
     */
    @GET("admin/artist/{id}")
    suspend fun getArtistInfoById(
        @Header("FluidToken") token: String,
        @Path("id") artistId: String): ApiResult<ResultVO<ArtistVO>>

    /**
     * 根据ID删除对应歌手
     * @param token 管理员Token
     * @param artistId 歌手Id
     */
    @DELETE("admin/artist/{id}")
    suspend fun deleteArtistInfoById(
        @Header("FluidToken") token: String,
        @Path("id") artistId: String): ApiResult<ResultVO<Nothing>>

    /**
     * 查询歌手信息（分页）
     * @param token 管理员Token
     * @param pageDto 分页信息
     */
    @POST("admin/artist/query")
    suspend fun queryArtistInfo(
        @Header("FluidToken") token: String,
        @Body pageDto: PageDTO
    ): ApiResult<ResultVO<PageVO<ArtistVO>>>
}