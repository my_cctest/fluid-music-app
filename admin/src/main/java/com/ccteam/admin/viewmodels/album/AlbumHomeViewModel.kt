package com.ccteam.admin.viewmodels.album

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.ccteam.admin.api.AlbumAdminDataSource
import com.ccteam.admin.core.AdminCore
import com.ccteam.admin.domain.AlbumPagingSource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/1
 */
@HiltViewModel
class AlbumHomeViewModel @Inject constructor(
    private val adminCore: AdminCore,
    private val albumAdminDataSource: AlbumAdminDataSource
): ViewModel() {

    val albumListFlow = Pager(
        PagingConfig(pageSize = 15,initialLoadSize = 15)
    ){
        AlbumPagingSource(adminCore,albumAdminDataSource)
    }.flow
        .cachedIn(viewModelScope)
        .flowOn(Dispatchers.IO)
}