package com.ccteam.admin.viewmodels.artist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.ccteam.admin.api.ArtistAdminDataSource
import com.ccteam.admin.core.AdminCore
import com.ccteam.admin.domain.ArtistPagingSource
import com.ccteam.admin.utils.Event
import com.orhanobut.logger.Logger
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/2/5
 **/
@HiltViewModel
class ArtistHomeViewModel @Inject constructor(
    private val adminCore: AdminCore,
    private val artistAdminDataSource: ArtistAdminDataSource
): ViewModel() {

    // 初始化歌手列表页的分页库
    val artistListFlow = Pager(
        PagingConfig(pageSize = 15,initialLoadSize = 15)
    ){
        ArtistPagingSource(adminCore,artistAdminDataSource)
    }.flow
        .cachedIn(viewModelScope)
        .flowOn(Dispatchers.IO)
}