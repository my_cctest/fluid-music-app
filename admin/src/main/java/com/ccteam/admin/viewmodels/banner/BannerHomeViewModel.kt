package com.ccteam.admin.viewmodels.banner

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.ccteam.admin.api.BannerAdminDataSource
import com.ccteam.admin.core.AdminCore
import com.ccteam.admin.domain.BannerPagingSource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/26
 */
@HiltViewModel
class BannerHomeViewModel @Inject constructor(
    private val adminCore: AdminCore,
    private val bannerAdminDataSource: BannerAdminDataSource
): ViewModel() {

    val bannerListFlow = Pager(
        PagingConfig(pageSize = 15,initialLoadSize = 15)
    ){
        BannerPagingSource(adminCore,bannerAdminDataSource)
    }.flow
        .cachedIn(viewModelScope)
        .flowOn(Dispatchers.IO)
}