package com.ccteam.admin.utils

/**
 * @author Xiaoc
 * @since 2021/2/18
 *
 * UI错误信息展示
 * @param tag 错误标志
 * @param description 错误描述，可为空
 */
data class ErrorMessage(
    val tag: Int,
    val description: String? = null
){
    companion object{
        /**
         * 错误信息显示在 snack 上的类型
         */
        const val ERROR_SNACK_TYPE = 0x0010
    }
}

/**
 * 加载信息
 * 由于错误信息和加载信息需要同时操作UI
 * 所以我们将其设定为加载信息，方便进行判断
 */
sealed class LoadMessage{

    /**
     * 当前为处理成功状态
     */
    object Success : LoadMessage()


    /**
     * 当前状态为空，表示没有数据展示，但是已经加载完成
     */
    object Empty: LoadMessage()

    /**
     * 当前为未加载状态
     */
    object NotLoading : LoadMessage()

    /**
     * 当前为正在加载状态
     */
    object Loading : LoadMessage()

    /**
     * 当前为错误状态
     * @param errorMessage 错误数据信息
     */
    class Error(
        val errorMessage: ErrorMessage
    ) : LoadMessage()
}