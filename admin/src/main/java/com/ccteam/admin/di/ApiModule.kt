package com.ccteam.admin.di

import com.ccteam.admin.api.*
import com.ccteam.network.retrofit
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * @author Xiaoc
 * @since 2021/2/5
 *
 * 提供Retrofit Api代理类的依赖注入方法
 * 基于 Hilt
 **/
@Module
@InstallIn(SingletonComponent::class)
class ApiModule {

    /**
     * 注入 [ArtistAdminDataSource] 接口
     */
    @Provides
    @Singleton
    fun provideArtistAdminApiDataSource(): ArtistAdminDataSource = retrofit.create(ArtistAdminDataSource::class.java)

    /**
     * 注入 [AlbumAdminDataSource] 接口
     */
    @Provides
    @Singleton
    fun provideAlbumAdminApiDataSource(): AlbumAdminDataSource = retrofit.create(AlbumAdminDataSource::class.java)

    /**
     * 注入 [MusicAdminDataSource] 接口
     */
    @Provides
    @Singleton
    fun provideMusicAdminApiDataSource(): MusicAdminDataSource = retrofit.create(MusicAdminDataSource::class.java)

    /**
     * 注入 [BitrateAdminDataSource] 接口
     */
    @Provides
    @Singleton
    fun provideBitrateApiDataSource(): BitrateAdminDataSource = retrofit.create(BitrateAdminDataSource::class.java)

    /**
     * 注入 [UploadAdminDataSource] 接口
     */
    @Provides
    @Singleton
    fun provideUploadAdminApiDataSource(): UploadAdminDataSource = retrofit.create(UploadAdminDataSource::class.java)

    /**
     * 注入 [BannerAdminDataSource] 接口
     */
    @Provides
    @Singleton
    fun provideBannerAdminApiDataSource(): BannerAdminDataSource = retrofit.create(BannerAdminDataSource::class.java)

    /**
     * 注入 [BaseAdminDataSource] 接口
     */
    @Provides
    @Singleton
    fun provideBaseAdminApiDataSource(): BaseAdminDataSource = retrofit.create(BaseAdminDataSource::class.java)
}