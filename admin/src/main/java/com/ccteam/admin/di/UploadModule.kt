package com.ccteam.admin.di

import com.qiniu.android.common.FixedZone
import com.qiniu.android.storage.Configuration
import com.qiniu.android.storage.UploadManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * @author Xiaoc
 * @since 2021/3/9
 */
private val config = Configuration.Builder()
    .connectTimeout(90)
    .useHttps(false)
    .zone(FixedZone.zone2)
    .build()

@Module
@InstallIn(SingletonComponent::class)
class UploadModule {

    @Singleton
    @Provides
    fun provideUploadManager(): UploadManager{
        return UploadManager(config)
    }
}