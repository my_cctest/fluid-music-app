package com.ccteam.admin.core

import android.annotation.SuppressLint
import android.content.Context

/**
 * 添加信息类型
 */
const val ADD_TYPE = 0
/**
 * 编辑信息类型
 */
const val EDIT_TYPE = 1