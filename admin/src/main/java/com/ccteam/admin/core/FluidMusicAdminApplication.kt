package com.ccteam.admin.core

import android.app.Application
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import dagger.hilt.android.HiltAndroidApp

/**
 * @author Xiaoc
 * @since 2021/2/5
 **/
@HiltAndroidApp
class FluidMusicAdminApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        // 初始化日志器
        Logger.addLogAdapter(AndroidLogAdapter())
    }
}