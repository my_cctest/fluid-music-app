package com.ccteam.admin.repositories

import com.ccteam.admin.api.MusicAdminDataSource
import com.ccteam.admin.core.AdminCore
import com.ccteam.admin.vo.Resource
import com.ccteam.model.music.MusicDTO
import com.ccteam.model.music.MusicDetailVO
import com.ccteam.model.music.MusicInfoNoIdDTO
import com.ccteam.model.music.MusicVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Xiaoc
 * @since 2021/3/3
 */
@Singleton
class MusicAdminRepository @Inject constructor(
    private val adminCore: AdminCore,
    private val musicDataSource: MusicAdminDataSource
) {

    /**
     * 得到专辑的音乐列表
     */
    suspend fun getAlbumMusicList(albumId: String): Resource<List<MusicDetailVO>>{
        return withContext(Dispatchers.IO){
            object: BaseSuspendResource<List<MusicDetailVO>, ResultVO<List<MusicDetailVO>>>() {
                override suspend fun handleNetwork(): ApiResult<ResultVO<List<MusicDetailVO>>> {
                    return musicDataSource.getMusicInfoByAlbumId(
                        adminCore.userInfo.value.adminToken,albumId)
                }

                override fun processResponse(response: ApiResult.Success<ResultVO<List<MusicDetailVO>>>): List<MusicDetailVO> {
                    return response.data.data ?: emptyList()
                }

            }.asResource()
        }
    }

    /**
     * 得到专辑的音乐列表
     */
    suspend fun getMusicInfo(musicId: String): Resource<MusicDetailVO>{
        return withContext(Dispatchers.IO){
            object: BaseSuspendResource<MusicDetailVO, ResultVO<MusicDetailVO>>() {
                override suspend fun handleNetwork(): ApiResult<ResultVO<MusicDetailVO>> {
                    return musicDataSource.getMusicInfoById(
                        adminCore.userInfo.value.adminToken,musicId)
                }

                override fun processResponse(response: ApiResult.Success<ResultVO<MusicDetailVO>>): MusicDetailVO {
                    return response.data.data!!
                }

            }.asResource()
        }
    }

    /**
     * 修改音乐信息
     */
    suspend fun editMusicInfo(musicDTO: MusicDTO): Resource<Nothing?>{
        return withContext(Dispatchers.IO){
            object: BaseSuspendResource<Nothing?, ResultVO<Nothing>>() {
                override suspend fun handleNetwork(): ApiResult<ResultVO<Nothing>> {
                    return musicDataSource.editMusicInfo(
                        adminCore.userInfo.value.adminToken,musicDTO)
                }

                override fun processResponse(response: ApiResult.Success<ResultVO<Nothing>>): Nothing? {
                    return null
                }

            }.asResource()
        }
    }

    /**
     * 添加音乐信息
     */
    suspend fun addMusicInfo(musicDTO: MusicInfoNoIdDTO): Resource<Nothing?>{
        return withContext(Dispatchers.IO){
            object: BaseSuspendResource<Nothing?, ResultVO<Nothing>>() {
                override suspend fun handleNetwork(): ApiResult<ResultVO<Nothing>> {
                    return musicDataSource.addMusicInfo(
                        adminCore.userInfo.value.adminToken,musicDTO)
                }

                override fun processResponse(response: ApiResult.Success<ResultVO<Nothing>>): Nothing? {
                    return null
                }

            }.asResource()
        }
    }

    /**
     * 删除音乐信息
     */
    suspend fun deleteMusicInfo(musicId: String): Resource<Nothing?>{
        return withContext(Dispatchers.IO){
            object: BaseSuspendResource<Nothing?, ResultVO<Nothing>>() {
                override suspend fun handleNetwork(): ApiResult<ResultVO<Nothing>> {
                    return musicDataSource.deleteMusic(
                        adminCore.userInfo.value.adminToken,musicId)
                }

                override fun processResponse(response: ApiResult.Success<ResultVO<Nothing>>): Nothing? {
                    return null
                }

            }.asResource()
        }
    }

}