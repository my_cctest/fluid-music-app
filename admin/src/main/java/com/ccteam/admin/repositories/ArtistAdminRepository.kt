package com.ccteam.admin.repositories

import com.ccteam.admin.api.ArtistAdminDataSource
import com.ccteam.admin.core.AdminCore
import com.ccteam.admin.vo.Resource
import com.ccteam.model.artist.ArtistInfoDTO
import com.ccteam.model.artist.ArtistInfoNoIdDTO
import com.ccteam.model.artist.ArtistVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Xiaoc
 * @since 2021/2/5
 **/
@Singleton
class ArtistAdminRepository @Inject constructor(
    private val adminCore: AdminCore,
    private val artistAdminDataSource: ArtistAdminDataSource
) {

    /**
     * 添加歌手信息
     * @param artistInfoNoId 不含ID的歌手信息DTO
     */
    suspend fun addArtist(artistInfoNoId: ArtistInfoNoIdDTO): Resource<Nothing?>{
        return withContext(Dispatchers.IO){
            return@withContext object: BaseSuspendResource<Nothing?,ResultVO<Nothing>>(){

                override suspend fun handleNetwork(): ApiResult<ResultVO<Nothing>> {
                    return artistAdminDataSource.addArtistInfo(
                        adminCore.userInfo.value.adminToken,artistInfoNoId)
                }

                override fun processResponse(response: ApiResult.Success<ResultVO<Nothing>>): Nothing? {
                    return null
                }

            }.asResource()
        }
    }

    /**
     * 编辑歌手信息
     * @param artistInfo 歌手信息DTO
     */
    suspend fun editArtist(artistInfo: ArtistInfoDTO): Resource<Nothing?>{
        return withContext(Dispatchers.IO){
            return@withContext object: BaseSuspendResource<Nothing?,ResultVO<Nothing>>(){
                override suspend fun handleNetwork(): ApiResult<ResultVO<Nothing>> {
                    return artistAdminDataSource.editArtistInfo(
                        adminCore.userInfo.value.adminToken,artistInfo)
                }

                override fun processResponse(response: ApiResult.Success<ResultVO<Nothing>>): Nothing? {
                    return null
                }
            }.asResource()
        }
    }

    /**
     * 删除歌手信息
     * @param id 歌手ID
     */
    suspend fun deleteArtist(id: String): Resource<Nothing?>{
        return withContext(Dispatchers.IO){
            return@withContext object: BaseSuspendResource<Nothing?,ResultVO<Nothing>>(){
                override suspend fun handleNetwork(): ApiResult<ResultVO<Nothing>> {
                    return artistAdminDataSource.deleteArtistInfoById(
                        adminCore.userInfo.value.adminToken,id)
                }

                override fun processResponse(response: ApiResult.Success<ResultVO<Nothing>>): Nothing? {
                    return null
                }

            }.asResource()
        }
    }

    /**
     * 删除歌手信息
     * @param id 歌手ID
     */
    suspend fun getArtist(id: String): Resource<ArtistVO>{
        return withContext(Dispatchers.IO){
            return@withContext object: BaseSuspendResource<ArtistVO,ResultVO<ArtistVO>>(){
                override suspend fun handleNetwork(): ApiResult<ResultVO<ArtistVO>> {
                    return artistAdminDataSource.getArtistInfoById(
                        adminCore.userInfo.value.adminToken,id)
                }

                override fun processResponse(response: ApiResult.Success<ResultVO<ArtistVO>>): ArtistVO {
                    return response.data.data!!
                }

            }.asResource()
        }
    }

}