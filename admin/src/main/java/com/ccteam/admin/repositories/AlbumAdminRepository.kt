package com.ccteam.admin.repositories

import com.ccteam.admin.api.AlbumAdminDataSource
import com.ccteam.admin.core.AdminCore
import com.ccteam.admin.vo.Resource
import com.ccteam.model.album.AlbumDTO
import com.ccteam.model.album.AlbumInfoNoIdDTO
import com.ccteam.model.album.AlbumVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Xiaoc
 * @since 2021/3/1
 *
 * 专辑管理仓库层
 */
@Singleton
class AlbumAdminRepository @Inject constructor(
    private val adminCore: AdminCore,
    private val albumAdminDataSource: AlbumAdminDataSource
) {

    suspend fun editAlbumInfo(albumInfo: AlbumDTO): Resource<Nothing?>{
        return withContext(Dispatchers.IO){
            return@withContext object: BaseSuspendResource<Nothing?,ResultVO<Nothing>>(){
                override suspend fun handleNetwork(): ApiResult<ResultVO<Nothing>> {
                    return albumAdminDataSource.editAlbumInfo(adminCore.userInfo.value.adminToken,albumInfo)
                }

                override fun processResponse(response: ApiResult.Success<ResultVO<Nothing>>): Nothing? {
                    return null
                }

            }.asResource()
        }

    }

    suspend fun addAlbumInfo(albumInfo: AlbumInfoNoIdDTO): Resource<Nothing?>{
        return withContext(Dispatchers.IO){
            return@withContext object: BaseSuspendResource<Nothing?,ResultVO<Nothing>>(){
                override suspend fun handleNetwork(): ApiResult<ResultVO<Nothing>> {
                    return albumAdminDataSource.addAlbumInfo(
                        adminCore.userInfo.value.adminToken,albumInfo)
                }

                override fun processResponse(response: ApiResult.Success<ResultVO<Nothing>>): Nothing? {
                    return null
                }
            }.asResource()
        }

    }

    suspend fun deleteAlbumInfo(id: String): Resource<Nothing?>{
        return withContext(Dispatchers.IO){
            return@withContext object: BaseSuspendResource<Nothing?,ResultVO<Nothing>>(){
                override suspend fun handleNetwork(): ApiResult<ResultVO<Nothing>> {
                    return albumAdminDataSource.deleteAlbumInfo(
                        adminCore.userInfo.value.adminToken,id)
                }

                override fun processResponse(response: ApiResult.Success<ResultVO<Nothing>>): Nothing? {
                    return null
                }
            }.asResource()
        }

    }

    suspend fun getAlbumInfo(id: String): Resource<AlbumVO>{
        return withContext(Dispatchers.IO){
            return@withContext object: BaseSuspendResource<AlbumVO,ResultVO<AlbumVO>>(){
                override suspend fun handleNetwork(): ApiResult<ResultVO<AlbumVO>> {
                    return albumAdminDataSource.getAlbumInfoById(
                        adminCore.userInfo.value.adminToken,id)
                }

                override fun processResponse(response: ApiResult.Success<ResultVO<AlbumVO>>): AlbumVO {
                    return response.data.data!!
                }
            }.asResource()
        }

    }
}