package com.ccteam.admin.repositories

import com.ccteam.admin.api.BannerAdminDataSource
import com.ccteam.admin.core.AdminCore
import com.ccteam.admin.vo.Resource
import com.ccteam.model.banner.BannerDTO
import com.ccteam.model.banner.BannerNoIdDTO
import com.ccteam.model.banner.BannerVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Xiaoc
 * @since 2021/3/27
 */
@Singleton
class BannerAdminRepository @Inject constructor(
    private val adminCore: AdminCore,
    private val bannerAdminDataSource: BannerAdminDataSource
) {

    /**
     * 查看轮播图信息
     * @param id 轮播图ID
     */
    suspend fun getBannerInfo(id: String): Resource<BannerVO> {
        return withContext(Dispatchers.IO){
            return@withContext object: BaseSuspendResource<BannerVO, ResultVO<BannerVO>>(){

                override suspend fun handleNetwork(): ApiResult<ResultVO<BannerVO>> {
                    return bannerAdminDataSource.getBannerInfo(
                        adminCore.userInfo.value.adminToken,id)
                }

                override fun processResponse(response: ApiResult.Success<ResultVO<BannerVO>>): BannerVO {
                    return response.data.data!!
                }

            }.asResource()
        }
    }

    /**
     * 编辑轮播图信息
     * @param bannerDTO 轮播图DTO
     */
    suspend fun editBannerInfo(bannerDTO: BannerDTO): Resource<Nothing?> {
        return withContext(Dispatchers.IO){
            return@withContext object: BaseSuspendResource<Nothing?, ResultVO<Nothing>>(){

                override suspend fun handleNetwork(): ApiResult<ResultVO<Nothing>> {
                    return bannerAdminDataSource.editBannerInfo(
                        adminCore.userInfo.value.adminToken,bannerDTO)
                }

                override fun processResponse(response: ApiResult.Success<ResultVO<Nothing>>): Nothing? {
                    return null
                }

            }.asResource()
        }
    }

    /**
     * 创建轮播图信息
     * @param bannerDTO 轮播图DTO
     */
    suspend fun createBannerInfo(bannerDTO: BannerNoIdDTO): Resource<Nothing?> {
        return withContext(Dispatchers.IO){
            return@withContext object: BaseSuspendResource<Nothing?, ResultVO<Nothing>>(){

                override suspend fun handleNetwork(): ApiResult<ResultVO<Nothing>> {
                    return bannerAdminDataSource.createBannerInfo(
                        adminCore.userInfo.value.adminToken,bannerDTO)
                }

                override fun processResponse(response: ApiResult.Success<ResultVO<Nothing>>): Nothing? {
                    return null
                }

            }.asResource()
        }
    }

    /**
     * 删除轮播图信息
     * @param id 轮播图ID
     */
    suspend fun deleteBannerInfo(id: String): Resource<Nothing?> {
        return withContext(Dispatchers.IO){
            return@withContext object: BaseSuspendResource<Nothing?, ResultVO<Nothing>>(){

                override suspend fun handleNetwork(): ApiResult<ResultVO<Nothing>> {
                    return bannerAdminDataSource.deleteBannerInfo(
                        adminCore.userInfo.value.adminToken,id)
                }

                override fun processResponse(response: ApiResult.Success<ResultVO<Nothing>>): Nothing? {
                    return null
                }

            }.asResource()
        }
    }
}