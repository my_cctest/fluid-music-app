package com.ccteam.admin.repositories

import com.ccteam.admin.api.BitrateAdminDataSource
import com.ccteam.admin.core.AdminCore
import com.ccteam.admin.vo.Resource
import com.ccteam.model.music.BitrateDTO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Xiaoc
 * @since 2021/3/12
 */
@Singleton
class BitrateAdminRepository @Inject constructor(
    private val adminCore: AdminCore,
    private val bitrateDataSource: BitrateAdminDataSource
) {

    /**
     * 创建音源内容
     * @param id 音源ID
     */
    suspend fun createBitrateList(bitrateList: List<BitrateDTO>): Resource<Nothing?>{
        return withContext(Dispatchers.IO){
            object: BaseSuspendResource<Nothing?, ResultVO<Nothing>>() {
                override suspend fun handleNetwork(): ApiResult<ResultVO<Nothing>> {
                    return bitrateDataSource.createBitrateList(
                        adminCore.userInfo.value.adminToken,bitrateList)
                }

                override fun processResponse(response: ApiResult.Success<ResultVO<Nothing>>): Nothing? {
                    return null
                }

            }.asResource()
        }
    }
}