package com.ccteam.admin.repositories

import com.ccteam.admin.api.UploadAdminDataSource
import com.ccteam.admin.vo.Resource
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Xiaoc
 * @since 2021/3/10
 *
 * 文件上传仓库层
 */
@Singleton
class UploadAdminRepository @Inject constructor(
    private val uploadDataSource: UploadAdminDataSource
) {

    /**
     * 得到图片上传Token
     */
    suspend fun getImageToken(): Resource<String>{
        return withContext(Dispatchers.IO){
            return@withContext object: BaseSuspendResource<String,ResultVO<String>>(){
                override suspend fun handleNetwork(): ApiResult<ResultVO<String>> {
                    return uploadDataSource.getImageToken()
                }

                override fun processResponse(response: ApiResult.Success<ResultVO<String>>): String {
                    return response.data.data ?: ""
                }

            }.asResource()
        }
    }

    /**
     * 得到音频上传Token
     */
    suspend fun getMediaToken(): Resource<String>{
        return withContext(Dispatchers.IO){
            return@withContext object: BaseSuspendResource<String,ResultVO<String>>(){
                override suspend fun handleNetwork(): ApiResult<ResultVO<String>> {
                    return uploadDataSource.getMediaToken()
                }

                override fun processResponse(response: ApiResult.Success<ResultVO<String>>): String {
                    return response.data.data ?: ""
                }

            }.asResource()
        }
    }

}