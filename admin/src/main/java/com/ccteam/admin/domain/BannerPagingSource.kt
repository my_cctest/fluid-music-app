package com.ccteam.admin.domain

import com.ccteam.admin.api.BannerAdminDataSource
import com.ccteam.admin.core.AdminCore
import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.banner.BannerVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/26
 *
 * 轮播图分页数据源
 *
 * 负责分页查询轮播图列表
 */
class BannerPagingSource @Inject constructor(
    private val adminCore: AdminCore,
    private val bannerAdminDataSource: BannerAdminDataSource
): BasePagingSource<BannerVO>() {

    override suspend fun loadData(pageInfo: PageDTO): ApiResult<ResultVO<PageVO<BannerVO>>> {
        return bannerAdminDataSource.getBannerList(adminCore.userInfo.value.adminToken,pageInfo)
    }
}