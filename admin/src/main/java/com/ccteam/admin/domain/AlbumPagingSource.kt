package com.ccteam.admin.domain

import com.ccteam.admin.api.AlbumAdminDataSource
import com.ccteam.admin.core.AdminCore
import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.album.AlbumVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/3/1
 *
 * 专辑列表分页数据源
 *
 * 负责分页查询专辑列表
 */
class AlbumPagingSource @Inject constructor(
    private val adminCore: AdminCore,
    private val albumAdminDataSource: AlbumAdminDataSource
): BasePagingSource<AlbumVO>() {

    override suspend fun loadData(pageInfo: PageDTO): ApiResult<ResultVO<PageVO<AlbumVO>>> {
        return albumAdminDataSource.queryAlbumInfo(adminCore.userInfo.value.adminToken,pageInfo)
    }
}