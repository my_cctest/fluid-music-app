package com.ccteam.admin.domain

import com.ccteam.admin.api.ArtistAdminDataSource
import com.ccteam.admin.core.AdminCore
import com.ccteam.model.PageDTO
import com.ccteam.model.PageVO
import com.ccteam.model.artist.ArtistVO
import com.ccteam.network.ApiResult
import com.ccteam.network.ResultVO
import javax.inject.Inject

/**
 * @author Xiaoc
 * @since 2021/2/5
 *
 * 歌手信息分页数据源
 *
 * 负责分页查询歌手列表
 **/
class ArtistPagingSource @Inject constructor(
    private val adminCore: AdminCore,
    private val artistAdminDataSource: ArtistAdminDataSource
): BasePagingSource<ArtistVO>() {

    override suspend fun loadData(pageInfo: PageDTO): ApiResult<ResultVO<PageVO<ArtistVO>>> {
        return artistAdminDataSource.queryArtistInfo(adminCore.userInfo.value.adminToken,pageInfo)
    }
}