package com.ccteam.admin

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.ccteam.admin.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}

const val REFRESH_REQUEST_KEY = "Refresh_request_key"
const val ARTIST_SEPARATOR = "/"