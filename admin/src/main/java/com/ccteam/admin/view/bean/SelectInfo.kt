package com.ccteam.admin.view.bean

import android.os.Parcelable
import androidx.recyclerview.widget.DiffUtil
import kotlinx.parcelize.Parcelize

/**
 * @author Xiaoc
 * @since 2021/3/26
 *
 * 选择内容返回结果用到的Bean
 *
 * @param selectId 选择内容的ID
 * @param selectTitle 选择内容的介绍
 */
@Parcelize
data class SelectInfoResult(
    val selectId: String,
    val selectTitle: String
): Parcelable{
    companion object{
        val diffCallback = object: DiffUtil.ItemCallback<SelectInfoResult>(){
            override fun areItemsTheSame(oldItem: SelectInfoResult, newItem: SelectInfoResult): Boolean = oldItem.selectId == newItem.selectId

            override fun areContentsTheSame(oldItem: SelectInfoResult, newItem: SelectInfoResult): Boolean = newItem.selectId == oldItem.selectId

        }
    }
}

/**
 * 选择内容显示所用的通用Bean
 * @param id 对应ID
 * @param title 对应内容标题
 * @param imgUrl 对应内容图片地址
 */
data class SelectInfo(
    val id: String,
    val title: String,
    val imgUrl: String
){
    companion object{
        val diffCallback = object: DiffUtil.ItemCallback<SelectInfo>(){
            override fun areItemsTheSame(oldItem: SelectInfo, newItem: SelectInfo): Boolean = oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: SelectInfo, newItem: SelectInfo): Boolean = newItem.id == oldItem.id

        }
    }
}