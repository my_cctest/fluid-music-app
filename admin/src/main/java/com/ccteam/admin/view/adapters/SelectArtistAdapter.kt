package com.ccteam.admin.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ccteam.admin.databinding.SelectArtistItemBinding
import com.ccteam.admin.view.bean.SelectInfoResult

/**
 * @author Xiaoc
 * @since 2021/4/20
 *
 * 选择歌手的列表显示适配器
 * [com.ccteam.admin.view.fragments.music.MusicAddFragment]等选择歌手要显示的列表适用
 */
class SelectArtistAdapter(
    private val callback: SelectArtistItemClickCallback
): ListAdapter<SelectInfoResult,
        SelectArtistAdapter.SelectArtistViewHolder>(SelectInfoResult.diffCallback){

    inner class SelectArtistViewHolder(
        val binding: SelectArtistItemBinding
    ): RecyclerView.ViewHolder(binding.root){
        var item: SelectInfoResult? = null

        init {
            binding.btnDeleteSelectItem.setOnClickListener {
                item?.let {
                    callback.deleteClick(it)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectArtistViewHolder {
        return SelectArtistViewHolder(SelectArtistItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: SelectArtistViewHolder, position: Int) {
        getItem(position)?.let { item ->
            holder.item = item
            holder.binding.tvArtistName.text = item.selectTitle
        }
    }
}

interface SelectArtistItemClickCallback{
    fun deleteClick(item: SelectInfoResult)
}