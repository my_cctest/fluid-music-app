package com.ccteam.admin.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.ccteam.admin.R
import com.ccteam.admin.databinding.FragmentMainBinding
import com.ccteam.admin.utils.EventObserver
import com.ccteam.admin.utils.LoadMessage
import com.ccteam.admin.viewmodels.MainActivityViewModel
import com.google.android.material.button.MaterialButton
import dagger.hilt.android.AndroidEntryPoint

/**
 * @author Xiaoc
 * @since 2021/4/26
 */
@AndroidEntryPoint
class MainFragment: Fragment(){

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    private val mainActivityViewModel by viewModels<MainActivityViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMainBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // 设置抽屉布局与Navigation的结合，这里由于MainFragment内嵌一个Fragment容器，所以需要得到子Fragment才能进行设置
        val navController = if(!childFragmentManager.fragments.isNullOrEmpty()){
            childFragmentManager.fragments[0]?.findNavController() ?: findNavController()
        } else {
            findNavController()
        }
        val appBarConfiguration = AppBarConfiguration(navController.graph,binding.drawerLayout)
        NavigationUI.setupWithNavController(binding.toolbarMain,navController,appBarConfiguration)
        binding.drawerMain.setupWithNavController(navController)

        mainActivityViewModel.adminName.observe(viewLifecycleOwner, {
            binding.drawerMain.getHeaderView(0).findViewById<TextView>(R.id.tv_admin_name).text = it
        })

        mainActivityViewModel.logoutLoadMessage.observe(viewLifecycleOwner, EventObserver{
            if(it is LoadMessage.Success){
                findNavController().navigate(MainFragmentDirections.actionMainFragmentToLoginFragment())
            } else if(it is LoadMessage.Error){
                Toast.makeText(requireContext(),getString(R.string.description_logout_fail), Toast.LENGTH_SHORT).show()
            }
        })

        binding.drawerMain.getHeaderView(0).findViewById<MaterialButton>(R.id.btn_logout).setOnClickListener {
            if(mainActivityViewModel.logoutLoadMessage.value?.peekContent() == LoadMessage.Loading){
                return@setOnClickListener
            }
            mainActivityViewModel.logout()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()

        _binding = null
    }
}