package com.ccteam.admin.view.fragments.common

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.ccteam.admin.R
import com.ccteam.admin.databinding.FragmentUploadBinding
import com.ccteam.admin.utils.EventObserver
import com.ccteam.admin.viewmodels.common.UploadViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UploadFragment : Fragment() {

    private var _binding: FragmentUploadBinding? = null
    private val binding get() = _binding!!

    private val uploadViewModel by viewModels<UploadViewModel>()

    companion object{
        const val UPLOAD_REQUEST_KEY = "upload_request"

        const val UPLOAD_URL_RESULT ="upload_url_result"

        /**
         * 上传图片类型
         */
        const val PHOTO_UPDATE_TYPE = 4
    }

//    private val args: UploadFragmentArgs by navArgs()

    /**
     * 选择图片的内容
     * 选择完手机中的图片后，将其存储到ViewModel中并进行对应UI更新
     */
    private val selectImage = registerForActivityResult(ActivityResultContracts.GetContent()) { uri ->
        uri?.let {
            uploadViewModel.setSelectUri(it)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentUploadBinding.inflate(inflater,container,false)
//        if(args.uploadType == PHOTO_UPDATE_TYPE){
//            binding.btnSelect.text = getString(R.string.description_select_photo)
//        } else {
//            binding.btnSelect.text = getString(R.string.description_select_media)
//        }
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.btnSelect.setOnClickListener {
//            if(args.uploadType == PHOTO_UPDATE_TYPE){
//                selectImage.launch("image/*")
//            } else {
//                selectImage.launch("audio/*")
//            }
        }

        binding.btnUpload.setOnClickListener {

        }

        uploadViewModel.uploading.observe(viewLifecycleOwner, { uploading ->
            binding.btnUpload.isEnabled = !uploading
            binding.btnSelect.isEnabled = !uploading
            binding.groupUploadProgressInfo.isVisible = uploading
            if(uploading){
                binding.progressLoading.show()
            } else {
                binding.progressLoading.hide()
            }
        })

        uploadViewModel.selectUri.observe(viewLifecycleOwner, {
            updateUI(it)
        })

        uploadViewModel.uploadProgress.observe(viewLifecycleOwner, {
            binding.progressLoading.setProgress(it,true)
        })

        uploadViewModel.errorMessage.observe(viewLifecycleOwner, EventObserver {
            Snackbar.make(
                binding.root,
                it.description.toString(),
                Snackbar.LENGTH_SHORT
            ).setAnchorView(binding.btnUpload).show()
        })

        uploadViewModel.isSuccess.observe(viewLifecycleOwner, EventObserver {
            if(it.success){
                Snackbar.make(
                    binding.root,
                    getString(R.string.description_success),
                    Snackbar.LENGTH_SHORT
                ).setAnchorView(binding.btnUpload).show()
                setFragmentResult(UPLOAD_REQUEST_KEY, bundleOf(UPLOAD_URL_RESULT to it.result))
                findNavController().navigateUp()
            }
        })

    }

    private fun updateUI(uri: Uri){
//        if(args.uploadType == PHOTO_UPDATE_TYPE){
//            Glide.with(binding.ivUploadPreview)
//                .load(uri)
//                .into(binding.ivUploadPreview)
//        }
//        if(uri == Uri.EMPTY){
//            binding.tvUri.text = getString(R.string.description_file_uri,"无")
//        } else {
//            binding.tvUri.text = getString(R.string.description_file_uri,uri)
//        }

    }

    override fun onDestroyView() {
        super.onDestroyView()

        _binding = null
    }
}