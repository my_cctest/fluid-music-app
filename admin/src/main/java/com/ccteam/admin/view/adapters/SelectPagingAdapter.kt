package com.ccteam.admin.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ccteam.admin.R
import com.ccteam.admin.databinding.AlbumSelectArtistItemBinding
import com.ccteam.admin.view.bean.SelectInfo

/**
 * @author Xiaoc
 * @since 2021/3/26
 */
/**
 * 基于分页 [PagingDataAdapter] 的RecyclerView 适配器
 */
class SelectPagingAdapter(
    val callback: SelectItemClicked
): PagingDataAdapter<SelectInfo, SelectPagingAdapter.SelectItemViewHolder>(
    SelectInfo.diffCallback){

    inner class SelectItemViewHolder(
        val binding: AlbumSelectArtistItemBinding
    ): RecyclerView.ViewHolder(binding.root){
        var item: SelectInfo? = null

        init {

            binding.btnSelectArtist.setOnClickListener {
                item?.let {
                    callback.itemSelect(it)
                }
            }
        }
    }

    override fun onBindViewHolder(holder: SelectItemViewHolder, position: Int) {
        getItem(position)?.let { item ->
            holder.item = item
            holder.binding.tvArtistName.text = item.title
            Glide.with(holder.binding.ivArtist)
                .load(item.imgUrl)
                .placeholder(R.drawable.img_default_artist)
                .into(holder.binding.ivArtist)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectItemViewHolder {
        return SelectItemViewHolder(AlbumSelectArtistItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }
}

interface SelectItemClicked{
    fun itemSelect(item: SelectInfo)
}