package com.ccteam.admin.view.bean

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

/**
 * @author Xiaoc
 * @since 2021/3/11
 *
 * 上传结果包装对象，返回给需要上传的Fragment结果
 * @param type 当前上传的类型（例如音质上传type就为不同音质的类型，图片上传就是图片标志类型）
 * 用于上传Fragment的内容进行区分当前上传回调成功的是什么内容的Url地址
 * @param url 上传成功得到的url
 */
@Parcelize
data class UploadResult(
    val type: Int,
    val url: String
): Parcelable
