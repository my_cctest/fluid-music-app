package com.ccteam.admin.vo

import androidx.annotation.IntDef

/**
 *
 * @author Xiaoc
 * @since 2021/1/6
 *
 * 响应内容的包装类，它主要用于与UI层状态进行关联
 *
 * 对比 [com.ccteam.network.ResultVO] 它更注重于与UI层的状态，而非与服务端返回的数据关联
 * @param data 数据
 * @param message 信息
 */
sealed class Resource<T>(
    val data: T? = null,
    val message: String? = null
) {
    class Success<T>(data: T) : Resource<T>(data)
    class Loading<T>(data: T? = null) : Resource<T>(data)
    class Error<T>(message: String, data: T? = null) : Resource<T>(data, message)

    override fun toString(): String {
        return "Resource(data=$data, message=$message)"
    }
}
