package com.ccteam.admin

import com.ccteam.admin.utils.FfmpegUtils
import org.junit.Test

import org.junit.Assert.*
import kotlin.math.min

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    private val BITRATE_NQ = 128
    private val BITRATE_HQ = 320
    private val BITRATE_SQ = 1020

    private val BITRATE_NQ_TAG = "NQ"
    private val BITRATE_HQ_TAG = "HQ"
    private val BITRATE_SQ_TAG = "SQ"

    @Test
    fun addition_isCorrect() {
    }
}