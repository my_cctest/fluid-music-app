# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

-keep class * extends com.google.protobuf.GeneratedMessageLite { *; }

-keep class * extends androidx.navigation.NavDirections { *; }
-keep class * extends androidx.navigation.NavArgs { *; }

#不混淆Parcelable实现类中的CREATOR字段，
#CREATOR字段是绝对不能改变的，包括大小写都不能变，不然整个Parcelable工作机制都会失败。
-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

# 打包时屏蔽掉日志相关
-assumenosideeffects class com.orhanobut.logger.Logger{
    public static void e(...);
    public static void i(...);
    public static void w(...);
    public static void d(...);
    public static void v(...);
}