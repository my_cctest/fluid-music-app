package com.ccteam.model.moment

/**
 * @author Xiaoc
 * @since 2021/5/3
 *
 * 朋友圈信息展示VO，用于展示朋友圈基本信息
 */
data class MomentVO(
    /**
     * 朋友圈ID
     */
    val id: String,

    /**
     * 朋友圈文字内容
     */
    val content: String,

    /**
     * 朋友圈发布时间
     */
    val createTime: String,

    /**
     * 朋友圈评论数
     */
    val commentNum: Int,

    /**
     * 朋友圈点赞数
     */
    val upNum: Int,

    /**
     * 朋友圈分享内容对应的ID
     */
    val shareId: String?,

    /**
     * 朋友圈分享内容对应类型
     */
    val type: Int,

    /**
     * 朋友圈分享内容
     */
    val momentShareVO: MomentShareVO?,

    /**
     * 朋友圈图片列表
     */
    val images: List<MomentImageVO>,

    /**
     * 发布朋友圈用户信息
     */
    val userMomentVO: UserMomentVO
)

/**
 * 朋友圈分享内容信息VO
 */
data class MomentShareVO(
    /**
     * 分享内容标题
     */
    val title: String?,

    /**
     * 分享内容副标题
     */
    val subTitle: String?,

    /**
     * 分享内容封面
     */
    val imgUrl: String?
)

/**
 * 朋友圈用户信息
 */
data class UserMomentVO(
    /**
     * 用户ID
     */
    val id: String,

    /**
     * 用户名
     */
    val name: String,

    /**
     * 用户头像地址
     */
    val photoUrl: String?
)

/**
 * 朋友圈图片信息VO
 */
data class MomentImageVO(
    /**
     * 图片ID
     */
    val id: String,

    /**
     * 图片对应朋友圈ID
     */
    val momentId: String,

    /**
     * 图片地址
     */
    val url: String
)

/**
 * 朋友圈精简VO，不带用户信息
 */
data class MomentSimplyInfoVO(
    /**
     * 朋友圈ID
     */
    val id: String,

    /**
     * 朋友圈文字内容
     */
    val content: String,

    /**
     * 朋友圈发布时间
     */
    val createTime: String,

    /**
     * 朋友圈评论数
     */
    val commentNum: Int,

    /**
     * 朋友圈点赞数
     */
    val upNum: Int,

    /**
     * 朋友圈分享内容对应的ID
     */
    val shareId: String?,

    /**
     * 朋友圈分享内容对应类型
     */
    val type: Int,

    /**
     * 朋友圈分享内容
     */
    val momentShareVO: MomentShareVO?,

    /**
     * 朋友圈图片列表
     */
    val images: List<MomentImageVO>
)


/**
 * 分享内容不存在
 */
const val MOMENT_SHARE_TYPE_NONE = 0

/**
 * 分享类型为歌单
 */
const val MOMENT_SHARE_TYPE_PLAYLIST = 1

/**
 * 分享类型为专辑
 */
const val MOMENT_SHARE_TYPE_ALBUM = 2

/**
 * 分享类型为歌曲
 */
const val MOMENT_SHARE_TYPE_MUSIC = 3