package com.ccteam.model.moment.comment

import com.ccteam.model.moment.UserMomentVO

/**
 * @author Xiaoc
 * @since 2021/5/3
 *
 * 评论回复相关VO，用于接收评论回复信息
 */
data class CommentVO(
    /**
     * 评论ID
     */
    val id: String,

    /**
     * 回复给某评论的ID
     */
    val commentId: String?,

    /**
     * 评论内容
     */
    val content: String,

    /**
     * 发布评论时间
     */
    val createTime: String,

    /**
     * 评论对应朋友圈ID
     */
    val momentId: String,

    /**
     * 回复信息列表
     */
    val replyDetails: List<ReplyInfoVO>?,

    /**
     * 回复数
     */
    val replyNum: Int?,

    /**
     * 回复给用户的信息
     */
    val toUser: UserMomentVO?,

    /**
     * 评论回复类型
     */
    val type: Int,

    /**
     * 发表该评论/回复的用户信息
     */
    val user: UserMomentVO
){

    /**
     * 回复内容相关VO
     */
    data class ReplyInfoVO(
        /**
         * 回复ID
         */
        val id: String,

        /**
         * 回复的对应评论ID
         */
        val commentId: String,

        /**
         * 回复内容
         */
        val content: String,

        /**
         * 回复发布时间
         */
        val createTime: String,

        /**
         * 回复对应朋友圈ID
         */
        val momentId: String,

        /**
         * 回复给用户的信息
         */
        val toUser: UserMomentVO,

        /**
         * 评论回复类型
         */
        val type: Int,

        /**
         * 发表该评论/回复的用户信息
         */
        val user: UserMomentVO
    )
}

/**
 * 该评论类型为主评论
 */
const val COMMENT_TYPE_COMMENT = 0

/**
 * 该评论类型为回复
 */
const val COMMENT_TYPE_REPLAY = 1

/**
 * 该评论类型为回复某条回复
 */
const val COMMENT_TYPE_REPLAY_REPLAY = 2