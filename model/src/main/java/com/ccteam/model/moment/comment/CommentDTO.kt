package com.ccteam.model.moment.comment

/**
 * @author Xiaoc
 * @since 2021/5/3
 *
 * 评论信息DTO，带ID，用于修改评论时使用
 */
data class CommentDTO(
    /**
     * 评论ID
     */
    val id: String,

    /**
     * 回复给评论的ID
     */
    val commentId: String,

    /**
     * 评论/回复内容
     */
    val content: String,

    /**
     * 朋友圈ID
     */
    val momentId: String,

    /**
     * 评论/回复给对应的用户ID
     */
    val toUserId: String,

    /**
     * 评论类型
     */
    val type: Int,

    /**
     * 发布评论/回复的用户ID
     */
    val userId: String
)

/**
 * 评论信息DTO，不带ID，用于创建评论时使用
 */
data class CommentNoIdDTO(

    /**
     * 回复给评论的ID
     */
    val commentId: String?,

    /**
     * 评论/回复内容
     */
    val content: String,

    /**
     * 朋友圈ID
     */
    val momentId: String,

    /**
     * 评论/回复给对应的用户ID
     */
    val toUserId: String?,

    /**
     * 评论类型
     */
    val type: Int,

    /**
     * 发布评论/回复的用户ID
     */
    val userId: String
)