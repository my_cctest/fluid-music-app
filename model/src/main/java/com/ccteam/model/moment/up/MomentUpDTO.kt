package com.ccteam.model.moment.up

/**
 * @author Xiaoc
 * @since 2021/5/3
 *
 * 朋友圈点赞DTO
 */
data class MomentUpDTO(
    /**
     * 点赞ID
     */
    val id: String?,

    /**
     * 朋友圈ID
     */
    val momentId: String,

    /**
     * 是否点赞用户ID
     */
    val userId: String
)
