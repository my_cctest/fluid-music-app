package com.ccteam.model.moment

/**
 * @author Xiaoc
 * @since 2021/3/28
 *
 * 朋友圈信息DTO，含ID，用于修改等操作时使用
 */
data class MomentDTO(
    /**
     * 朋友圈ID
     */
    val id: String,

    /**
     * 朋友圈内容
     */
    val content: String,

    /**
     * 朋友圈图片地址列表
     */
    val images: List<String>,

    /**
     * 分享内容ID
     */
    val shareId: Int?,

    /**
     * 用户ID
     */
    val userId: String,

    /**
     * 朋友圈分享内容类型
     */
    val type: Int
)

/**
 * 朋友圈信息DTO，不含ID，用于创建等操作
 */
data class MomentNoIdDTO(
    /**
     * 朋友圈内容
     */
    val content: String,

    /**
     * 朋友圈图片地址列表
     */
    val images: List<String>,

    /**
     * 分享内容ID
     */
    val shareId: String?,

    /**
     * 用户ID
     */
    val userId: String,

    /**
     * 朋友圈分享内容类型
     */
    val type: Int
)
