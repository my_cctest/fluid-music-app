package com.ccteam.model.music

/**
 * @author Xiaoc
 * @since 2021/3/3
 *
 * 歌曲DTO
 * 歌曲标准DTO，用于编辑/修改歌曲使用，需要提供ID
 */
data class MusicDTO(
    /**
     * 歌曲ID
     */
    val id: String,

    /**
     * 专辑ID
     */
    val albumId: String? = null,

    /**
     * 专辑名
     */
    val albumName: String? = null,

    /**
     * 歌手名
     */
    val artists: String? = null,

    /**
     * 歌手ID
     */
    val artistsId: List<String>? = null,

    /**
     * 歌曲时长
     */
    val duration: String? = null,

    /**
     * 歌曲音源地址
     */
    val url: String? = null,

    /**
     * 歌曲最大音质
     */
    val maxBitrate: Int? = null,

    /**
     * 歌曲歌词
     */
    val lrc: String? = null,

    /**
     * 歌曲名
     */
    val name: String? = null,

    /**
     * 歌曲年份
     */
    val songYear: String? = null,

    /**
     * 歌曲音轨
     */
    val track: Int? = null
)

/**
 * 歌曲无Id DTO，用于创建歌曲使用，不提供ID
 */
data class MusicInfoNoIdDTO(
    /**
     * 专辑ID
     */
    val albumId: String,

    /**
     * 专辑名
     */
    val albumName: String,

    /**
     * 歌手名
     */
    val artists: String,

    /**
     * 歌手ID
     */
    val artistsId: List<String>,

    /**
     * 歌手时长
     */
    val duration: String,

    /**
     * 歌曲歌词
     */
    val lrc: String?,

    /**
     * 歌曲名
     */
    val name: String,

    /**
     * 歌曲年份
     */
    val songYear: String,

    /**
     * 歌曲音轨
     */
    val track: Int,

    /**
     * 歌曲最大音质，因为是创建歌曲，没有提供音源，则为默认无音源
     */
    val maxBitrate: Int = BITRATE_NO_TAG
)