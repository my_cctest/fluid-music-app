package com.ccteam.model.music

/**
 * @author Xiaoc
 * @since 2021/5/3
 */
data class BitrateDTO(

    /**
     * 比特率类型
     */
    val bitrate: Int,

    /**
     * 歌曲ID
     */
    val songId: String,

    /**
     * 文件Key，包含不同分片文件的文件名
     */
    val fileKey: String
)



/**
 * 当前无音源标志
 */
const val BITRATE_NO_TAG = 0

/**
 * 当前音源为标准
 */
const val BITRATE_NQ_TAG = 1

/**
 * 当前音源为高清
 */
const val BITRATE_HQ_TAG = 2

/**
 * 当前音源为无损
 */
const val BITRATE_SQ_TAG = 3
