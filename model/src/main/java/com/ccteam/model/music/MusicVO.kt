package com.ccteam.model.music

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

/**
 * @author Xiaoc
 * @since 2021/5/3
 */
@JsonIgnoreProperties(ignoreUnknown = true)
data class MusicVO(
    /**
     * 歌曲ID
     */
    val id: String,

    /**
     * 歌曲标题
     */
    val name: String,

    /**
     * 歌曲时长
     */
    val duration: Long,

    /**
     * 歌曲最佳音质 NO（无音源）/ NQ（标清音质）/ HQ（高清音质）/ SQ（无损音质）
     */
    val maxBitrate: String,

    /**
     * 歌曲曲目
     */
    val track: Int,

    /**
     * 歌曲专辑封面
     */
    val imgUrl: String?,

    /**
     * 歌手名，多个歌手用 '/' 区分
     */
    val artists: String,

    /**
     * 音源地址
     */
    val url: String?,

    /**
     * 歌手ID列表
     */
    val artistsId: List<String>,

    /**
     * 专辑ID
     */
    val albumId: String,

    /**
     * 专辑名
     */
    val albumName: String,

    /**
     * 用户名（歌单查询时使用，歌单的歌曲含有创建该歌曲的用户信息）
     */
    val userId: String?,

    /**
     * 歌单歌曲ID关联唯一值（歌单查询时使用）
     */
    val playlistSongId: String?
)

/**
 * 音乐信息精简VO，用于显示精简的音乐信息
 */
data class MusicSimplifyVO(
    /**
     * 音乐ID
     */
    val id: String,

    /**
     * 歌手名，多个歌手用 '/' 区分
     */
    val artists: String,

    /**
     * 专辑封面地址
     */
    val imgUrl: String?,

    /**
     * 音乐标题
     */
    val name: String
)

/**
 * 歌曲详细信息VO，用于展示歌曲详细信息
 */
data class MusicDetailVO(
    /**
     * 音乐ID
     */
    val id: String,

    /**
     * 音乐标题
     */
    val name: String,

    /**
     * 歌曲时长
     */
    val duration: Long,

    /**
     * 歌曲最佳音质 NO（无音源）/ NQ（标清音质）/ HQ（高清音质）/ SQ（无损音质）
     */
    val maxBitrate: String,

    /**
     * 歌曲曲目
     */
    val track: Int,

    /**
     * 歌曲专辑封面
     */
    val imgUrl: String,

    /**
     * 歌手
     */
    val songYear: String,

    /**
     * 歌词
     */
    val lrc: String?,

    /**
     * 歌手名，多个歌手用 '/' 区分
     */
    val artists: String,

    /**
     * 音源地址
     */
    val url: String?,

    /**
     * 歌手ID列表
     */
    val artistsId: List<String>,

    /**
     * 专辑ID
     */
    val albumId: String,

    /**
     * 专辑名
     */
    val albumName: String
)

const val BITRATE_NO = "NO"
const val BITRATE_NQ = "NQ"
const val BITRATE_HQ = "HQ"
const val BITRATE_SQ = "SQ"