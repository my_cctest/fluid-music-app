package com.ccteam.model

/**
 * @author Xiaoc
 * @since 2021/5/3
 *
 * 分页DTO
 */
data class PageDTO(
    /**
     * 当前页显示数量
     */
    val size: Int,

    /**
     * 当前页数
     */
    val current: Int,

    /**
     * 是否按拼音排序
     */
    var byPinyin: Boolean = true
)
