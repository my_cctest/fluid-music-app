package com.ccteam.model.setting

/**
 * @author Xiaoc
 * @since 2021/5/3
 *
 * 设置相关信息VO
 */
data class SettingInfoVO(
    /**
     * 设置ID
     */
    val id: String,

    /**
     * 设置对应的用户ID
     */
    val userId: String,

    /**
     * 设置JSON内容
     */
    val settingJson: String,

    /**
     * 更新时间
     */
    val updateTime: String
)
