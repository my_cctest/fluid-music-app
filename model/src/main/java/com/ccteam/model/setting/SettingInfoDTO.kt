package com.ccteam.model.setting

/**
 * @author Xiaoc
 * @since 2021/5/3
 *
 * 设置相关信息DTO
 */
data class SettingInfoDTO(
    /**
     * 用户ID
     */
    val userId: String,

    /**
     * 设置信息JSON
     */
    val settingJson: String
)

data class SettingInfoEditDTO(
    /**
     * 设置ID
     */
    val id: String,

    /**
     * 用户ID
     */
    val userId: String,
    /**
     * 设置信息JSON
     */
    val settingJson: String
)