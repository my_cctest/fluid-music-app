package com.ccteam.model.album

/**
 * @author Xiaoc
 * @since 2021/3/1
 *
 * 专辑信息DTO
 * 该数据类不包含id，用于修改专辑信息等操作
 */
data class AlbumDTO(
    /**
     * 专辑ID
     */
    val id: String,

    /**
     * 专辑年份
     */
    val albumYear: String,

    /**
     * 歌手ID
     */
    val artistId: String,

    /**
     * 歌手名
     */
    val artistName: String,

    /**
     * 专辑封面地址
     */
    val imgUrl: String,

    /**
     * 专辑介绍
     */
    val introduction: String?,

    /**
     * 专辑名
     */
    val name: String
)

/**
 * 专辑信息DTO
 * 该数据类包含id，用于创建专辑信息等操作
 */
data class AlbumInfoNoIdDTO(
    /**
     * 专辑年份
     */
    val albumYear: String,

    /**
     * 歌手ID
     */
    val artistId: String,

    /**
     * 歌手名
     */
    val artistName: String,

    /**
     * 专辑封面地址
     */
    val imgUrl: String,

    /**
     * 专辑介绍
     */
    val introduction: String?,

    /**
     * 专辑名
     */
    val name: String
)