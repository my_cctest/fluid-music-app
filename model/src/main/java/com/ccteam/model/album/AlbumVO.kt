package com.ccteam.model.album

/**
 * @author Xiaoc
 * @since 2021/2/6
 *
 * 专辑信息VO，用于接收专辑相关的内容
 **/
data class AlbumVO(
    /**
     * 专辑ID
     */
    val id: String,

    /**
     * 专辑名
     */
    val name: String,

    /**
     * 歌手名
     */
    val artistName: String,

    /**
     * 专辑发行日期
     */
    val albumYear: String,

    /**
     * 专辑封面地址
     */
    val imgUrl: String,

    /**
     * 专辑简介
     */
    val introduction: String,

    /**
     * 专辑曲目数
     */
    val trackNum: Int,

    /**
     * 专辑对应的歌手ID
     */
    val artistId: String
)

/**
 * 专辑信息精简VO，主要用于显示精简的专辑信息
 */
data class AlbumSimplifyVO(
    /**
     * 专辑ID
     */
    val id: String,

    /**
     * 歌手名
     */
    val artistName: String,

    /**
     * 专辑封面
     */
    val imgUrl: String,

    /**
     * 专辑名
     */
    val name: String
)