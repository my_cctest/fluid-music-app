package com.ccteam.model.user

/**
 * @author Xiaoc
 * @since 2021/5/3
 *
 * 用户信息VO
 */
data class UserVO(
    /**
     * 用户ID
     */
    val id: String,

    /**
     * 用户名
     */
    val name: String,

    /**
     * 用户性别
     */
    val gender: String,

    /**
     * 用户生日
     */
    val birthday: String?,

    /**
     * 用户帐号状态
     */
    val state: String,

    /**
     * 用户简介
     */
    val introduction: String?,

    /**
     * 用户头像地址
     */
    val photoUrl: String?,

    /**
     * 朋友圈数
     */
    val momentNum: Int,

    /**
     * 歌单数
     */
    val playlistNum: Int
)

/**
 * 用户详细信息VO，包含手机号
 */
data class UserDetailVO(
    /**
     * 用户ID
     */
    val id: String,

    /**
     * 用户名
     */
    val name: String,

    /**
     * 用户性别
     */
    val gender: String,

    /**
     * 用户生日
     */
    val birthday: String?,

    /**
     * 用户帐号状态
     */
    val state: String,

    /**
     * 用户简介
     */
    val introduction: String?,

    /**
     * 用户头像地址
     */
    val photoUrl: String?,

    /**
     * 手机号
     */
    val phone: String
)