package com.ccteam.model.user

/**
 * @author Xiaoc
 * @since 2021/3/18
 *
 * 用户信息DTO，不带password字段，用户修改普通用户信息
 * 为空代表不修改该字段
 */
data class UserNoPasswordDTO(
    /**
     * 用户ID
     */
    val id: String,

    /**
     * 用户名
     */
    val name: String?,

    /**
     * 生日
     */
    val birthday: String?,

    /**
     * 用户签名
     */
    val introduction: String?,

    /**
     * 用户性别
     */
    val gender: String?,

    /**
     * 用户头像地址
     */
    val photoUrl: String?,
)

/**
 * 用户登录DTO
 * 分为密码登录和验证码登录
 * 如果为密码登录，则code验证码字段为null
 * 如果为验证码登录，则password字段为null
 * 涉及到password内容需要rsa加密后
 */
data class UserLoginDTO(
    val code: String?,
    val phone: String,
    val password: String?,
)

/**
 * 用户密码修改DTO，带password字段和验证码字段，用于用户忘记密码时使用
 */
data class UserPasswordVerifyDTO(
    val phone: String,
    val code: String,
    val password: String
)

/**
 * 用户注册DTO
 */
data class UserRegisterDTO(
    val code: String,
    val name: String,
    val password: String,
    val phone: String
)

/**
 * 用户修改手机号DTO
 */
data class UserPhoneVerifyDTO(
    val phone: String,
    val code: String
)