package com.ccteam.model.user

/**
 * @author Xiaoc
 * @since 2021/4/6
 *
 * 管理员登录DTO
 */
data class AdminUserDTO(
    /**
     * 管理员名
     */
    val username: String,

    /**
     * 管理员密码
     */
    val password: String
)