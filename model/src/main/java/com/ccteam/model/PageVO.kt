package com.ccteam.model

/**
 * @author Xiaoc
 * @since 2021/5/3
 */
data class PageVO<out T>(
    val current: Int,
    val size: Int,
    val total: Long,
    val records: List<T>
)
