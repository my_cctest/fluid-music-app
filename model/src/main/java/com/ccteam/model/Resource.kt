package com.ccteam.model

import com.ccteam.network.ApiError


/**
 *
 * @author Xiaoc
 * @since 2021/1/6
 *
 * 响应内容的包装类，它主要用于与UI层状态进行关联
 *
 * 对比 [com.ccteam.network.ResultVO] 它更注重于与UI层的状态，而非与服务端返回的数据关联
 * @param data 数据
 * @param errorCode 错误代码（用于UI显示对应错误内容策略）
 * @param message 信息
 */
sealed class Resource<out T>(
    val data: T? = null,
    val errorCode: Int = ApiError.nothingCode,
    val message: String? = null
) {
    class Success<out T>(data: T) : Resource<T>(data)
    class Loading<out T>(data: T? = null) : Resource<T>(data)
    class Error<out T>(errorCode: Int,message: String, data: T? = null) : Resource<T>(data,errorCode,message)

    override fun toString(): String {
        return "Resource(data=$data, message=$message, errorCode=$errorCode)"
    }
}
