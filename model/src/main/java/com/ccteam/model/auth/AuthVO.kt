package com.ccteam.model.auth

/**
 * @author Xiaoc
 * @since 2021/5/3
 *
 * 认证后取得的Token内容VO
 */
data class AuthVO(
    /**
     * 用户ID
     */
    val userId: String,

    /**
     * Token名，加载Header中Key值
     */
    val tokenName: String,

    /**
     * Token值，用户Token
     */
    val tokenValue: String
)
