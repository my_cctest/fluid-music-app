package com.ccteam.model.banner

/**
 * @author Xiaoc
 * @since 2021/5/3
 *
 * 轮播图信息VO
 */
data class BannerVO(
    /**
     * 轮播图ID
     */
    val id: String,

    /**
     * 轮播图图片地址
     */
    val url: String,

    /**
     * 轮播图对应内容ID
     */
    val itemId: String,

    /**
     * 轮播图类型
     */
    val type: Int,

    /**
     * 轮播图是否应展示
     */
    val isShow: Int
)

/**
 * 轮播图显示类型为专辑
 */
const val BANNER_TYPE_ALBUM = 0

/**
 * 轮播图显示类型为歌手
 */
const val BANNER_TYPE_ARTIST = 1
