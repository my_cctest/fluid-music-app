package com.ccteam.model.banner

/**
 * @author Xiaoc
 * @since 2021/3/26
 *
 * 轮播图DTO，带ID，用于修改轮播图时使用
 */
data class BannerDTO(
    /**
     * 轮播图ID
     */
    val id: String,

    /**
     * 轮播图是否显示
     */
    val isShow: Int,

    /**
     * 轮播图内容对应ID
     */
    val itemId: String,

    /**
     * 轮播图类型
     */
    val type: Int,

    /**
     * 轮播图图片地址
     */
    val url: String
)

/**
 * 不带id的轮播图DTO，用于创建轮播图时使用
 */
data class BannerNoIdDTO(
    /**
     * 轮播图是否显示
     */
    val isShow: Int,

    /**
     * 轮播图内容对应ID
     */
    val itemId: String,

    /**
     * 轮播图类型
     */
    val type: Int,

    /**
     * 轮播图图片地址
     */
    val url: String
)