package com.ccteam.model.artist

/**
 * @author Xiaoc
 * @since 2021/5/3
 *
 * 歌手信息VO，用于显示歌手信息
 */
data class ArtistVO(
    /**
     * 歌手ID
     */
    val id: String,

    /**
     * 歌手封面地址
     */
    val imgUrl: String,

    /**
     * 歌手名
     */
    val name: String,

    /**
     * 曲目数
     */
    val trackNum: Int,

    /**
     * 专辑数
     */
    val albumNum: Int
)

/**
 * 歌手精简信息VO，用于显示歌手的简单版信息
 */
data class ArtistSimplifyVO(
    /**
     * 歌手ID
     */
    val id: String,

    /**
     * 歌手封面地址
     */
    val imgUrl: String,

    /**
     * 歌手名
     */
    val name: String
)
