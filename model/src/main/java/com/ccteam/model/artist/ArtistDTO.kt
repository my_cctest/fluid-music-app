package com.ccteam.model.artist

/**
 * @author Xiaoc
 * @since 2021/2/5
 *
 * 提交歌手信息的基本DTO
 * 该数据类不包含id，用于创建歌手信息等操作
 */
data class ArtistInfoNoIdDTO(
    /**
     * 歌手头像地址
     */
    val imgUrl: String,

    /**
     * 歌手名
     */
    val name: String
)

/**
 * 该数据类包含id，用于编辑歌手信息等操作
 */
data class ArtistInfoDTO(
    /**
     * 歌手ID
     */
    val id: String,

    /**
     * 歌手头像地址
     */
    val imgUrl: String,

    /**
     * 歌手名
     */
    val name: String
)
