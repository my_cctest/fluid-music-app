package com.ccteam.model.playlistsheet

/**
 * @author Xiaoc
 * @since 2021/5/3
 *
 * 歌单VO
 */
data class PlaylistSheetVO(
    /**
     * 歌单ID
     */
    val id: String,

    /**
     * 歌单类型
     */
    val type: Int,

    /**
     * 歌单介绍
     */
    val introduction: String?,

    /**
     * 歌单图片
     */
    val imgUrl: String?,

    /**
     * 歌单名称
     */
    val playlistName: String,

    /**
     * 用户ID
     */
    val userId: String?,

    /**
     * 用户头像
     */
    val userImage: String?,

    /**
     * 用户名
     */
    val userName: String?
)

/**
 * 歌单类型为个人私密
 */
const val PLAYLIST_SHEET_TYPE_PRIVATE = 0

/**
 * 歌单类型为个人公开
 */
const val PLAYLIST_SHEET_TYPE_SELF_PUBLIC = 1

/**
 * 歌单类型为共享
 */
const val PLAYLIST_SHEET_TYPE_PUBLIC = 2
