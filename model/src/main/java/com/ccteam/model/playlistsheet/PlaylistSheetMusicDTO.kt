package com.ccteam.model.playlistsheet

/**
 * @author Xiaoc
 * @since 2021/3/15
 *
 * 歌单歌曲DTO
 */
data class PlaylistSheetMusicDTO(
    /**
     * 歌曲所在歌单ID
     */
    val playlistId: String,

    /**
     * 歌曲ID
     */
    val songId: String,

    /**
     * 用户ID
     */
    val userId: String
)
