package com.ccteam.model.playlistsheet

/**
 * @author Xiaoc
 * @since 2021/5/3
 *
 * 歌单信息DTO（包含ID，用于更新等操作）
 */
data class PlaylistSheetDTO(
    /**
     * 歌单ID
     */
    val id: String,

    /**
     * 歌单封面地址
     */
    val imgUrl: String?,

    /**
     * 歌单简介
     */
    val introduction: String?,

    /**
     * 歌单名
     */
    val playlistName: String?,

    /**
     * 歌单创建用户的ID
     */
    val userId: String?
)

/**
 * 歌单信息DTO（不包含ID，用于创建等操作）
 */
data class PlaylistSheetNoIdDTO(
    /**
     * 歌单名
     */
    val playlistName: String,

    /**
     * 歌单类型
     */
    val type: Int,

    /**
     * 创建歌单用户的ID
     */
    val userId: String
)
